﻿using Microsoft.AspNetCore.Builder;

namespace Ilsp.BitacoraIvr.WebApi.NLog
{
    public static class RequestResponseLoggingMiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestResponseLogging(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestResponseLoggingMiddleware>();
        }
    }
}
