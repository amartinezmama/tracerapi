﻿using System.Collections.Generic;

namespace Ilsp.BitacoraIvr.WebApi.Dto
{
    public class ResponseAction<TE, TR>
    {
        public bool Success { get; set; }
        public IList<TE> Errors { get; set; }
        public string Message { get; set; }
        public TR Item { get; set; }
        public IEnumerable<TR> Items { get; set; }
        public int Total { get; set; }
    }
}
