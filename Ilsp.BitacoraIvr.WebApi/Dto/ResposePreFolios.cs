﻿namespace Ilsp.BitacoraIvr.WebApi.Dto
{
    public class ResposePreFolios
    {
        public long Bitacora { get; set; }
        public int Ticket { get; set; }
        public string CustomerName { get; set; }
        public string ServiceSource { get; set; }
        public string ServiceDestination { get; set; }
        public int ServiceType { get; set; }
        public string ServiceStatus { get; set; }
        public int ReasonCanceled { get; set; }
        public string Comments { get; set; }
        public string AppointmentDate { get; set; }
        public string CreationDate { get; set; }
    }
}