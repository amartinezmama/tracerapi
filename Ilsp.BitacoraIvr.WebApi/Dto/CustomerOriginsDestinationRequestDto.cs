﻿using System.ComponentModel.DataAnnotations;

namespace Ilsp.BitacoraIvr.WebApi.Dto
{
    public class CustomerOriginsDestinationRequestDto: ParamsFilters
    {
        [Required]
        public int id { get; set; }
        [Required]
        public string type { get; set; }
        [Required]
        public bool isPending { get; set; }
    }

    public class CustomerUpdateRequestDto
    {
        public int customerId { get; set; }
        public string id { get; set; }
        public int type { get; set; }
        public string name { get; set; }
        public bool selected { get; set; }
    }

    public class ValidName
    {
        public string name { get; set; }
    }

    public class SaveUpdate
    {
        public string displayLabel { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string state { get; set; }
        public string municipality { get; set; }
        public string street { get; set; }
        public string number { get; set; }
        public string town { get; set; }
        public string postcode { get; set; }
        public string address { get; set; }
        public float latitude { get; set; }
        public float longitude { get; set; }
    }

    public class DeleteCustomer
    {
        public int id { get; set; }
    }
}
