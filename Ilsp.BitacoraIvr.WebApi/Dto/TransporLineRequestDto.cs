﻿using System.ComponentModel.DataAnnotations;

namespace Ilsp.BitacoraIvr.WebApi.Dto
{
    public class TransporLineRequestDto:ParamsFilters
    {
        [Required]
        public int id { get; set; }
        public string name { get; set; }
        public bool selected { get; set; }
        public int customerId { get; set; }
       
    }

    public class TransportLineResponseDto
    {
        public string id { get; set; }
        public string name { get; set; }
        public bool selected { get; set; }
    }

    public class ParamsFilters
    {
        public int page { get; set; }
        public int sizePage { get; set; } = 200;
        public string search { get; set; }
    }
}
