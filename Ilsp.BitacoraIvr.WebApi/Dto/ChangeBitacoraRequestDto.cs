﻿using System.ComponentModel.DataAnnotations;

namespace Ilsp.BitacoraIvr.WebApi.Dto
{
    public class ChangeBitacoraRequestDto
    {

        [Required]
        [StringLength(18)]
        public string id_bitacora { get; set; }

        [Required]
        [StringLength(150)]
        public string reason { get; set; }

        [Required]
        [StringLength(1)]
        public string status { get; set; }
    }
}
