﻿using System.ComponentModel.DataAnnotations;

namespace Ilsp.BitacoraIvr.WebApi.Dto
{
    public class TurnOffAlertsRequestDto
    {
        [Required]
        public string idBitacora { get; set; }
    }

    public class TurnOffAlert
    {
        public int? id { get; set; } = 0;
        public int id_Alert { get; set; }

        [Required]
        public string id_bitacora { get; set; }

        [Required]
        [StringLength(50)]
        public string id_gps { get; set; }

        [Required]
        [StringLength(75)]
        public string name_alert { get; set; }

        [Required]
        [Display(Name = "Minutes")]
        public int minutes_disabled { get; set; }

        [Required]
        [StringLength(150)]
        [Display(Name = "Observations")]
        public string observations { get; set; }
    }

}
