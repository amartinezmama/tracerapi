﻿using IdentityServer4.AccessTokenValidation;
using Ilsp.BitacoraIvr.WebApi.NLog;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Config;
using NLog.Extensions.Logging;
using NLog.Web;
using NLog.Web.LayoutRenderers;
using System.IO.Compression;

namespace Ilsp.BitacoraIvr.WebApi
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public Startup(IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile(path: "appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsEnvironment(environmentName: "Development"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            //create paramater for nlog
            ConfigurationItemFactory.Default.LayoutRenderers.RegisterDefinition("aspnet-request-ip", typeof(AspNetRequestIpLayoutRenderer));

            env.ConfigureNLog(configFileRelativePath: "nlog.config");

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddSingleton(Configuration);
            services.AddSingleton<IConfiguration>(Configuration);

            services.AddCors(options =>
            {
                // this defines a CORS policy called "default"
                options.AddPolicy(name: "webapp", configurePolicy: policy =>
                {
                    policy.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            services.Configure<GzipCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Optimal;
            });
            services.AddResponseCompression(options =>
            {
                options.EnableForHttps = true;
                options.Providers.Add<GzipCompressionProvider>();
            });

            services.AddMvcCore()
                .AddAuthorization()
                .AddJsonFormatters()
                .AddDataAnnotations();

            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = Configuration.GetValue<string>(key: "IdentityServerUri");
                    options.RequireHttpsMetadata = Configuration.GetValue<bool>(key: "RequireHttps");
                    options.ApiName = "bitacora.api";
                });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection(key: "Logging"));
            loggerFactory.AddDebug();

            loggerFactory.AddNLog();

            // this uses the policy called "default"
            //app.UseCors(policyName: "webapp");
            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
            });

            app.UseAuthentication();

            app.UseRequestResponseLogging();
            //app.UseIdentityServerAuthentication(new IdentityServerAuthenticationOptions
            //{
            //    Authority = this.Configuration.GetValue<string>(key: "IdentityServerUri"),
            //    ApiName = "bitacora.api",
            //    RequireHttpsMetadata = this.Configuration.GetValue<bool>(key: "RequireHttps"),
            //    NameClaimType = "name",
            //    RoleClaimType = "role",
            //});

            app.UseResponseCompression();
            app.UseMvc();
        }
    }
}
