﻿using Microsoft.AspNetCore.Mvc;

namespace Ilsp.BitacoraIvr.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        [HttpGet]
        public IActionResult Get()
        {
            return Ok("Hello World!");
        }
    }
}
