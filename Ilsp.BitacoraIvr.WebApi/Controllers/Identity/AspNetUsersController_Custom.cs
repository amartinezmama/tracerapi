﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Bl.IdentityLq;
using Ilsp.BitacoraIvr.Ent.Identity;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.Identity
{
    public partial class AspNetUsersController
    {
        [HttpGet]
        [Route("SearchByCustom/{value}")]
        public async Task<IActionResult> SearchByCustom(string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, UserEnt>
                {
                    Items = new List<UserEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new AspNetUsersBl(GetAdoIdentityConnection(), User.Identity.Name))
                    {
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.Search(searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("GetAssigUsersToCustomer/{customerId}")]
        public async Task<IActionResult> GetAssigUsersToCustomer(string customerId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, UserEnt>
                {
                    Items = new List<UserEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var customerIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                    List<string> usersDs;
                    using (var bl = new CatCustomersBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        usersDs = bl.GetUsersAssignedByCustomer(customerIdDec).ToList();
                    }
                    using (var bl = new AspNetUsersBl(GetAdoIdentityConnection(), User.Identity.Name))
                    {
                        var users = bl.GetAllCustom().ToList();
                        users.ForEach(item =>
                            {
                                item.Selected = usersDs.Any(u => u.ToLower().Equals(item.UserName.ToLower()));
                            });
                        response.Items = users.ToArray();
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("SearchActiveUsers/{value}")]
        public async Task<IActionResult> SearchActiveUsers(string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, UserEnt>
                {
                    Items = new List<UserEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var hasPermission = User.Claims.Any(x => x.Type.Equals("BitacoraPermission") && x.Value.Equals("AssignBitacoraToOthers"));
                    using (var bl = new AspNetUsersBl(GetAdoIdentityConnection(), User.Identity.Name))
                    {
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = hasPermission ? bl.GetAllActiveUsers(searchDec) : bl.GetActiveUsers(searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("SearchActiveUsersToAssign/{value}")]
        public async Task<IActionResult> SearchActiveUsersToAssign(string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, UserEnt>
                {
                    Items = new List<UserEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new AspNetUsersBl(GetAdoIdentityConnection(), User.Identity.Name))
                    {
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.GetActiveUsers(searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

    }
}
