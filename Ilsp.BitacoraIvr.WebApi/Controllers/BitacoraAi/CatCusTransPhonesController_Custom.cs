﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using CatCusTransPhonesBl = Ilsp.BitacoraIvr.Bl.BitacoraAiLq.CatCusTransPhonesBl;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatCusTransPhonesController
    {
        /// <summary>
        /// Get Items from Entity
        /// </summary>
        /// <param name="model">Filters and Sorts</param>
        /// <returns></returns>
        [HttpPost("GetGridCustom")]
        public async Task<IActionResult> GetGridCustom([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new CatCusTransPhonesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        return Ok(bl.GetListPagerCustom(model));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        /// <summary>
        /// Get Item from Entity
        /// </summary>
        /// <param name="customerId">Customer</param>
        /// <param name="transportLineId">Transport Line</param>
        /// <returns></returns>
        [HttpGet]
        [Route("FirstCustom/{customerId}/{transportLineId}")]
        public async Task<IActionResult> FirstCustom(string customerId, string transportLineId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CatCusTransPhonesEnt>
                {
                    Items = new List<CatCusTransPhonesEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl = new CatCusTransPhonesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var customerIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                        var transportLineIdDec = Encoding.UTF8.GetString(Convert.FromBase64String(transportLineId));
                        var res = bl.First(customerIdDec, transportLineIdDec);
                        var catCusTrans = new CatCusTransPhonesEnt();
                        using (var cuBl = new CatCustomersBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            catCusTrans.Customer = cuBl.First(customerIdDec);
                        }

                        using (var trBl = new CatTransportLinesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            catCusTrans.TransportLine = trBl.First(customerIdDec, transportLineIdDec);
                        }

                        catCusTrans.Phones = res.Phones;
                        response.Item = catCusTrans;
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CatCusTransPhonesEnt>
                {
                    Items = new List<CatCusTransPhonesEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var itemFront = (CatCusTransPhonesEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(CatCusTransPhonesEnt));
                    List<Filter> filters;
                    List<CatCusTransPhones> beforePhones;
                    var result = false;

                    using (var bl = new Bl.BitacoraAiEf.CatCusTransPhonesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        filters = new List<Filter>
                        {
                            new Filter
                            {
                                Field = "CustomerId",
                                Value = itemFront.Customer.Id.ToString(),
                                Operator = "Equals"
                            },
                            new Filter
                            {
                                Field = "TransportLineId",
                                Value = itemFront.TransportLine.Id,
                                Operator = "Equals"
                            }
                        };
                        beforePhones = bl.GetList(filters, new List<KendoSort>(), "AND").ToList();
                    }

                    using (var tran = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bl = new Bl.BitacoraAiEf.CatCusTransPhonesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            foreach (var phone in beforePhones)
                            {
                                filters = new List<Filter>
                                {
                                    new Filter
                                    {
                                        Field = "CustomerId",
                                        Value = itemFront.Customer.Id.ToString(),
                                        Operator = "Equals"
                                    },
                                    new Filter
                                    {
                                        Field = "TransportLineId",
                                        Value = itemFront.TransportLine.Id,
                                        Operator = "Equals"
                                    },
                                    new Filter
                                    {
                                        Field = "Phone",
                                        Value = phone.Phone,
                                        Operator = "Equals"
                                    }
                                };
                                bl.DeleteWithOutTransaction(filters);
                            }

                            foreach (var phone in itemFront.Phones)
                            {
                                filters = new List<Filter>
                                {
                                    new Filter
                                    {
                                        Field = "CustomerId",
                                        Value = itemFront.Customer.Id.ToString(),
                                        Operator = "Equals"
                                    },
                                    new Filter
                                    {
                                        Field = "TransportLineId",
                                        Value = itemFront.TransportLine.Id,
                                        Operator = "Equals"
                                    },
                                    new Filter
                                    {
                                        Field = "Phone",
                                        Value = phone,
                                        Operator = "Equals"
                                    }
                                };
                                result = bl.SaveWithOutTransaction(new CatCusTransPhones
                                {
                                    CustomerId = itemFront.Customer.Id,
                                    TransportLineId = itemFront.TransportLine.Id,
                                    Phone = phone,
                                    CreateUser = User.Identity.Name,
                                    CreateDate = DateTime.Now
                                }, filters);
                                if (!result)
                                {
                                    break;
                                }
                            }
                        }

                        if (result)
                        {
                            tran.Complete();
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Update Item on Entity
        /// </summary>
        /// <param name="dto">Data to update</param>
        /// <returns></returns>
        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CatCusTransPhonesEnt>
                {
                    Items = new List<CatCusTransPhonesEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var itemFront = (CatCusTransPhonesEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(CatCusTransPhonesEnt));
                    List<Filter> filters;
                    List<CatCusTransPhones> beforePhones;
                    var result = false;

                    using (var bl = new Bl.BitacoraAiEf.CatCusTransPhonesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        filters = new List<Filter>
                        {
                            new Filter
                            {
                                Field = "CustomerId",
                                Value = itemFront.Customer.Id.ToString(),
                                Operator = "Equals"
                            },
                            new Filter
                            {
                                Field = "TransportLineId",
                                Value = itemFront.TransportLine.Id,
                                Operator = "Equals"
                            }
                        };
                        beforePhones = bl.GetList(filters, new List<KendoSort>(), "AND").ToList();
                    }

                    using (var tran = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bl = new Bl.BitacoraAiEf.CatCusTransPhonesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            foreach (var phone in beforePhones)
                            {
                                filters = new List<Filter>
                                {
                                    new Filter
                                    {
                                        Field = "CustomerId",
                                        Value = itemFront.Customer.Id.ToString(),
                                        Operator = "Equals"
                                    },
                                    new Filter
                                    {
                                        Field = "TransportLineId",
                                        Value = itemFront.TransportLine.Id,
                                        Operator = "Equals"
                                    },
                                    new Filter
                                    {
                                        Field = "Phone",
                                        Value = phone.Phone,
                                        Operator = "Equals"
                                    }
                                };
                                bl.DeleteWithOutTransaction(filters);
                            }

                            foreach (var phone in itemFront.Phones)
                            {
                                filters = new List<Filter>
                                {
                                    new Filter
                                    {
                                        Field = "CustomerId",
                                        Value = itemFront.Customer.Id.ToString(),
                                        Operator = "Equals"
                                    },
                                    new Filter
                                    {
                                        Field = "TransportLineId",
                                        Value = itemFront.TransportLine.Id,
                                        Operator = "Equals"
                                    },
                                    new Filter
                                    {
                                        Field = "Phone",
                                        Value = phone,
                                        Operator = "Equals"
                                    }
                                };
                                result = bl.SaveWithOutTransaction(new CatCusTransPhones
                                {
                                    CustomerId = itemFront.Customer.Id,
                                    TransportLineId = itemFront.TransportLine.Id,
                                    Phone = phone,
                                    CreateUser = User.Identity.Name,
                                    CreateDate = DateTime.Now
                                }, filters);
                                if (!result)
                                {
                                    break;
                                }
                            }
                        }

                        if (result)
                        {
                            tran.Complete();
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Delete one Item
        /// </summary>
        /// <param name="dto">Item if Entity</param>
        /// <returns></returns>
        [HttpPost("Delete")]
        public async Task<IActionResult> Delete([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CatCusTransPhonesEnt>
                {
                    Items = new List<CatCusTransPhonesEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var itemFront = (CatCusTransPhonesEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(CatCusTransPhonesEnt));
                    var filters = new List<Filter>
                    {
                        new Filter
                        {
                            Field = "CustomerId",
                            Value = itemFront.Customer.Id.ToString(),
                            Operator = "Equals"
                        },
                        new Filter
                        {
                            Field = "TransportLineId",
                            Value = itemFront.TransportLine.Id,
                            Operator = "Equals"
                        }
                    };

                    bool result;
                    using (var bl = new Bl.BitacoraAiEf.CatTransportLinesVehiclesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        result = bl.DeleteMultiple(filters, "AND");
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

    }
}
