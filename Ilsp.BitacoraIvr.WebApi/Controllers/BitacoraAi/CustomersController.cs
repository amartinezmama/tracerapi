﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Ilsp.BitacoraIvr.WebApi.Dto;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.Common;
using System.Collections.Generic;
using System.Transactions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    [Route("api/[controller]")]
    [Authorize]
    public class CustomersController : BaseController
    {
        public CustomersController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor) : base(configuration, loggerFactory, accessor)
        {
        }

        [HttpGet]
        public async Task<IActionResult> Get(int op, int pr)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    var response = new ResponseAction<ValidationFailure, CustomerEnt>
                    {
                        Success = false
                    };
                    using (var bl = new CustomerBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Items = bl.GetCustomers();
                        response.Success = true;
                        response.Total = response.Items.Count();
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpPost("saveOUpdateOriginsDestinations")]
        public async Task<IActionResult> saveOUpdateOriginsDestinations([FromBody]SaveUpdate req)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var scope = new TransactionScope())
                    {
                        using (var db = GetDbConnection())
                        {
                            var response = new ResponseAction<ValidationFailure, int>
                            {
                                Success = false
                            };
                            if (req.id != null)
                            {
                                var id = Convert.ToInt32(req.id);
                                var result = db.sp_origin_destination_update(id, req.name, req.name,
                                    req.state,
                                    req.municipality, req.street, req.number, req.town, req.postcode,
                                    req.displayLabel, req.latitude, req.longitude, User.Identity.Name);
                                if (result >= 1)
                                {
                                    scope.Complete();
                                    response.Success = true;
                                    response.Message = "Operación exitosa";
                                    return Ok(response);
                                }
                                else
                                {
                                    return StatusCode(500);
                                }
                            }
                            else
                            {
                                var result = db.sp_origin_destination_create(req.name,req.name,req.state,req.municipality,req.street,
                                    req.number,req.town, req.postcode, req.displayLabel,req.latitude,req.longitude,User.Identity.Name);
                                if (result >= 1)
                                {
                                    scope.Complete();
                                    response.Success = true;
                                    response.Message = "Operación exitosa";
                                    return Ok(response);
                                }
                                switch (result)
                                {
                                    case -1:
                                        var error = "Ya existe una ubicación con este nombre";
                                        return BadRequest(error);
                                    default:
                                        Logger.LogError(Resources.Resource.Generic_Error);
                                        return StatusCode(500);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpPost("grid")]
        public async Task<IActionResult> GetGrid([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var db = GetDbConnection())
                    {
                        using (var originDestinyBl = new OriginDestinyBl(db))
                        {
                            return Ok(originDestinyBl.GetGrid(model));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var db = GetDbConnection())
                    {
                        var response = new ResponseAction<ValidationFailure, int>
                        {
                            Success = false
                        };
                        var result = db.sp_origin_destination_delete(id, User.Identity.Name);
                        if (result >= 1)
                        {
                            response.Success = true;
                            response.Message = "Operación exitosa";
                            return Ok(response);
                        }
                        switch (result)
                        {
                            case -1:
                                var error = "Location Cant Delete";
                                ModelState.AddModelError("Name", error);
                                Logger.LogWarning(error);
                                return BadRequest(GetErrors(ModelState));
                            default:
                                Logger.LogError(Resources.Resource.Generic_Error);
                                return StatusCode(500);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });

        }

        private Dictionary<string, object> GetErrors(ModelStateDictionary modelState)
        {
            var errors = new Dictionary<string, object>();
            foreach (var key in ModelState.Keys)
            {
                var item = ModelState[key];
                if (item.Errors.Any())
                {
                    errors[key] = new
                    {
                        errors = item.Errors.Select(error => error.ErrorMessage)
                    };
                }
            }
            return errors;
        }
    }
}
