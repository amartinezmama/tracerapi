﻿using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    [Route("api/[controller]")]
    [Authorize]
    public class TransportLinesController : BaseController
    {
        public TransportLinesController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor) : base(configuration, loggerFactory, accessor)
        {
        }

        [Route("transportlines")]
        public async Task<IActionResult> transportlines()
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var db = GetDbConnection())
                    {
                        return Ok((from customer in db.sp_get_customers()
                                   select new
                                   {
                                       customer.Id,
                                       customer.Name,
                                       Selected = false
                                   }).ToArray());
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpPost]
        [Route("transportlines")]
        public async Task<IActionResult> Gettransportlines([FromBody]TransporLineRequestDto request)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    if(!string.IsNullOrEmpty(request.id.ToString()))
                    {
                        using (var db = GetDbConnection())
                        {
                            var transportLines = db.sp_get_ax_transport_lines().ToArray();
                            var transportLinesAdded = db.sp_get_transport_lines_by_customer(request.id).ToArray();
                            var result = (from line in transportLines
                                          select new
                                          {
                                              line.Id,
                                              line.Name,
                                              Selected = transportLinesAdded.Any(x => x.Id.Equals(line.Id))
                                          });
                            return Ok(result);
                        }
                    }
                    return Ok(new List<string>());
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        //[HttpPost]
        //[Route("updateTransportlines")]
        //public async Task<IActionResult> updateTransportlines([FromBody] TransporLineRequestDto req)
        //{
        //    return await Task.Run<IActionResult>(() =>
        //    {
        //        try
        //        {
        //            var response = new ResponseAction<ValidationFailure, int>
        //            {
        //                Success = false
        //            };
        //            using (var db = GetDbConnection())
        //            {
        //                if (req.selected)
        //                    db.sp_transport_line_add_by_customer(req.customerId, User.Identity.Name, req.id.ToString());
        //                else
        //                    db.sp_transport_line_remove_by_customer(req.customerId, User.Identity.Name, req.id.ToString());
        //            }
        //            response.Message = "La operación fue exitosa";
        //            response.Success = true;
        //            return Ok(response);
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.LogError(ex.ToString());
        //            return StatusCode(500);
        //        }
        //    });
        //}


        //[HttpPost]
        //[Route("transportlinesNotAssigned")]
        //public async Task<IActionResult> TransportlinesStatus([FromBody]TransporLineRequestDto request)
        //{
        //    return await Task.Run<IActionResult>(() =>
        //    {
        //        var response = new ResponseAction<ValidationFailure, TypeDataDualList>
        //        {
        //            Success = false
        //        };
        //        try
        //        {
        //            if (request.id == 0)
        //            {
        //                response.Message = "El ID es requerido";
        //                return Ok(response);
        //            }
        //            using (var db = GetDbConnection())
        //            {
        //                using (var bl = new TransportlinesBl(db))
        //                {
        //                    return Ok(bl.linesNotAssigned(request.id, request.page, request.sizePage, request.search));
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.LogError(ex.ToString());
        //            return StatusCode(500);
        //        }
        //    });
        //}

        //[HttpPost]
        //[Route("transportlinesAssigned")]
        //public async Task<IActionResult> TransportlinesAssigned([FromBody]TransporLineRequestDto request)
        //{
        //    return await Task.Run<IActionResult>(() =>
        //    {
        //        var response = new ResponseAction<ValidationFailure, TypeDataDualList>
        //        {
        //            Success = false
        //        };
        //        try
        //        {
        //            if (request.id == 0)
        //            {
        //                response.Message = "El ID es requerido";
        //                return Ok(response);
        //            }
        //            using (var db = GetDbConnection())
        //            {
        //                using (var bl = new TransportlinesBl(db))
        //                {
        //                    return Ok(bl.linesAssigned(request.id, request.page, request.sizePage, request.search));
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.LogError(ex.ToString());
        //            return StatusCode(500);
        //        }
        //    });
        //}
    }
}
