﻿using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Newtonsoft.Json;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
	[Route("api/[controller]")]
	[Authorize]
	public partial class CatCustomersOriginsDestinationsController : BaseController
	{
        public CatCustomersOriginsDestinationsController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor) : base(configuration, loggerFactory, accessor)
        {
            Logger = loggerFactory.CreateLogger<CatCustomersOriginsDestinationsController>();
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, cat_customers_origins_destinations>
                {
					Items = new List<cat_customers_origins_destinations>(),
					Success = false
                };
                try
                {
                    using (var bl = new CatCustomersOriginsDestinationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Items = bl.GetAll();
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpPost("GetGrid")]
        public async Task<IActionResult> GetGrid([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new CatCustomersOriginsDestinationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        return Ok(bl.GetListPager(model, "AND"));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpGet]
        [Route("SearchBy/{property}/{value}")]
        public async Task<IActionResult> SearchBy(string property, string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, cat_customers_origins_destinations>
                {
					Items = new List<cat_customers_origins_destinations>(),
					Success = false
                };
                try
                {
                    using (var bl = new CatCustomersOriginsDestinationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var kendoRequest = new KendoRequest();
                        var filters = new List<Filter>();
                        var orders = new List<KendoSort>();
                        filters.Add(new Filter
                        {
							Field = property,
							Value = value,
							Operator = "Contains"
                        });
                        orders.Add(new KendoSort
                        {
							Field = property,
							Dir = "ASC"
                        });
                        kendoRequest.Filter = new KendoFilter
                        {
							Filters = filters,
							Logic = "Contains"
                        };
                        kendoRequest.Sort = orders;
                        kendoRequest.Page = 1;
                        response.Items = JsonConvert.DeserializeObject<IEnumerable<cat_customers_origins_destinations>>(
                            JsonConvert.SerializeObject(bl.GetListPager(kendoRequest, "AND").Data));
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("GetOneBy/{property}/{value}")]
        public async Task<IActionResult> GetBy(string property, string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, cat_customers_origins_destinations>
                {
					Items = new List<cat_customers_origins_destinations>(),
					Success = false
                };
                try
                {
                    using (var bl = new CatCustomersOriginsDestinationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
						var filters = new List<Filter>
						{
							new Filter
							{
								Field = Encoding.UTF8.GetString(Convert.FromBase64String(property)),
								Value = Encoding.UTF8.GetString(Convert.FromBase64String(value)),
								Operator = "Equals"
							}
						};
						response.Item = bl.GetOne(filters);
						response.Success = true;
						return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }
	}
}
