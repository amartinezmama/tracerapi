﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    [Route("api/[controller]")]
    [Authorize]

    public partial class CatProjectsController : BaseController
    {
        public CatProjectsController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor) : base(configuration, loggerFactory, accessor)
        {
            Logger = loggerFactory.CreateLogger<CatProjectsController>();
        }

        [HttpGet]
        [Route("SearchByCustom/{axCustomerId}/{value}")]
        public async Task<IActionResult> SearchByCustom(string axCustomerId, string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, ProjectEnt>
                {
                    Items = new List<ProjectEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatProjectsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var axCustomerIdDec = Encoding.UTF8.GetString(Convert.FromBase64String(axCustomerId));
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.Search(axCustomerIdDec, searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("FirstCustom/{axCustomerId}/{projectId}")]
        public async Task<IActionResult> FirstCustom(string axCustomerId, string projectId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, ProjectEnt>
                {
                    Items = new List<ProjectEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatProjectsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var axCustomerIdDec = Encoding.UTF8.GetString(Convert.FromBase64String(axCustomerId));
                        var projectIdDec = Encoding.UTF8.GetString(Convert.FromBase64String(projectId));
                        response.Item = bl.First(axCustomerIdDec, projectIdDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }
    }
}
