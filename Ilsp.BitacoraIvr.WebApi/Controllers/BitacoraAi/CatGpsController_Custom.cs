﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.Tl;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatGpsController
    {

        [HttpGet]
        [Route("SearchByCustom/{gpsType}/{customerId}/{value}")]
        public async Task<IActionResult> SearchByCustom(string gpsType, string customerId, string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, GpsEnt>
                {
                    Items = new List<GpsEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatGpsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var gpsTypeDec = Encoding.UTF8.GetString(Convert.FromBase64String(gpsType));
                        var customerIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.Search(gpsTypeDec, customerIdDec, searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get historical positions in range dates by Gps
        /// </summary>
        /// <param name="value">Value filter</param>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">End Date</param>
        [HttpGet]
        [Route("GetHistorical/{value}/{startDate}/{endDate}")]
        public async Task<IActionResult> GetHistorical(string value, string startDate, string endDate)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, GpsHistoric>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<GpsHistoric>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var di = Encoding.UTF8.GetString(Convert.FromBase64String(startDate));
                    var df = Encoding.UTF8.GetString(Convert.FromBase64String(endDate));

                    var dateInicio = OptionsDateTime.ToDateTime(di);
                    var dateFinal = OptionsDateTime.ToDateTime(df);

                    var gpsId = Encoding.UTF8.GetString(Convert.FromBase64String(value));

                    List<GpsHistoric> result;

                    using (var bl = new CatGpsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        result = bl.GetListSameMonth(gpsId, dateInicio, dateFinal, true);
                    }

                    response.Items = result.ToArray();
                    response.Success = true;
                    response.Message = Resources.Resource.Successful_Operation;
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError($"Error: {ex}\n Data: {value}, {startDate}, {endDate}");
                    Log($"Error: {ex}\n Data: {value}, {startDate}, {endDate}");
                    return Ok(response);
                }
            });
        }

        //[HttpGet]
        //[Route("FirstCustom/{colorId}")]
        //public async Task<IActionResult> FirstCustom(string colorId)
        //{
        //    return await Task.Run<IActionResult>(() =>
        //    {
        //        var response = new ResponseAction<ValidationFailure, GpsEnt>
        //        {
        //            Items = new List<GpsEnt>(),
        //            Success = false,
        //            Message = Resources.Resource.Generic_Error
        //        };
        //        try
        //        {
        //            using (var bl = new CatGpsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
        //            {
        //                var colorIdDec = Encoding.UTF8.GetString(Convert.FromBase64String(colorId));
        //                response.Item = bl.First(colorIdDec);
        //                response.Success = true;
        //                response.Message = Resources.Resource.Successful_Operation;
        //                return Ok(response);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.LogError(ex.ToString());
        //            response.Message = Resources.Resource.Generic_Error;
        //            return Ok(response);
        //        }
        //    });
        //}

        /// <summary>
        /// Get Custodies by Gps
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCustodiesOfServiceByGps/{search}")]
        public async Task<IActionResult> GetCustodiesOfServiceByGps(string search)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraCustodyEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false
                };
                try
                {
                    using (var bl = new CatGpsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(search));
                        response.Items = bl.GetCustodiesOfServiceByGps(searchDec);
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid Alias of gps and get info
        /// </summary>
        /// <param name="alias">alias</param>
        /// <param name="customerid">Customer Id</param>
        /// <returns>true or false if name exist on db</returns>
        [HttpGet]
        [Route("ValidateDisponibilityGpsByAlias/{customerid}/{alias}")]
        public async Task<IActionResult> ValidateDisponibilityGpsByAlias(string customerid, string alias)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, GpsInfo>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<GpsInfo>(),
                    Success = true,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var aliasDecoded = Encoding.UTF8.GetString(Convert.FromBase64String(alias));
                    var customerDecoded = Encoding.UTF8.GetString(Convert.FromBase64String(customerid));
                    if (int.TryParse(customerDecoded, out var _))
                    {
                        using (var db = GetDbConnection())
                        {
                            using (var bl = new CatGpsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                            {
                                response.Items = bl.ValidateDisponibilityGpsByAlias(aliasDecoded, int.Parse(customerDecoded)).ToArray();
                                response.Success = true;
                                var liErrors = new List<ValidationFailure>();
                                if (!response.Items.Any())
                                {
                                    liErrors.Add(new ValidationFailure("GPS", string.Format(Resources.Resource.Item_Not_Registered, Resources.Resource.Gps)));
                                    response.Success = false;
                                }
                                else
                                {
                                    foreach (var gpsInfo in response.Items)
                                    {
                                        if (gpsInfo.TimeLastLocation >= gpsInfo.TimeMaxLastLocation)
                                        {
                                            liErrors.Add(new ValidationFailure("GPS", string.Format(Resources.Resource.The_Item_Does_Not_Report_Position, Resources.Resource.Gps + " " + gpsInfo.GpsAlias)));
                                            response.Success = false;
                                        }
                                        else if (gpsInfo.CustomerId != null && !gpsInfo.CustomerId.Equals(int.Parse(customerDecoded)))
                                        {
                                            liErrors.Add(new ValidationFailure("GPS", string.Format(Resources.Resource.Item_Does_Not_Belong_To_The_Item, Resources.Resource.Gps + " " + gpsInfo.GpsAlias, Resources.Resource.Customer)));
                                            response.Success = false;
                                        }
                                        else if (!gpsInfo.GpsType.Equals(4) && !gpsInfo.GpsType.Equals(5))
                                        {
                                            liErrors.Add(new ValidationFailure("GPS", string.Format(Resources.Resource.The_Item_Is_Not_A_Valid_Device_Type, Resources.Resource.Gps + " " + gpsInfo.GpsAlias)));
                                            response.Success = false;
                                        }
                                        else if (gpsInfo.GpsStatus.Equals("A") && !string.IsNullOrEmpty(gpsInfo.BitacoraAlias))
                                        {
                                            liErrors.Add(new ValidationFailure("GPS", string.Format(Resources.Resource.Item_Associated_With_Item, Resources.Resource.Gps + " " + gpsInfo.GpsAlias, gpsInfo.BitacoraAlias)));
                                            response.Success = false;
                                        }
                                        else if (!gpsInfo.GpsStatus.Equals("A"))
                                        {
                                            liErrors.Add(new ValidationFailure("GPS", string.Format(Resources.Resource.Item_Is_Disabled, Resources.Resource.Gps + " " + gpsInfo.GpsAlias)));
                                            response.Success = false;
                                        }
                                    }
                                }
                                response.Errors = liErrors;
                            }
                            return Ok(response);
                        }
                    }
                    else
                    {
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid Alias of gps and get info for Pre-Captures
        /// </summary>
        /// <param name="alias">alias</param>
        /// <param name="customerid">Customer Id</param>
        /// <returns>true or false if name exist on db</returns>
        [HttpGet]
        [Route("ValidateDisponibilityGpsByAliasForPreCaptures/{customerid}/{alias}")]
        public async Task<IActionResult> ValidateDisponibilityGpsByAliasForPreCaptures(string customerid, string alias)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, GpsInfo>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<GpsInfo>(),
                    Success = true,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var aliasDecoded = Encoding.UTF8.GetString(Convert.FromBase64String(alias));
                    var customerDecoded = Encoding.UTF8.GetString(Convert.FromBase64String(customerid));
                    if (int.TryParse(customerDecoded, out var _))
                    {
                        using (var db = GetDbConnection())
                        {
                            using (var bl = new CatGpsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                            {
                                response.Items = bl.ValidateDisponibilityGpsByAliasForPreCaptures(aliasDecoded, int.Parse(customerDecoded)).ToArray();
                                response.Success = true;
                                var liErrors = new List<ValidationFailure>();
                                if (!response.Items.Any())
                                {
                                    liErrors.Add(new ValidationFailure("GPS", string.Format(Resources.Resource.Item_Not_Registered, Resources.Resource.Gps)));
                                    response.Success = false;
                                }
                                else
                                {
                                    foreach (var gpsInfo in response.Items)
                                    {
                                        if (gpsInfo.TimeLastLocation >= gpsInfo.TimeMaxLastLocation)
                                        {
                                            liErrors.Add(new ValidationFailure("GPS", string.Format(Resources.Resource.The_Item_Does_Not_Report_Position, Resources.Resource.Gps + " " + gpsInfo.GpsAlias)));
                                            response.Success = false;
                                        }
                                        else if (gpsInfo.CustomerId != null && !gpsInfo.CustomerId.Equals(int.Parse(customerDecoded)))
                                        {
                                            liErrors.Add(new ValidationFailure("GPS", string.Format(Resources.Resource.Item_Does_Not_Belong_To_The_Item, Resources.Resource.Gps + " " + gpsInfo.GpsAlias, Resources.Resource.Customer)));
                                            response.Success = false;
                                        }
                                        else if (!gpsInfo.GpsType.Equals(4) && !gpsInfo.GpsType.Equals(5))
                                        {
                                            liErrors.Add(new ValidationFailure("GPS", string.Format(Resources.Resource.The_Item_Is_Not_A_Valid_Device_Type, Resources.Resource.Gps + " " + gpsInfo.GpsAlias)));
                                            response.Success = false;
                                        }
                                        else if (gpsInfo.GpsStatus.Equals("A") && !string.IsNullOrEmpty(gpsInfo.BitacoraAlias))
                                        {
                                            liErrors.Add(new ValidationFailure("GPS", string.Format(Resources.Resource.Item_Associated_With_Item, Resources.Resource.Gps + " " + gpsInfo.GpsAlias, gpsInfo.BitacoraAlias)));
                                            response.Success = false;
                                        }
                                        else if (!gpsInfo.GpsStatus.Equals("A"))
                                        {
                                            liErrors.Add(new ValidationFailure("GPS", string.Format(Resources.Resource.Item_Is_Disabled, Resources.Resource.Gps + " " + gpsInfo.GpsAlias)));
                                            response.Success = false;
                                        }
                                    }
                                }
                                response.Errors = liErrors;
                            }
                            return Ok(response);
                        }
                    }
                    else
                    {
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

    }
}
