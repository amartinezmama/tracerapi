﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatMonitoringFrecuenciesController
    {

        [HttpGet]
        [Route("GetAllCustom")]
        public async Task<IActionResult> GetAllCustom()
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, MonitoringFrecuencyEnt>
                {
                    Items = new List<MonitoringFrecuencyEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl = new CatMonitoringFrecuenciesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Items = bl.GetAll().ToList().Select(item => new MonitoringFrecuencyEnt
                        {
                            Id = item.id_monitoring_frecuency,
                            Name = item.description,
                            Value = item.value
                        }).ToArray();
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

    }
}
