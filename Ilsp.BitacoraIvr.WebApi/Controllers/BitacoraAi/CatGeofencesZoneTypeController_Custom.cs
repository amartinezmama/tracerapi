﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatGeofencesZoneTypeController
    {
        [HttpGet]
        [Route("GetListCustom")]
        public async Task<IActionResult> GetListCustom()
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, ZoneTypeEnt>
                {
                    Items = new List<ZoneTypeEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl = new CatGeofencesZoneTypeBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var appId = Configuration.GetValue<string>("ApplicationDb");
                        var filters = new List<Filter>
                        {
                            new Filter
                            {
                                Field = "applicationId",
                                Value = appId,
                                Operator = "Equals"
                            }
                        };
                        response.Items = bl.GetList(filters, new List<KendoSort>(), "AND").Select(x => new ZoneTypeEnt
                        {
                            Id = x.id,
                            Name = x.name,
                            Color = x.color
                        }).ToList();
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }
    }
}
