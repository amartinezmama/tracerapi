﻿using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.OntimeAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.FluentValidators.BitacoraRules;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Ilsp.BitacoraIvr.WebApi.Mq;
using Ilsp.Common.MessageContracts;
using Ilsp.Common.MessageContracts.Ontime;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Ilsp.BitacoraIvr.WebApi.Utilities;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class BitacorasController
    {

        [HttpPost("GetGridPreCaptures")]
        public async Task<IActionResult> GetGridPreCaptures([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var showAll = User.Claims.Any(x => x.Type.Equals("BitacoraPermission") && x.Value.Equals("ShowAllPreCaptures"));
                        return Ok(bl.GetGridPreCaptures(model, showAll));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpPost("GetGridPreBitacoras")]
        public async Task<IActionResult> GetGridPreBitacoras([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.VwPrebitacorasBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var showAll = User.Claims.Any(x => x.Type.Equals("BitacoraPermission") && x.Value.Equals("ShowAllPreBitacoras"));
                        return Ok(bl.GetGridPreBitacoras(model, showAll));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        //[HttpPost("GetGridBitacoras")]
        //public async Task<IActionResult> GetGridBitacoras([FromBody] KendoRequest model)
        //{
        //    return await Task.Run<IActionResult>(() =>
        //    {
        //        try
        //        {
        //            using (var bl = new VwBitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
        //            {
        //                return Ok(bl.GetListPagerCustom(model));
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.LogError(ex.ToString());
        //            return StatusCode(500);
        //        }
        //    });
        //}

        /// <summary>
        /// Get Bitacora
        /// </summary>
        /// <param name="bitacoraId">Bitacora</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetToEdit/{bitacoraId}")]
        public async Task<IActionResult> Get(string bitacoraId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {


                        var bitacoraid = long.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(bitacoraId)));
                        //valida si la bitacora no se encuentra en edicion
                        var bitacora = bl.ValidPersmissionToEdit(bitacoraid,
                            _accessor.HttpContext.Connection.RemoteIpAddress.ToString());
                        if (bitacora != null &&
                            !bitacora.UserName.ToUpper().Equals(User.Identity.Name.ToUpper()))
                        {
                            //si la bitacora ya se encuentra en edicion se regresa la informacion del bloqueo al usuario
                            response.Success = false;
                            response.Message = Resources.Resource.Access_Denied +
                                               string.Format(
                                                   Resources.Resource
                                                       .The_Bitacora_Alias_Is_Being_Edited_By_The_User_Name,
                                                   bitacora.Alias, bitacora.UserName);
                            return Ok(response);
                        }

                        //obtiene los datos para la edicion de la bitacora
                        response.Item = bl.GetToEdit(bitacoraid);
                        if (response.Item != null)
                        {
                            response.Success = true;
                            bitacora = bl.ValidPersmissionToEdit(bitacoraid,
                                _accessor.HttpContext.Connection.RemoteIpAddress.ToString());
                            if (bitacora != null &&
                                !bitacora.UserName.ToUpper().Equals(User.Identity.Name.ToUpper()))
                            {
                                //si la bitacora ya se encuentra en edicion se regresa la informacion del bloqueo al usuario
                                response.Success = false;
                                response.Message = Resources.Resource.Access_Denied +
                                                   string.Format(
                                                       Resources.Resource
                                                           .The_Bitacora_Alias_Is_Being_Edited_By_The_User_Name,
                                                       bitacora.Alias, bitacora.UserName);
                                return Ok(response);
                            }

                            //crea el registro de la edicion de la bitacora para evitar que otro usuario no la pueda editar
                            bl.InsertEditingUserName(bitacoraid,
                                _accessor.HttpContext.Connection.RemoteIpAddress.ToString());
                            return Ok(response);
                        }

                        //no se encontro la bitacora
                        response.Message = Resources.Resource.No_Data_Found;
                        response.Success = false;
                        response.Item = null;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraEnt>
                {
                    Items = new List<BitacoraEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                var liErrors = new List<ValidationFailure>();
                try
                {
                    var result = false;
                    var bitacora = (BitacoraEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(BitacoraEnt));

                    #region FluentValidator

                    ValidationResult fluentResult;
                    switch (bitacora.Status)
                    {
                        case "R":
                            using (var val = new PreCaptureValidator())
                            {
                                fluentResult = val.Validate(bitacora);
                            }
                            break;
                        case "P":
                            using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                            {
                                using (var val = new PreBitacoraValidator(bitacoraBl))
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            break;
                        default:
                            response.Success = false;
                            response.Message = Resources.Resource.Operation_Not_Allowed;
                            return Created("Error", response);
                    }
                    if (!fluentResult.IsValid)
                    {
                        response.Errors = fluentResult.Errors;
                        return Created("Error", response);
                    }

                    #endregion

                    #region Transaction to insert Bitacora

                    var bitacoraBk = bitacora;
                    string bitacoraId = null;
                    var mode = string.Empty;
                    using (var scope = new TransactionScope())
                    {
                        using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            if (bitacoraBl.Create(bitacoraBk, ref bitacoraId) && !string.IsNullOrEmpty(bitacoraId))
                            {
                                response.Message =
                                    string.Format(Resources.Resource.Item_Was_Action_With_The_Identificador,
                                        Resources.Resource.Binnacle, Resources.Resource.Created, bitacoraBk.Alias);
                                response.Message += "<br>";
                                scope.Complete();
                                Logger.LogInformation(
                                    string.Format(Resources.Resource.Item_Was_Action_With_The_Identificador,
                                        Resources.Resource.Binnacle, Resources.Resource.Created,
                                        bitacoraId)
                                );
                                mode = "create";
                                response.Success = true;
                            }
                            else
                            {
                                response.Success = false;
                                response.Message = Resources.Resource.Generic_Error;
                            }
                        }
                    }

                    #endregion

                    #region Carga la nueva configuracion de la Bitacora

                    //read new configuration
                    if (response.Success)
                    {
                        try
                        {
                            using (var bl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                            {
                                bitacoraBk = bl.GetToEdit(long.Parse(bitacoraId ?? "0"));
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += "No se pudo leer la nueva configuración de la bitacora: " + bitacoraBk.Id;
                        }
                    }
                    else
                    {
                        Logger.LogError(string.Format(Resources.Resource.Error_On_Structure_Data_When_Action_Bitacora, mode, bitacoraId));
                        Log("User: " + User.Identity.Name + "\n" + string.Format(Resources.Resource.Error_On_Structure_Data_When_Action_Bitacora, Resources.Resource.Update, bitacoraId));
                    }

                    #endregion

                    #region Ver si el usuario pertenece a OnTime, si los destinos son de Origen Procter y envio de mensaje a colas de Rabbit

                    if (response.Success && bitacoraBk.Customer.OnTime)
                    {
                        try
                        {
                            int.TryParse(Configuration.GetSection("OnTimeCustomers").Value, out int onTimeCustomerProcter);
                            var failed = false;
                            List<DestinyEnt> noProcterDestinations;
                            if (bitacoraBk.Customer.Id == onTimeCustomerProcter)
                            {
                                using (var proterBl = new Bl.BitacoraAiEf.CatProcterOriginsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                                {
                                    var filters = bitacoraBk.Destinies.Select(d => new Filter
                                    {
                                        Field = "id_origin_destination",
                                        Value = d.Id.ToString(),
                                        Operator = "Equals"
                                    }).ToList();
                                    var destiniesAreProcter = proterBl.GetList(filters, new List<KendoSort>(), "OR");
                                    noProcterDestinations = bitacoraBk.Destinies.Where(d => !destiniesAreProcter.Any(p => p.id_origin_destination.Equals(d.Id))).ToList();
                                }
                            }
                            else
                            {
                                noProcterDestinations = bitacoraBk.Destinies.ToList();
                            }
                            using (var scope = new TransactionScope())
                            {
                                using (var ontimedb = new ShipmentsBl(GetOntimeAiConnection(), User.Identity.Name))
                                {
                                    ontimedb.ClearAllBitacoras(long.Parse(bitacoraBk.Id));
                                    foreach (var destination in noProcterDestinations)
                                    {
                                        if (!ontimedb.UpdateShipmentsWithBitacoraNumber(long.Parse(bitacoraBk.Id), destination.DeliveryNumber))
                                        {
                                            failed = true;
                                        }
                                    }
                                    scope.Complete();
                                }
                            }
                            //Send message to OnTime
                            try
                            {
                                if (response.Success && bitacoraBk.Status.Equals("A"))
                                {
                                    SendOnTimeMessage(bitacoraId, bitacoraBk.Status);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LogError(ex.ToString());
                                Log("User: " + User.Identity.Name + "\n" + ex);
                                response.Message += Resources.Resource.Rabbit_Error;
                            }
                            if (failed)
                            {
                                liErrors.Add(new ValidationFailure("OnTime", Resources.Resource.The_shipping_numbers_were_not_modified_on_OnTime));
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += Resources.Resource.OnTime_Error;
                        }
                    }

                    #endregion

                    #region Crea el Gps virtual

                    if (response.Success)
                    {
                        try
                        {
                            using (var scope = new TransactionScope())
                            {
                                using (var gpsBl = new Bl.BitacoraAiLq.CatGpsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                                {
                                    gpsBl.CreateGpsWsBitacora(long.Parse(bitacoraBk.Id));
                                    scope.Complete();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += Resources.Resource.GPS_Virtual_Error;
                        }
                    }

                    #endregion

                    #region Envio de mensaje de edicion de Bitacora a Rabbit

                    try
                    {
                        if (response.Success && (bitacoraBk.EnableBitacora || bitacoraBk.Status.Equals("P") || bitacoraBk.Status.Equals("A")))
                        {
                            SendMessage(bitacoraId, "create");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex.ToString());
                        Log("User: " + User.Identity.Name + "\n" + ex);
                        response.Message += Resources.Resource.Rabbit_Error;
                    }

                    #endregion

                    response.Errors = liErrors;

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpPost("CreatePreFolios")]
        public async Task<IActionResult> CreatePreFolios([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraEnt>
                {
                    Items = new List<BitacoraEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                var liErrors = new List<ValidationFailure>();
                try
                {
                    var bitacora = (BitacoraEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(BitacoraEnt));
                    bitacora.GenerateOrder = true;

                    using (var monTypeBl = new Bl.BitacoraAiLq.CatMonitoringTypesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var monType = monTypeBl.First(bitacora.MonitoringType.Id);
                        bitacora.MonitoringType = monType;
                    }

                    using (var cusTypeBl = new Bl.BitacoraAiLq.CatCustodianTypesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var cusType = cusTypeBl.First(bitacora.Custody.CustodyType.Id);
                        bitacora.Custody.CustodyType = cusType;
                    }

                    #region FluentValidator

                    ValidationResult fluentResult;
                    switch (bitacora.Status)
                    {
                        case "R":
                            using (var val = new PreFoliosCreateValidator())
                            {
                                fluentResult = val.Validate(bitacora);
                            }
                            break;
                        default:
                            response.Success = false;
                            response.Message = Resources.Resource.Operation_Not_Allowed;
                            return Created("Error", response);
                    }
                    if (!fluentResult.IsValid)
                    {
                        response.Errors = fluentResult.Errors;
                        return Created("Error", response);
                    }

                    #endregion

                    #region Transaction to insert Bitacora

                    var bitacoraBk = bitacora;

                    string bitacoraId = null;
                    var mode = string.Empty;
                    using (var scope = new TransactionScope())
                    {
                        using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            if (bitacoraBl.Create(bitacoraBk, ref bitacoraId, true) && !string.IsNullOrEmpty(bitacoraId))
                            {
                                response.Message =
                                    string.Format(Resources.Resource.Item_Was_Action_With_The_Identificador,
                                        Resources.Resource.Binnacle, Resources.Resource.Created, bitacoraBk.Alias);
                                response.Message += "<br>";
                                scope.Complete();
                                Logger.LogInformation(
                                    string.Format(Resources.Resource.Item_Was_Action_With_The_Identificador,
                                        Resources.Resource.Binnacle, Resources.Resource.Created,
                                        bitacoraId)
                                );
                                mode = "create";
                                response.Success = true;
                            }
                            else
                            {
                                response.Success = false;
                                response.Message = Resources.Resource.Generic_Error;
                            }
                        }
                    }

                    #endregion

                    #region Carga la nueva configuracion de la Bitacora

                    //read new configuration
                    if (response.Success)
                    {
                        try
                        {
                            using (var bl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                            {
                                bitacoraBk = bl.GetToEdit(long.Parse(bitacoraId ?? "0"));
                                response.Item = bitacoraBk;
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += "No se pudo leer la nueva configuración de la bitacora: " + bitacoraBk.Id;
                        }
                    }
                    else
                    {
                        Logger.LogError(string.Format(Resources.Resource.Error_On_Structure_Data_When_Action_Bitacora, mode, bitacoraId));
                        Log("User: " + User.Identity.Name + "\n" + string.Format(Resources.Resource.Error_On_Structure_Data_When_Action_Bitacora, Resources.Resource.Update, bitacoraId));
                    }

                    #endregion

                    #region Crea el Gps virtual

                    if (response.Success)
                    {
                        try
                        {
                            using (var scope = new TransactionScope())
                            {
                                using (var gpsBl = new Bl.BitacoraAiLq.CatGpsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                                {
                                    gpsBl.CreateGpsWsBitacora(long.Parse(bitacoraBk.Id));
                                    scope.Complete();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += Resources.Resource.GPS_Virtual_Error;
                        }
                    }

                    #endregion

                    #region Envio de mensaje de edicion de Bitacora a Rabbit

                    try
                    {
                        if (response.Success && (bitacoraBk.EnableBitacora || bitacoraBk.Status.Equals("P") || bitacoraBk.Status.Equals("A")))
                        {
                            //SendMessageToRethink(bitacoraId);
                            SendMessage(bitacoraId, "create");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex.ToString());
                        Log("User: " + User.Identity.Name + "\n" + ex);
                        response.Message += Resources.Resource.Rabbit_Error;
                    }

                    #endregion

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpPost("CloneBitacora")]
        public async Task<IActionResult> CloneBitacora([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraEnt>
                {
                    Items = new List<BitacoraEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    var bitacoraGrid = (BitacoraGrid)JsonConvert.DeserializeObject(dto.ToString(), typeof(BitacoraGrid));
                    string bitacoraId = null;
                    BitacoraEnt bitacoraBk;
                    using (var bl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        bitacoraBk = bl.GetToEdit(long.Parse(bitacoraGrid.Id));
                    }
                    var listBitacoras = new List<string>();
                    if (bitacoraBk != null)
                    {
                        using (var scope = new TransactionScope())
                        {
                            using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                            {
                                for (int i = 0; i < bitacoraGrid.Duplicates; i++)
                                {
                                    if (bitacoraBl.CreateCloneBitacoraWithServiceOrder(long.Parse(bitacoraGrid.Id), bitacoraBk, ref bitacoraId) && !string.IsNullOrEmpty(bitacoraId))
                                    {
                                        response.Message =
                                            string.Format(Resources.Resource.Item_Was_Action_With_The_Identificador,
                                                Resources.Resource.Binnacle, Resources.Resource.Created, bitacoraBk.Alias);
                                        response.Message += "<br>";
                                        //scope.Complete();
                                        Logger.LogInformation(
                                            string.Format(Resources.Resource.Item_Was_Action_With_The_Identificador,
                                                Resources.Resource.Binnacle, Resources.Resource.Created,
                                                bitacoraId)
                                        );
                                        listBitacoras.Add(bitacoraId);
                                        response.Success = true;
                                    }
                                    else
                                    {
                                        response.Success = false;
                                        response.Message = Resources.Resource.Generic_Error;
                                        break;
                                    }
                                }

                                if (response.Success)
                                {
                                    scope.Complete();
                                }
                            }
                        }
                        if (response.Success && (bitacoraBk.EnableBitacora || bitacoraBk.Status.Equals("P")))
                        {
                            foreach (var bitacora in listBitacoras)
                            {
                                SendMessage(bitacora, "create");
                            }
                        }
                        return Created("Success", response);
                    }
                    response.Success = true;
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(async () =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraEnt>
                {
                    Items = new List<BitacoraEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                var liErrors = new List<ValidationFailure>();
                try
                {
                    var result = false;
                    var bitacora = (BitacoraEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(BitacoraEnt));
                    var bitacoraToEdit = new BitacoraEnt();
                    var bitacoraId = bitacora.Id;
                    var mode = string.Empty;
                    var validateGps = false;

                    #region FluentValidator

                    var fluentResult = new ValidationResult();
                    switch (bitacora.Status)
                    {
                        case "R":
                            if (bitacora.IsAxActive == false && bitacora.GenerateOrder)
                            {
                                using (var val = new PreCaptureOrderServideValidator())
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            else if (bitacora.EnableBitacora)
                            {
                                using (var val = new PreCaptureActivateValidator())
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            else
                            {
                                using (var val = new PreCaptureValidator())
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            break;
                        case "O":
                            if (bitacora.EnableBitacora)
                            {
                                using (var val = new PreCaptureActivateValidator())
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            else
                            {
                                using (var val = new PreCaptureValidator())
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            break;
                        case "P":
                            using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                            {
                                using (var val = new PreBitacoraValidator(bitacoraBl))
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            break;
                        case "E":
                            using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                            {
                                using (var val = new PreBitacoraValidator(bitacoraBl))
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            break;
                        case "A":
                            using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                            {
                                using (var val = new PreBitacoraValidator(bitacoraBl))
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            break;
                        default:
                            response.Success = false;
                            response.Message = Resources.Resource.Operation_Not_Allowed;
                            if (!response.Success)
                            {
                                return Created("Error", response);
                            }
                            break;
                    }

                    if (!fluentResult.IsValid)
                    {
                        response.Errors = fluentResult.Errors;
                        return Created("Error", response);
                    }

                    #endregion

                    #region Transaction to insert Bitacora

                    using (var scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            var bitacoraOnEdition = bitacoraBl.ValidPersmissionToEdit(long.Parse(bitacora.Id), _accessor.HttpContext.Connection.RemoteIpAddress.ToString());
                            if (bitacoraOnEdition == null)
                            {
                                //si la bitacora no esta en edicion .. se notifica al usuario de que ya no se esta editando
                                response.Success = false;
                                response.Message = Resources.Resource.Operation_Not_Allowed +
                                                   string.Format(
                                                       Resources.Resource
                                                           .The_Bitacora_Alias_Is_Not_In_Edit_Mode,
                                                       bitacora.Alias);
                                return Created("Success", response);
                            }
                            if (bitacora.EnableBitacora && bitacora.Status.Equals("O"))
                            {
                                validateGps = true;
                                bitacora.EnableBitacora = false;
                            }
                            if (bitacoraBl.Update(bitacora))
                            {
                                bitacoraId = bitacora.Id;
                                response.Message =
                                    string.Format(Resources.Resource.Item_Was_Action_With_The_Identificador,
                                        Resources.Resource.Binnacle, Resources.Resource.Updated, bitacora.Alias);
                                response.Message += "<br>";
                                bitacoraBl.FinalizeEditionBitacora(long.Parse(bitacora.Id), _accessor.HttpContext.Connection.RemoteIpAddress.ToString());
                                scope.Complete();
                                Logger.LogInformation(string.Format("Bitacora updated with Id: " + bitacoraId));
                                mode = "edition";
                                response.Success = true;
                                response.Item = bitacora;
                            }
                            else
                            {
                                response.Success = false;
                                response.Message = Resources.Resource.Generic_Error;
                            }
                        }
                    }

                    #endregion


                    #region Carga la bitacora actual

                    //read new configuration
                    if (fluentResult.IsValid)
                    {
                        try
                        {
                            using (var bl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                            {
                                bitacoraToEdit = bl.GetToEdit(long.Parse(bitacoraId ?? "0"));
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += "No se pudo leer la nueva configuración de la bitacora: " + bitacoraToEdit.Alias;
                        }
                    }
                    else
                    {
                        Logger.LogError(string.Format(Resources.Resource.Error_On_Structure_Data_When_Action_Bitacora, mode, bitacoraId));
                        Log("User: " + User.Identity.Name + "\n" + string.Format(Resources.Resource.Error_On_Structure_Data_When_Action_Bitacora, Resources.Resource.Update, bitacoraId));
                    }

                    #endregion

                    #region Actualizar número de acuerdo en Dynamics
                    if (bitacora.Agreement != null)
                    {
                        using (var client = new OrderServiceReference.OrdenesServiciosWebClient(Configuration.GetSection("EndPointOSAx").Value))
                        {
                            // Configure WCF client
                            client.ChannelFactory.Credentials.UserName.UserName = Configuration.GetSection("DynamicsWcfClient")["UserName"];
                            client.ChannelFactory.Credentials.UserName.Password = Configuration.GetSection("DynamicsWcfClient")["Password"];
                            client.ChannelFactory.Credentials.Windows.ClientCredential.Domain = Configuration.GetSection("DynamicsWcfClient")["Domain"];
                            client.ChannelFactory.Credentials.Windows.ClientCredential.UserName = Configuration.GetSection("DynamicsWcfClient")["UserName"];
                            client.ChannelFactory.Credentials.Windows.ClientCredential.Password = Configuration.GetSection("DynamicsWcfClient")["Password"];

                            //Configure Context of Dynamics
                            var callContext = new OrderServiceReference.CallContext
                            {
                                Company = Configuration.GetSection("DynamicsCallContext")["Company"],
                                MessageId = Guid.NewGuid().ToString()
                            };

                            var orderServiceDynamics = await client.UpdateOrdenServicesAsync(callContext, bitacoraToEdit.ServiceOrder, bitacoraToEdit.Project.Id, bitacora.Agreement.Id, bitacoraToEdit.CreateDate);
                            var dynamicsResponse = orderServiceDynamics.response;

                            if (dynamicsResponse.Trim().Equals("true"))
                            {
                                result = true;
                            }
                            else
                            {
                                response.Item = null;
                                response.Success = false;
                                response.Message = dynamicsResponse;
                                Logger.LogError(dynamicsResponse);
                                return Ok(response);
                            }
                        }

                    }
                    #endregion

                    #region Valida que los Gps esten disponibles cuando pasa de la fase de Pre-Captura a Pre-Bitacora

                    if (validateGps)
                    {
                        try
                        {
                            bitacoraToEdit.EnableBitacora = true;
                            using (var scope = new TransactionScope())
                            {
                                List<BitacoraGps> gpss;
                                using (var gpsBl = new Bl.BitacoraAiLq.CatGpsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                                {
                                    gpss = gpsBl.GetAllInBitacora(long.Parse(bitacoraToEdit.Id));
                                }
                                if (!gpss.Any())
                                {
                                    using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                                    {
                                        if (bitacoraBl.UpdateStatus(bitacoraToEdit))
                                        {
                                            bitacoraId = bitacoraToEdit.Id;
                                            scope.Complete();
                                            Logger.LogInformation("Bitacora gps validated updated with Id: " + bitacoraId);
                                            Logger.LogInformation(
                                                "Bitacora gps validated updated with Id: " + bitacoraId);
                                        }
                                        else
                                        {
                                            response.Success = false;
                                            bitacoraToEdit.EnableBitacora = false;
                                        }
                                    }
                                }
                                else
                                {
                                    response.Success = false;
                                    bitacoraToEdit.EnableBitacora = false;
                                    response.Message +=
                                        Resources.Resource.GPS_disponible_gps_Error + " (" +
                                        string.Join(",",
                                            gpss.Select(x => x.Alias).ToList()) + ")";
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += Resources.Resource.GPS_valid_Error;
                        }
                    }

                    #endregion

                    #region Ver si el usuario pertenece a OnTime, si los destinos son de Origen Procter y envio de mensaje a colas de Rabbit

                    if (response.Success && bitacoraToEdit.Customer.OnTime)
                    {
                        try
                        {
                            int.TryParse(Configuration.GetSection("OnTimeCustomers").Value, out int onTimeCustomerProcter);
                            var failed = false;
                            List<DestinyEnt> noProcterDestinations;
                            if (bitacoraToEdit.Customer.Id == onTimeCustomerProcter)
                            {
                                using (var proterBl = new Bl.BitacoraAiEf.CatProcterOriginsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                                {
                                    var filters = bitacoraToEdit.Destinies.Select(d => new Filter
                                    {
                                        Field = "id_origin_destination",
                                        Value = d.Id.ToString(),
                                        Operator = "Equals"
                                    }).ToList();
                                    var destiniesAreProcter = proterBl.GetList(filters, new List<KendoSort>(), "OR");
                                    noProcterDestinations = bitacoraToEdit.Destinies.Where(d => !destiniesAreProcter.Any(p => p.id_origin_destination.Equals(d.Id))).ToList();
                                }
                            }
                            else
                            {
                                noProcterDestinations = bitacoraToEdit.Destinies.ToList();
                            }
                            using (var scope = new TransactionScope())
                            {
                                using (var ontimedb = new ShipmentsBl(GetOntimeAiConnection(), User.Identity.Name))
                                {
                                    ontimedb.ClearAllBitacoras(long.Parse(bitacoraToEdit.Id));
                                    foreach (var destination in noProcterDestinations)
                                    {
                                        if (!ontimedb.UpdateShipmentsWithBitacoraNumber(long.Parse(bitacoraToEdit.Id), destination.DeliveryNumber))
                                        {
                                            failed = true;
                                        }
                                    }
                                    scope.Complete();
                                }
                            }
                            //Send message to OnTime
                            try
                            {
                                if (response.Success && bitacoraToEdit.Status.Equals("A"))
                                {
                                    SendOnTimeMessage(bitacoraId, bitacoraToEdit.Status);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LogError(ex.ToString());
                                Log("User: " + User.Identity.Name + "\n" + ex);
                                response.Message += Resources.Resource.Rabbit_Error;
                            }
                            if (failed)
                            {
                                liErrors.Add(new ValidationFailure("OnTime", Resources.Resource.The_shipping_numbers_were_not_modified_on_OnTime));
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += Resources.Resource.OnTime_Error;
                        }
                    }

                    #endregion

                    #region Crea el Gps virtual

                    if (response.Success)
                    {
                        try
                        {
                            using (var scope = new TransactionScope())
                            {
                                using (var gpsBl = new Bl.BitacoraAiLq.CatGpsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                                {
                                    gpsBl.CreateGpsWsBitacora(long.Parse(bitacoraToEdit.Id));
                                    scope.Complete();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += Resources.Resource.GPS_Virtual_Error;
                        }
                    }

                    #endregion

                    #region Envio de mensaje de edicion de Bitacora a Rabbit

                    try
                    {
                        if (response.Success && (bitacoraToEdit.EnableBitacora || bitacoraToEdit.Status.Equals("P") || bitacoraToEdit.Status.Equals("A")))
                        {
                            //SendMessageToRethink(bitacoraId);
                            SendMessage(bitacoraId, "edition");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex.ToString());
                        Log("User: " + User.Identity.Name + "\n" + ex);
                        response.Message += Resources.Resource.Rabbit_Error;
                    }

                    #endregion

                    #region SendToMqBitacoraStatus

                    var senderMQ = new SenderMqBitacora(Configuration);
                    senderMQ.SendToMqBitacoraStatus(bitacoraId, User.Identity.Name);

                    #endregion


                    response.Errors = liErrors;

                    if (result)
                    {
                        response.Item = bitacora;
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpPost("UpdatePreFolio")]
        public async Task<IActionResult> UpdatePreFolio([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(async () =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraEnt>
                {
                    Items = new List<BitacoraEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                var liErrors = new List<ValidationFailure>();
                try
                {
                    var result = false;
                    var bitacora = (BitacoraEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(BitacoraEnt));
                    var bitacoraToEdit = new BitacoraEnt();
                    var bitacoraId = bitacora.Id;
                    var mode = string.Empty;
                    var validateGps = false;

                    using (var monTypeBl = new Bl.BitacoraAiLq.CatMonitoringTypesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var monType = monTypeBl.First(bitacora.MonitoringType.Id);
                        bitacora.MonitoringType = monType;
                    }

                    using (var cusTypeBl = new Bl.BitacoraAiLq.CatCustodianTypesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var cusType = cusTypeBl.First(bitacora.Custody.CustodyType.Id);
                        bitacora.Custody.CustodyType = cusType;
                    }

                    #region FluentValidator

                    var fluentResult = new ValidationResult();
                    switch (bitacora.Status)
                    {
                        case "R":
                            if (bitacora.IsAxActive == false && bitacora.GenerateOrder)
                            {
                                using (var val = new PreCaptureOrderServideValidator())
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            else if (bitacora.EnableBitacora)
                            {
                                using (var val = new PreCaptureActivateValidator())
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            else
                            {
                                using (var val = new PreCaptureValidator())
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            break;
                        case "O":
                            if (bitacora.EnableBitacora)
                            {
                                using (var val = new PreCaptureActivateValidator())
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            else
                            {
                                using (var val = new PreFoliosCreateValidator())
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            break;
                        case "P":
                            using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                            {
                                using (var val = new PreBitacoraValidator(bitacoraBl))
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            break;
                        case "E":
                            using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                            {
                                using (var val = new PreBitacoraValidator(bitacoraBl))
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            break;
                        case "A":
                            using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                            {
                                using (var val = new PreBitacoraValidator(bitacoraBl))
                                {
                                    fluentResult = val.Validate(bitacora);
                                }
                            }
                            break;
                        default:
                            response.Success = false;
                            response.Message = Resources.Resource.Operation_Not_Allowed;
                            if (!response.Success)
                            {
                                return Created("Error", response);
                            }
                            break;
                    }

                    if (!fluentResult.IsValid)
                    {
                        response.Errors = fluentResult.Errors;
                        return Created("Error", response);
                    }

                    #endregion

                    #region Transaction to insert Bitacora

                    using (var scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            var bitacoraOnEdition = bitacoraBl.ValidPersmissionToEdit(long.Parse(bitacora.Id), _accessor.HttpContext.Connection.RemoteIpAddress.ToString());
                            if (bitacoraOnEdition == null)
                            {
                                //si la bitacora no esta en edicion .. se notifica al usuario de que ya no se esta editando
                                response.Success = false;
                                response.Message = Resources.Resource.Operation_Not_Allowed +
                                                   string.Format(
                                                       Resources.Resource
                                                           .The_Bitacora_Alias_Is_Not_In_Edit_Mode,
                                                       bitacora.Alias);
                                return Created("Success", response);
                            }
                            if (bitacora.EnableBitacora && bitacora.Status.Equals("O"))
                            {
                                validateGps = true;
                                bitacora.EnableBitacora = false;
                            }
                            if (bitacoraBl.Update(bitacora))
                            {
                                bitacoraId = bitacora.Id;
                                response.Message =
                                    string.Format(Resources.Resource.Item_Was_Action_With_The_Identificador,
                                        Resources.Resource.Binnacle, Resources.Resource.Updated, bitacora.Alias);
                                response.Message += "<br>";
                                bitacoraBl.FinalizeEditionBitacora(long.Parse(bitacora.Id), _accessor.HttpContext.Connection.RemoteIpAddress.ToString());
                                scope.Complete();
                                Logger.LogInformation(string.Format("Bitacora updated with Id: " + bitacoraId));
                                mode = "edition";
                                response.Success = true;
                                response.Item = bitacora;
                            }
                            else
                            {
                                response.Success = false;
                                response.Message = Resources.Resource.Generic_Error;
                            }
                        }
                    }

                    #endregion


                    #region Carga la bitacora actual

                    //read new configuration
                    if (fluentResult.IsValid)
                    {
                        try
                        {
                            using (var bl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                            {
                                bitacoraToEdit = bl.GetToEdit(long.Parse(bitacoraId ?? "0"));
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += "No se pudo leer la nueva configuración de la bitacora: " + bitacoraToEdit.Alias;
                        }
                    }
                    else
                    {
                        Logger.LogError(string.Format(Resources.Resource.Error_On_Structure_Data_When_Action_Bitacora, mode, bitacoraId));
                        Log("User: " + User.Identity.Name + "\n" + string.Format(Resources.Resource.Error_On_Structure_Data_When_Action_Bitacora, Resources.Resource.Update, bitacoraId));
                    }

                    #endregion

                    #region Actualizar número de acuerdo en Dynamics
                    if (bitacora.Agreement != null)
                    {
                        using (var client = new OrderServiceReference.OrdenesServiciosWebClient(Configuration.GetSection("EndPointOSAx").Value))
                        {
                            // Configure WCF client
                            client.ChannelFactory.Credentials.UserName.UserName = Configuration.GetSection("DynamicsWcfClient")["UserName"];
                            client.ChannelFactory.Credentials.UserName.Password = Configuration.GetSection("DynamicsWcfClient")["Password"];
                            client.ChannelFactory.Credentials.Windows.ClientCredential.Domain = Configuration.GetSection("DynamicsWcfClient")["Domain"];
                            client.ChannelFactory.Credentials.Windows.ClientCredential.UserName = Configuration.GetSection("DynamicsWcfClient")["UserName"];
                            client.ChannelFactory.Credentials.Windows.ClientCredential.Password = Configuration.GetSection("DynamicsWcfClient")["Password"];

                            //Configure Context of Dynamics
                            var callContext = new OrderServiceReference.CallContext
                            {
                                Company = Configuration.GetSection("DynamicsCallContext")["Company"],
                                MessageId = Guid.NewGuid().ToString()
                            };

                            var orderServiceDynamics = await client.UpdateOrdenServicesAsync(callContext, bitacoraToEdit.ServiceOrder, bitacoraToEdit.Project.Id, bitacora.Agreement.Id, bitacoraToEdit.CreateDate);
                            var dynamicsResponse = orderServiceDynamics.response;

                            if (dynamicsResponse.Trim().Equals("true"))
                            {
                                result = true;
                            }
                            else
                            {
                                response.Item = null;
                                response.Success = false;
                                response.Message = dynamicsResponse;
                                Logger.LogError(dynamicsResponse);
                                return Ok(response);
                            }
                        }

                    }
                    #endregion

                    #region Valida que los Gps esten disponibles cuando pasa de la fase de Pre-Captura a Pre-Bitacora

                    if (validateGps)
                    {
                        try
                        {
                            bitacoraToEdit.EnableBitacora = true;
                            using (var scope = new TransactionScope())
                            {
                                List<BitacoraGps> gpss;
                                using (var gpsBl = new Bl.BitacoraAiLq.CatGpsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                                {
                                    gpss = gpsBl.GetAllInBitacora(long.Parse(bitacoraToEdit.Id));
                                }
                                if (!gpss.Any())
                                {
                                    using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                                    {
                                        if (bitacoraBl.UpdateStatus(bitacoraToEdit))
                                        {
                                            bitacoraId = bitacoraToEdit.Id;
                                            scope.Complete();
                                            Logger.LogInformation("Bitacora gps validated updated with Id: " + bitacoraId);
                                            Logger.LogInformation(
                                                "Bitacora gps validated updated with Id: " + bitacoraId);
                                        }
                                        else
                                        {
                                            response.Success = false;
                                            bitacoraToEdit.EnableBitacora = false;
                                        }
                                    }
                                }
                                else
                                {
                                    response.Success = false;
                                    bitacoraToEdit.EnableBitacora = false;
                                    response.Message +=
                                        Resources.Resource.GPS_disponible_gps_Error + " (" +
                                        string.Join(",",
                                            gpss.Select(x => x.Alias).ToList()) + ")";
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += Resources.Resource.GPS_valid_Error;
                        }
                    }

                    #endregion

                    #region Ver si el usuario pertenece a OnTime, si los destinos son de Origen Procter y envio de mensaje a colas de Rabbit

                    if (response.Success && bitacoraToEdit.Customer.OnTime)
                    {
                        try
                        {
                            int.TryParse(Configuration.GetSection("OnTimeCustomers").Value, out int onTimeCustomerProcter);
                            var failed = false;
                            List<DestinyEnt> noProcterDestinations;
                            if (bitacoraToEdit.Customer.Id == onTimeCustomerProcter)
                            {
                                using (var proterBl = new Bl.BitacoraAiEf.CatProcterOriginsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                                {
                                    var filters = bitacoraToEdit.Destinies.Select(d => new Filter
                                    {
                                        Field = "id_origin_destination",
                                        Value = d.Id.ToString(),
                                        Operator = "Equals"
                                    }).ToList();
                                    var destiniesAreProcter = proterBl.GetList(filters, new List<KendoSort>(), "OR");
                                    noProcterDestinations = bitacoraToEdit.Destinies.Where(d => !destiniesAreProcter.Any(p => p.id_origin_destination.Equals(d.Id))).ToList();
                                }
                            }
                            else
                            {
                                noProcterDestinations = bitacoraToEdit.Destinies.ToList();
                            }
                            using (var scope = new TransactionScope())
                            {
                                using (var ontimedb = new ShipmentsBl(GetOntimeAiConnection(), User.Identity.Name))
                                {
                                    ontimedb.ClearAllBitacoras(long.Parse(bitacoraToEdit.Id));
                                    foreach (var destination in noProcterDestinations)
                                    {
                                        if (!ontimedb.UpdateShipmentsWithBitacoraNumber(long.Parse(bitacoraToEdit.Id), destination.DeliveryNumber))
                                        {
                                            failed = true;
                                        }
                                    }
                                    scope.Complete();
                                }
                            }
                            //Send message to OnTime
                            try
                            {
                                if (response.Success && bitacoraToEdit.Status.Equals("A"))
                                {
                                    SendOnTimeMessage(bitacoraId, bitacoraToEdit.Status);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LogError(ex.ToString());
                                Log("User: " + User.Identity.Name + "\n" + ex);
                                response.Message += Resources.Resource.Rabbit_Error;
                            }
                            if (failed)
                            {
                                liErrors.Add(new ValidationFailure("OnTime", Resources.Resource.The_shipping_numbers_were_not_modified_on_OnTime));
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += Resources.Resource.OnTime_Error;
                        }
                    }

                    #endregion

                    #region Crea el Gps virtual

                    if (response.Success)
                    {
                        try
                        {
                            using (var scope = new TransactionScope())
                            {
                                using (var gpsBl = new Bl.BitacoraAiLq.CatGpsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                                {
                                    gpsBl.CreateGpsWsBitacora(long.Parse(bitacoraToEdit.Id));
                                    scope.Complete();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += Resources.Resource.GPS_Virtual_Error;
                        }
                    }

                    #endregion

                    #region Envio de mensaje de edicion de Bitacora a Rabbit

                    try
                    {
                        if (response.Success && (bitacoraToEdit.EnableBitacora || bitacoraToEdit.Status.Equals("P") || bitacoraToEdit.Status.Equals("A")))
                        {
                            SendMessage(bitacoraId, "edition");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex.ToString());
                        Log("User: " + User.Identity.Name + "\n" + ex);
                        response.Message += Resources.Resource.Rabbit_Error;
                    }

                    #endregion

                    #region SendToMqBitacoraStatus

                    var senderMq = new SenderMqBitacora(Configuration);
                    senderMq.SendToMqBitacoraStatus(bitacoraId, User.Identity.Name);

                    #endregion

                    response.Errors = liErrors;

                    if (result)
                    {
                        response.Item = bitacora;
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpPost("UpdateTransportToPreFolios")]
        public async Task<IActionResult> UpdateTransportToPreFolios([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraEnt>
                {
                    Items = new List<BitacoraEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                var liErrors = new List<ValidationFailure>();
                try
                {
                    var result = false;
                    var bitacora = (BitacoraEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(BitacoraEnt));
                    BitacoraEnt bitacoraBefore = null;
                    if (!string.IsNullOrEmpty(bitacora.Id))
                    {
                        var bitacoraid = long.Parse(bitacora.Id);
                        using (var bl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            bitacoraBefore = bl.GetToEdit(bitacoraid);
                        }
                    }

                    if (bitacoraBefore != null)
                    {
                        bitacoraBefore.TransportLine = bitacora.TransportLine;
                        bitacoraBefore.Transport.Operators = bitacora.Transport.Operators;
                        bitacoraBefore.Transport.Plate = bitacora.Transport.Plate;
                        bitacoraBefore.Transport.UnitStamps = bitacora.Transport.UnitStamps;
                        bitacoraBefore.Transport.Gpss = bitacora.Transport.Gpss;
                        bitacoraBefore.Transport.Convoy = bitacora.Transport.Convoy;
                        bitacoraBefore.Transport.Containers = bitacora.Transport.Containers;
                        bitacoraBefore.Transport.Observations = bitacora.Transport.Observations;
                        bitacoraBefore.EnableBitacora = bitacora.EnableBitacora;

                    }
                    else
                    {
                        response.Success = false;
                        response.Message = Resources.Resource.Operation_Not_Allowed;
                        return Created("Error", response);
                    }

                    #region FluentValidator

                    var fluentResult = new ValidationResult();
                    switch (bitacoraBefore.Status)
                    {
                        case "O":
                            if (bitacoraBefore.EnableBitacora)
                            {
                                using (var val = new PreFoliosActivateValidator())
                                {
                                    fluentResult = val.Validate(bitacoraBefore);
                                }
                            }
                            else
                            {
                                using (var val = new TransportPreFoliosValidator())
                                {
                                    fluentResult = val.Validate(bitacoraBefore);
                                }
                            }
                            break;
                        default:
                            response.Success = false;
                            response.Message = Resources.Resource.Operation_Not_Allowed;
                            if (!response.Success)
                            {
                                return Created("Error", response);
                            }
                            break;
                    }

                    if (!fluentResult.IsValid)
                    {
                        response.Errors = fluentResult.Errors;
                        response.Message = string.Join(", ", fluentResult.Errors.Select(m => m.ErrorMessage));
                        return Created("Error", response);
                    }

                    #endregion

                    #region Transaction to insert Bitacora

                    var bitacoraBk = bitacoraBefore;
                    string bitacoraId = null;
                    var mode = string.Empty;
                    var validateGps = false;
                    using (var scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            var bitacoraOnEdition = bitacoraBl.ValidPersmissionToEdit(long.Parse(bitacoraBk.Id), _accessor.HttpContext.Connection.RemoteIpAddress.ToString());
                            if (bitacoraOnEdition == null)
                            {
                                //si la bitacora no esta en edicion .. se notifica al usuario de que ya no se esta editando
                                response.Success = false;
                                response.Message = Resources.Resource.Operation_Not_Allowed +
                                                   string.Format(
                                                       Resources.Resource
                                                           .The_Bitacora_Alias_Is_Not_In_Edit_Mode,
                                                       bitacoraBk.Alias);
                                return Created("Success", response);
                            }
                            if (bitacoraBk.EnableBitacora && bitacoraBk.Status.Equals("O"))
                            {
                                validateGps = true;
                                bitacoraBk.EnableBitacora = false;
                            }
                            if (bitacoraBl.UpdateAddTransportToPreFolios(bitacoraBk, true))
                            {
                                bitacoraId = bitacoraBk.Id;
                                response.Message =
                                    string.Format(Resources.Resource.Item_Was_Action_With_The_Identificador,
                                        Resources.Resource.Binnacle, Resources.Resource.Updated, bitacoraBk.Alias);
                                response.Message += "<br>";
                                bitacoraBl.FinalizeEditionBitacora(long.Parse(bitacoraBk.Id), _accessor.HttpContext.Connection.RemoteIpAddress.ToString());
                                scope.Complete();
                                Logger.LogInformation(string.Format("Bitacora updated with Id: " + bitacoraId));
                                mode = "edition";
                                response.Success = true;
                            }
                            else
                            {
                                response.Success = false;
                                response.Message = Resources.Resource.Generic_Error;
                            }
                        }
                    }

                    #endregion

                    #region Carga la nueva configuracion de la Bitacora

                    //read new configuration
                    //if (response.Success)
                    //{
                    //    try
                    //    {
                    //        using (var bl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    //        {
                    //            bitacoraBk = bl.GetToEdit(long.Parse(bitacoraId ?? "0"));
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        Logger.LogError(ex.ToString());
                    //        Log("User: " + User.Identity.Name + "\n" + ex);
                    //        //response.Message += "No se pudo leer la nueva configuración de la bitacora: " + bitacoraBk.Alias;
                    //    }
                    //}
                    //else
                    //{
                    //    Logger.LogError(string.Format(Resources.Resource.Error_On_Structure_Data_When_Action_Bitacora, mode, bitacoraId));
                    //    Log("User: " + User.Identity.Name + "\n" + string.Format(Resources.Resource.Error_On_Structure_Data_When_Action_Bitacora, Resources.Resource.Update, bitacoraId));
                    //}

                    #endregion

                    #region Valida que los Gps esten disponibles cuando pasa de la fase de Pre-Captura a Pre-Bitacora

                    if (validateGps)
                    {
                        try
                        {
                            bitacoraBk.EnableBitacora = true;
                            using (var scope = new TransactionScope())
                            {
                                List<BitacoraGps> gpss;
                                using (var gpsBl = new Bl.BitacoraAiLq.CatGpsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                                {
                                    gpss = gpsBl.GetAllInBitacora(long.Parse(bitacoraBk.Id));

                                    if (!gpss.Any(gp => gp.Assigned))
                                    {
                                        using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name, gpsBl._ctx))
                                        {
                                            if (bitacoraBl.UpdateStatus(bitacoraBk, true))
                                            {
                                                bitacoraId = bitacoraBk.Id;
                                                scope.Complete();
                                                Logger.LogInformation("Bitacora gps validated updated with Id: " + bitacoraId);
                                                Logger.LogInformation(
                                                    "Bitacora gps validated updated with Id: " + bitacoraId);
                                                response.Item = bitacoraBk;
                                                response.Success = true;
                                            }
                                            else
                                            {
                                                response.Success = false;
                                                bitacoraBk.EnableBitacora = false;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        response.Success = false;
                                        bitacoraBk.EnableBitacora = false;
                                        response.Message +=
                                            Resources.Resource.GPS_disponible_gps_Error + " (" +
                                            string.Join(",",
                                                gpss.Where(x => x.Assigned)
                                                    .Select(x => x.Alias).ToList()) + ")";
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += Resources.Resource.GPS_valid_Error;
                        }
                    }

                    #endregion

                    #region Ver si el usuario pertenece a OnTime, si los destinos son de Origen Procter y envio de mensaje a colas de Rabbit

                    if (response.Success && bitacoraBk.Customer.OnTime)
                    {
                        try
                        {
                            int.TryParse(Configuration.GetSection("OnTimeCustomers").Value, out int onTimeCustomerProcter);
                            var failed = false;
                            List<DestinyEnt> noProcterDestinations;
                            if (bitacoraBk.Customer.Id == onTimeCustomerProcter)
                            {
                                using (var proterBl = new Bl.BitacoraAiEf.CatProcterOriginsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                                {
                                    var filters = bitacoraBk.Destinies.Select(d => new Filter
                                    {
                                        Field = "id_origin_destination",
                                        Value = d.Id.ToString(),
                                        Operator = "Equals"
                                    }).ToList();
                                    var destiniesAreProcter = proterBl.GetList(filters, new List<KendoSort>(), "OR");
                                    noProcterDestinations = bitacoraBk.Destinies.Where(d => !destiniesAreProcter.Any(p => p.id_origin_destination.Equals(d.Id))).ToList();
                                }
                            }
                            else
                            {
                                noProcterDestinations = bitacoraBk.Destinies.ToList();
                            }
                            using (var scope = new TransactionScope())
                            {
                                using (var ontimedb = new ShipmentsBl(GetOntimeAiConnection(), User.Identity.Name))
                                {
                                    ontimedb.ClearAllBitacoras(long.Parse(bitacoraBk.Id));
                                    foreach (var destination in noProcterDestinations)
                                    {
                                        if (!ontimedb.UpdateShipmentsWithBitacoraNumber(long.Parse(bitacoraBk.Id), destination.DeliveryNumber))
                                        {
                                            failed = true;
                                        }
                                    }
                                    scope.Complete();
                                }
                            }
                            //Send message to OnTime
                            try
                            {
                                if (response.Success && bitacoraBk.Status.Equals("A"))
                                {
                                    SendOnTimeMessage(bitacoraId, bitacoraBk.Status);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LogError(ex.ToString());
                                Log("User: " + User.Identity.Name + "\n" + ex);
                                response.Message += Resources.Resource.Rabbit_Error;
                            }
                            if (failed)
                            {
                                liErrors.Add(new ValidationFailure("OnTime", Resources.Resource.The_shipping_numbers_were_not_modified_on_OnTime));
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += Resources.Resource.OnTime_Error;
                        }
                    }

                    #endregion

                    #region Crea el Gps virtual

                    if (response.Success)
                    {
                        try
                        {
                            using (var scope = new TransactionScope())
                            {
                                using (var gpsBl = new Bl.BitacoraAiLq.CatGpsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                                {
                                    gpsBl.CreateGpsWsBitacora(long.Parse(bitacoraBk.Id));
                                    scope.Complete();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += Resources.Resource.GPS_Virtual_Error;
                        }
                    }

                    #endregion

                    #region Envio de mensaje de edicion de Bitacora a Rabbit

                    try
                    {
                        if (response.Success && (bitacoraBk.EnableBitacora || bitacoraBk.Status.Equals("P") || bitacoraBk.Status.Equals("A")))
                        {
                            SendMessage(bitacoraId, "edition");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex.ToString());
                        Log("User: " + User.Identity.Name + "\n" + ex);
                        //response.Message += Resources.Resource.Rabbit_Error;
                    }

                    #endregion

                    #region Envio de estatus de bitacora para actualizar Grid de Folios a Rabbit

                    var senderMq = new SenderMqBitacora(Configuration);
                    senderMq.SendToMqBitacoraStatus(bitacoraId, User.Identity.Name);

                    #endregion

                    response.Errors = liErrors;

                    if (result)
                    {
                        response.Message = Resources.Resource.Successful_Operation;
                    }
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Delete one Bitacora
        /// </summary>
        /// <param name="dto">Bitacora</param>
        /// <returns></returns>
        [HttpPost("Delete")]
        public async Task<IActionResult> Delete([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var response = new ResponseAction<ValidationFailure, BitacoraGrid>
                {
                    Items = new List<BitacoraGrid>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var bitacora = (BitacoraGrid)JsonConvert.DeserializeObject(dto.ToString(), typeof(BitacoraGrid));

                    #region Transaction

                    bool result;
                    using (var scope = new TransactionScope())
                    {
                        using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            result = bitacoraBl.ChangeStatus(long.Parse(bitacora.Id), bitacora.StatusId);
                            if (result)
                            {
                                Logger.LogInformation("Bitacora updated status with Id: " + bitacora.Id + " amd status: " + bitacora.StatusId);
                                scope.Complete();
                                response.Success = true;
                                response.Message = Resources.Resource.Successful_Operation;
                            }
                        }
                    }

                    if (result)
                    {
                        try
                        {
                            SendMessageToCancel(bitacora.Id);
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());
                            Log("User: " + User.Identity.Name + "\n" + ex);
                            response.Message += Resources.Resource.Rabbit_Error;
                        }
                    }

                    #endregion

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid permission to edit Bitacora
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("validPersmissionToEdit/{binnacle}")]
        public async Task<IActionResult> ValidPersmissionToEdit(string binnacle)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, string>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false
                };
                try
                {
                    var bitacoraid = Encoding.UTF8.GetString(Convert.FromBase64String(binnacle));
                    bitacoraid = string.IsNullOrEmpty(bitacoraid) ? "0" : bitacoraid;
                    using (var bl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var bitacora = bl.ValidPersmissionToEdit(long.Parse(bitacoraid), _accessor.HttpContext.Connection.RemoteIpAddress.ToString());
                        if (bitacora != null && !bitacora.UserName.ToUpper().Equals(User.Identity.Name.ToUpper()))
                        {
                            response.Success = false;
                            response.Message = Resources.Resource.Access_Denied +
                                               string.Format(
                                                   Resources.Resource
                                                       .The_Bitacora_Alias_Is_Being_Edited_By_The_User_Name,
                                                   bitacora.Alias, bitacora.UserName);
                        }
                        else
                        {
                            response.Success = true;
                        }
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Cancel edition Bitacora
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("cancelPersmissionToEdit/{binnacle}")]
        public async Task<IActionResult> CancelPersmissionToEdit(string binnacle)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, string>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false
                };
                try
                {
                    var bitacoraid = Encoding.UTF8.GetString(Convert.FromBase64String(binnacle));
                    bitacoraid = string.IsNullOrEmpty(bitacoraid) ? "0" : bitacoraid;
                    using (var bl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        bl.FinalizeEditionBitacora(long.Parse(bitacoraid), _accessor.HttpContext.Connection.RemoteIpAddress.ToString());
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Cancel all edition Bitacora
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("cancelAllPersmissionToEdit")]
        public async Task<IActionResult> CancelAllPersmissionToEdit()
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, string>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        bl.FinalizeEditionAllBitacora(_accessor.HttpContext.Connection.RemoteIpAddress.ToString());
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpPost("SetBitacoraFalseMovement")]
        public async Task<IActionResult> SetBitacoraFalseMovement([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(async () =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraFalseMovementEnt>
                {
                    Items = new List<BitacoraFalseMovementEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    var entity = (BitacoraFalseMovementEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(BitacoraFalseMovementEnt));

                    bool result;

                    #region Send cancell to Dynamics
                    using (var client = new OrderServiceReference.OrdenesServiciosWebClient(Configuration.GetSection("EndPointOSAx").Value))
                    {


                        // Configure WCF client
                        client.ChannelFactory.Credentials.UserName.UserName =
                            Configuration.GetSection("DynamicsWcfClient")["UserName"];
                        client.ChannelFactory.Credentials.UserName.Password =
                            Configuration.GetSection("DynamicsWcfClient")["Password"];
                        client.ChannelFactory.Credentials.Windows.ClientCredential.Domain =
                            Configuration.GetSection("DynamicsWcfClient")["Domain"];
                        client.ChannelFactory.Credentials.Windows.ClientCredential.UserName =
                            Configuration.GetSection("DynamicsWcfClient")["UserName"];
                        client.ChannelFactory.Credentials.Windows.ClientCredential.Password =
                            Configuration.GetSection("DynamicsWcfClient")["Password"];

                        //Configure Context of Dynamics
                        var callContext = new OrderServiceReference.CallContext
                        {
                            Company = Configuration.GetSection("DynamicsCallContext")["Company"],
                            MessageId = Guid.NewGuid().ToString()
                        };


                        await client.StageChangeAsync(callContext, entity.ServiceOrder, "Terminado");


                    }

                    #endregion

                    using (var scope = new TransactionScope())
                    {

                        using (var bl = new Bl.BitacoraAiEf.BitacorasBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var filters = new List<Filter>
                            {
                                new Filter {Field = "id_bitacora", Value = entity.IdBitacora, Operator = "Equals"}
                            };
                            result = bl.UpdateWithOutTransaction(filters, itemToSave =>
                            {
                                itemToSave.status = "C";
                                itemToSave.update_date = DateTime.Now;
                                itemToSave.update_user = User.Identity.Name;
                            });
                        }

                        using (var blA = new Bl.BitacoraAiEf.BitacorasAxActivationBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var filters = new List<Filter>
                            {
                                new Filter {Field = "id", Value = entity.Id, Operator = "Equals"}
                            };
                            result = blA.UpdateWithOutTransaction(filters, itemToSave =>
                            {
                                itemToSave.IsFalseMovement = true;
                                itemToSave.EndServiceDate = DateTime.Now;
                                itemToSave.update_date = DateTime.Now;
                                itemToSave.update_user = User.Identity.Name;
                            });

                        }

                        if (result)
                        {
                            scope.Complete();
                            response.Success = true;
                            response.Message = Resources.Resource.Successful_Operation;
                        }


                    }

                    #region SendToMqBitacoraStatus

                    var senderMQ = new SenderMqBitacora(Configuration);
                    senderMQ.SendToMqBitacoraStatus(entity.IdBitacora, User.Identity.Name);

                    #endregion

                    return Ok(response);


                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [Route("gridbitacoras")]
        public async Task<IActionResult> Gridbitacoras([FromBody]KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var db = GetDbConnection())
                    {
                        var bl = new Bl.BitacoraAiEf.Bitacora_Frencuencia_Monitoreo(db);
                        var role = User.Claims.FirstOrDefault(c => "roleid".Equals(c.Type.ToLower()));
                        return Ok(bl.GetGridBitacoras(model, User.Identity.Name, role != null && role.Value.Contains(Configuration.GetSection("AdminRolId").Value)));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpPost("setFrecuency")]
        public async Task<IActionResult> SetFrecuency([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, SetFrecuencyMonitoring>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<SetFrecuencyMonitoring>(),
                    Success = true
                };
                try
                {
                    using (var scope = new TransactionScope())
                    {
                        using (var db = GetDbConnection())
                        {
                            using (var mf = new Bl.BitacoraAiEf.FrecuencyMonitoring(db))
                            {
                                if (mf.SetFrecuencyMonitoring(dto.Bitacoras, dto.Frecuency, dto.Comment, User.Identity.Name))
                                {
                                    scope.Complete();
                                    response.Success = true;
                                    response.Message = Resources.Resource.Successful_Operation;
                                }
                                else
                                {
                                    response.Success = false;
                                    response.Message = Resources.Resource.Generic_Error;
                                }
                            }
                        }
                    }
                    return Created("Success", response);
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [Route("getforftreport")]
        public async Task<IActionResult> GetForFtReport(string alias)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var db = GetDbConnection())
                    {
                        return Ok(db.sp_get_bitacoras_for_ft_report(alias).ToArray());
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        private void SendOnTimeMessage(string bitacoraId, string status = "A")
        {
            // Send message to Mq            
            using (var sender = new MqSender(Configuration, "MqOnTime"))
            {
                //send assignment
                var message = JsonConvert.SerializeObject(new BitacoraStatusMessage
                {
                    Status = status,
                    Id = bitacoraId,
                    StatusDetail = null
                });
                sender.Send(message, "operative.shipment");
                Logger.LogInformation("Send message to OnTime with BitacoraId: " + bitacoraId);
            }
        }

        private void SendMessage(string bitacoraId, string mode)
        {
            using (var bi = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
            {
                var bitacora = bi.GetBitacoraMessage(bitacoraId, GetAdoIdentityConnection(), Configuration.GetSection("RolesToGetUsers").Value, "create".Equals(mode));
                // Send message to Mq            
                using (var sender = new MqSender(Configuration, "Mq"))
                {
                    //send assignment
                    var message = JsonConvert.SerializeObject(bitacora);
                    sender.Send(message, "bitacora." + mode);
                    Logger.LogInformation("Send message Bitacora Edited with Id: " + bitacoraId);
                }
            }
        }

        private void SendMessageToCancel(string bitacoraId)
        {
            // Send message to Mq            
            using (var sender = new MqSender(Configuration, "Mq"))
            {
                //send assignment
                var message = JsonConvert.SerializeObject(new MonitoringMessage
                {
                    Id = bitacoraId,
                    Status = "C"
                });
                sender.Send(message, "bitacora.status.update");
            }
        }

        [HttpPost("GetGridBitacorasFrecuences")]
        public async Task<IActionResult> GetGridBitacorasFrecuences([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new Bl.BitacoraAiLq.CatMonitoringFrecuenciesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        return Ok(bl.GetGridBitacoras(model, false));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpPost("GetGridBitacorasCustodyAbandon")]
        public async Task<IActionResult> GetGridBitacorasCustodyAbandon([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var showAll = User.Claims.Any(x => x.Type.Equals("BitacoraPermission") && x.Value.Equals("ShowAllPreBitacoras"));
                        return Ok(bl.GetGridBitacorasCustodyAbandon(model, showAll));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }
        
        [HttpPost("UpdateCustodyAbandon")]
        public async Task<IActionResult> UpdateCustodyAbandon([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraEnt>
                {
                    Items = new List<BitacoraEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                var liErrors = new List<ValidationFailure>();
                try
                {
                    var bitacora = (BitacoraEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(BitacoraEnt));

                    #region FluentValidator

                    var fluentResult = new ValidationResult();
                    switch (bitacora.Status)
                    {
                        case "A":
                            using (var val = new BitacoraCustodyAbandonValidator())
                            {
                                fluentResult = val.Validate(bitacora);
                            }
                            break;
                        default:
                            response.Success = false;
                            response.Message = Resources.Resource.Operation_Not_Allowed;
                            if (!response.Success)
                            {
                                return Created("Error", response);
                            }
                            break;
                    }

                    if (!fluentResult.IsValid)
                    {
                        response.Errors = fluentResult.Errors;
                        return Created("Error", response);
                    }

                    #endregion

                    #region Transaction to insert Bitacora

                    var bitacoraBk = bitacora;
                    string bitacoraId = null;
                    var mode = string.Empty;
                    using (var scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bitacoraBl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            if (bitacora.Id != null)
                            {
                                var bitacoraOnEdition = bitacoraBl.ValidPersmissionToEdit(long.Parse(bitacoraBk.Id),
                                    _accessor.HttpContext.Connection.RemoteIpAddress.ToString());
                                if (bitacoraOnEdition == null)
                                {
                                    //si la bitacora no esta en edicion .. se notifica al usuario de que ya no se esta editando
                                    response.Success = false;
                                    response.Message = Resources.Resource.Operation_Not_Allowed +
                                                       string.Format(
                                                           Resources.Resource
                                                               .The_Bitacora_Alias_Is_Not_In_Edit_Mode,
                                                           bitacoraBk.Alias);
                                    return Created("Success", response);
                                }

                                if (bitacoraBl.UpdateCustodyAbandon(bitacoraBk))
                                {
                                    bitacoraId = bitacoraBk.Id;
                                    response.Message =
                                        string.Format(Resources.Resource.Item_Was_Action_With_The_Identificador,
                                            Resources.Resource.Binnacle, Resources.Resource.Updated, bitacoraBk.Alias);
                                    response.Message += "<br>";
                                    bitacoraBl.FinalizeEditionBitacora(long.Parse(bitacoraBk.Id),
                                        _accessor.HttpContext.Connection.RemoteIpAddress.ToString());
                                    scope.Complete();
                                    Logger.LogInformation("Bitacora updated with Id: " + bitacoraId);
                                    mode = "edition";
                                    response.Success = true;
                                }
                                else
                                {
                                    response.Success = false;
                                    response.Message = Resources.Resource.Generic_Error;
                                }
                            }
                        }
                    }

                    #endregion

                    #region Carga la nueva configuracion de la Bitacora

                    try
                    {
                        if (response.Success && (bitacoraBk.EnableBitacora || bitacoraBk.Status.Equals("P") || bitacoraBk.Status.Equals("A")))
                        {
                            SendMessage(bitacoraId, "edition");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex.ToString());
                        Log("User: " + User.Identity.Name + "\n" + ex);
                        //response.Message += Resources.Resource.Rabbit_Error;
                    }

                    #endregion

                    #region Envio de estatus de bitacora para actualizar Grid de Folios a Rabbit

                    var senderMQ = new SenderMqBitacora(Configuration);
                    senderMQ.SendToMqBitacoraStatus(bitacoraId, User.Identity.Name);

                    #endregion

                    response.Errors = liErrors;

                    if (response.Success)
                    {
                        response.Message = Resources.Resource.Successful_Operation;
                    }
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpPost("setFrecuency")]
        public async Task<IActionResult> SetFrecuency([FromBody] SetFrecuencyMonitoring dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, SetFrecuencyMonitoring>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<SetFrecuencyMonitoring>(),
                    Success = true
                };
                try
                {
                    using (var scope = new TransactionScope())
                    {
                        using (var db = GetDbConnection())
                        {
                            using (var mf = new Bl.BitacoraAiEf.FrecuencyMonitoring(db))
                            {
                                if (mf.SetFrecuencyMonitoring(dto.Bitacoras, dto.Frecuency, dto.Comment, User.Identity.Name))
                                {
                                    scope.Complete();
                                    response.Success = true;
                                    response.Message = Resources.Resource.Successful_Operation;
                                }
                                else
                                {
                                    response.Success = false;
                                    response.Message = Resources.Resource.Generic_Error;
                                }
                            }
                        }
                    }
                    return Created("Success", response);
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        //private void SendMessageToRethink(string bitacoraId)
        //{
        //    using (var bl = new BitacorasRethinkDb(GetAdoBitacoraAiConnection(), GetRethinkConnection(), User.Identity.Name))
        //    {
        //        bl.SendMessageRethinkDb(bitacoraId);
        //    }
        //}

        [HttpPost("GetGridBitacorasAssigment")]
        public async Task<IActionResult> GetGridBitacorasAssigment([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new Bl.BitacoraAiLq.BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var showAll = User.Claims.Any(x => x.Type.Equals("BitacoraPermission") && x.Value.Equals("ShowAllPreCaptures"));
                        return Ok(bl.GetGridBitacorasAssigment(model, showAll));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

    }
}