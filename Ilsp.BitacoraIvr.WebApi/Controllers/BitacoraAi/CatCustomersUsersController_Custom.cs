﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Ilsp.BitacoraIvr.Bl.IdentityLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.Ent.Identity;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using BitacorasUsersBl = Ilsp.BitacoraIvr.Bl.BitacoraAiLq.BitacorasUsersBl;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatCustomersUsersController
    {

        [HttpPost("AssignUserByCustomer")]
        public async Task<IActionResult> AssignUserByCustomer([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, UserEnt>
                {
                    Items = new List<UserEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    bool result;
                    var itemFront = (UserEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(UserEnt));
                    using (var scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bl = new CatCustomersUsersBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var filters = new List<Filter>
                            {
                                new Filter{Field = "customer_id", Value = itemFront.CustomerId, Operator = "Equals"},
                                new Filter{Field = "user_id", Value = itemFront.UserName, Operator = "Equals"}
                            };
                            var itemToSave = new cat_customers_users
                            {
                                user_id = itemFront.UserName,
                                customer_id = int.Parse(itemFront.CustomerId),
                                create_date = DateTime.Now,
                                update_user = User.Identity.Name
                            };
                            result = bl.SaveWithOutTransaction(itemToSave, filters);
                        }

                        if (result)
                        {
                            scope.Complete();
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpPost("UnassignUserByCustomer")]
        public async Task<IActionResult> UnassignUserByCustomer([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, UserEnt>
                {
                    Items = new List<UserEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    bool result;
                    var itemFront = (UserEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(UserEnt));
                    using (var scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bl = new CatCustomersUsersBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var filters = new List<Filter>
                            {
                                new Filter{Field = "customer_id", Value = itemFront.CustomerId, Operator = "Equals"},
                                new Filter{Field = "user_id", Value = itemFront.UserName, Operator = "Equals"}
                            };
                            result = bl.DeleteWithOutTransaction(filters);
                        }

                        if (result)
                        {
                            scope.Complete();
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpPost("AssignCustomerByUser")]
        public async Task<IActionResult> AssignCustomerByUser([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    bool result;
                    var itemFront = (CustomerEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(CustomerEnt));
                    using (var scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bl = new CatCustomersUsersBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var filters = new List<Filter>
                            {
                                new Filter{Field = "customer_id", Value = itemFront.Id.ToString(), Operator = "Equals"},
                                new Filter{Field = "user_id", Value = itemFront.UserName, Operator = "Equals"}
                            };
                            var itemToSave = new cat_customers_users
                            {
                                user_id = itemFront.UserName,
                                customer_id = itemFront.Id,
                                create_date = DateTime.Now,
                                update_user = User.Identity.Name
                            };
                            result = bl.SaveWithOutTransaction(itemToSave, filters);
                        }

                        if (result)
                        {
                            scope.Complete();
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpPost("UnassignCustomerByUser")]
        public async Task<IActionResult> UnassignCustomerByUser([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    bool result;
                    var itemFront = (CustomerEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(CustomerEnt));
                    using (var scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bl = new CatCustomersUsersBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var filters = new List<Filter>
                            {
                                new Filter{Field = "customer_id", Value = itemFront.Id.ToString(), Operator = "Equals"},
                                new Filter{Field = "user_id", Value = itemFront.UserName, Operator = "Equals"}
                            };
                            result = bl.DeleteWithOutTransaction(filters);
                        }

                        if (result)
                        {
                            scope.Complete();
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("GetUsersForAssignBitacoras/{customerId}/{bitacoraId}")]
        public async Task<IActionResult> GetUsersForAssignBitacoras(string customerId, string bitacoraId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, UserEnt>
                {
                    Items = new List<UserEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var customerIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                    var bitacoraIdDec = long.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(bitacoraId)));
                    List<UserEnt> usersDs;
                    using (var bl = new AspNetUsersBl(GetAdoIdentityConnection(), User.Identity.Name))
                    {
                        usersDs = bl.GetUsersForAssignBitacoras().ToList();
                    }
                    using (var bl = new BitacorasUsersBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var customerUsers = bl.GetUsersAssignOrUnassignUsersOnBitacora(customerIdDec, bitacoraIdDec).ToList();
                        usersDs.ForEach(item =>
                        {
                            item.Selected = customerUsers.Any(x => x.Selected && x.UserName.ToLower().Equals(item.UserName.ToLower()));
                        });
                        var result = usersDs;
                        response.Items = result;
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

    }
}
