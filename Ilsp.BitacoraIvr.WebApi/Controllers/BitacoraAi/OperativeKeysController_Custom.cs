﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    [Route("api/[controller]")]
    [Authorize]
    public partial class OperativeKeysController : BaseController
    {

        public OperativeKeysController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor) : base(configuration, loggerFactory, accessor)
        {
            Logger = loggerFactory.CreateLogger<OperativeKeysController>();
        }

        /// <summary>
        /// Get active operative keys
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, OperativeKeyEnt>
                {
                    Items = new List<OperativeKeyEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatOperativeKeysBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var roleId = User.Claims.FirstOrDefault(c => "roleid".Equals(c.Type.ToLower()));
                        response.Items = bl.GetOpKeysByRoleId(roleId.Value);
                        response.Success = true;
                        return Ok(response.Items);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get active operative keys
        /// </summary>
        /// <returns></returns>
        [HttpGet("byalert/{alertId}/{bitacoraId}")]
        public async Task<IActionResult> GetByAlert(int alertId, decimal bitacoraId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, sp_get_operative_keys_by_alertResult>
                {
                    Items = new List<sp_get_operative_keys_by_alertResult>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatOperativeKeysBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Items = bl.GetOpKeysByRoleIdAndAlertId(bitacoraId, alertId);
                        response.Success = true;
                        return Ok(response.Items);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }
    }
}
