﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Ilsp.BitacoraIvr.WebApi.Mq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Transactions;
using Ilsp.BitacoraIvr.Bl.IdentityEf;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatSecurityKitsController
    {
        /// <summary>
        /// Search Item on Entity
        /// </summary>
        /// <param name="customerId">Cutomer</param>
        /// <param name="value">Value to search</param>
        /// <returns></returns>
        [HttpGet]
        [Route("SearchByCustom/{customerId}/{value}")]
        public async Task<IActionResult> SearchByCustom(string customerId, string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, SecurityKitEnt>
                {
                    Items = new List<SecurityKitEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatSecurityKitsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var customerIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.Search(customerIdDec, searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get Item from Entity
        /// </summary>
        /// <param name="customerId">Cutomer</param>
        /// <param name="securityKitId">Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("FirstCustom/{customerId}/{securityKitId}")]
        public async Task<IActionResult> FirstCustom(string customerId, string securityKitId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, SecurityKitEnt>
                {
                    Items = new List<SecurityKitEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatSecurityKitsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var customerIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                        var securityKitIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(securityKitId)));
                        response.Item = bl.First(customerIdDec, securityKitIdDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get Items from Entity
        /// </summary>
        /// <param name="model">Filters and Sorts</param>
        /// <returns></returns>
        [HttpPost("GetGridCustom")]
        public async Task<IActionResult> GetGridCustom([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.VwCatSecurityKitsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var response = bl.GetGridCustom(model);
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        /// <summary>
        /// Get Items from Entity
        /// </summary>
        /// <param name="model">Filters and Sorts</param>
        /// <returns></returns>
        [HttpPost("GetGridByCustomer")]
        public async Task<IActionResult> GetGridByCustomer([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    var customers = new List<int>();
                    using (var bl = new AspNetUsersBl(GetIdentityConnection(), User.Identity.Name))
                    {
                        var item = bl.GetOne(new List<Filter>
                        {
                            new Filter {Field = "UserName", Operator = "Equals", Value = User.Identity.Name}
                        }, new[] { "AspNetUserClaims" });
                        if (item != null)
                        {
                            customers = item.AspNetUserClaims.Where(c => c.ClaimType.Equals("CustomerId")).Select(c => int.Parse(c.ClaimValue)).ToList();
                        }
                    }
                    using (var bl = new Bl.BitacoraAiEf.CatSecurityKitsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var includes = new[]
                        {
                            "cat_security_kits_routes",
                            "cat_security_kits_georeferences",
                            "cat_customers_security_kits"
                        };
                        var response = bl.GetGridCustomByCustomers(model, customers, includes);
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, SecurityKitEnt>
                {
                    Items = new List<SecurityKitEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity =
                        (SecurityKitEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(SecurityKitEnt));

                    using (var scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bl = new CatSecurityKitsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            result = bl.Create(entity);
                            if (result)
                            {
                                scope.Complete();
                            }
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        try
                        {
                            SendMessage();
                        }
                        catch (Exception ex)
                        {
                            response.Message += Resources.Resource.Rabbit_Error;
                            Logger.LogError(ex.ToString());
                        }
                    }
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Update Item on Entity
        /// </summary>
        /// <param name="dto">Data to update</param>
        /// <returns></returns>
        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, SecurityKitEnt>
                {
                    Items = new List<SecurityKitEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity =
                        (SecurityKitEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(SecurityKitEnt));

                    using (var scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bl = new CatSecurityKitsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            result = bl.Update(entity);
                            if (result)
                            {
                                scope.Complete();
                            }
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        try
                        {
                            SendMessage();
                        }
                        catch (Exception ex)
                        {
                            response.Message += Resources.Resource.Rabbit_Error;
                            Logger.LogError(ex.ToString());
                        }
                    }
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Delete one Item
        /// </summary>
        /// <param name="dto">Item if Entity</param>
        /// <returns></returns>
        [HttpPost("Delete")]
        public async Task<IActionResult> Delete([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, SecurityKitGridEnt>
                {
                    Items = new List<SecurityKitGridEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    bool result;
                    var entity =
                        (SecurityKitGridEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(SecurityKitGridEnt));

                    using (var bl =
                        new Bl.BitacoraAiEf.CatSecurityKitsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "id_security_kit", Value = entity.Id.ToString(), Operator = "Equals"}
                        };
                        result = bl.Update(filters, itemToSave =>
                        {
                            itemToSave.id_status = 0;
                            itemToSave.user_mod = User.Identity.Name;
                            itemToSave.mod_date = DateTime.Now;
                        });
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid name
        /// </summary>
        /// <param name="name">Name</param>
        /// <returns>true or false if name exist on db</returns>
        [HttpGet]
        [Route("ValidNameCreate/{name}")]
        public async Task<IActionResult> ValidNameCreate(string name)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, SecurityKitEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<SecurityKitEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl =
                        new Bl.BitacoraAiEf.CatSecurityKitsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var nameDec = Encoding.UTF8.GetString(Convert.FromBase64String(name));
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "name", Value = nameDec, Operator = "Equals"},
                            new Filter {Field = "id_status", Value = "1", Operator = "Equals"}
                        };
                        var item = bl.GetOne(filters);
                        if (item != null)
                        {
                            response.Success = true;
                            response.Message = Resources.Resource.Name_Already_Exists;
                        }
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid name
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="id">Id</param>
        /// <returns>true or false if name exist on db</returns>
        [HttpGet]
        [Route("ValidNameUpdate/{name}/{id}")]
        public async Task<IActionResult> ValidNameUpdate(string name, string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, SecurityKitEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<SecurityKitEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl =
                        new Bl.BitacoraAiEf.CatSecurityKitsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var nameDec = Encoding.UTF8.GetString(Convert.FromBase64String(name));
                        var idDec = Encoding.UTF8.GetString(Convert.FromBase64String(id));
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "name", Value = nameDec, Operator = "Equals"},
                            new Filter {Field = "id_security_kit", Value = idDec, Operator = "NotEquals"},
                            new Filter {Field = "id_status", Value = "1", Operator = "Equals"}
                        };
                        var item = bl.GetOne(filters);
                        if (item != null)
                        {
                            response.Success = true;
                            response.Message = Resources.Resource.Name_Already_Exists;
                        }
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get Item from Entity
        /// </summary>
        /// <param name="securityKitId">Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("OneCustom/{securityKitId}")]
        public async Task<IActionResult> OneCustom(string securityKitId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, SecurityKitEnt>
                {
                    Items = new List<SecurityKitEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.CatSecurityKitsBl(GetEfBitacoraAiConnection(), User.Identity.Name, GetAdoBitacoraAiConnection()))
                    {
                        var includes = new[]
                        {
                            "cat_security_kits_routes",
                            "cat_security_kits_georeferences",
                            "cat_customers_security_kits",
                            "cat_security_kits_control_points"
                        };
                        var securityKitIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(securityKitId)));
                        response.Item = bl.FirstCustom(securityKitIdDec, includes);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Clone Item from Entity
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("CloneSecurityKit/{id}")]
        public async Task<IActionResult> CloneSecurityKit(string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, SecurityKitEnt>
                {
                    Items = new List<SecurityKitEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var scope = new TransactionScope())
                    {
                        using (var bl = new CatSecurityKitsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            var securityKitIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(id)));
                            var result = bl.CloneSecurityKit(securityKitIdDec);
                            if (result)
                            {
                                scope.Complete();
                            }
                            response.Success = result;
                            response.Message = Resources.Resource.Successful_Operation;
                            return Ok(response);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        private void SendMessage()
        {
            using (var sender = new MqSender(Configuration, "Mq"))
            {
                //send assignment
                var message = "geofence";
                sender.Send(message, "reload.config");
            }
        }

    }
}
