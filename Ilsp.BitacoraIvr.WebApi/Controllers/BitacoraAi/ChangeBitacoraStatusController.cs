﻿using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using Ilsp.BitacoraIvr.WebApi.Mq;
using Newtonsoft.Json;
using Ilsp.Common.MessageContracts;
using FluentValidation.Results;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    [Route("api/[controller]")]
    [Authorize]
    public class ChangeBitacoraStatusController : BaseController
    {

        public ChangeBitacoraStatusController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor) : base(configuration, loggerFactory, accessor)
        {
        }

        [HttpGet]
        public async Task<IActionResult> Get(int op, string pr, string date_start, string date_end, string status, string alias)
        {
            return await Task.Run<IActionResult>(() =>
            {
            try
            {
                    using (var db = GetDbConnection())
                    {
                        switch (op)
                        {
                            case 1:
                                var active = db.sp_get_bitacoras_by_status('A').ToArray();
                                var prebit = db.sp_get_bitacoras_by_status('P').ToArray();
                                var r = active.Concat(prebit).ToArray();
                                return Ok(r);
                            case 2:
                                var res = db.sp_get_bitacoras_for_reactivation(alias).ToArray();
                                return Ok(res);
                            case 3:
                                return Ok(db.sp_get_bitacoras_client_origin_destination(pr).ToArray());
                            case 4:
                                return Ok(db.sp_get_bitacoras_report_status_history().ToArray());
                            case 5:
                                var dateStart = Convert.ToDateTime(date_start);
                                var dateEnd = Convert.ToDateTime(date_end);
                                if (!int.TryParse(pr, out var customer))
                                    customer = 0; // all customers
                                if (status == null)
                                    status = "[ac]"; // all operations
                                var result = db.sp_get_bitacoras_report_status_history_date(dateStart, dateEnd, status, customer).ToArray();
                                return Ok(result);
                            case 6:
                                return Ok(db.sp_get_customers_for_status_history().ToArray());
                            default:
                                return StatusCode(200, 1);
                        }
                    }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return StatusCode(500);
            }
            });
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ChangeBitacoraRequestDto request)
        {
            return await Task.Run<IActionResult>(() => 
            {
                try
                {
                    if (!ModelState.IsValid)
                    {
                        return BadRequest(ModelState);
                    }

                    var response = new ResponseAction<ValidationFailure, int>
                    {
                        Success = false
                    };

                    using (var db = GetDbConnection())
                    {
                        long.TryParse(request.id_bitacora, out _);
                        var result = 0;
                        if (request.status == "A" || request.status == "C")
                        {
                            result = db.sp_create_bitacoras_history_status(request.id_bitacora, 
                                                                           User.Identity.Name,
                                                                           request.status, request.reason);

                        }
                        if (result == 0)
                        {
                            response.Message = $"No se pudo cambiar el estatus a la Bitácora {request.id_bitacora}";
                            return Ok(response);
                        }
                        using (var sender = new MqSender(Configuration, "Mq"))
                        {
                            var message = JsonConvert.SerializeObject(new MonitoringMessage
                            {
                                Id = request.id_bitacora,
                                Status = request.status
                            });
                            sender.Send(message, "bitacora.status.update");
                        }
                        response.Success = true;
                        response.Message = "Operación exitosa";
                        return Ok(response);
                    }

                } catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }


}
}
