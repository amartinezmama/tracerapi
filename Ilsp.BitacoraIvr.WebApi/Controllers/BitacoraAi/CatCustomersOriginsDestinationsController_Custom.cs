﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.Tl.Enums;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatCustomersOriginsDestinationsController
    {

        /// <summary>
        /// Get list by Id
        /// </summary>
        /// <param name="customerId">Id of Item</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetOriginsByCustomer/{customerId}")]
        public async Task<IActionResult> GetOriginsByCustomer(string customerId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, OriginAndDestinyEnt>
                {
                    Items = new List<OriginAndDestinyEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatCustomersOriginsDestinationsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var idDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                        response.Items = bl.GetOriginsByCustomer(idDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get list by Id
        /// </summary>
        /// <param name="customerId">Id of Item</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDestinationsByCustomer/{customerId}")]
        public async Task<IActionResult> GetDestinationsByCustomer(string customerId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, OriginAndDestinyEnt>
                {
                    Items = new List<OriginAndDestinyEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatCustomersOriginsDestinationsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var idDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                        response.Items = bl.GetDestinationsByCustomer(idDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("AddOrigin")]
        public async Task<IActionResult> AddOrigin([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, OriginAndDestinyEnt>
                {
                    Items = new List<OriginAndDestinyEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity = (OriginAndDestinyEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(OriginAndDestinyEnt));

                    using (var scope = new TransactionScope())
                    {
                        using (var bl = new Bl.BitacoraAiEf.CatCustomersOriginsDestinationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var filters = new List<Filter>
                            {
                                new Filter {Field = "customer_id", Value = entity.CustomerId.ToString(), Operator = "Equals"},
                                new Filter {Field = "origin_destination_id", Value = entity.Id.ToString(), Operator = "Equals"},
                                new Filter {Field = "type", Value = "1", Operator = "Equals"}
                            };
                            var itemToSave = new cat_customers_origins_destinations
                            {
                                customer_id = entity.CustomerId,
                                origin_destination_id = entity.Id,
                                type = byte.Parse("1"),
                                create_date = DateTime.Now,
                                user_mod = User.Identity.Name
                            };
                            result = bl.SaveWithOutTransaction(itemToSave, filters);

                            if (result)
                            {
                                scope.Complete();
                            }
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("RemoveOrigin")]
        public async Task<IActionResult> RemoveOrigin([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, OriginAndDestinyEnt>
                {
                    Items = new List<OriginAndDestinyEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity = (OriginAndDestinyEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(OriginAndDestinyEnt));

                    using (var scope = new TransactionScope())
                    {
                        using (var bl = new Bl.BitacoraAiEf.CatCustomersOriginsDestinationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var filters = new List<Filter>
                            {
                                new Filter {Field = "customer_id", Value = entity.CustomerId.ToString(), Operator = "Equals"},
                                new Filter {Field = "origin_destination_id", Value = entity.Id.ToString(), Operator = "Equals"},
                                new Filter {Field = "type", Value = "1", Operator = "Equals"}
                            };

                            result = bl.DeleteWithOutTransaction(filters);

                            if (result)
                            {
                                scope.Complete();
                            }
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("AddDestiny")]
        public async Task<IActionResult> AddDestiny([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, OriginAndDestinyEnt>
                {
                    Items = new List<OriginAndDestinyEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity = (OriginAndDestinyEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(OriginAndDestinyEnt));

                    using (var scope = new TransactionScope())
                    {
                        using (var bl = new Bl.BitacoraAiEf.CatCustomersOriginsDestinationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var filters = new List<Filter>
                            {
                                new Filter {Field = "customer_id", Value = entity.CustomerId.ToString(), Operator = "Equals"},
                                new Filter {Field = "origin_destination_id", Value = entity.Id.ToString(), Operator = "Equals"},
                                new Filter {Field = "type", Value = "0", Operator = "Equals"}
                            };
                            var itemToSave = new cat_customers_origins_destinations
                            {
                                customer_id = entity.CustomerId,
                                origin_destination_id = entity.Id,
                                type = byte.Parse("0"),
                                create_date = DateTime.Now,
                                user_mod = User.Identity.Name
                            };
                            result = bl.SaveWithOutTransaction(itemToSave, filters);

                            if (result)
                            {
                                scope.Complete();
                            }
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("RemoveDestiny")]
        public async Task<IActionResult> RemoveDestiny([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, OriginAndDestinyEnt>
                {
                    Items = new List<OriginAndDestinyEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity = (OriginAndDestinyEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(OriginAndDestinyEnt));

                    using (var scope = new TransactionScope())
                    {
                        using (var bl = new Bl.BitacoraAiEf.CatCustomersOriginsDestinationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var filters = new List<Filter>
                            {
                                new Filter {Field = "customer_id", Value = entity.CustomerId.ToString(), Operator = "Equals"},
                                new Filter {Field = "origin_destination_id", Value = entity.Id.ToString(), Operator = "Equals"},
                                new Filter {Field = "type", Value = "0", Operator = "Equals"}
                            };

                            result = bl.DeleteWithOutTransaction(filters);

                            if (result)
                            {
                                scope.Complete();
                            }
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

    }
}
