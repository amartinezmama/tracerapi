﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using CatCustomerFieldsBl = Ilsp.BitacoraIvr.Bl.BitacoraAiEf.CatCustomerFieldsBl;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatCustomersTransportLineController
    {

        /// <summary>
        /// Get list by Id
        /// </summary>
        /// <param name="customerId">Id of Item</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTransportLinesByCustomer/{customerId}")]
        public async Task<IActionResult> GetTransportLinesByCustomer(string customerId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, TransportLineEnt>
                {
                    Items = new List<TransportLineEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatCustomersTransportLineBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var idDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                        response.Items = bl.GetTransportLinesByCustomer(idDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("Add")]
        public async Task<IActionResult> Add([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, TransportLineEnt>
                {
                    Items = new List<TransportLineEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity = (TransportLineEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(TransportLineEnt));

                    using (var scope = new TransactionScope())
                    {
                        using (var bl = new Bl.BitacoraAiEf.CatCustomersTransportLineBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var filters = new List<Filter>
                            {
                                new Filter {Field = "customer_id", Value = entity.CustomerId.ToString(), Operator = "Equals"},
                                new Filter {Field = "transport_line_id", Value = entity.Id, Operator = "Equals"}
                            };
                            var itemToSave = new cat_customers_transport_line
                            {
                                customer_id = entity.CustomerId,
                                transport_line_id = entity.Id,
                                create_date = DateTime.Now,
                                create_user = User.Identity.Name
                            };
                            result = bl.SaveWithOutTransaction(itemToSave, filters);

                            if (result)
                            {
                                scope.Complete();
                            }
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("Remove")]
        public async Task<IActionResult> Remove([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, TransportLineEnt>
                {
                    Items = new List<TransportLineEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity = (TransportLineEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(TransportLineEnt));

                    using (var scope = new TransactionScope())
                    {
                        using (var bl = new Bl.BitacoraAiEf.CatCustomersTransportLineBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var filters = new List<Filter>
                            {
                                new Filter {Field = "customer_id", Value = entity.CustomerId.ToString(), Operator = "Equals"},
                                new Filter {Field = "transport_line_id", Value = entity.Id, Operator = "Equals"}
                            };

                            result = bl.DeleteWithOutTransaction(filters);

                            if (result)
                            {
                                scope.Complete();
                            }
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

    }
}
