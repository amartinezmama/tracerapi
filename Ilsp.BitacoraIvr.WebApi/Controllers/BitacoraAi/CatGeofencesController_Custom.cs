﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Bl.PostgreDb;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.Ent.PostgreDb;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatGeofencesController
    {
        /// <summary>
        /// Get one Item
        /// </summary>
        /// <param name="id">Id of Item</param>
        /// <returns></returns>
        [HttpGet]
        [Route("FirstCustom/{id}")]
        public async Task<IActionResult> FirstCustom(string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, GeofenceEnt>
                {
                    Items = new List<GeofenceEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.CatGeofencesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var includes = new[] { "cat_geofences_zone_type", "cat_geofences_type" };
                        var idDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(id)));
                        response.Item = bl.FirstCustom(idDec, includes);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get Items from Entity
        /// </summary>
        /// <param name="model">Filters and Sorts</param>
        /// <returns></returns>
        [HttpPost("GetGridCustom")]
        public async Task<IActionResult> GetGridCustom([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    var appId = int.Parse(Configuration.GetValue<string>("ApplicationDb"));
                    model.Filter.Filters.Add(new Filter { Field = "applicationId", Value = appId.ToString(), Operator = "Equals" });
                    using (var bl = new Bl.BitacoraAiEf.VwCatGeofencesIvrBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var response = bl.GetGridCustom(model);
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, GeofenceEnt>
                {
                    Items = new List<GeofenceEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity =
                        (GeofenceEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(GeofenceEnt));
                    var appId = int.Parse(Configuration.GetValue<string>("ApplicationDb"));
                    using (var bl = new CatGeofencesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        result = bl.CreateCustom(entity, appId);
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    try
                    {
                        //Send Postgress
                        using (var pSql = new PostgreTransations<GeofencePostgre>(GetGeocodingPostgreConnection()))
                        {
                            var entityPost = new GeofencePostgre
                            {
                                idgeofence = entity.Id,
                                idzonetype = entity.ZoneType.Id,
                                idgeofencestype = entity.GeofenceType.Id,
                                name = entity.Name,
                                radio = entity.Radio,
                                updateuser = User.Identity.Name,
                                date = DateTime.Now,
                                geom = string.Join(",", entity.Points.Select(x => x.lng + " " + x.lat).ToList()),
                                action = 1
                            };
                            pSql.ExecNonQuery(entityPost, "insert_geofence");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex.ToString());
                        response.Message += Resources.Resource.Postgress_Error;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Update Item on Entity
        /// </summary>
        /// <param name="dto">Data to update</param>
        /// <returns></returns>
        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, GeofenceEnt>
                {
                    Items = new List<GeofenceEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity =
                        (GeofenceEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(GeofenceEnt));

                    using (var bl = new CatGeofencesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        result = bl.UpdateCustom(entity);
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    try
                    {
                        //Send Postgress
                        using (var pSql = new PostgreTransations<GeofencePostgre>(GetGeocodingPostgreConnection()))
                        {
                            var entityPost = new GeofencePostgre
                            {
                                idgeofence = entity.Id,
                                idzonetype = entity.ZoneType.Id,
                                idgeofencestype = entity.GeofenceType.Id,
                                name = entity.Name,
                                radio = entity.Radio,
                                updateuser = User.Identity.Name,
                                date = DateTime.Now,
                                geom = string.Join(",", entity.Points.Select(x => x.lng + " " + x.lat).ToList()),
                                action = 0
                            };
                            pSql.ExecNonQuery(entityPost, "insert_geofence");
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex.ToString());
                        response.Message += Resources.Resource.Postgress_Error;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Delete one Item
        /// </summary>
        /// <param name="dto">Item if Entity</param>
        /// <returns></returns>
        [HttpPost("Delete")]
        public async Task<IActionResult> Delete([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, GeofenceGridEnt>
                {
                    Items = new List<GeofenceGridEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    bool result;
                    var entity =
                        (GeofenceGridEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(GeofenceGridEnt));

                    using (var bl =
                        new Bl.BitacoraAiEf.CatGeofencesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "id_geofence", Value = entity.Id.ToString(), Operator = "Equals"}
                        };
                        result = bl.Update(filters, itemToSave =>
                        {
                            itemToSave.id_status = 0;
                            itemToSave.mod_user = User.Identity.Name;
                            itemToSave.mod_date = DateTime.Now;
                        });
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid name
        /// </summary>
        /// <param name="name">Name</param>
        /// <returns>true or false if name exist on db</returns>
        [HttpGet]
        [Route("ValidNameCreate/{name}")]
        public async Task<IActionResult> ValidNameCreate(string name)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, GeofenceEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<GeofenceEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl =
                        new Bl.BitacoraAiEf.CatGeofencesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var appId = int.Parse(Configuration.GetSection("ApplicationDb").Value);
                        var nameDec = Encoding.UTF8.GetString(Convert.FromBase64String(name));
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "name", Value = nameDec, Operator = "Equals"},
                            new Filter {Field = "applicationId", Value = appId.ToString(), Operator = "Equals"},
                            new Filter {Field = "id_status", Value = "1", Operator = "Equals"}
                        };
                        var item = bl.GetOne(filters);
                        if (item != null)
                        {
                            response.Success = true;
                            response.Message = Resources.Resource.Name_Already_Exists;
                        }
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid name
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="id">Id</param>
        /// <returns>true or false if name exist on db</returns>
        [HttpGet]
        [Route("ValidNameUpdate/{name}/{id}")]
        public async Task<IActionResult> ValidNameUpdate(string name, string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, GeofenceEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<GeofenceEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl =
                        new Bl.BitacoraAiEf.CatGeofencesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var appId = int.Parse(Configuration.GetSection("ApplicationDb").Value);
                        var nameDec = Encoding.UTF8.GetString(Convert.FromBase64String(name));
                        var idDec = Encoding.UTF8.GetString(Convert.FromBase64String(id));
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "name", Value = nameDec, Operator = "Equals"},
                            new Filter {Field = "applicationId", Value = appId.ToString(), Operator = "Equals"},
                            new Filter {Field = "id_geofence", Value = idDec, Operator = "NotEquals"},
                            new Filter {Field = "id_status", Value = "1", Operator = "Equals"}
                        };
                        var item = bl.GetOne(filters);
                        if (item != null)
                        {
                            response.Success = true;
                            response.Message = Resources.Resource.Name_Already_Exists;
                        }
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("SearchByCustom/{value}")]
        public async Task<IActionResult> SearchByCustom(string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, GeofenceEnt>
                {
                    Items = new List<GeofenceEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatGeofencesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var appId = int.Parse(Configuration.GetValue<string>("ApplicationDb"));
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.Search(appId, searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

    }
}
