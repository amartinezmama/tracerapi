﻿using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Newtonsoft.Json;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
	
	public partial class BitacoraEvidencesPhotosController : BaseController
	{
        [HttpGet]
        [Route("SearchByCustom/{property}/{value}")]
        public async Task<IActionResult> SearchBy_Custom(string property, string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraEvidencesPhotos>
                {
					Items = new List<BitacoraEvidencesPhotos>(),
					Success = false
                };
                try
                {
                    using (var bl = new BitacoraEvidencesPhotosBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var kendoRequest = new KendoRequest();
                        var filters = new List<Filter>();
                        var orders = new List<KendoSort>();
                        filters.Add(new Filter
                        {
							Field = property,
							Value = value,
							Operator = "Equals"
                        });
                        orders.Add(new KendoSort
                        {
							Field = property,
							Dir = "ASC"
                        });
                        kendoRequest.Filter = new KendoFilter
                        {
							Filters = filters,
							Logic = "Contains"
                        };
                        kendoRequest.Sort = orders;
                        kendoRequest.Page = 1;
                        response.Items = JsonConvert.DeserializeObject<IEnumerable<BitacoraEvidencesPhotos>>(
                            JsonConvert.SerializeObject(bl.GetListPager(kendoRequest, "AND").Data));
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        
	}
}
