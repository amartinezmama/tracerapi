﻿using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Mq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [Authorize]

    public class DynamicRoutesController : BaseController
    {
        public DynamicRoutesController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor) : base(configuration, loggerFactory, accessor)
        {
            Logger = loggerFactory.CreateLogger<MonitoringBitacorasController>();
        }

        [HttpGet("IsAuthorized/{phone}")]
        public async Task<ActionResult<string>> IsValidPhone(string phone)
        {
            try
            {
                if (string.IsNullOrEmpty(phone))
                {
                    ModelState.AddModelError("phone", "You must put a not empty phone");
                    return BadRequest(ModelState);
                }

                using (var bl = new DynamicRoutesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                {
                    var result = await bl.IsValidPhone(phone);

                    if (result != null)
                    {
                        return Ok(result.Phone);
                    }

                    return Forbid();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return StatusCode(500);
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateGeofence([FromBody] DynamicRouteEnt model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                using (var bl = new DynamicRoutesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                {
                    var appId = int.Parse(Configuration.GetValue<string>("ApplicationDb"));
                    var result = await bl.CreateRoute(model, appId);

                    // Send message to Mq            
                    using (var sender = new MqSender(Configuration, "BitacoraStatus"))
                    {
                        var message = JsonConvert.SerializeObject(new
                        {
                            Hub = "alert",
                            ClientMethod = "bitacoraHasNewRoute",
                            Groups = "allClients",
                            Data = model.BitacoraId
                        });
                        sender.Send(message, "notification.message");
                        Logger.LogInformation("Send message to SignalR with BitacoraId: " + model.BitacoraId);
                    }

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return StatusCode(500);
            }
        }

        [HttpPut("SetFirebaseToken/{phone}")]
        public async Task<ActionResult> UpdateFirebaeToken(string phone, [FromBody] UpdateFirebaseTokenEnt model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                using (var bl = new DynamicRoutesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                {
                    var result = await bl.SetFirebaseToken(phone, model.Token);

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return StatusCode(500);
            }
        }

        [HttpGet("GetAuthorizedStays/{bitacoraId}")]
        public async Task<ActionResult> GetAuthorizedStays(decimal bitacoraId)
        {
            using (var bl = new DynamicRoutesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
            {
                var result = await bl.GetAuthorizedGeofencesForRoute(bitacoraId);

                return Ok(result);
            }
        }

        [HttpGet("ValidateBitacoraRoute/{phone}")]
        public async Task<ActionResult> ValidateBitacoraRoute(string phone)
        {
            try
            {
                if (string.IsNullOrEmpty(phone))
                {
                    ModelState.AddModelError("phone", "You must put a not empty phone");
                    return BadRequest(ModelState);
                }

                using (var bl = new DynamicRoutesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                {
                    var result = await bl.IsValidPhone(phone);


                    using (var sender = new MqSender(Configuration, "Mq"))
                    {
                        if (result.BitacoraId != null)
                        {
                            sender.Send(result.BitacoraId.ToString(), "route.request");
                        }
                    }


                    return Ok(result.Phone);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return StatusCode(500);
            }
        }
    }
}