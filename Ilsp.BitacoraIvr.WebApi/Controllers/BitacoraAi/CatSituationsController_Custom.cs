﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatSituationsController
    {

        /// <summary>
        /// Get monitoring frequencies
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(string operativeKey)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, object>
                {
                    Items = new List<object>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatSituationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var items = bl.GetList(new List<Filter>(), new List<KendoSort>(), "AND").Select(x => new
                        {
                            id = x.id_situation,
                            x.description
                        }).OrderBy(x => x.description).ToList();
                        response.Items = items;
                        return Ok(response.Items);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

    }
}
