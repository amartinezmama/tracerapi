﻿using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class BitacorasStaysController
    {

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("CreateStay")]
        public async Task<IActionResult> CreateStay([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, StayEnt>
                {
                    Items = new List<StayEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                var liErrors = new List<ValidationFailure>();
                try
                {
                    bool result;
                    var entity = (StayEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(StayEnt));

                    BitacoraEnt bitacoraToEdit = null;
                    var customerId = "0";
                    var folioNumber = "0";
                    BitacorasStays bitacorastay = null;
                    using (var bl = new BitacorasBl(GetEfBitacoraAiConnection(), User.Identity.Name, GetAdoBitacoraAiConnection()))
                    {
                        bitacoraToEdit = bl.GetToEdit(long.Parse(entity.BitacoraId.ToString()));
                        customerId = bitacoraToEdit.Customer.Id.ToString();

                    }

                    //using (var bl = new Bl.BitacoraAiLq.BitacorasStaysBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    //{
                    //    folioNumber = bl.GetFolioByCustomer(customerId).ToString();
                    //}


                    using (var scope = new TransactionScope())
                    {

                        using (var bl = new BitacorasStaysBl(GetEfBitacoraAiConnection(), User.Identity.Name, GetAdoBitacoraAiConnection()))
                        {
                            var errors = new List<string>();
                            var filters = new List<Filter>
                        {
                            new Filter {Field = "Id", Value = "0", Operator = "Equals"}
                        };

                            bitacorastay = new BitacorasStays
                            {
                                BitacoraId = entity.BitacoraId,
                                TimeStay = entity.TimeStay,
                                PlaceStay = entity.PlaceStay,
                                AgreementId = entity.AgreementId,
                                CreateUser = User.Identity.Name,
                                CreateDate = DateTime.Now
                                    
                            };
                            result = bl.SaveWithOutTransaction(bitacorastay, filters);
                        }

                        using (var bl = new BitacorasActivationBl_Custom(GetEfBitacoraAiConnection(), User.Identity.Name, GetAdoBitacoraAiConnection()))
                        {

                            var errors = new List<string>();
                            var filters = new List<Filter>
                        {
                            new Filter {Field = "Id", Value = "0", Operator = "Equals"}
                        };


                            result = bl.SaveWithOutTransaction(new bitacoras_ax_activation
                            {
                                id_bitacora = decimal.Parse(bitacoraToEdit.Id),
                                ax_agreement_id = bitacoraToEdit.Agreement.Id,
                                ax_customer_id = bitacoraToEdit.BusinessCustomer.Id,
                                ax_proyect_id = bitacoraToEdit.Project.Id,
                                create_date = DateTime.Now,
                                custody_folio = "0",
                                update_user = User.Identity.Name,
                                status = "0",
                                ServiceTypeId = 2,
                                SentToBilling = false,
                                CancellationFolioStatusId = 1,
                                BitacorasStaysId = bitacorastay.Id,
                                ax_service_order = "En Proceso"

                            }, filters, errors);
                        }

                        response.Errors = liErrors;

                        if (result)
                        {
                            scope.Complete();
                            response.Success = true;
                            response.Message = Resources.Resource.Successful_Operation;
                        }
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }


        //[HttpPost("GetGridBitacorasForStays")]
        //public async Task<IActionResult> GetGridBitacoras([FromBody] KendoRequest model)
        //{
        //    return await Task.Run<IActionResult>(() =>
        //    {
        //        try
        //        {
        //            using (var bl = new VwBitacorasForStaysBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
        //            {
        //                return Ok(bl.GetGridBitacorasForStays(model));
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.LogError(ex.ToString());
        //            return StatusCode(500);
        //        }
        //    });
        //}

        //[HttpGet]
        //[Route("GetBitacoraForStay/{bitacoraId}")]
        //public async Task<IActionResult> GetBitacoraForStay(string bitacoraId)
        //{
        //    return await Task.Run<IActionResult>(() =>
        //    {
        //        var response = new ResponseAction<ValidationFailure, BitacoraStayEnt>
        //        {
        //            Errors = new List<ValidationFailure>(),
        //            Success = false
        //        };
        //        try
        //        {
        //            using (var bl = new VwBitacorasForStaysBl(GetEfBitacoraAiConnection(), User.Identity.Name))
        //            {
        //                var filters = new List<Filter>
        //                {
        //                    new Filter{Field = "BitacoraId", Value = Encoding.UTF8.GetString(Convert.FromBase64String(bitacoraId)), Operator = "Equals"}
        //                };
        //                response.Item = bl.GetOneBitacorasForStays(filters);
        //                response.Message = Resources.Resource.No_Data_Found;
        //                response.Success = false;
        //                return Ok(response);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            response.Message = Resources.Resource.Generic_Error;
        //            Logger.LogError(ex.ToString());
        //            return Ok(response);
        //        }
        //    });
        //}

        [HttpGet]
        [Route("GetStaysByBitacora/{bitacoraId}")]
        public async Task<IActionResult> GetStaysByBitacora(string bitacoraId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, StayEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false
                };
                try
                {
                    using (var bl = new BitacorasStaysBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter{Field = "BitacoraId", Value = Encoding.UTF8.GetString(Convert.FromBase64String(bitacoraId)), Operator = "Equals"}
                        };
                        response.Items = bl.GetStaysByBitacora(filters);
                        response.Message = Resources.Resource.No_Data_Found;
                        response.Success = false;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("DeleteStay")]
        public async Task<IActionResult> DeleteStay([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, StayEnt>
                {
                    Items = new List<StayEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                var liErrors = new List<ValidationFailure>();
                try
                {
                    bool result;
                    var entity = (StayEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(StayEnt));

                    using (var bl = new BitacorasStaysBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "Id", Value = entity.Id.ToString(), Operator = "Equals"}
                        };
                        result = bl.Delete(filters);
                    }

                    response.Errors = liErrors;

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }
    }
}