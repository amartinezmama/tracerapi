﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatCatalogsCommonController
    {

        [HttpGet]
        [Route("GetRelevances")]
        public async Task<IActionResult> GetRelevances()
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, RelevanceEnt>
                {
                    Items = new List<RelevanceEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatCatalogsCommonBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter{Field = "cat_type",Value = "RelevanceType",Operator = "Equals"}
                        };
                        response.Items = bl.GetList(filters, new List<KendoSort>(), "AND").Select(x => new RelevanceEnt
                        {
                            Id = x.cat_type_id,
                            Name = x.cat_type_name,
                            Description = x.cat_type_description,
                            Color = x.cat_color,
                            Type = x.cat_type
                        }).ToArray();
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

    }
}
