﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatMonitoringTypesController
    {

        [HttpGet]
        [Route("GetAllCustom")]
        public async Task<IActionResult> GetAllCustom()
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, MonitoringTypeEnt>
                {
                    Items = new List<MonitoringTypeEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatMonitoringTypesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Items = bl.All();
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("SearchByCustom/{value}")]
        public async Task<IActionResult> SearchByCustom(string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, MonitoringTypeEnt>
                {
                    Items = new List<MonitoringTypeEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatMonitoringTypesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.Search(searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get Item from Entity
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("FirstCustom/{id}")]
        public async Task<IActionResult> FirstCustom(string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, MonitoringTypeEnt>
                {
                    Items = new List<MonitoringTypeEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatMonitoringTypesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var idDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(id)));
                        response.Item = bl.First(idDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get Items from Entity
        /// </summary>
        /// <param name="model">Filters and Sorts</param>
        /// <returns></returns>
        [HttpPost("GetGridCustom")]
        public async Task<IActionResult> GetGridCustom([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.CatMonitoringTypesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var response = bl.GetGridCustom(model, new string[0]);
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, MonitoringTypeEnt>
                {
                    Items = new List<MonitoringTypeEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                var liErrors = new List<ValidationFailure>();
                try
                {
                    bool result;
                    var entity = (MonitoringTypeEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(MonitoringTypeEnt));

                    using (var bl = new Bl.BitacoraAiEf.CatMonitoringTypesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var errors = new List<string>();
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "description", Value = entity.Name, Operator = "Equals"},
                            new Filter {Field = "status_id", Value = "1", Operator = "Equals"}
                        };
                        if (bl.GetOne(filters) != null)
                        {
                            liErrors.Add(new ValidationFailure("Name", Resources.Resource.Name_Already_Exists));
                            result = false;
                        }
                        else
                        {
                            result = bl.Save(new cat_monitoring_types
                            {
                                description = entity.Name,
                                comments = entity.Name,
                                user_mod = User.Identity.Name,
                                create_date = DateTime.Now,
                                is_mirror_account = entity.IsMirrorAccount,
                                gpsrequired = entity.Gpsrequired,
                                gpsallow = entity.Gpsallow,
                                custodyrequired = entity.Custodyrequired,
                                custodiesallow = entity.Custodiesallow,
                                securitykitrequired = entity.Securitykitrequired,
                                securitykitallow = entity.Securitykitallow,
                                lockrequired = entity.Lockrequired,
                                trabapatinrequired = entity.Lockrequired,
                                is_custody_abandon = entity.IsCustodyAbandon,
                                is_loading_witness = entity.IsLoadingWitness,
                                status_id = 1
                            }, filters, errors);
                        }
                    }

                    response.Errors = liErrors;

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Update Item on Entity
        /// </summary>
        /// <param name="dto">Data to update</param>
        /// <returns></returns>
        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, MonitoringTypeEnt>
                {
                    Items = new List<MonitoringTypeEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity =
                        (MonitoringTypeEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(MonitoringTypeEnt));

                    using (var bl =
                        new Bl.BitacoraAiEf.CatMonitoringTypesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "id_monitoring_type", Value = entity.Id.ToString(), Operator = "Equals"}
                        };
                        result = bl.Update(filters, itemToSave =>
                        {
                            itemToSave.description = entity.Name;
                            itemToSave.comments = entity.Name;
                            itemToSave.user_mod = User.Identity.Name;
                            itemToSave.mod_date = DateTime.Now;
                            itemToSave.is_mirror_account = entity.IsMirrorAccount;
                            itemToSave.gpsrequired = entity.Gpsrequired;
                            itemToSave.gpsallow = entity.Gpsallow;
                            itemToSave.custodyrequired = entity.Custodyrequired;
                            itemToSave.custodiesallow = entity.Custodiesallow;
                            itemToSave.securitykitrequired = entity.Securitykitrequired;
                            itemToSave.securitykitallow = entity.Securitykitallow;
                            itemToSave.lockrequired = entity.Lockrequired;
                            itemToSave.trabapatinrequired = entity.Lockrequired;
                            itemToSave.is_custody_abandon = entity.IsCustodyAbandon;
                            itemToSave.is_loading_witness = entity.IsLoadingWitness;
                        });
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Delete one Item
        /// </summary>
        /// <param name="dto">Item if Entity</param>
        /// <returns></returns>
        [HttpPost("Delete")]
        public async Task<IActionResult> Delete([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, MonitoringTypeEnt>
                {
                    Items = new List<MonitoringTypeEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    bool result;
                    var entity =
                        (MonitoringTypeEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(MonitoringTypeEnt));

                    using (var bl =
                        new Bl.BitacoraAiEf.CatMonitoringTypesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "id_monitoring_type", Value = entity.Id.ToString(), Operator = "Equals"}
                        };
                        result = bl.Update(filters, itemToSave =>
                        {
                            itemToSave.status_id = 0;
                            itemToSave.user_mod = User.Identity.Name;
                            itemToSave.mod_date = DateTime.Now;
                        });
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid name
        /// </summary>
        /// <param name="name">Name</param>
        /// <returns>true or false if name exist on db</returns>
        [HttpGet]
        [Route("ValidNameCreate/{name}")]
        public async Task<IActionResult> ValidNameCreate(string name)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, MonitoringTypeEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<MonitoringTypeEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.CatMonitoringTypesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var nameDec = Encoding.UTF8.GetString(Convert.FromBase64String(name));
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "description", Value = nameDec, Operator = "Equals"},
                            new Filter {Field = "status_id", Value = "1", Operator = "Equals"}
                        };
                        var item = bl.GetOne(filters);
                        if (item != null)
                        {
                            response.Success = true;
                            response.Message = Resources.Resource.Name_Already_Exists;
                        }
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid name
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="id">Id</param>
        /// <returns>true or false if name exist on db</returns>
        [HttpGet]
        [Route("ValidNameUpdate/{name}/{id}")]
        public async Task<IActionResult> ValidNameUpdate(string name, string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, MonitoringTypeEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<MonitoringTypeEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.CatMonitoringTypesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var nameDec = Encoding.UTF8.GetString(Convert.FromBase64String(name));
                        var idDec = Encoding.UTF8.GetString(Convert.FromBase64String(id));
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "description", Value = nameDec, Operator = "Equals"},
                            new Filter {Field = "id_monitoring_type", Value = idDec, Operator = "NotEquals"},
                            new Filter {Field = "status_id", Value = "1", Operator = "Equals"}
                        };
                        var item = bl.GetOne(filters);
                        if (item != null)
                        {
                            response.Success = true;
                            response.Message = Resources.Resource.Name_Already_Exists;
                        }
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

    }
}
