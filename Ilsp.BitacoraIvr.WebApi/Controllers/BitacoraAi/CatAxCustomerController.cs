﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    [Route("api/[controller]")]
    [Authorize]
    public class CatAxCustomerController : BaseController
    {
        public CatAxCustomerController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor) : base(configuration, loggerFactory, accessor)
        {
            Logger = loggerFactory.CreateLogger<CatAxCustomerController>();
        }

        [HttpGet]
        [Route("SearchByCustom/{customerId}/{value}")]
        public async Task<IActionResult> SearchByCustom(string customerId, string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, AxCustomerEnt>
                {
                    Items = new List<AxCustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatAxCustomerBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var customerIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.Search(customerIdDec, searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("FirstCustom/{customerId}/{axCustomerId}")]
        public async Task<IActionResult> FirstCustom(string customerId, string axCustomerId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, AxCustomerEnt>
                {
                    Items = new List<AxCustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatAxCustomerBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var customerIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                        var axCustomerIdDec = Encoding.UTF8.GetString(Convert.FromBase64String(axCustomerId));
                        response.Item = bl.First(customerIdDec, axCustomerIdDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }
    }
}
