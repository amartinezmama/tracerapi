﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatBrandsController
    {

        [HttpGet]
        [Route("SearchByCustom/{customerId}/{value}")]
        public async Task<IActionResult> SearchByCustom(string customerId, string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BrandEnt>
                {
                    Items = new List<BrandEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatBrandsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var customerIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.Search(customerIdDec, searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("SearchOnlyByCustom/{value}")]
        public async Task<IActionResult> SearchOnlyByCustom(string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BrandEnt>
                {
                    Items = new List<BrandEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatBrandsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.SearchOnly(searchDec.Trim());
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("FirstCustom/{brandId}")]
        public async Task<IActionResult> FirstCustom(string brandId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BrandEnt>
                {
                    Items = new List<BrandEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatBrandsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var brandIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(brandId)));
                        response.Item = bl.First(brandIdDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

    }
}
