﻿using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Bl.Geocoder;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Ilsp.BitacoraIvr.WebApi.Mq;
using Ilsp.Common.MessageContracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.Common.Geocoding.BusinessEntities;
using Ilsp.Common.MessageContracts.Bitacora;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    [Route("api/[controller]")]
    [Authorize]
    public class MonitoringBitacorasController : BaseController
    {
        public MonitoringBitacorasController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor) : base(configuration, loggerFactory, accessor)
        {
            Logger = loggerFactory.CreateLogger<MonitoringBitacorasController>();
        }

        /// <summary>
        /// Get collection of bitacoras for monitoring
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        [Route("GetById/{bitacoraId}")]
        //[Route("{bitacoraId:decimal?}")]
        public async Task<IActionResult> GetById(string bitacoraId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraMessage>
                {
                    Items = new List<BitacoraMessage>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasMonitoringBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var bitacoraIdDec = Encoding.UTF8.GetString(Convert.FromBase64String(bitacoraId));
                        long? bitacoraIdDecR = string.IsNullOrEmpty(bitacoraIdDec.Trim()) ? (long?)null : long.Parse(bitacoraIdDec.Trim());
                        var role = User.Claims.FirstOrDefault(c => "roleid".Equals(c.Type.ToLower()));
                        var canSeeReactionClaim = User.Claims.FirstOrDefault(c => "showreactionitems".Equals(c.Type.ToLower()));
                        response.Success = true;
                        response.Items = bl.GetCustom(User.Identity.Name, role?.Value, canSeeReactionClaim != null, bitacoraIdDecR);
                        return Ok(response.Items);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get collection of associated bitacoras for monitoring
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        [Route("GetAssociatedById/{bitacoraId}")]
        //[Route("{bitacoraId:decimal?}")]
        public async Task<IActionResult> GetAssociatedById(string bitacoraId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraMessage>
                {
                    Items = new List<BitacoraMessage>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasMonitoringBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var bitacoraIdDec = Encoding.UTF8.GetString(Convert.FromBase64String(bitacoraId));
                        var bitacoraIdDecR = string.IsNullOrEmpty(bitacoraIdDec.Trim()) ? (decimal?)null : decimal.Parse(bitacoraIdDec.Trim());
                        response.Success = true;
                        response.Items = bl.GetAssociatedBitacoras(bitacoraIdDecR);
                        return Ok(response.Items);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get collection of bitacoras for monitoring
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        [Route("massive")]
        public async Task<IActionResult> GetMassive()
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, object>
                {
                    Items = new List<object>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasMonitoringBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var role = User.Claims.FirstOrDefault(c => "roleid".Equals(c.Type.ToLower()));
                        var canSeeReactionClaim = User.Claims.FirstOrDefault(c => "showreactionitems".Equals(c.Type.ToLower()));
                        response.Success = true;
                        response.Items = bl.Massive(User.Identity.Name, role?.Value, canSeeReactionClaim != null);
                        return Ok(response.Items);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get last locations of bitacora
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("lastlocations/{id}")]
        public async Task<IActionResult> GetLastLocations(string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, IGrouping<string, sp_get_last_locations_of_bitacora_Result>>
                {
                    Items = new List<IGrouping<string, sp_get_last_locations_of_bitacora_Result>>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasMonitoringBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Success = true;
                        response.Items = bl.LastLocations(long.Parse(id));
                        return Ok(response.Items);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get last locations of bitacora
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("routes/{id}")]
        public async Task<IActionResult> GetRoutes(string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, object>
                {
                    Items = new List<object>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatRoutesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var kit = bl.GetRoutesByBitacora(long.Parse(id));
                        response.Success = true;
                        response.Items = kit.ToArray();
                        return Ok(response.Items);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get last locations of bitacora
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("globalZones")]
        [ResponseCache(Duration = 604800, Location = ResponseCacheLocation.Client)]
        public async Task<IActionResult> GetGlobalZones()
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, object>
                {
                    Items = new List<object>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatGeofencesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Items = bl.GetGeofencesWithZone();
                        response.Success = true;
                        return Ok(response.Items);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get last locations of bitacora
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("lastlocation/{id}")]
        public async Task<IActionResult> GetLastLocation(string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, object>
                {
                    Items = new List<object>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasMonitoringBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Success = true;
                        response.Item = bl.LastLocation(long.Parse(id));
                        return Ok(response.Item);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get last locations of bitacoras gps
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("lastlocationofbitacorasgps/{id}")]
        public async Task<IActionResult> GetLastLocationOfBitacorasGps(decimal? id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, object>
                {
                    Items = new List<object>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasMonitoringBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Success = true;
                        response.Item = new
                        {
                            gps = bl.LastLocationBitacoraGps(id == null ? (long?)null : long.Parse(id.ToString())),
                            travel = bl.LastLocations(id == null ? (long?)null : long.Parse(id.ToString()))
                        };
                        return Ok(response.Item);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get locations report
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("locationsreport/{id}")]
        public async Task<IActionResult> GetLocationReportOfBitacora(decimal? id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, object>
                {
                    Items = new List<object>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasMonitoringBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Success = true;
                        response.Item = bl.LastLocationBitacoraReport(id == null ? (long?)null : long.Parse(id.ToString()));
                        return Ok(response.Item);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get last locations of bitacora
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("datasheet/{id}")]
        public async Task<IActionResult> GetDatasheet(string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, object>
                {
                    Items = new List<object>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasMonitoringBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Success = true;
                        response.Item = bl.LastLocations(long.Parse(id)).FirstOrDefault();
                        return Ok(response.Item);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpPost]
        public async Task<IActionResult> CreateMonitoring([FromBody] dynamic data)
        {
            return await Task.Run<IActionResult>(async () =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraMonitoringEnt>
                {
                    Items = new List<BitacoraMonitoringEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    MonitoringModel model = JsonConvert.DeserializeObject<MonitoringModel>(data.ToString());
                    if (!TryValidateModel(model))
                    {
                        return BadRequest(ModelState);
                    }
                    if (!ModelState.IsValid)
                    {
                        return BadRequest(ModelState);
                    }
                    double? latitude = null;
                    double? longitude = null;
                    // Set lat & lng
                    if (!string.IsNullOrEmpty(model.Latitude))
                    {
                        latitude = double.Parse(model.Latitude);
                    }
                    if (!string.IsNullOrEmpty(model.Longitude))
                    {
                        longitude = double.Parse(model.Longitude);
                    }
                    // Validate LineGps
                    if (!string.IsNullOrEmpty(model.LineGps))
                    {
                        var splitData = model.LineGps.Split(',');
                        latitude = double.Parse(splitData[0]);
                        longitude = double.Parse(splitData[1]);
                        int lineGpsTimes;
                        using (var bl = new BitacorasInspectionsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            lineGpsTimes = bl.ValidateThreeGpsLineLocation(long.Parse(model.BitacoraId), latitude, longitude);
                        }
                        if (lineGpsTimes == 1)
                        {
                            ModelState.AddModelError("LineGps", Resources.Resource.LineGpsMoreThanThreeTimes);
                            return BadRequest(ModelState);
                        }
                    }
                    var opKeyAlerts = new XElement("OpKeyAlerts");
                    if (model.OpKeyAlerts != null && model.OpKeyAlerts.Count > 0)
                    {
                        model.OpKeyAlerts.ForEach(a =>
                        {
                            opKeyAlerts.Add(new XElement("opKeyAlert", new XElement("alertId", a.AlertId), new XElement("opKeyId", a.OpKeyAlert.Id)));
                        });
                    }

                    if (!ReverseGeocodingBI.Instance.IsInitialized)
                    {
                        ReverseGeocodingBI.Instance.Initialize(Configuration.GetConnectionString("GeocodingConnection"));
                    }

                    var address = new Address();

                    if (latitude.HasValue && longitude.HasValue)
                    {
                        address = await ReverseGeocodingBI.Instance.GetAddress(latitude, longitude);
                    }

                    BitacoraMonitoringEnt result;
                    using (var bl = new BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        result = bl.CreateMonitoring(model, latitude, longitude, address, opKeyAlerts);
                    }
                    if (result != null)
                    {
                        var claim = User.FindFirst(c => "AgentId".Equals(c.Type));
                        int.TryParse(claim?.Value, out var agentId);
                        using (var sender = new MqSender(Configuration, "Mq"))
                        {
                            var message = JsonConvert.SerializeObject(new MonitoringMessage
                            {
                                Id = model.BitacoraId,
                                Status = result.status,
                                StatusDetail = (short)result.statusDetail,
                                LastPosition = new Location
                                {
                                    Latitude = latitude,
                                    Longitude = longitude
                                },
                                NextMonitoring = result.nextMonitoring ?? 10,
                                OperativeKey = model.Status.Id,
                                OpKeyAlerts =
                                    model.OpKeyAlerts?
                                        .GroupBy(o => o.OpKeyAlert.Id)
                                        .ToDictionary(o => o.Key, o => o.Key),
                                User = User.Identity.Name,
                                AgentId = agentId == 0 ? (int?)null : agentId,
                                MonitoringFrequencyId = model.MonitoringFrequency,
                                RequiredCallByFrequency = model.RequiredCallByFrequency
                            });
                            sender.Send(message, "monitoring");
                        }

                        response.Success = true;
                        response.Item = result;
                        return Ok(result);
                    }

                    response.Message = "Can't create monitoring";
                    return StatusCode(500, "Can't create monitoring");
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = "Can't create monitoring";
                    return StatusCode(500, "Can't create monitoring");
                }
            });
        }

        [HttpPost]
        [Route("closebitacora")]
        public async Task<IActionResult> CloseBitacoraMonitoring([FromBody] MonitoringModel model)
        {
            return await Task.Run<IActionResult>(async () =>
            {
                var response = new ResponseAction<ValidationFailure, sp_create_bitacora_monitoring_Result>
                {
                    Items = new List<sp_create_bitacora_monitoring_Result>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    // Set lat & lng
                    double? latitude = null;
                    double? longitude = null;
                    // Set lat & lng
                    if (!string.IsNullOrEmpty(model.Latitude))
                    {
                        latitude = double.Parse(model.Latitude);
                    }
                    if (!string.IsNullOrEmpty(model.Longitude))
                    {
                        longitude = double.Parse(model.Longitude);
                    }

                    if (!ReverseGeocodingBI.Instance.IsInitialized)
                    {
                        ReverseGeocodingBI.Instance.Initialize(Configuration.GetConnectionString("GeocodingConnection"));
                    }
                    var address = await ReverseGeocodingBI.Instance.GetAddress(latitude, longitude);
                    sp_create_bitacora_monitoring_Result result;
                    using (var bl = new BitacorasMonitoringBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        result = bl.CreateMonitoring(model, latitude, longitude, address);
                    }
                    if (result != null)
                    {

                        using (var sender = new MqSender(Configuration, "Mq"))
                        {
                            var message = JsonConvert.SerializeObject(new MonitoringMessage
                            {
                                Id = model.BitacoraId,
                                Status = result.status,
                                StatusDetail = result.statusDetail,
                                LastPosition = new Location
                                {
                                    Latitude = latitude,
                                    Longitude = longitude
                                },
                                NextMonitoring = result.nextMonitoring ?? 10,
                                OperativeKey = result.status == "Z" ? 25 : model.Status.Id,
                                OpKeyAlerts = new Dictionary<int, int>()
                            });
                            sender.Send(message, "monitoring");
                        }

                        response.Success = true;
                        response.Item = result;
                        return Ok(result);
                    }

                    response.Message = "Can't create monitoring";
                    return StatusCode(500, "Can't create monitoring");
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }


        /// <summary>
        /// Sends a bitacora to reaction
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("onreaction")]
        public async Task<IActionResult> OnReaction([FromBody] BitacoraReactionViewModel model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, int>
                {
                    Items = new List<int>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        using (var sender = new MqSender(Configuration, "Mq"))
                        {

                            var reactionModel = bl.GetReactionEvent(model.Id);
                            reactionModel.Comments = model.Comments;
                            reactionModel.User = User.Identity.Name;
                            var message = JsonConvert.SerializeObject(reactionModel);
                            sender.Send(message, "reaction");
                        }

                        var result = bl.UpdateBitacoraStatusDetail(long.Parse(model.Id), 3, "reaction");
                        if (result > 0)
                        {
                            response.Item = result;
                            response.Success = true;
                            return Ok(result);
                        }

                        response.Message = "Bitacora can't update";
                        return StatusCode(500, "Bitacora can't update");
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpPost]
        [Route("validatelinegps/{id}")]
        public async Task<IActionResult> ValidateLineGps(string id, [FromBody] Location location)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, int>
                {
                    Items = new List<int>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    if (location == null)
                    {
                        ModelState.AddModelError("location", "Argument Null");
                        return BadRequest(ModelState);
                    }

                    using (var bl = new BitacorasInspectionsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var result = bl.ValidateThreeGpsLineLocation(long.Parse(id), location.Latitude, location.Longitude);
                        response.Success = result != 1;
                        return Ok(result != 1);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get collection of bitacoras by Convoy
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        [Route("ConvoyLocations/{bitacoraId}")]
        public async Task<IActionResult> ConvoyLocations(string bitacoraId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraConvoyEnt>
                {
                    Items = new List<BitacoraConvoyEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasMonitoringBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var bitacoraIdDec = Encoding.UTF8.GetString(Convert.FromBase64String(bitacoraId));
                        long bitacoraIdDecR = long.Parse(bitacoraIdDec.Trim());
                        response.Items = bl.GetBitacorasOnConvoy(bitacoraIdDecR);
                        response.Success = true;
                        return Ok(response.Items);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpPost]
        [Route("call/{id}/{opKey}")]
        public async Task<IActionResult> Call(string id, int opKey)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, int>
                {
                    Items = new List<int>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var claim = User.FindFirst(c => "AgentId".Equals(c.Type));
                    int.TryParse(claim?.Value, out var agentId);
                    using (var sender = new MqSender(Configuration, "Mq"))
                    {
                        var message = JsonConvert.SerializeObject(new MakeBitacoraCall
                        {
                            Id = id,
                            AgentId = agentId == 0 ? (int?)null : agentId,
                            OpKey = opKey
                        });
                        sender.Send(message, "make.call");
                    }
                    response.Item = 1;
                    response.Success = true;
                    return Ok(response.Item);
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get bitacoras ready for starting road
        /// </summary>        
        /// <returns></returns>
        [HttpGet]
        [Route("GetReadyForStartingRoad")]
        public async Task<IActionResult> GetReadyForStartingRoad()
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, string>
                {
                    Items = new List<string>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasMonitoringBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var role = User.Claims.FirstOrDefault(c => "roleid".Equals(c.Type.ToLower()));
                        response.Items = bl.GetBitacorasReadyForStartingRoad(User.Identity.Name, role?.Value, null);
                        response.Success = true;
                        return Ok(response.Items);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpPost]
        [Route("SendAlexaNotification/{bitacoraId}")]
        public async Task<IActionResult> SendAlexaNotification(string bitacoraId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, bool>
                {
                    Items = new List<bool>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasMonitoringBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var alexaNotification = bl.GetAlexaNotificationByProcess(bitacoraId, 3);
                        var echoNames = alexaNotification.EchoDevices
                            .Split(',')
                            .Where(e => !string.IsNullOrEmpty(e))
                            .ToArray();
                        if (!echoNames.Any())
                        {
                            Logger.LogDebug($"There aren't echo devices for bitacora {bitacoraId}");
                        }
                        using (var sender = new MqSender(Configuration, "MqAlexa"))
                        {
                            sender.Send(JsonConvert.SerializeObject(new
                            {
                                message = alexaNotification.Message,
                                duration = alexaNotification.Duration,
                                times = alexaNotification.Times,
                                echoNames
                            }));
                        }
                        response.Message = Resources.Resource.Successful_Operation;
                        response.Success = response.Item = true;
                        return Ok(response.Item);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }
    }
}
