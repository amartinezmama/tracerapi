﻿using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{

    public partial class CatRegExpressionsController
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CatRegExpressions>
                {
                    Items = new List<CatRegExpressions>(),
                    Success = false
                };
                try
                {
                    using (var bl = new CatRegExpressionsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Items = bl.GetAll();
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }


    }
}
