﻿using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Ilsp.BitacoraIvr.WebApi.Mq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    [Route("api/[controller]")]
    [Authorize]
    public class TurnOffAlertsController: BaseController
    {
        public TurnOffAlertsController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor) : base(configuration, loggerFactory, accessor)
        {
            Logger = loggerFactory.CreateLogger<TurnOffAlertsController>();
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return await Task.Run<IActionResult>(()=> 
            {
                try
                {
                    var response = new ResponseAction<ValidationFailure, TurnOffAlertsModel>
                    {
                        Success = false,
                    };

                    using (var db = GetDbConnection())
                    {
                        using (var bl = new TurnOffAlertsBl(db))
                        {
                            response.Items = bl.GetBitacorasAlerts(); 
                            response.Success = true;
                        }
                        return Ok(response);
                    }

                } catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpGet("getBitacoras")]
        public async Task<IActionResult> GetBitacora()
        {
            return await Task.Run<IActionResult>(() => 
            {
                var response = new ResponseAction<ValidationFailure, AlertsBitacora>
                {
                    Success = false,
                };

                try
                {
                    using (var db = GetDbConnection())
                    {
                        using (var bl = new TurnOffAlertsBl(db))
                        {
                            response.Items = bl.Bitacoras();
                            response.Success = true;
                            return Ok(response);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                };
            });
        }

        [HttpPost("gpsForBitacora")]
        public async Task<IActionResult> GpsForBitacora([FromBody]TurnOffAlertsRequestDto request)
        {
            return await Task.Run<IActionResult>(() => 
            {
                var response = new ResponseAction<ValidationFailure, GetGpsBitacoraModel>
                {
                    Success = false,
                };

                try
                {
                    if (!ModelState.IsValid)
                    {
                        response.Message = "El campo idBitacora es requerido";
                        return Ok(response);
                    }
                    
                    using (var db = GetDbConnection())
                    {
                        using (var bl = new TurnOffAlertsBl(db))
                        {
                            response.Items = bl.GetGpsBitacoras(request.idBitacora);
                            response.Success = true;
                            return Ok(response);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpGet("getAlertsBitacora")]
        public async Task<IActionResult> GetAlertsBitacor()
        {
            return await Task.Run<IActionResult>(() => 
            {
                var response = new ResponseAction<ValidationFailure,GetAlertsModel>
                {
                    Success = false,
                };
                try
                {
                    using (var db = GetDbConnection())
                    {
                        using (var bl = new TurnOffAlertsBl(db))
                        {
                            response.Items = bl.GetAlertsBitacora();
                            response.Success = true;
                            return Ok(response);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpPost("saveAlert")]
        public async Task<IActionResult> SaveAlert([FromBody]TurnOffAlert request)
        {
            return await Task.Run<IActionResult>(() => 
            {
                var response = new ResponseAction<ValidationFailure, string>
                {
                    Success = false,
                };

                try
                {
                    if (!ModelState.IsValid)
                    {
                        return BadRequest(ModelState);
                    }
                    using (var db = GetDbConnection())
                    {
                        long.TryParse(request.id_bitacora, out _);
                        var alertsBeforeInsert = db.sp_get_bitacoras_alerts_status_for_mq(request.id_bitacora).ToArray();

                        var result = db.sp_create_bitacora_alert_status(request.id_bitacora,
                                                                        request.id_gps,
                                                                        request.name_alert,
                                                                        request.minutes_disabled,
                                                                        request.observations,
                                                                        User.Identity.Name);
                        if (result == 0)
                        {
                            Logger.LogError(Resources.Resource.Generic_Error);
                            response.Message = Resources.Resource.Generic_Error;
                            return Ok(response);
                        }
                        if (result == 2)
                        {
                            Logger.LogError("Alert already disabled ");
                            response.Message = Resources.Resource.Generic_Error;
                            return Ok(response);
                        }

                        var alertsAfterInsert = db.sp_get_bitacoras_alerts_status_for_mq(request.id_bitacora).ToArray();

                        var alertForMq = (from alertAfter in alertsAfterInsert
                                          where !(from alertBefore in alertsBeforeInsert
                                                  select alertBefore.AlertStatusId).Contains(alertAfter.AlertStatusId)
                                          select new
                                          {
                                              Id = alertAfter.AlertStatusId,
                                              alertAfter.AlertId,
                                              alertAfter.BitacoraId,
                                              alertAfter.GpsId,
                                              alertAfter.Minutes,
                                              alertAfter.Status
                                          }
                        ).ToArray();

                        using (var sender = new MqSender(Configuration, "Mq"))
                        {
                            foreach (var alert in alertForMq)
                            {
                                var message = JsonConvert.SerializeObject(new DisabledAlertModel
                                {
                                    Id = alert.Id,
                                    AlertId = alert.AlertId,
                                    BitacoraId = alert.BitacoraId,
                                    GpsId = alert.GpsId,
                                    Minutes = alert.Minutes,
                                    Status = alert.Status.ToString(),
                                    Timer = null
                                });
                                sender.Send(message, "alert_disabled");
                            }
                        }

                        response.Message = "Operación exitosa";
                        response.Success = true;
                        return Ok(response);
                    }
                } catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpPost("deleteAlert")]
        public async Task<IActionResult> DeleteAlert([FromBody]TurnOffAlert request)
        {
            return await Task.Run<IActionResult>(() => 
            {
                var response = new ResponseAction<ValidationFailure, string>
                {
                    Success = false,
                };

                try
                {
                    if (!ModelState.IsValid)
                    {
                        string msg = string.Join(", ", ModelState.Values
                                           .SelectMany(v => v.Errors)
                                           .Select(x => x.ErrorMessage));
                        response.Message = msg;
                        return Ok(response);
                    }
                    using (var db = GetDbConnection())
                    {
                        var result = db.sp_delete_bitacora_alert_status(request.id, User.Identity.Name);

                        if (result == 0)
                        {
                            Logger.LogError(Resources.Resource.Generic_Error);
                            response.Message = Resources.Resource.Generic_Error;
                            return Ok(response);
                        }
                        using (var sender = new MqSender(Configuration, "Mq"))
                        {
                            var message = JsonConvert.SerializeObject(new DisabledAlertModel
                            {
                                AlertId = request.id_Alert,
                                BitacoraId = request.id_bitacora,
                                GpsId = request.id_gps,
                                Minutes = request.minutes_disabled,
                                Status = "2",
                                Timer = null
                            });
                            sender.Send(message, "alert_disabled");
                        }
                        response.Success = true;
                        response.Message = "Operación exitosa";
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }
    }
}
