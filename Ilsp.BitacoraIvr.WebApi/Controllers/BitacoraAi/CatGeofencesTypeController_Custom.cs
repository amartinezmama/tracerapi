﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatGeofencesTypeController
    {
        [HttpGet]
        [Route("GetListCustom")]
        public async Task<IActionResult> GetListCustom()
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, GeofenceTypeEnt>
                {
                    Items = new List<GeofenceTypeEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl = new CatGeofencesTypeBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Items = bl.GetList(new List<Filter>(), new List<KendoSort>(), "AND").Select(x => new GeofenceTypeEnt
                        {
                            Id = x.id,
                            Name = x.name
                        }).ToList();
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }
    }
}
