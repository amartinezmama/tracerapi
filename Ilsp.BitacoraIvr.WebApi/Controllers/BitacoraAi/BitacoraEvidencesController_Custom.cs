﻿using FluentValidation.Results;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Ilsp.BitacoraIvr.WebApi.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class BitacoraEvidencesController
    {
        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("CreateData")]
        public async Task<IActionResult> CreateData([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result = true;
                    var entity = (CustomerEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(CustomerEnt));

                    using (var scope = new TransactionScope())
                    {
                        using (var bl = new Ilsp.BitacoraIvr.Bl.BitacoraAiEf.BitacoraEvidencesBl(
                            GetEfBitacoraAiConnection(), User.Identity.Name, GetAdoBitacoraAiConnection()))
                        {
                            foreach (var field in entity.EvidenceFields)
                            {
                                var filters = new List<Filter>
                                {
                                    new Filter
                                    {
                                        Field = "CustomerId", Value = entity.Id.ToString(), Operator = "Equals"
                                    },
                                    new Filter
                                    {
                                        Field = "BitacoraId", Value = entity.BitacoraId.ToString(), Operator = "Equals"
                                    },
                                    new Filter
                                    {
                                        Field = "FieldId", Value = field.FieldId.ToString(), Operator = "Equals"
                                    }

                                };
                                var itemToSave = new BitacoraEvidences
                                {
                                    CustomerId = entity.Id,
                                    FieldId = field.FieldId ?? 0,
                                    BitacoraId = entity.BitacoraId,
                                    Value = field.Value,
                                    CreateUser = User.Identity.Name,
                                    CreateDate = DateTime.Now
                                };
                                if (!bl.SaveWithOutTransaction(itemToSave, filters))
                                {
                                    result = false;
                                    break;
                                }
                            }

                        }


                        if (result)
                        {
                            scope.Complete();
                            response.Success = true;
                            response.Message = Resources.Resource.Successful_Operation;
                        }


                        #region SendToMqBitacoraStatus

                        var senderMQ = new SenderMqBitacora(Configuration);
                        senderMQ.SendToMqBitacoraStatus(entity.BitacoraId.ToString(), User.Identity.Name);

                        #endregion

                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Update Item on Entity
        /// </summary>
        /// <param name="dto">Data to update</param>
        /// <returns></returns>
        [HttpPost("UpdateData")]
        public async Task<IActionResult> UpdateData([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    var result = true;
                    var entity = (CustomerEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(CustomerEnt));
                    List<BitacoraEvidences> liBefore;
                    using (var bl = new Ilsp.BitacoraIvr.Bl.BitacoraAiEf.BitacoraEvidencesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "CustomerId", Value = entity.Id.ToString(), Operator = "Equals"},
                            new Filter {Field = "BitacoraId", Value = entity.BitacoraId.ToString(), Operator = "Equals"}
                        };
                        liBefore = bl.GetList(filters, new List<KendoSort>(), "AND").ToList();
                    }

                    using (var scope = new TransactionScope())
                    {
                        using (var bl = new Ilsp.BitacoraIvr.Bl.BitacoraAiEf.BitacoraEvidencesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var itemsToDelete = liBefore.Where(b => !entity.EvidenceFields.Any(a => a.FieldId.Equals(b.FieldId))).ToList();
                            var itemsToInsert = entity.EvidenceFields.Where(a => !liBefore.Any(b => b.FieldId.Equals(a.FieldId))).ToList();

                            var itemsToUpdate = liBefore.Where(b => entity.EvidenceFields.Any(a => a.FieldId.Equals(b.FieldId) && (a.Value != null) && (!a.Value.Equals(b.Value)))).ToList();

                            if (itemsToDelete.Any())
                            {

                                foreach (var field in itemsToDelete)
                                {
                                    var filters = new List<Filter>
                                    {
                                        new Filter
                                        {
                                            Field = "CustomerId", Value = entity.Id.ToString(), Operator = "Equals"
                                        },
                                        new Filter
                                        {
                                            Field = "FieldId", Value = field.FieldId.ToString(), Operator = "Equals"
                                        },
                                        new Filter
                                        {
                                            Field = "BitacoraId", Value = entity.BitacoraId.ToString(),
                                            Operator = "Equals"
                                        }
                                    };
                                    if (!bl.DeleteWithOutTransaction(filters))
                                    {
                                        result = false;
                                        break;
                                    }
                                }
                            }

                            if (itemsToInsert.Any())
                            {
                                foreach (var field in itemsToInsert)
                                {
                                    var filters = new List<Filter>
                                    {
                                        new Filter
                                        {
                                            Field = "CustomerId", Value = entity.Id.ToString(), Operator = "Equals"
                                        },
                                        new Filter
                                        {
                                            Field = "FieldId", Value = field.FieldId.ToString(), Operator = "Equals"
                                        },
                                        new Filter
                                        {
                                            Field = "BitacoraId", Value = entity.BitacoraId.ToString(),
                                            Operator = "Equals"
                                        }
                                    };
                                    var itemToSave = new BitacoraEvidences
                                    {
                                        CustomerId = entity.Id,
                                        FieldId = field.FieldId ?? 0,
                                        BitacoraId = entity.BitacoraId,
                                        Value = field.Value,
                                        CreateUser = User.Identity.Name,
                                        CreateDate = DateTime.Now
                                    };
                                    if (!bl.SaveWithOutTransaction(itemToSave, filters))
                                    {
                                        result = false;
                                        break;
                                    }
                                }
                            }

                            if (itemsToUpdate.Any())
                            {
                                foreach (var field in itemsToUpdate)
                                {
                                    var filters = new List<Filter>
                                    {
                                        new Filter
                                        {
                                            Field = "CustomerId", Value = entity.Id.ToString(), Operator = "Equals"
                                        },
                                        new Filter
                                        {
                                            Field = "FieldId", Value = field.FieldId.ToString(), Operator = "Equals"
                                        },
                                        new Filter
                                        {
                                            Field = "BitacoraId", Value = entity.BitacoraId.ToString(),
                                            Operator = "Equals"
                                        }
                                    };
                                    if (!bl.UpdateWithOutTransaction(filters, itemToUpdate =>
                                    {
                                        itemToUpdate.Value = entity.EvidenceFields
                                            .First(b => b.FieldId.Equals(field.FieldId)).Value;
                                        itemToUpdate.UpdateDate = DateTime.Now;
                                        itemToUpdate.UpdateUser = User.Identity.Name;

                                    }))
                                    {
                                        result = false;
                                        break;
                                    }
                                }
                            }


                            if (result)
                            {
                                scope.Complete();
                            }
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    #region SendToMqBitacoraStatus

                    var senderMQ = new SenderMqBitacora(Configuration);
                    senderMQ.SendToMqBitacoraStatus(entity.BitacoraId.ToString(), User.Identity.Name);

                    #endregion

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("CreateDataPhoto")]
        public async Task<IActionResult> CreateDataPhoto([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result = true;
                    var entity = (CustomerEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(CustomerEnt));
                    var directoryPath = Path.Combine(Configuration.GetSection("BitacoraFoliosPhotoDirectory").Value, entity.BitacoraId.ToString());
                    var servicePath = $"{Configuration.GetSection("BitacoraFoliosPhotoDirectoryService").Value}/{entity.BitacoraId.ToString()}";

                    using (var scope = new TransactionScope())
                    {
                        using (var bl = new Ilsp.BitacoraIvr.Bl.BitacoraAiEf.BitacoraEvidencesPhotosBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            foreach (var field in entity.Photos)
                            {
                                field.Name = Guid.NewGuid().ToString();
                                var format = (field.Photo.Split(',')[0].Substring(5)).Split(';')[0];

                                var filters = new List<Filter>
                                    {
                                        new Filter {Field = "Id", Value = "0", Operator = "Equals"}
                                    };
                                var itemToSave = new BitacoraEvidencesPhotos
                                {
                                    CustomerId = entity.Id,
                                    BitacoraId = entity.BitacoraId,
                                    Photo = field.Photo,
                                    PhotoFilePath = $"{servicePath}/{field.Name}.{format.Split('/')[1]}",
                                    CreateUser = User.Identity.Name,
                                    CreateDate = DateTime.Now
                                };
                                if (!bl.SaveWithOutTransaction(itemToSave, filters))
                                {
                                    result = false;
                                    break;
                                }
                            }


                            if (result)
                            {
                                scope.Complete();
                            }
                        }
                    }
                    if (result)
                    {
                        try
                        {
                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(directoryPath);
                            }

                            foreach (var file in entity.Photos)
                            {
                                foreach (var field in entity.Photos)
                                {
                                    var imageB64 = field.Photo.Split(',')[1];
                                    var format = (field.Photo.Split(',')[0].Substring(5)).Split(';')[0];

                                    System.IO.File.WriteAllBytes(Path.Combine(directoryPath, field.Name + "." + format.Split('/')[1])
                                        , Convert.FromBase64String(imageB64));
                                }

                            }

                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());

                        }

                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    #region SendToMqBitacoraStatus

                    var senderMQ = new SenderMqBitacora(Configuration);
                    senderMQ.SendToMqBitacoraStatus(entity.BitacoraId.ToString(), User.Identity.Name);

                    #endregion

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Delete one Item
        /// </summary>
        /// <param name="dto">Item if Entity</param>
        /// <returns></returns>
        [HttpPost("DeleteDataPhoto")]
        public async Task<IActionResult> DeleteDataPhoto([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = true,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    bool result = false;
                    var entity = (CustomerEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(CustomerEnt));
                    var directoryPath = Path.Combine(Configuration.GetSection("BitacoraFoliosPhotoDirectory").Value, entity.BitacoraId.ToString());

                    using (var scope = new TransactionScope())
                    {
                        using (var bl =
                            new Ilsp.BitacoraIvr.Bl.BitacoraAiEf.BitacoraEvidencesPhotosBl(GetEfBitacoraAiConnection(),
                                User.Identity.Name))
                        {
                            foreach (var photo in entity.Photos)
                            {
                                var filters = new List<Filter>
                                {
                                    new Filter {Field = "Id", Value = photo.Id.ToString(), Operator = "Equals"}
                                };
                                if (bl.DeleteWithOutTransaction(filters))
                                {
                                    result = true;
                                }
                                else
                                {
                                    response.Success = false;
                                    response.Message = Resources.Resource.Generic_Error;
                                    break;
                                }
                            }
                            if (result)
                            {
                                scope.Complete();
                                response.Success = true;
                                response.Message = Resources.Resource.Successful_Operation;
                            }
                        }
                    }

                    if (result)
                    {
                        try
                        {

                            if (Directory.Exists(directoryPath))
                            {
                                foreach (var file in entity.Photos)
                                {
                                    var pathDelete = Path.GetFullPath(file.PhotoFilePath);
                                    System.IO.File.Delete(pathDelete);

                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex.ToString());

                        }

                    }

                    #region SendToMqBitacoraStatus

                    var senderMQ = new SenderMqBitacora(Configuration);
                    senderMQ.SendToMqBitacoraStatus(entity.BitacoraId.ToString(), User.Identity.Name);

                    #endregion

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }


        /// <summary>
        /// Delete one Item
        /// </summary>
        /// <param name="bitacoraId">Item if Entity</param>
        /// <param name="customerId">Item if Entity</param>
        /// <returns></returns>
        [HttpPost("GetEvidencesFields/{bitacoraId}/{customerId}")]
        public async Task<IActionResult> GetEvidencesFields(string bitacoraId, string customerId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, sp_get_bitacora_evidences_customerResult>
                {
                    Items = new List<sp_get_bitacora_evidences_customerResult>(),
                    Success = true,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new Ilsp.BitacoraIvr.Bl.BitacoraAiLq.BitacoraEvidencesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var bitId = Encoding.UTF8.GetString(Convert.FromBase64String(bitacoraId));
                        var cusId = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                        response.Items = bl.GetEvidencesFields(bitId, cusId);
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }


    }
}
