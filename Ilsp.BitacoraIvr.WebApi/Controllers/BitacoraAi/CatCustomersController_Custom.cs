﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatCustomersController
    {

        /// <summary>
        /// Get Items from Entity
        /// </summary>
        /// <param name="model">Filters and Sorts</param>
        /// <returns></returns>
        [HttpPost("GetGridCustom")]
        public async Task<IActionResult> GetGridCustom([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.VwCatCustomersBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var response = bl.GetGridCustom(model);
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpGet]
        [Route("SearchByCustom/{value}")]
        public async Task<IActionResult> SearchByCustom(string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiLq.CatCustomersBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.Search(searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }
        
        [HttpGet]
        [Route("SearchByCustomFolios/{value}")]
        public async Task<IActionResult> SearchByCustomFolios(string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.CatCustomersBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.GetAll().Where(m => m.FolioProccess.Equals(true)).Select(m=> new CustomerEnt
                        {
                            Id = m.id_customer,
                            Name = m.account_name,
                            FiscalName = m.fiscal_name,
                            FiscalAddress = m.fiscal_address,
                            Webpage = m.webpage,
                            Email = m.send_email ?? false,
                            Sms = m.send_sms ?? false,
                            Logo = m.logo,
                            Event = m.send_event ?? false,
                            Daily = m.send_daily ?? false,
                            Weekly = m.send_weekly ?? false,
                            Monthly = m.send_monthly ?? false,
                            ModifiedBy = m.user_mod,
                            CreationDate = m.create_date.ToString("dd/MMM/yyyy HH:mm:ss"),
                            ModifiedDate = m.mod_date?.ToString("dd/MMM/yyyy HH:mm:ss"),
                            OnTime = m.ontime,
                            ValidPreBitacora = m.validbitacora,
                            FolioProccess = m.FolioProccess?? false
                        } ).OrderBy(m=> m.Name).ToList();
                        if (!string.IsNullOrEmpty(searchDec))
                        {
                            response.Items = response.Items.Where(m => m.Name.IndexOf(searchDec,StringComparison.OrdinalIgnoreCase) != -1).ToList();
                        }

                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }
        
        [HttpGet]
        [Route("FirstCustom/{customerId}")]
        public async Task<IActionResult> FirstCustom(string customerId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiLq.CatCustomersBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var customerIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                        response.Item = bl.First(customerIdDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("GetCustomersByUser")]
        public async Task<IActionResult> GetCustomersByUser()
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiLq.CatCustomersBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Items = bl.GetCustomersByUser();
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }
        /// <summary>
        /// Get Items from Entity
        /// </summary>
        /// <param name="model">Filters and Sorts</param>
        /// <returns></returns>
        [HttpPost("GetGridFields")]
        public async Task<IActionResult> GetGridFields([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.CatCustomersBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var includes = new[] { "CatCustomerFields" };
                        return Ok(bl.GetListPagerCustom(model, "AND", includes));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("GetAssigCustomersToUser/{username}")]
        public async Task<IActionResult> GetAssigCustomersToUser(string username)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var usernameDec = Encoding.UTF8.GetString(Convert.FromBase64String(username));
                    List<int> customersDs;
                    using (var bl = new Bl.BitacoraAiLq.CatCustomersBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        customersDs = bl.GetCustomersAssignedByUser(usernameDec).ToList();
                        var customers = bl.GetAllCustom().ToList();
                        customers.ForEach(item => { item.Selected = customersDs.Any(c => c.Equals(item.Id)); });
                        response.Items = customers.ToArray();
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                var liErrors = new List<ValidationFailure>();
                try
                {
                    bool result;
                    var entity = (CustomerEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(CustomerEnt));

                    using (var scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bl = new Bl.BitacoraAiLq.CatCustomersBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            result = bl.CreateCustom(entity);
                        }

                        if (result)
                        {
                            scope.Complete();
                        }
                    }

                    response.Errors = liErrors;

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Update Item on Entity
        /// </summary>
        /// <param name="dto">Data to update</param>
        /// <returns></returns>
        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity = (CustomerEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(CustomerEnt));

                    var customerBefore = new CustomerEnt();
                    if (entity.Id != 0)
                    {
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "id_customer", Value = entity.Id.ToString(), Operator = "Equals"}
                        };
                        using (var bl = new Bl.BitacoraAiEf.CatCustomersOperativeKeysTransportLinesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var includes = new[] { "cat_operative_keys" };
                            customerBefore.OpKeyTransportLines = bl.GetListCustom(filters, includes);
                        }
                        using (var bl = new Bl.BitacoraAiLq.CatCustomersAxCustomersBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            customerBefore.FiscalNames = bl.GetFiscalNamesByCustomer(entity.Id);
                        }
                        using (var bl = new Bl.BitacoraAiEf.CatCustomersOperativeKeysBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var includes = new[] { "cat_operative_keys" };
                            customerBefore.OperativeKeys = bl.GetListCustom(filters, includes);
                        }
                    }

                    using (var scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bl = new Bl.BitacoraAiLq.CatCustomersBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            result = bl.UpdateCustom(entity, customerBefore);
                        }

                        if (result)
                        {
                            scope.Complete();
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Delete one Item
        /// </summary>
        /// <param name="dto">Item if Entity</param>
        /// <returns></returns>
        [HttpPost("Delete")]
        public async Task<IActionResult> Delete([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerGrid>
                {
                    Items = new List<CustomerGrid>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    bool result;
                    var entity =
                        (CustomerGrid)JsonConvert.DeserializeObject(dto.ToString(), typeof(CustomerGrid));

                    using (var bl = new Bl.BitacoraAiEf.CatCustomersBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "id_customer", Value = entity.Id.ToString(), Operator = "Equals"}
                        };
                        result = bl.Update(filters, itemToSave =>
                        {
                            itemToSave.status = false;
                            itemToSave.user_mod = User.Identity.Name;
                            itemToSave.mod_date = DateTime.Now;
                        });
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid name
        /// </summary>
        /// <param name="name">Name</param>
        /// <returns>true or false if name exist on db</returns>
        [HttpGet]
        [Route("ValidNameCreate/{name}")]
        public async Task<IActionResult> ValidNameCreate(string name)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, GeofenceEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<GeofenceEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.CatCustomersBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var nameDec = Encoding.UTF8.GetString(Convert.FromBase64String(name));
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "account_name", Value = nameDec, Operator = "Equals"},
                            new Filter {Field = "status", Value = "1", Operator = "Equals"}
                        };
                        var item = bl.GetOne(filters);
                        if (item != null)
                        {
                            response.Success = true;
                            response.Message = Resources.Resource.Name_Already_Exists;
                        }
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid name
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="id">Id</param>
        /// <returns>true or false if name exist on db</returns>
        [HttpGet]
        [Route("ValidNameUpdate/{name}/{id}")]
        public async Task<IActionResult> ValidNameUpdate(string name, string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, GeofenceEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<GeofenceEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.CatCustomersBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var nameDec = Encoding.UTF8.GetString(Convert.FromBase64String(name));
                        var idDec = Encoding.UTF8.GetString(Convert.FromBase64String(id));
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "account_name", Value = nameDec, Operator = "Equals"},
                            new Filter {Field = "id_customer", Value = idDec, Operator = "NotEquals"},
                            new Filter {Field = "status", Value = "1", Operator = "Equals"}
                        };
                        var item = bl.GetOne(filters);
                        if (item != null)
                        {
                            response.Success = true;
                            response.Message = Resources.Resource.Name_Already_Exists;
                        }
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("GetOne/{customerId}")]
        public async Task<IActionResult> GetOne(string customerId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var customerIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                    var filters = new List<Filter>
                    {
                        new Filter{Field = "id_customer", Value = customerIdDec.ToString(), Operator = "Equals"}
                    };
                    CustomerEnt customerItem;
                    using (var bl = new Bl.BitacoraAiEf.CatCustomersBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        customerItem = bl.GetOneCustom(filters, new string[0]);
                    }

                    if (customerItem != null)
                    {
                        using (var bl = new Bl.BitacoraAiEf.CatCustomersOperativeKeysTransportLinesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var includes = new[] { "cat_operative_keys" };
                            customerItem.OpKeyTransportLines = bl.GetListCustom(filters, includes);
                        }

                        using (var bl = new Bl.BitacoraAiLq.CatCustomersAxCustomersBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            customerItem.FiscalNames = bl.GetFiscalNamesByCustomer(customerIdDec);
                        }

                        using (var bl = new Bl.BitacoraAiEf.CatCustomersOperativeKeysBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var includes = new[] { "cat_operative_keys" };
                            customerItem.OperativeKeys = bl.GetListCustom(filters, includes);
                        }
                    }
                    response.Item = customerItem;
                    response.Success = true;
                    response.Message = Resources.Resource.Successful_Operation;
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("SearchByFiscalNames/{value}")]
        public async Task<IActionResult> SearchByFiscalNames(string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, AxCustomerEnt>
                {
                    Items = new List<AxCustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiLq.CatCustomersAxCustomersBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.SearchFiscalNames(searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("FirstFiscalName/{id}")]
        public async Task<IActionResult> FirstFiscalName(string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, AxCustomerEnt>
                {
                    Items = new List<AxCustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiLq.CatCustomersAxCustomersBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var idDec = Encoding.UTF8.GetString(Convert.FromBase64String(id));
                        response.Item = bl.First(idDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

    }
}
