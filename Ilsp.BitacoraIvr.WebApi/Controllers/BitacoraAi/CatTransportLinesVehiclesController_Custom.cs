﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatTransportLinesVehiclesController
    {
        /// <summary>
        /// Get Items from Entity
        /// </summary>
        /// <param name="model">Filters and Sorts</param>
        /// <returns></returns>
        [HttpPost("GetGridCustom")]
        public async Task<IActionResult> GetGridCustom([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new VwCatTransportLinesVehiclesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        return Ok(bl.GetGridCustom(model, new string[0]));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpGet]
        [Route("SearchByCustom/{customerId}/{transportLineId}/{value}")]
        public async Task<IActionResult> SearchByCustom(string customerId, string transportLineId, string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, PlateEnt>
                {
                    Items = new List<PlateEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiLq.CatTransportLinesVehiclesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var customerIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                        var transportLineIdDec = Encoding.UTF8.GetString(Convert.FromBase64String(transportLineId));
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.Search(customerIdDec, transportLineIdDec, searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get Item from Entity
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("FirstCustom/{transportLineId}/{plateId}")]
        public async Task<IActionResult> FirstCustom(string transportLineId, string plateId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, PlateEnt>
                {
                    Items = new List<PlateEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiLq.CatTransportLinesVehiclesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var transportLineIdDec = Encoding.UTF8.GetString(Convert.FromBase64String(transportLineId));
                        var plateIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(plateId)));
                        response.Item = bl.First(transportLineIdDec, plateIdDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, PlateEnt>
                {
                    Items = new List<PlateEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var errors = new List<string>();
                    var itemFront = (PlateEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(PlateEnt));
                    var filters = new List<Filter>
                    {
                        new Filter
                        {
                            Field = "num_plate",
                            Value = itemFront.NumPlate.ToUpper().Trim(),
                            Operator = "Equals"
                        }
                    };

                    bool result;
                    using (var bl = new Bl.BitacoraAiEf.CatTransportLinesVehiclesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        result = bl.Save(new cat_transport_lines_vehicles
                        {
                            num_plate = itemFront.NumPlate.ToUpper().Trim(),
                            ax_transport_line_id = itemFront.TransportLine.Id,
                            id_unit_type = itemFront.UnitType.Id,
                            num_eco = itemFront.Economic.ToUpper().Trim(),
                            model_name = itemFront.Model.Id.ToString(),
                            observations = itemFront.Comments?.ToUpper().Trim(),
                            id_brand = itemFront.Brand.Id,
                            status = true,
                            update_user = User.Identity.Name,
                            create_date = DateTime.Now
                        }, filters, errors);
                    }

                    if (result)
                    {
                        using (var bl = new Bl.BitacoraAiEf.CatTransportLinesVehiclesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var item = bl.GetOne(filters);
                            if (item != null)
                            {
                                itemFront.Id = item.id_vehicle;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }

                    if (result)
                    {
                        using (var bl = new Bl.BitacoraAiEf.CatTransportLinesVehiclesColorsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            foreach (var color in itemFront.Colors)
                            {
                                filters = new List<Filter>
                                {
                                    new Filter
                                    {
                                        Field = "id_vehicle",
                                        Value = itemFront.Id.ToString(),
                                        Operator = "Equals"
                                    },
                                    new Filter
                                    {
                                        Field = "id_color",
                                        Value = color.Id,
                                        Operator = "Equals"
                                    }
                                };
                                bl.Save(new cat_transport_lines_vehicles_colors
                                {
                                    id_color = color.Id,
                                    id_vehicle = itemFront.Id ?? 0
                                }, filters, errors);
                            }
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Update Item on Entity
        /// </summary>
        /// <param name="dto">Data to update</param>
        /// <returns></returns>
        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, PlateEnt>
                {
                    Items = new List<PlateEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var errors = new List<string>();
                    var itemFront = (PlateEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(PlateEnt));
                    var filters = new List<Filter>
                    {
                        new Filter
                        {
                            Field = "id_vehicle",
                            Value = itemFront.Id.ToString(),
                            Operator = "Equals"
                        }
                    };

                    bool result;
                    using (var bl = new Bl.BitacoraAiEf.CatTransportLinesVehiclesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        result = bl.Update(filters, itemToSave =>
                        {
                            itemToSave.num_plate = itemFront.NumPlate.ToUpper().Trim();
                            itemToSave.ax_transport_line_id = itemFront.TransportLine.Id;
                            itemToSave.id_unit_type = itemFront.UnitType.Id;
                            itemToSave.num_eco = itemFront.Economic.ToUpper().Trim();
                            itemToSave.model_name = itemFront.Model.Id.ToString();
                            itemToSave.observations = itemFront.Comments.ToUpper().Trim();
                            itemToSave.id_brand = itemFront.Brand.Id;
                            itemToSave.update_user = User.Identity.Name;
                            itemToSave.update_date = DateTime.Now;
                        });
                    }

                    if (result)
                    {
                        using (var bl = new Bl.BitacoraAiEf.CatTransportLinesVehiclesColorsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            filters = new List<Filter>
                            {
                                new Filter
                                {
                                    Field = "id_vehicle",
                                    Value = itemFront.Id.ToString(),
                                    Operator = "Equals"
                                }
                            };
                            bl.Delete(filters);
                            foreach (var color in itemFront.Colors)
                            {
                                bl.Save(new cat_transport_lines_vehicles_colors
                                {
                                    id_color = color.Id,
                                    id_vehicle = itemFront.Id ?? 0
                                }, filters, errors);
                            }
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Delete one Item
        /// </summary>
        /// <param name="dto">Item if Entity</param>
        /// <returns></returns>
        [HttpPost("Delete")]
        public async Task<IActionResult> Delete([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, PlateGrid>
                {
                    Items = new List<PlateGrid>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var itemFront = (PlateGrid)JsonConvert.DeserializeObject(dto.ToString(), typeof(PlateGrid));
                    var filters = new List<Filter>
                    {
                        new Filter
                        {
                            Field = "id_vehicle",
                            Value = itemFront.Id.ToString(),
                            Operator = "Equals"
                        }
                    };

                    bool result;
                    using (var bl = new Bl.BitacoraAiEf.CatTransportLinesVehiclesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        result = bl.Update(filters, itemToSave =>
                        {
                            itemToSave.status = false;
                            itemToSave.update_user = User.Identity.Name;
                            itemToSave.update_date = DateTime.Now;
                        });
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

    }
}
