﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Ilsp.Common.MessageContracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    [Route("api/[controller]")]
    [Authorize]
    public partial class AuditController : BaseController
    {

        public AuditController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor) : base(configuration, loggerFactory, accessor)
        {
            Logger = loggerFactory.CreateLogger<AuditController>();
        }

        [HttpPost]
        [Route("alert")]
        public async Task<IActionResult> CreateAlertNotification([FromBody] AlertNotification model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, AlertNotification>
                {
                    Items = new List<AlertNotification>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    if (string.IsNullOrEmpty(model?.Alert?.name) || string.IsNullOrEmpty(model.BitacoraId)) return Ok();

                    using (var bl = new AuditBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var notification = string.Format(Resources.Resource.AlertNotification, model.Alert.name, model.BitacoraId);
                        return Ok(bl.CreateAlertNotification(long.Parse(model.BitacoraId), notification));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpPost]
        [Route("reaction")]
        public async Task<IActionResult> CreateReactionNotification([FromBody] BitacoraMessage model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraMessage>
                {
                    Items = new List<BitacoraMessage>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new AuditBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var notification = string.Format(Resources.Resource.ReactionNotification, model.bitacora);
                        return Ok(bl.CreateReactionNotification(long.Parse(model.id), notification));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpPost]
        [Route("news")]
        public async Task<IActionResult> CreateNewsNotification([FromBody] BitacoraNewsNotification model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraNewsNotification>
                {
                    Items = new List<BitacoraNewsNotification>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new AuditBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        return Ok(bl.CreateNewsNotification(long.Parse(model.bitacoraId), model.news));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpPost]
        [Route("notifications")]
        public async Task<IActionResult> CreateNotificationsNotification([FromBody] BitacoraNewsNotification model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraNewsNotification>
                {
                    Items = new List<BitacoraNewsNotification>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new AuditBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        return Ok(bl.CreateNotificationsNotification(long.Parse(model.bitacoraId), model.body));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpPost]
        [Route("disablenews")]
        public async Task<IActionResult> DisableBitacoraNews([FromBody] BitacoraNewsNotification model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraNewsNotification>
                {
                    Items = new List<BitacoraNewsNotification>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new AuditBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        return Ok(bl.DisableBitacoraNews(long.Parse(model.bitacoraId), model.news));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

    }
}
