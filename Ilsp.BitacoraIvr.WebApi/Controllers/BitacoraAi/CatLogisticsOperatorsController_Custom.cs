﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatLogisticsOperatorsController
    {

        [HttpGet]
        [Route("SearchByCustom/{value}")]
        public async Task<IActionResult> SearchByCustom(string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, LogisticOperatorEnt>
                {
                    Items = new List<LogisticOperatorEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatLogisticsOperatorsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.Search(searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("FirstCustom/{id}")]
        public async Task<IActionResult> FirstCustom(string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, LogisticOperatorEnt>
                {
                    Items = new List<LogisticOperatorEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatLogisticsOperatorsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var idDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(id)));
                        response.Item = bl.First(idDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

    }
}