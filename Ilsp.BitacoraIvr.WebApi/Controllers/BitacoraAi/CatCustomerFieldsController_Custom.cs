﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatCustomerFieldsController
    {
        /// <summary>
        /// Get one Item
        /// </summary>
        /// <param name="id">Id of Item</param>
        /// <returns></returns>
        [HttpGet]
        [Route("FirstCustom/{id}")]
        public async Task<IActionResult> FirstCustom(string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, object>
                {
                    Items = new List<object>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatCustomersBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var includes = new[] { "CatCustomerFields" };
                        var idDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(id)));
                        var itemResult = bl.FirstCustomFields(idDec, includes);
                        response.Item = new { itemResult.Id, itemResult.EvidenceFields };
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result = true;
                    var entity = (CustomerEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(CustomerEnt));

                    using (var scope = new TransactionScope())
                    {
                        using (var bl = new CatCustomerFieldsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            foreach (var i in entity.EvidenceFields)
                            {
                                var filters = new List<Filter>
                                {
                                    new Filter {Field = "CustomerId", Value = entity.Id.ToString(), Operator = "Equals"},
                                    new Filter {Field = "Field", Value = i.Field, Operator = "Equals"}
                                };
                                var itemToSave = new CatCustomerFields
                                {
                                    CustomerId = entity.Id,
                                    Field = i.Field,
                                    IsRequired = i.IsRequired,
                                    StatusId = 1,
                                    CreateUser = User.Identity.Name,
                                    CreateDate = DateTime.Now
                                };
                                if (!bl.SaveWithOutTransaction(itemToSave, filters))
                                {
                                    result = false;
                                    break;
                                }
                            }

                            if (result)
                            {
                                scope.Complete();
                            }
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Update Item on Entity
        /// </summary>
        /// <param name="dto">Data to update</param>
        /// <returns></returns>
        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CustomerEnt>
                {
                    Items = new List<CustomerEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    var result = true;
                    var entity = (CustomerEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(CustomerEnt));
                    List<CatCustomerFields> liBefore;
                    using (var bl = new CatCustomerFieldsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "CustomerId", Value = entity.Id.ToString(), Operator = "Equals"}
                        };
                        liBefore = bl.GetList(filters, new List<KendoSort>(), "AND").ToList();
                    }

                    using (var scope = new TransactionScope())
                    {
                        using (var bl = new CatCustomerFieldsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var itemsToDelete = liBefore.Where(b => !entity.EvidenceFields.Any(a => a.Field.ToLower().Equals(b.Field.ToLower())));
                            var itemsToInsert = entity.EvidenceFields.Where(a => !liBefore.Any(b => b.Field.ToLower().Equals(a.Field.ToLower())));
                            foreach (var field in itemsToDelete)
                            {
                                var filters = new List<Filter>
                                {
                                    new Filter {Field = "CustomerId", Value = entity.Id.ToString(), Operator = "Equals"},
                                    new Filter {Field = "Field", Value = field.Field, Operator = "Equals"}
                                };
                                if (!bl.DeleteWithOutTransaction(filters))
                                {
                                    result = false;
                                    break;
                                }
                            }

                            foreach (var field in itemsToInsert)
                            {
                                var filters = new List<Filter>
                                {
                                    new Filter {Field = "CustomerId", Value = entity.Id.ToString(), Operator = "Equals"},
                                    new Filter {Field = "Field", Value = field.Field, Operator = "Equals"}
                                };
                                var itemToSave = new CatCustomerFields
                                {
                                    CustomerId = entity.Id,
                                    Field = field.Field,
                                    IsRequired = field.IsRequired,
                                    StatusId = 1,
                                    CreateUser = User.Identity.Name,
                                    CreateDate = DateTime.Now
                                };
                                if (!bl.SaveWithOutTransaction(itemToSave, filters))
                                {
                                    result = false;
                                    break;
                                }
                            }

                            if (result)
                            {
                                scope.Complete();
                            }
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Delete one Item
        /// </summary>
        /// <param name="dto">Item if Entity</param>
        /// <returns></returns>
        [HttpPost("Delete")]
        public async Task<IActionResult> Delete([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, CatCustomerFields>
                {
                    Items = new List<CatCustomerFields>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    var entity = (CustomerEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(CustomerEnt));
                    using (var bl = new CatCustomerFieldsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "CustomerId", Value = entity.Id.ToString(), Operator = "Equals"}
                        };
                        response.Items = bl.GetList(filters, new List<KendoSort>(), "AND").ToList();
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }
    }
}
