﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Ilsp.BitacoraIvr.WebApi.Mq;
using Ilsp.Common.MessageContracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class BitacorasController
    {

        /// <summary>
        /// Get Bitacora
        /// </summary>
        /// <param name="search">Search</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetBitacoras/{search}")]
        public async Task<IActionResult> GetBitacoras(string search)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false
                };
                try
                {
                    using (var bl = new BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(search));
                        response.Items = bl.GetBitacoras(searchDec.Trim());
                        response.Message = Resources.Resource.Successful_Operation;
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get Items
        /// </summary>
        /// <param name="bitacoraId">Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetGpsByBitacora/{bitacoraId}")]
        public async Task<IActionResult> GetGpsByBitacora(string bitacoraId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraGps>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false
                };
                try
                {
                    using (var bl = new BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var bitacoraIdDec = Encoding.UTF8.GetString(Convert.FromBase64String(bitacoraId));
                        response.Items = bl.GetGpsByBitacoras(bitacoraIdDec);
                        response.Message = Resources.Resource.Successful_Operation;
                        response.Success = true;
                        response.Item = null;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpPost("GetGridBitacorasTurnOffAlerts")]
        public async Task<IActionResult> GetGridBitacorasTurnOffAlerts([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        return Ok(bl.GetGridBitacorasTurnOffAlerts(model));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpPost("CreateTurnOffAlerts")]
        public async Task<IActionResult> CreateTurnOffAlerts([FromBody] BitacoraTurnOffAlertEnt model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var alertsBeforeInsert = bl.GetBitacorasAlertsForMq(model.BitacoraId).ToArray();

                        var result = bl.CreateBitacorasAlertsForMq(model.BitacoraId,
                            model.GpsId,
                            model.AlertName,
                            string.IsNullOrEmpty(model.MinutesDisabled) ? 1 : int.Parse(model.MinutesDisabled),
                            model.Observations);

                        if (result == 0)
                        {
                            Logger.LogError(Resources.Resource.Generic_Error);
                            return StatusCode(600, model);
                        }

                        if (result == 2)
                        {
                            Logger.LogError("Alert already disabled ");
                            return StatusCode(700, model);
                        }

                        var alertsAfterInsert = bl.GetBitacorasAlertsForMq(model.BitacoraId).ToArray();

                        var alertForMq = (from alertAfter in alertsAfterInsert
                                          where !(from alertBefore in alertsBeforeInsert
                                                  select alertBefore.AlertStatusId).Contains(alertAfter.AlertStatusId)
                                          select new DisabledAlert
                                          {
                                              Id = alertAfter.AlertStatusId,
                                              AlertId = alertAfter.AlertId,
                                              BitacoraId = alertAfter.BitacoraId,
                                              GpsId = alertAfter.GpsId,
                                              Minutes = alertAfter.Minutes,
                                              Status = alertAfter.Status
                                          }
                            ).ToArray();

                        using (var sender = new MqSender(Configuration, "Mq"))
                        {
                            foreach (var alert in alertForMq)
                            {
                                var message = JsonConvert.SerializeObject(new DisabledAlert
                                {
                                    Id = alert.Id,
                                    AlertId = alert.AlertId,
                                    BitacoraId = alert.BitacoraId,
                                    GpsId = alert.GpsId,
                                    Minutes = alert.Minutes,
                                    Status = alert.Status,
                                    Timer = null
                                });
                                sender.Send(message, "alert_disabled");
                            }
                        }

                        Logger.LogInformation("Alerts created: ");
                        return Created(model.AlertName, model);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpPost("DeleteTurnOffAlerts")]
        public async Task<IActionResult> DeleteTurnOffAlerts([FromBody] BitacoraTurnOffAlertEnt model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var db = GetDbConnection())
                    {
                        var result = db.sp_delete_bitacora_alert_status(int.Parse(model.Id), User.Identity.Name);

                        if (result == 0)
                        {
                            Logger.LogError(Resources.Resource.Generic_Error);
                            return StatusCode(600, model);
                        }

                        using (var sender = new MqSender(Configuration, "Mq"))
                        {
                            var message = JsonConvert.SerializeObject(new DisabledAlert
                            {
                                AlertId = int.Parse(model.AlertId),
                                BitacoraId = model.BitacoraId,
                                GpsId = model.GpsId,
                                Minutes = string.IsNullOrEmpty(model.MinutesDisabled) ? 1 : int.Parse(model.MinutesDisabled),
                                Status = 2,
                                Timer = null
                            });
                            sender.Send(message, "alert_disabled");
                        }
                        Logger.LogInformation("Alert canceled: " + result);
                        return StatusCode(201, result);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

    }
}
