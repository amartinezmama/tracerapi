﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatOriginsDestinationsController
    {
        /// <summary>
        /// Get Items from Entity
        /// </summary>
        /// <param name="model">Filters and Sorts</param>
        /// <returns></returns>
        [HttpPost("GetGridCustom")]
        public async Task<IActionResult> GetGridCustom([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.VwCatOriginDestinationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var response = bl.GetGridCustom(model);
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        /// <summary>
        /// Get Item from Entity
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("FirstCustom/{id}")]
        public async Task<IActionResult> FirstCustom(string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, OriginAndDestinyEnt>
                {
                    Items = new List<OriginAndDestinyEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.CatOriginsDestinationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var idDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(id)));
                        var includes = new string[0];
                        response.Item = bl.FirstCustom(idDec, includes);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, OriginAndDestinyEnt>
                {
                    Items = new List<OriginAndDestinyEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                var liErrors = new List<ValidationFailure>();
                try
                {
                    bool result;
                    var entity =
                        (OriginAndDestinyEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(OriginAndDestinyEnt));

                    using (var bl =
                        new Bl.BitacoraAiEf.CatOriginsDestinationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var errors = new List<string>();
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "name", Value = entity.Name, Operator = "Equals"}
                        };
                        result = bl.Save(new cat_origins_destinations
                        {
                            name = entity.Name,
                            alias = entity.Name,
                            state = entity.State,
                            municipality = entity.Municipality,
                            street = entity.Street,
                            number = entity.Number,
                            town = entity.Town,
                            postcode = entity.Postcode,
                            address = entity.Address,
                            latitude = double.Parse(entity.Latitude),
                            longitude = double.Parse(entity.Longitude),
                            user_mod = User.Identity.Name,
                            create_date = DateTime.Now,
                            id_status = 1
                        }, filters, errors);
                        if (!result)
                        {
                            liErrors.Add(new ValidationFailure("Name", errors.First()));
                        }
                    }

                    response.Errors = liErrors;

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Update Item on Entity
        /// </summary>
        /// <param name="dto">Data to update</param>
        /// <returns></returns>
        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, OriginAndDestinyEnt>
                {
                    Items = new List<OriginAndDestinyEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity =
                        (OriginAndDestinyEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(OriginAndDestinyEnt));

                    using (var bl =
                        new Bl.BitacoraAiEf.CatOriginsDestinationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "id_origin_destination", Value = entity.Id.ToString(), Operator = "Equals"}
                        };
                        result = bl.Update(filters, itemToSave =>
                        {
                            itemToSave.name = entity.Name;
                            itemToSave.alias = entity.Name;
                            itemToSave.state = entity.State;
                            itemToSave.municipality = entity.Municipality;
                            itemToSave.street = entity.Street;
                            itemToSave.number = entity.Number;
                            itemToSave.town = entity.Town;
                            itemToSave.postcode = entity.Postcode;
                            itemToSave.address = entity.Address;
                            itemToSave.latitude = double.Parse(entity.Latitude);
                            itemToSave.longitude = double.Parse(entity.Longitude);
                            itemToSave.user_mod = User.Identity.Name;
                            itemToSave.mod_date = DateTime.Now;
                        });
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Delete one Item
        /// </summary>
        /// <param name="dto">Item if Entity</param>
        /// <returns></returns>
        [HttpPost("Delete")]
        public async Task<IActionResult> Delete([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, OriginAndDestinyGrid>
                {
                    Items = new List<OriginAndDestinyGrid>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    bool result;
                    var entity =
                        (OriginAndDestinyGrid)JsonConvert.DeserializeObject(dto.ToString(), typeof(OriginAndDestinyGrid));

                    using (var bl =
                        new Bl.BitacoraAiEf.CatOriginsDestinationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "id_origin_destination", Value = entity.Id.ToString(), Operator = "Equals"}
                        };
                        result = bl.Update(filters, itemToSave =>
                        {
                            itemToSave.id_status = 0;
                            itemToSave.user_mod = User.Identity.Name;
                            itemToSave.mod_date = DateTime.Now;
                        });
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid name
        /// </summary>
        /// <param name="name">Name</param>
        /// <returns>true or false if name exist on db</returns>
        [HttpGet]
        [Route("ValidNameCreate/{name}")]
        public async Task<IActionResult> ValidNameCreate(string name)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, OriginAndDestinyEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<OriginAndDestinyEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl =
                        new Bl.BitacoraAiEf.CatOriginsDestinationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var nameDec = Encoding.UTF8.GetString(Convert.FromBase64String(name));
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "name", Value = nameDec, Operator = "Equals"},
                            new Filter {Field = "id_status", Value = "1", Operator = "Equals"}
                        };
                        var item = bl.GetOne(filters);
                        if (item != null)
                        {
                            response.Success = true;
                            response.Message = Resources.Resource.Name_Already_Exists;
                        }
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid name
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="id">Id</param>
        /// <returns>true or false if name exist on db</returns>
        [HttpGet]
        [Route("ValidNameUpdate/{name}/{id}")]
        public async Task<IActionResult> ValidNameUpdate(string name, string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, OriginAndDestinyEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<OriginAndDestinyEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl =
                        new Bl.BitacoraAiEf.CatOriginsDestinationsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var nameDec = Encoding.UTF8.GetString(Convert.FromBase64String(name));
                        var idDec = Encoding.UTF8.GetString(Convert.FromBase64String(id));
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "name", Value = nameDec, Operator = "Equals"},
                            new Filter {Field = "id_origin_destination", Value = idDec, Operator = "NotEquals"},
                            new Filter {Field = "id_status", Value = "1", Operator = "Equals"}
                        };
                        var item = bl.GetOne(filters);
                        if (item != null)
                        {
                            response.Success = true;
                            response.Message = Resources.Resource.Name_Already_Exists;
                        }
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

    }
}
