﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Ilsp.BitacoraIvr.WebApi.Mq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatRoutesController
    {
        /// <summary>
        /// Get Items from Entity
        /// </summary>
        /// <param name="model">Filters and Sorts</param>
        /// <returns></returns>
        [HttpPost("GetGridCustom")]
        public async Task<IActionResult> GetGridCustom([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.VwCatRoutesIvrBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        //var appId = int.Parse(Configuration.GetValue<string>("ApplicationDb"));
                        //model.Filter.Filters.Add(new Filter { Field = "applicationId", Value = appId.ToString(), Operator = "Equals" });
                        var includes = new string[0];
                        var response = bl.GetGridCustom(model, includes);
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, RouteEnt>
                {
                    Items = new List<RouteEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity =
                        (RouteEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(RouteEnt));
                    var appId = int.Parse(Configuration.GetValue<string>("ApplicationDb"));
                    var routeId = 0;
                    using (var bl = new CatRoutesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        result = bl.Create(entity, appId, ref routeId);
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }
                    try
                    {
                        //Send MQ
                        SendMessage();
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex.ToString());
                        response.Message += Resources.Resource.Rabbit_Error;
                    }
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Update Item on Entity
        /// </summary>
        /// <param name="dto">Data to update</param>
        /// <returns></returns>
        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, RouteEnt>
                {
                    Items = new List<RouteEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity =
                        (RouteEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(RouteEnt));
                    var appId = int.Parse(Configuration.GetValue<string>("ApplicationDb"));
                    using (var bl = new CatRoutesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        result = bl.Update(entity, appId);
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }
                    try
                    {
                        //Send MQ
                        SendMessage();
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex.ToString());
                        response.Message += Resources.Resource.Rabbit_Error;
                    }
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Delete one Item
        /// </summary>
        /// <param name="dto">Item if Entity</param>
        /// <returns></returns>
        [HttpPost("Delete")]
        public async Task<IActionResult> Delete([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, RouteEnt>
                {
                    Items = new List<RouteEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    bool result;
                    var entity =
                        (RouteEnt)JsonConvert.DeserializeObject(dto.ToString(), typeof(RouteEnt));

                    using (var bl =
                        new Bl.BitacoraAiEf.CatRoutesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "id_route", Value = entity.Id.ToString(), Operator = "Equals"}
                        };
                        result = bl.Update(filters, itemToSave =>
                        {
                            itemToSave.id_status = 0;
                            itemToSave.update_user = User.Identity.Name;
                            itemToSave.update_date = DateTime.Now;
                        });
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }
                    try
                    {
                        //Send MQ
                        SendMessage();
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex.ToString());
                        response.Message += Resources.Resource.Rabbit_Error;
                    }
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid name
        /// </summary>
        /// <param name="name">Name</param>
        /// <returns>true or false if name exist on db</returns>
        [HttpGet]
        [Route("validname/{name}")]
        public async Task<IActionResult> Get(string name)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, RouteEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<RouteEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl =
                        new Bl.BitacoraAiEf.CatRoutesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var nameDec = Encoding.UTF8.GetString(Convert.FromBase64String(name));
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "name", Value = nameDec, Operator = "Equals"}
                        };
                        var item = bl.GetOne(filters);
                        if (item != null)
                        {
                            response.Success = true;
                            response.Message = Resources.Resource.Name_Already_Exists;
                        }
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get Item from Entity
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("OneCustom/{id}")]
        public async Task<IActionResult> OneCustom(string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, RouteEnt>
                {
                    Items = new List<RouteEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.CatRoutesBl(GetEfBitacoraAiConnection(), User.Identity.Name, GetAdoBitacoraAiConnection()))
                    {
                        var includes = new[] { "cat_catalogs_common" };
                        var idDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(id)));
                        response.Item = bl.FirstCustom(idDec, includes);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("SearchByCustom/{value}")]
        public async Task<IActionResult> SearchByCustom(string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, RouteEnt>
                {
                    Items = new List<RouteEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatRoutesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var appId = int.Parse(Configuration.GetValue<string>("ApplicationDb"));
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.Search(appId, searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid name
        /// </summary>
        /// <param name="name">Name</param>
        /// <returns>true or false if name exist on db</returns>
        [HttpGet]
        [Route("ValidNameCreate/{name}")]
        public async Task<IActionResult> ValidNameCreate(string name)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, RouteEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<RouteEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl =
                        new Bl.BitacoraAiEf.CatRoutesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var appId = int.Parse(Configuration.GetSection("ApplicationDb").Value);
                        var nameDec = Encoding.UTF8.GetString(Convert.FromBase64String(name));
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "name", Value = nameDec, Operator = "Equals"},
                            new Filter {Field = "applicationId", Value = appId.ToString(), Operator = "Equals"},
                            new Filter {Field = "id_status", Value = "1", Operator = "Equals"}
                        };
                        var item = bl.GetOne(filters);
                        if (item != null)
                        {
                            response.Success = true;
                            response.Message = Resources.Resource.Name_Already_Exists;
                        }
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid name
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="id">Id</param>
        /// <returns>true or false if name exist on db</returns>
        [HttpGet]
        [Route("ValidNameUpdate/{name}/{id}")]
        public async Task<IActionResult> ValidNameUpdate(string name, string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, RouteEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<RouteEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl =
                        new Bl.BitacoraAiEf.CatRoutesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var appId = int.Parse(Configuration.GetSection("ApplicationDb").Value);
                        var nameDec = Encoding.UTF8.GetString(Convert.FromBase64String(name));
                        var idDec = Encoding.UTF8.GetString(Convert.FromBase64String(id));
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "name", Value = nameDec, Operator = "Equals"},
                            new Filter {Field = "applicationId", Value = appId.ToString(), Operator = "Equals"},
                            new Filter {Field = "id_route", Value = idDec, Operator = "NotEquals"},
                            new Filter {Field = "id_status", Value = "1", Operator = "Equals"}
                        };
                        var item = bl.GetOne(filters);
                        if (item != null)
                        {
                            response.Success = true;
                            response.Message = Resources.Resource.Name_Already_Exists;
                        }
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        private void SendMessage()
        {
            using (var sender = new MqSender(Configuration, "Mq"))
            {
                //send assignment
                var message = "route";
                sender.Send(message, "reload.config");
            }
        }
    }
}
