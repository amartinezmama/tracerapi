﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.Ent.Identity;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class BitacorasUsersController
    {

        [HttpPost("GetGridBitacorasAssigment")]
        public async Task<IActionResult> GetGridBitacorasAssigment([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var showAll = User.Claims.Any(x => x.Type.Equals("BitacoraPermission") && x.Value.Equals("ShowAllPreCaptures"));
                        return Ok(bl.GetGridBitacorasAssigment(model, showAll));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpPost("GetGridBitacorasForReassigment")]
        public async Task<IActionResult> GetGridBitacorasForReassigment([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        return Ok(bl.GetGridBitacorasForReassigment(model));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpPost("AssignOrUnassignUsersToBitacora")]
        public async Task<IActionResult> AssignOrUnassignUsersToBitacora([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacorasAssigment>
                {
                    Items = new List<BitacorasAssigment>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    bool result;
                    var itemFront = (BitacorasAssigment)JsonConvert.DeserializeObject(dto.ToString(), typeof(BitacorasAssigment));
                    using (var scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bl = new BitacorasUsersBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            result = bl.AssignOrUnassignUsersToBitacora(long.Parse(itemFront.Id), itemFront.AssignedUsers.ToLower());
                        }

                        if (result)
                        {
                            scope.Complete();
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpPost("ReassignOrUnassignUsersToBitacora")]
        public async Task<IActionResult> ReassignOrUnassignUsersToBitacora([FromBody] dynamic dataContent)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, object>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = true
                };
                try
                {
                    bool result;
                    var unassignUsers = (List<UserEnt>)JsonConvert.DeserializeObject(dataContent["unassignUsers"].ToString(), typeof(List<UserEnt>));
                    var assignUsers = (List<UserEnt>)JsonConvert.DeserializeObject(dataContent["assignUsers"].ToString(), typeof(List<UserEnt>));
                    var bitacoras = (List<BitacorasAssigment>)JsonConvert.DeserializeObject(dataContent["bitacoras"].ToString(), typeof(List<BitacorasAssigment>));
                    using (var scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (var bl = new BitacorasUsersBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                        {
                            result = bl.ReassignOrUnassignUsersToBitacora(
                                string.Join(",", unassignUsers.Select(x => x.UserName)),
                                string.Join(",", bitacoras.Select(x => x.Id)),
                                string.Join(",", assignUsers.Select(x => x.UserName))
                                );
                        }

                        if (result)
                        {
                            scope.Complete();
                        }
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

    }
}
