﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    [Route("api/[controller]")]
    [Authorize]
    public partial class CatPadLocksController : BaseController
    {
        public CatPadLocksController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor) : base(configuration, loggerFactory, accessor)
        {
            Logger = loggerFactory.CreateLogger<CatPadLocksController>();
        }

        [HttpGet]
        [Route("SearchByCustom/{value}")]
        public async Task<IActionResult> SearchByCustom(string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, PadLockEnt>
                {
                    Items = new List<PadLockEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatPadLocksBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.Search(searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("FirstCustom/{padLockId}")]
        public async Task<IActionResult> FirstCustom(string padLockId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, PadLockEnt>
                {
                    Items = new List<PadLockEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatPadLocksBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var padLockIdDec = Encoding.UTF8.GetString(Convert.FromBase64String(padLockId));
                        response.Item = bl.First(padLockIdDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }
    }
}
