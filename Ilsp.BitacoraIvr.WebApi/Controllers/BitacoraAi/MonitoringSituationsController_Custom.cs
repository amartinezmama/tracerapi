﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    [Route("api/[controller]")]
    [Authorize]
    public partial class MonitoringSituationsController : BaseController
    {
        public MonitoringSituationsController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor) : base(configuration, loggerFactory, accessor)
        {
            Logger = loggerFactory.CreateLogger<MonitoringSituationsController>();
        }

        /// <summary>
        /// Get monitoring frequencies
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("byopkey/{operativeKey}")]
        public async Task<IActionResult> GetByOperativeKey(int operativeKey)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, sp_get_situations_by_operative_key_Result>
                {
                    Items = new List<sp_get_situations_by_operative_key_Result>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatSituationsBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var items = bl.GetSituationsByOpKey(operativeKey);
                        response.Success = true;
                        response.Items = items;
                        return Ok(response.Items);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

    }
}
