﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class BitacorasController
    {

        /// <summary>
        /// Get Custodies of Bitacoras By Service Order
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCustodiesOfServiceByServiceOrder/{serviceOrder}")]
        public async Task<IActionResult> GetCustodiesOfServiceByServiceOrder(string serviceOrder)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraCustodyEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false
                };
                try
                {
                    using (var bl = new BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var serviceOrderDec = Encoding.UTF8.GetString(Convert.FromBase64String(serviceOrder));
                        response.Items = bl.GetCustodiesOfServiceByServiceOrder(serviceOrderDec);
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

    }
}