﻿using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Ilsp.BitacoraIvr.WebApi.Mq;
using Ilsp.BitacoraIvr.WebApi.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using bitacoras_ax_activation = Ilsp.BitacoraIvr.EntBitacoraAi.bitacoras_ax_activation;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{

    public partial class BitacorasAxActivationController
    {

        [HttpPost("GetGridCustom")]
        public async Task<IActionResult> GetGridCustom([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new Bl.BitacoraAiLq.BitacorasAxActivationBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        return Ok(bl.GetGridPagedList(model));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpPost("GetGridCustody")]
        public async Task<IActionResult> GetGridCustody([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new Bl.BitacoraAiLq.BitacorasAxActivationBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        return Ok(bl.GetGridCustody(model));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        [HttpGet]
        [Route("SearchByCustom/{property}/{value}")]
        public async Task<IActionResult> SearchByCustom(string property, string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, EntBitacoraAi.bitacoras_ax_activation>
                {
                    Items = new List<EntBitacoraAi.bitacoras_ax_activation>(),
                    Success = false
                };
                try
                {
                    using (var bl = new BitacorasAxActivationBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var kendoRequest = new KendoRequest();
                        var filters = new List<Filter>();
                        var orders = new List<KendoSort>();
                        filters.Add(new Filter
                        {
                            Field = property,
                            Value = value,
                            Operator = "Contains"
                        });
                        orders.Add(new KendoSort
                        {
                            Field = property,
                            Dir = "ASC"
                        });
                        kendoRequest.Filter = new KendoFilter
                        {
                            Filters = filters,
                            Logic = "Contains"
                        };
                        kendoRequest.Sort = orders;
                        kendoRequest.Page = 1;
                        response.Items = JsonConvert.DeserializeObject<IEnumerable<EntBitacoraAi.bitacoras_ax_activation>>(
                            JsonConvert.SerializeObject(bl.GetListPager(kendoRequest, "AND").Data));
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("GetOneByCustom/{property}/{value}")]
        public async Task<IActionResult> GetOneByCustom(string property, string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, EntBitacoraAi.bitacoras_ax_activation>
                {
                    Items = new List<bitacoras_ax_activation>(),
                    Success = false
                };
                try
                {
                    using (var bl = new BitacorasAxActivationBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter
                            {
                                Field = Encoding.UTF8.GetString(Convert.FromBase64String(property)),
                                Value = Encoding.UTF8.GetString(Convert.FromBase64String(value)),
                                Operator = "Equals"
                            }
                        };
                        response.Item = bl.GetOne(filters);
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpPost("RequestCancellBitacoraFolio")]
        public async Task<IActionResult> RequestCancellBitacoraFolio([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
           {
               var response = new ResponseAction<ValidationFailure, EntBitacoraAi.bitacoras_ax_activation>
               {
                   Items = new List<bitacoras_ax_activation>(),
                   Success = false,
                   Message = Resources.Resource.Generic_Error,
                   Errors = new List<ValidationFailure>()
               };
               var liErrors = new List<ValidationFailure>();
               try
               {
                   var result = false;
                   var entity = (BitacorasAxActivation)JsonConvert.DeserializeObject(dto.ToString(), typeof(BitacorasAxActivation));
                   bitacoras_ax_activation currentFolioBitacora = null;
                   var emailOutBoxId = 0;
                   var reasoncancellation = string.Empty;
                   var email = new CatEmails
                   {
                       DisplayName = User.Claims.Where(m => m.Type.Equals("name")).FirstOrDefault().Value,
                       Email = User.Claims.Where(m => m.Type.Equals("email")).FirstOrDefault().Value
                   };

                   #region FluentValidator

                   //var fluentResult = new ValidationResult();
                   //switch (bitacoraBefore.Status)
                   //{
                   //    case "O":
                   //        if (bitacoraBefore.EnableBitacora)
                   //        {
                   //            using (var val = new PreFoliosActivateValidator())
                   //            {
                   //                fluentResult = val.Validate(bitacoraBefore);
                   //            }
                   //        }
                   //        else
                   //        {
                   //            using (var val = new TransportPreFoliosValidator())
                   //            {
                   //                fluentResult = val.Validate(bitacoraBefore);
                   //            }
                   //        }
                   //        break;
                   //    default:
                   //        response.Success = false;
                   //        response.Message = Resources.Resource.Operation_Not_Allowed;
                   //        if (!response.Success)
                   //        {
                   //            return Created("Error", response);
                   //        }
                   //        break;
                   //}

                   //if (!fluentResult.IsValid)
                   //{
                   //    response.Errors = fluentResult.Errors;
                   //    return Created("Error", response);
                   //}

                   #endregion

                   #region Get Current FoliBitacora
                   using (var bl = new BitacorasAxActivationBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                   {
                       var filters = new List<Filter>
                               {
                                    new Filter
                                    {
                                        Field = "id",
                                        Value = entity.Id.ToString(),
                                        Operator = "Equals"
                                    }
                               };

                       currentFolioBitacora = bl.GetOne(filters);
                   }
                   #endregion

                   #region Get EmailId UserInfo

                   using (var bl = new CatEmailsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                   {
                       var filters = new List<Filter>
                           {
                                new Filter
                                {
                                    Field = "Email",
                                    Value = email.Email,
                                    Operator = "Equals"
                                }
                           };
                       email = bl.GetOne(filters);

                   }

                   #endregion

                   #region GetReasonCancellation

                   using (var bl = new CatReasonCancellationBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                   {
                       var filters = new List<Filter>
                           {
                                new Filter
                                {
                                    Field = "id",
                                    Value = entity.ReasonCancellationId.ToString(),
                                    Operator = "Equals"
                                }
                           };
                       reasoncancellation = bl.GetOne(filters).Description;
                   }


                   #endregion

                   #region Transaction to insert Bitacora

                   using (var scope = new TransactionScope())
                   {

                       #region Insert Email if no exist

                       if (email == null)
                       {
                           using (var bl = new CatEmailsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                           {
                               var filters = new List<Filter>
                               {
                                    new Filter
                                    {
                                        Field = "Email",
                                        Value = User.Claims.Where(m => m.Type.Equals("email")).FirstOrDefault().Value,
                                        Operator = "Equals"
                                    }
                               };

                               email = new CatEmails
                               {
                                   DisplayName = User.Claims.Where(m => m.Type.Equals("name")).FirstOrDefault().Value,
                                   Email = User.Claims.Where(m => m.Type.Equals("email")).FirstOrDefault().Value,
                                   IsSenderEmail = false,
                                   Password = null,
                                   StatusId = 1,
                                   CreateUser = "system",
                                   CreateDate = DateTime.Now
                               };

                               result = bl.SaveWithOutTransaction(email, filters);

                               if (!result)
                               {
                                   response.Errors = liErrors;
                                   response.Success = false;
                                   response.Message = Resources.Resource.Generic_Error;
                                   return Ok(response);
                               }


                           }
                       }

                       #endregion

                       #region Update FolioBitacora

                       using (var bl = new BitacorasAxActivationBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                       {
                           var filters = new List<Filter>
                           {
                                new Filter
                                {
                                    Field = "id",
                                    Value = entity.Id.ToString(),
                                    Operator = "Equals"
                                }
                           };

                           result = bl.UpdateWithOutTransaction(filters, itemtosave =>
                           {
                               itemtosave.CancellationFolioStatusId = entity.CancellationFolioStatusId;
                               itemtosave.ReasonCancellationComment = entity.ReasonCancellationComment;
                               itemtosave.ReasonCancellationId = entity.ReasonCancellationId;
                               itemtosave.EmailIdUserRequestCancellation = email.Id;
                               itemtosave.update_user = User.Identity.Name;
                               itemtosave.ReasonCancellationDate = DateTime.Now;
                               itemtosave.update_date = DateTime.Now;
                           });

                       }

                       #endregion

                       #region Send notification to process Email

                       if (result)
                       {
                           using (var outBoxHeaderBl = new EmailOutboxBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                           {
                               var filters = new List<Filter>
                               {
                                    new Filter {Field = "Id", Value = "0", Operator = "Equals"}
                               };
                               var data = new
                               {
                                   Folio = currentFolioBitacora.custody_folio,
                                   CancellationReason = reasoncancellation,
                                   CancellationComment = entity.ReasonCancellationComment,
                                   ServiceOrder = currentFolioBitacora.ax_service_order,
                                   UserRequest = email.DisplayName

                               };
                               var itemToSave = new EmailOutbox
                               {
                                   ProcessId = int.Parse(Configuration.GetSection("ProccessEmailRequestCancellation").Value),
                                   Data = JsonConvert.SerializeObject(data),
                                   CreateUser = User.Identity.Name,
                                   CreateDate = DateTime.Now,
                                   EmailStatusId = 1
                               };
                               result = outBoxHeaderBl.SaveWithOutTransaction(itemToSave, filters);
                               if (result)
                               {
                                   emailOutBoxId = itemToSave.Id;
                               }
                           }

                           using (var outBoxBodyBl = new EmailOuboxEmailsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                           {
                               var filters = new List<Filter>
                               {
                                    new Filter {Field = "Id", Value = "0", Operator = "Equals"}
                               };
                               var itemToSave = new EmailOuboxEmails
                               {
                                   EmailOutboxId = emailOutBoxId,
                                   EmailId = int.Parse(Configuration.GetSection("EmailIdRequestCancellation").Value),
                                   TypeId = "T"
                               };
                               result = outBoxBodyBl.SaveWithOutTransaction(itemToSave, filters);

                           }


                       }

                       #endregion

                       if (result)
                       {
                           scope.Complete();
                       }
                       else
                       {
                           response.Item = null;
                           response.Success = false;
                           response.Message = Resources.Resource.Generic_Error;
                           Logger.LogError(Resources.Resource.Generic_Error);
                           return Ok(response);
                       }

                   }

                   #endregion

                   #region Send To Mq-Email

                   if (result)
                   {
                       using (var sender = new MqSender(Configuration, "Email"))
                       {
                           var message = JsonConvert.SerializeObject(emailOutBoxId);
                           sender.Send(message);
                       }
                       response.Item = currentFolioBitacora;
                       response.Success = true;
                       response.Message = Resources.Resource.Successful_Operation;
                   }
                   else
                   {
                       response.Item = null;
                       response.Success = false;
                       response.Message = Resources.Resource.Generic_Error;
                       Logger.LogError(Resources.Resource.Generic_Error);
                       return Ok(response);
                   }

                   #endregion

                   return Ok(response);
               }
               catch (Exception ex)
               {
                   response.Errors = liErrors;
                   response.Success = false;
                   response.Message = Resources.Resource.Generic_Error;
                   Log("User: " + User.Identity.Name + "\n" + ex);
                   Logger.LogError(ex.ToString());
                   return Ok(response);
               }
           });
        }

        [HttpPost("AttendCancellBitacoraFolio")]
        public async Task<IActionResult> AttendCancellBitacoraFolio([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(async () =>
            {
                var response = new ResponseAction<ValidationFailure, EntBitacoraAi.bitacoras_ax_activation>
                {
                    Items = new List<bitacoras_ax_activation>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                var liErrors = new List<ValidationFailure>();
                try
                {
                    var result = false;
                    var entity = (BitacorasAxActivation)JsonConvert.DeserializeObject(dto.ToString(), typeof(BitacorasAxActivation));
                    bitacoras_ax_activation currentFolioBitacora = null;
                    var emailOutBoxId = 0;
                    var reasoncancellation = string.Empty;

                    #region FluentValidator

                    //var fluentResult = new ValidationResult();
                    //switch (bitacoraBefore.Status)
                    //{
                    //    case "O":
                    //        if (bitacoraBefore.EnableBitacora)
                    //        {
                    //            using (var val = new PreFoliosActivateValidator())
                    //            {
                    //                fluentResult = val.Validate(bitacoraBefore);
                    //            }
                    //        }
                    //        else
                    //        {
                    //            using (var val = new TransportPreFoliosValidator())
                    //            {
                    //                fluentResult = val.Validate(bitacoraBefore);
                    //            }
                    //        }
                    //        break;
                    //    default:
                    //        response.Success = false;
                    //        response.Message = Resources.Resource.Operation_Not_Allowed;
                    //        if (!response.Success)
                    //        {
                    //            return Created("Error", response);
                    //        }
                    //        break;
                    //}

                    //if (!fluentResult.IsValid)
                    //{
                    //    response.Errors = fluentResult.Errors;
                    //    return Created("Error", response);
                    //}

                    #endregion

                    #region Get Current FoliBitacora

                    using (var bl = new BitacorasAxActivationBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                                {
                                    new Filter
                                    {
                                        Field = "id",
                                        Value = entity.Id.ToString(),
                                        Operator = "Equals"
                                    }
                                };

                        currentFolioBitacora = bl.GetOne(filters);
                    }

                    #endregion

                    #region GetReasonCancellation

                    if (entity.CancellationFolioStatusId.Equals(4)) // Cancelación rechazada
                    {
                        using (var bl = new CatReasonCancellationBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var filters = new List<Filter>
                           {
                                new Filter
                                {
                                    Field = "id",
                                    Value = currentFolioBitacora.ReasonCancellationId.ToString(),
                                    Operator = "Equals"
                                }
                           };
                            reasoncancellation = bl.GetOne(filters).Description;
                        }
                    }


                    #endregion

                    #region Send cancell to Dynamics

                    if (entity.CancellationFolioStatusId.Equals(3)) // Cancelación aprovada
                    {
                        using (var client = new OrderServiceReference.OrdenesServiciosWebClient(Configuration.GetSection("EndPointOSAx").Value))
                        {
                            // Configure WCF client
                            client.ChannelFactory.Credentials.UserName.UserName =
                                Configuration.GetSection("DynamicsWcfClient")["UserName"];
                            client.ChannelFactory.Credentials.UserName.Password =
                                Configuration.GetSection("DynamicsWcfClient")["Password"];
                            client.ChannelFactory.Credentials.Windows.ClientCredential.Domain =
                                Configuration.GetSection("DynamicsWcfClient")["Domain"];
                            client.ChannelFactory.Credentials.Windows.ClientCredential.UserName =
                                Configuration.GetSection("DynamicsWcfClient")["UserName"];
                            client.ChannelFactory.Credentials.Windows.ClientCredential.Password =
                                Configuration.GetSection("DynamicsWcfClient")["Password"];

                            //Configure Context of Dynamics
                            var callContext = new OrderServiceReference.CallContext
                            {
                                Company = Configuration.GetSection("DynamicsCallContext")["Company"],
                                MessageId = Guid.NewGuid().ToString()
                            };

                            var orderServiceDynamics = await client.CancelOrderServicesAsync(callContext,
                                currentFolioBitacora.ax_service_order, currentFolioBitacora.ReasonCancellationId ?? 0,
                                currentFolioBitacora.ReasonCancellationComment);
                            var dynamicsResponse = orderServiceDynamics.response;
                            if (dynamicsResponse.Trim().Equals("true"))
                            {
                                result = true;
                            }
                            else
                            {
                                response.Item = null;
                                response.Success = false;
                                response.Message = dynamicsResponse;
                                Logger.LogError(dynamicsResponse);
                                return Ok(response);
                            }

                        }

                    }

                    #endregion

                    #region Transaction to insert Bitacora

                    using (var scope = new TransactionScope())
                    {

                        #region UpdateBitacoraStatus

                        if (entity.CancellationFolioStatusId.Equals(3))
                        {
                            if (entity.ServiceTypeId.Equals(1))
                            {
                                using (var bl = new BitacorasBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                                {
                                    var filters = new List<Filter>
                                    {
                                        new Filter
                                        {
                                            Field = "id_bitacora", Value = currentFolioBitacora.id_bitacora.ToString(),
                                            Operator = "Equals"
                                        }
                                    };
                                    result = bl.UpdateWithOutTransaction(filters, itemToSave =>
                                    {
                                        itemToSave.status = "C";
                                        itemToSave.update_date = DateTime.Now;
                                        itemToSave.update_user = User.Identity.Name;
                                    });
                                }
                            }
                        }

                        #endregion


                        #region Update FolioBitacora

                        using (var bl = new BitacorasAxActivationBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                        {
                            var filters = new List<Filter>
                            {
                                new Filter
                                {
                                    Field = "id",
                                    Value = entity.Id.ToString(),
                                    Operator = "Equals"
                                }
                            };

                            switch (entity.CancellationFolioStatusId)
                            {
                                case 3: // Cancelación aprovada
                                    result = bl.UpdateWithOutTransaction(filters, itemtosave =>
                                    {
                                        itemtosave.CancellationFolioStatusId = entity.CancellationFolioStatusId;
                                        itemtosave.ReasonCancellationDate = DateTime.Now;
                                        itemtosave.update_user = User.Identity.Name;
                                        itemtosave.update_date = DateTime.Now;
                                    });
                                    break;
                                case 4: // Cancelación rechazada
                                    result = bl.UpdateWithOutTransaction(filters, itemtosave =>
                                    {
                                        itemtosave.CancellationFolioStatusId = 1;
                                        itemtosave.CancellationRejectedComment = entity.CancellationRejectedComment;
                                        itemtosave.ReasonCancellationDate = DateTime.Now;
                                        itemtosave.update_user = User.Identity.Name;
                                        itemtosave.update_date = DateTime.Now;
                                    });
                                    break;
                                default:
                                    break;
                            }


                        }

                        #endregion

                        #region Send notification to process Email

                        if (result)
                        {
                            if (entity.CancellationFolioStatusId.Equals(4)) // Cancelación rechazada
                            {
                                using (var outBoxHeaderBl = new EmailOutboxBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                                {
                                    var filters = new List<Filter>
                                    {
                                        new Filter {Field = "Id", Value = "0", Operator = "Equals"}
                                    };

                                    var data = new
                                    {
                                        ServiceOrder = currentFolioBitacora.ax_service_order,
                                        Folio = currentFolioBitacora.custody_folio,
                                        CancellationReason = reasoncancellation,
                                        RejectedReason = entity.CancellationRejectedComment
                                    };
                                    var itemToSave = new EmailOutbox
                                    {
                                        ProcessId = int.Parse(Configuration.GetSection("ProccessEmailRejectCancellation").Value),
                                        Data = JsonConvert.SerializeObject(data),
                                        CreateUser = User.Identity.Name,
                                        CreateDate = DateTime.Now,
                                        EmailStatusId = 1
                                    };
                                    result = outBoxHeaderBl.SaveWithOutTransaction(itemToSave, filters);
                                    if (result)
                                    {
                                        emailOutBoxId = itemToSave.Id;
                                    }
                                }

                                using (var outBoxBodyBl = new EmailOuboxEmailsBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                                {
                                    var filters = new List<Filter>
                                    {
                                        new Filter {Field = "Id", Value = "0", Operator = "Equals"}
                                    };
                                    var itemToSave = new EmailOuboxEmails
                                    {
                                        EmailOutboxId = emailOutBoxId,
                                        EmailId = currentFolioBitacora.EmailIdUserRequestCancellation.Value,
                                        TypeId = "T"
                                    };

                                    result = outBoxBodyBl.SaveWithOutTransaction(itemToSave, filters);

                                }
                            }

                            response.Success = true;
                        }
                        else
                        {
                            response.Item = null;
                            response.Success = false;
                            response.Message = Resources.Resource.Generic_Error;
                            Logger.LogError(Resources.Resource.Generic_Error);
                            return Ok(response);
                        }

                        #endregion

                        if (result)
                        {
                            scope.Complete();
                        }
                        else
                        {
                            response.Item = null;
                            response.Success = false;
                            response.Message = Resources.Resource.Generic_Error;
                            Logger.LogError(Resources.Resource.Generic_Error);
                            return Ok(response);
                        }

                    }

                    #endregion

                    #region Send To Mq-Email

                    if (entity.CancellationFolioStatusId.Equals(4)) // Cancelación rechazada
                    {
                        if (result)
                        {
                            using (var sender = new MqSender(Configuration, "Email"))
                            {
                                var message = JsonConvert.SerializeObject(emailOutBoxId);
                                sender.Send(message);
                            }
                            response.Item = currentFolioBitacora;
                            response.Success = true;
                            response.Message = Resources.Resource.Successful_Operation;
                        }
                        else
                        {
                            response.Item = null;
                            response.Success = false;
                            response.Message = Resources.Resource.Generic_Error;
                            Logger.LogError(Resources.Resource.Generic_Error);
                            return Ok(response);
                        }

                    }
                    #endregion

                    #region SendToMqBitacoraStatus

                    var senderMQ = new SenderMqBitacora(Configuration);
                    senderMQ.SendToMqBitacoraStatus(entity.IdBitacora.ToString(), User.Identity.Name);

                    #endregion

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Errors = liErrors;
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpPost("SentToBilling")]
        public async Task<IActionResult> SentToBilling([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(async () =>
           {
               var response = new ResponseAction<ValidationFailure, bitacoras_ax_activation>
               {
                   Items = new List<bitacoras_ax_activation>(),
                   Success = false,
                   Message = Resources.Resource.Generic_Error,
                   Errors = new List<ValidationFailure>()
               };
               var liErrors = new List<ValidationFailure>();
               try
               {
                   var result = false;
                   var bitacoraFolio = (BitacorasAxActivation)JsonConvert.DeserializeObject(dto.ToString(), typeof(BitacorasAxActivation));
                   bitacoras_ax_activation bitacoraBefore = null;

                   #region ValidEvidences

                   if (bitacoraFolio.ServiceTypeId.Equals(1))
                   {
                       using (var bl = new Bl.BitacoraAiLq.BitacoraEvidencesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                       {

                           if (bl.ValidEvidences(bitacoraFolio.IdBitacora) == 0)
                           {
                               response.Success = false;
                               response.Message = Resources.Resource.InvalidNumberOfEvidences;
                               return Ok(response);
                           }


                       }

                       using (var bl = new BitacoraEvidencesPhotosBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                       {
                           var filters = new List<Filter>
                           {
                               new Filter
                               {
                                   Field = "BitacoraId",
                                   Value = bitacoraFolio.IdBitacora.ToString(),
                                   Operator = "Equals"
                               }
                           };
                           if (bl.GetOne(filters) == null)
                           {
                               response.Success = false;
                               response.Message = Resources.Resource.InvalidNumberOfEvidencesPhotos;
                               return Ok(response);
                           }

                       }


                   }

                   #endregion

                   #region Send cancell to Dynamics


                   using (var client = new OrderServiceReference.OrdenesServiciosWebClient(Configuration.GetSection("EndPointOSAx").Value))
                   {


                       // Configure WCF client
                       client.ChannelFactory.Credentials.UserName.UserName =
                           Configuration.GetSection("DynamicsWcfClient")["UserName"];
                       client.ChannelFactory.Credentials.UserName.Password =
                           Configuration.GetSection("DynamicsWcfClient")["Password"];
                       client.ChannelFactory.Credentials.Windows.ClientCredential.Domain =
                           Configuration.GetSection("DynamicsWcfClient")["Domain"];
                       client.ChannelFactory.Credentials.Windows.ClientCredential.UserName =
                           Configuration.GetSection("DynamicsWcfClient")["UserName"];
                       client.ChannelFactory.Credentials.Windows.ClientCredential.Password =
                           Configuration.GetSection("DynamicsWcfClient")["Password"];

                       //Configure Context of Dynamics
                       var callContext = new OrderServiceReference.CallContext
                       {
                           Company = Configuration.GetSection("DynamicsCallContext")["Company"],
                           MessageId = Guid.NewGuid().ToString()
                       };


                       await client.StageChangeAsync(callContext, bitacoraFolio.AxServiceOrder, "Entregado");


                   }



                   #endregion

                   #region Transaction to insert Bitacora                    

                   using (var scope = new TransactionScope())
                   {
                       using (var bl = new BitacorasAxActivationBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                       {
                           var filters = new List<Filter>
                           {
                                new Filter
                                {
                                    Field = "id",
                                    Value = bitacoraFolio.Id.ToString(),
                                    Operator = "Equals"
                                }
                           };

                           result = bl.UpdateWithOutTransaction(filters, itemtosave =>
                           {
                               itemtosave.SentToBilling = bitacoraFolio.SentToBilling;
                               itemtosave.SentToBillingDate = DateTime.Now;
                               itemtosave.update_user = User.Identity.Name;
                               itemtosave.update_date = DateTime.Now;

                           });

                           bitacoraBefore = bl.GetOne(filters);

                           if (result)
                           {
                               response.Message = string.Format(Resources.Resource.Item_Was_Action_With_The_Identificador,
                                                   Resources.Resource.Binnacle, Resources.Resource.Updated, bitacoraBefore.id_bitacora);
                               response.Message += "<br>";
                               Logger.LogInformation(string.Format("Bitacora Folio updated with Id: " + bitacoraFolio.Id));
                               response.Success = true;
                               scope.Complete();
                           }
                           else
                           {
                               response.Success = false;
                               response.Message = Resources.Resource.Generic_Error;
                           }

                       }

                   }

                   #endregion

                   #region SendToMqBitacoraStatus

                   var senderMQ = new SenderMqBitacora(Configuration);
                   senderMQ.SendToMqBitacoraStatus(bitacoraFolio.IdBitacora.ToString(), User.Identity.Name);

                   #endregion

                   //using (var bl =
                   //    new Bl.BitacoraAiLq.BitacorasAxActivationBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                   //{
                   //    var item = bl.GetBitacoraFolioById(bitacoraFolio.IdBitacora.ToString());

                   //    using (var sender = new MqSender(Configuration, "Email"))
                   //    {

                   //        var message = JsonConvert.SerializeObject(item);
                   //        sender.Send(message);
                   //    }
                   //}


                   if (result)
                   {
                       response.Item = bitacoraBefore;
                       response.Success = true;
                       response.Message = Resources.Resource.Successful_Operation;
                   }
                   return Ok(response);
               }
               catch (Exception ex)
               {
                   response.Success = false;
                   response.Message = Resources.Resource.Generic_Error;
                   Log("User: " + User.Identity.Name + "\n" + ex);
                   Logger.LogError(ex.ToString());
                   return Ok(response);
               }
           });
        }

        [HttpPost("GetServiceOrders")]
        public async Task<IActionResult> GetServiceOrders([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    var items = (List<BitacorasAxActivation>)JsonConvert.DeserializeObject(dto.ToString(), typeof(List<BitacorasAxActivation>));

                    using (var bl = new BitacorasAxActivationBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var kendoRequest = new KendoRequest();
                        var filters = new List<Filter>();

                        filters = items.Select(m => new Filter
                        {
                            Field = "id",
                            Value = m.Id.ToString(),
                            Operator = "EQUALS"
                        }).ToList();

                        var orders = new List<KendoSort>();

                        orders.Add(new KendoSort
                        {
                            Field = "create_date",
                            Dir = "DESC"
                        });
                        kendoRequest.Filter = new KendoFilter
                        {
                            Filters = filters,
                            Logic = "Contains"
                        };
                        kendoRequest.Sort = orders;
                        kendoRequest.Page = 1;

                        return Ok(bl.GetListPager(kendoRequest, "OR"));
                    }

                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());

                    return StatusCode(500);
                }

            });
        }


        [HttpGet]
        [Route("GetBitacoraFolioById/{bitacoraId}")]
        public async Task<IActionResult> GetBitacoraFolioById(string bitacoraId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    var id = Encoding.UTF8.GetString(Convert.FromBase64String(bitacoraId));

                    var senderMQ = new SenderMqBitacora(Configuration);
                    senderMQ.SendToMqBitacoraStatus(id, User.Identity.Name);


                    return Ok();

                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

    }
}
