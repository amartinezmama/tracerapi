﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatUnitTypesController
    {

        [HttpGet]
        [Route("SearchByCustom/{customerId}/{value}")]
        public async Task<IActionResult> SearchByCustom(string customerId, string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, UnitTypeEnt>
                {
                    Items = new List<UnitTypeEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatUnitTypesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var customerIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.Search(customerIdDec, searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("SearchOnlyByCustom/{value}")]
        public async Task<IActionResult> SearchOnlyByCustom(string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, UnitTypeEnt>
                {
                    Items = new List<UnitTypeEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatUnitTypesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.SearchOnly(searchDec.Trim());
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get Item from Entity
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("FirstCustom/{unitTypeId}")]
        public async Task<IActionResult> FirstCustom(string unitTypeId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, UnitTypeEnt>
                {
                    Items = new List<UnitTypeEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new CatUnitTypesBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var unitTypeIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(unitTypeId)));
                        response.Item = bl.First(unitTypeIdDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Get Items from Entity
        /// </summary>
        /// <param name="model">Filters and Sorts</param>
        /// <returns></returns>
        [HttpPost("GetGridCustom")]
        public async Task<IActionResult> GetGridCustom([FromBody] KendoRequest model)
        {
            return await Task.Run<IActionResult>(() =>
            {
                try
                {
                    using (var bl = new Bl.BitacoraAiEf.CatUnitTypesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var response = bl.GetGridCustom(model);
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    return StatusCode(500);
                }
            });
        }

        /// <summary>
        /// New Item on Entity
        /// </summary>
        /// <param name="dto">Data to create</param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, UnitTypeEnt>
                {
                    Items = new List<UnitTypeEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                var liErrors = new List<ValidationFailure>();
                try
                {
                    bool result;
                    var entity =
                        (UnitTypeEnt) JsonConvert.DeserializeObject(dto.ToString(), typeof(UnitTypeEnt));

                    using (var bl =
                        new Bl.BitacoraAiEf.CatUnitTypesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var errors = new List<string>();
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "description", Value = entity.Name, Operator = "Equals"}
                        };
                        result = bl.Save(new cat_unit_types
                        {
                            description = entity.Name,
                            active = true,
                            user_mod = User.Identity.Name,
                            create_date = DateTime.Now,
                            unitsallow = entity.UnitsAllow,
                            minunits = 0,
                            isvalid = false
                        }, filters, errors);
                        if (result)
                        {
                            liErrors.Add(new ValidationFailure("Name", errors.First()));
                        }
                    }

                    response.Errors = liErrors;

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Update Item on Entity
        /// </summary>
        /// <param name="dto">Data to update</param>
        /// <returns></returns>
        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, UnitTypeEnt>
                {
                    Items = new List<UnitTypeEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Errors = new List<ValidationFailure>()
                };
                try
                {
                    bool result;
                    var entity =
                        (UnitTypeEnt) JsonConvert.DeserializeObject(dto.ToString(), typeof(UnitTypeEnt));

                    using (var bl =
                        new Bl.BitacoraAiEf.CatUnitTypesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "id_unit_type", Value = entity.Id.ToString(), Operator = "Equals"}
                        };
                        result = bl.Update(filters, itemToSave =>
                        {
                            itemToSave.description = entity.Name;
                            itemToSave.user_mod = User.Identity.Name;
                            itemToSave.mod_date = DateTime.Now;
                            itemToSave.unitsallow = entity.UnitsAllow;
                        });
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Delete one Item
        /// </summary>
        /// <param name="dto">Item if Entity</param>
        /// <returns></returns>
        [HttpPost("Delete")]
        public async Task<IActionResult> Delete([FromBody] dynamic dto)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, UnitTypeEnt>
                {
                    Items = new List<UnitTypeEnt>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    bool result;
                    var entity =
                        (UnitTypeEnt) JsonConvert.DeserializeObject(dto.ToString(), typeof(UnitTypeEnt));

                    using (var bl =
                        new Bl.BitacoraAiEf.CatUnitTypesBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        var filters = new List<Filter>
                        {
                            new Filter {Field = "id_unit_type", Value = entity.Id.ToString(), Operator = "Equals"}
                        };
                        result = bl.Update(filters, itemToSave =>
                        {
                            itemToSave.active = true;
                            itemToSave.user_mod = User.Identity.Name;
                            itemToSave.mod_date = DateTime.Now;
                        });
                    }

                    if (result)
                    {
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                    }

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Log("User: " + User.Identity.Name + "\n" + ex);
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

    }
}