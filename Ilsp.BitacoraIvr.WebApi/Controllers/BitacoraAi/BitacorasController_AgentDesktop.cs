﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.AgentDesktop;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Ilsp.BitacoraIvr.WebApi.Mq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class BitacorasController
    {

        [HttpGet]
        [Route("GetPhonesByIlspContactId/{id}/{type}")]
        public async Task<IActionResult> GetPhonesByIlspContactId(string id, string type)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, ContactPhoneEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Items = bl.GetPhonesByIlspContactId(id, type);
                        response.Message = Resources.Resource.Successful_Operation;
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("GetApplicationConfiguration/{id}")]
        public async Task<IActionResult> GetApplicationConfiguration(string id)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, ApplicationConfigurationEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Item = bl.GetApplicationConfiguration(int.Parse(id));
                        response.Message = Resources.Resource.Successful_Operation;
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("GetScript/{appId}/{serviceId}/{type}/{situation}")]
        public async Task<IActionResult> GetScript(int appId, string serviceId, string type, string situation)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, string>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Item = bl.GetScript(appId, serviceId, type, situation);
                        response.Message = Resources.Resource.Successful_Operation;
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("GetScript/{appId}/{serviceId}/{type}/")]
        public async Task<IActionResult> GetScript(int appId, string serviceId, string type)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, string>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Item = bl.GetScript(appId, serviceId, type, null);
                        response.Message = Resources.Resource.Successful_Operation;
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("GetPhone/{phone}")]
        public async Task<IActionResult> GetPhone(string phone)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, string>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Item = bl.GetPhone(phone);
                        response.Message = Resources.Resource.Successful_Operation;
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("CanCloseContact/{contactId}/{type}")]
        public async Task<IActionResult> CanCloseContact(string contactId, string type)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, bool>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        var bitacoraIdDec = Encoding.UTF8.GetString(Convert.FromBase64String(contactId));
                        response.Item = bl.CanCloseContact(bitacoraIdDec, type);
                        response.Message = Resources.Resource.Successful_Operation;
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("ClearCall/{bitacoraId}")]
        public async Task<IActionResult> ClearCall(string bitacoraId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, bool>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error,
                    Item = false
                };

                try
                {
                    using (var sender = new MqSender(Configuration, "Mq"))
                    {
                        var message = bitacoraId;
                        sender.Send(message, "clear.call");
                        response.Item = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        response.Success = true;
                        return Ok(response);
                    }

                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }
        [HttpGet]
        [Route("SituationRequiredCall/{situation}")]
        public async Task<IActionResult> SituationRequiredCall(string situation)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, bool>
                {
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasMonitoringBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Item = bl.SituationRequiredCall(situation);
                        response.Message = Resources.Resource.Successful_Operation;
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        [HttpGet]
        [Route("SituationAutomatedCall/{bitacoraId}/{situation}")]
        public async Task<IActionResult> SituationRequiredCall(string bitacoraId, string situation)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, spGetAutomatedScriptResult>
                {
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new BitacorasMonitoringBl(GetAdoBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Item = bl.SituationAutomatedCall(bitacoraId, situation);
                        response.Message = Resources.Resource.Successful_Operation;
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

    }
}
