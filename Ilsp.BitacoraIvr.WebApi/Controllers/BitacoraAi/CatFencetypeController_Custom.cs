﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.BitacoraAi
{
    public partial class CatFencetypeController
    {
        [HttpGet]
        [Route("GetAllCustom")]
        public async Task<IActionResult> GetAllCustom()
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, FenceTypeEnt>
                {
                    Items = new List<FenceTypeEnt>(),
                    Success = false
                };
                try
                {
                    using (var bl = new CatFencetypeBl(GetEfBitacoraAiConnection(), User.Identity.Name))
                    {
                        response.Items = bl.GetAll().Select(item => new FenceTypeEnt
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Description = item.Description
                        }).ToArray();
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }
    }
}
