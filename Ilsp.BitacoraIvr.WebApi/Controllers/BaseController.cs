﻿using System;
using System.Configuration;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.Db;
using Ilsp.BitacoraIvr.Ent.RethinkDb;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers
{
    
    public class BaseController : Controller
    {
        protected readonly IConfiguration Configuration;
        protected ILogger Logger;
        protected IHttpContextAccessor _accessor;

        public BaseController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor)
        {
            Configuration = configuration;
            _accessor = accessor;
        }

        public BitacoraAiDataContext GetDbConnection()
        {
            return new BitacoraAiDataContext(GetAdoBitacoraAiConnection());
        }

        public string GetEfBitacoraAiConnection()
        {
            return Configuration.GetConnectionString("BitacoraAiEf");
        }

        public string GetAdoBitacoraAiConnection()
        {
            return Configuration.GetConnectionString("AdoBitacoraAi");
        }

        public string GetOntimeAiConnection()
        {
            return Configuration.GetConnectionString("OnTimeConnection");
        }

        public string GetIdentityConnection()
        {
            return Configuration.GetConnectionString("IdentityEf");
        }

        public string GetAdoIdentityConnection()
        {
            return Configuration.GetConnectionString("IdentityConnection");
        }

        public RethinkConnection GetRethinkConnection()
        {
            return new RethinkConnection
            {
                HostName = Configuration.GetValue<string>("RethinkDb.HostName"),
                DbName = Configuration.GetValue<string>("RethinkDb.DataBase"),
                Port = Convert.ToInt32(Configuration.GetValue<string>("RethinkDb.Port")),
                UserName = Configuration.GetValue<string>("RethinkDb.UserName"),
                Password = Configuration.GetValue<string>("RethinkDb.Password")
            };
        }

        public string GetGeocodingPostgreConnection()
        {
            return Configuration.GetConnectionString("GeocodingPostgreConnection");
        }

        /// <summary>
        /// Insert error on DB
        /// </summary>
        /// <returns></returns>
        public void Log(string error)
        {
            try
            {
                using (var db = new BitacoraAiDataContext(GetAdoBitacoraAiConnection()))
                {
                    db.sp_log_errors("API IVR", error);
                }
            }
            catch
            {
                // ignored
            }
        }
    }
}
