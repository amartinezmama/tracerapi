﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.OntimeAiLq;
using Ilsp.BitacoraIvr.Ent.OntimeAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.OntimeAi
{
    [Route("api/[controller]")]
    [Authorize]
    public class ShipmentsController : BaseController
    {

        public ShipmentsController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor) : base(configuration, loggerFactory, accessor)
        {
            Logger = loggerFactory.CreateLogger<ShipmentsController>();
        }

        [HttpGet]
        [Route("SearchByCustom/{customerId}/{value}")]
        public async Task<IActionResult> SearchByCustom(string customerId, string value)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, Shipment>
                {
                    Items = new List<Shipment>(),
                    Success = false,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new ShipmentsBl(GetOntimeAiConnection(), User.Identity.Name))
                    {
                        var customerIdDec = int.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(customerId)));
                        var searchDec = Encoding.UTF8.GetString(Convert.FromBase64String(value));
                        response.Items = bl.SearchDeliveryNumber(customerIdDec, searchDec);
                        response.Success = true;
                        response.Message = Resources.Resource.Successful_Operation;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

        /// <summary>
        /// Valid if Shipment Number is assigned
        /// </summary>
        /// <param name="shipmentnumber">Shipment Number</param>
        /// <param name="bitacoraid">Bitacora</param>
        /// <returns>true or false</returns>
        [HttpGet]
        [Route("ValidAssignation/{shipmentnumber}/{bitacoraid}")]
        public async Task<IActionResult> ValidShipmentNumberIsAssigned(string shipmentnumber, string bitacoraid)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, Shipment>
                {
                    Errors = new List<ValidationFailure>(),
                    Items = new List<Shipment>(),
                    Success = true,
                    Message = Resources.Resource.Generic_Error
                };
                try
                {
                    using (var bl = new ShipmentsBl(GetOntimeAiConnection(), User.Identity.Name))
                    {
                        var shipmentnumberDec = Encoding.UTF8.GetString(Convert.FromBase64String(shipmentnumber));
                        var bitacoraidDec = long.Parse(Encoding.UTF8.GetString(Convert.FromBase64String(bitacoraid)));
                        response.Success = bl.ValidShipmentNumberAssigned(shipmentnumberDec, bitacoraidDec);
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.ToString());
                    response.Message = Resources.Resource.Generic_Error;
                    return Ok(response);
                }
            });
        }

    }
}
