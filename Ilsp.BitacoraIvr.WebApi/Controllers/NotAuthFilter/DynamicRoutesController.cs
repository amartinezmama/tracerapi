﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiEf;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.WebApi.Dto;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Ilsp.BitacoraIvr.WebApi.Controllers.NotAuthFilter
{
    [Route("api/[controller]")]
    public class DynamicRoutesController : BaseController
    {
        public DynamicRoutesController(IConfiguration configuration, ILoggerFactory loggerFactory, IHttpContextAccessor accessor) : base(configuration, loggerFactory, accessor)
        {
            Logger = loggerFactory.CreateLogger<DynamicRoutesController>();
        }

        /// <summary>
        /// Get Dynamic Routes
        /// </summary>
        /// <param name="bitacoraId">Alias</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDynamicRoutes/{bitacoraId}")]
        public async Task<IActionResult> GetDynamicRoutes(string bitacoraId)
        {
            return await Task.Run<IActionResult>(() =>
            {
                var response = new ResponseAction<ValidationFailure, BitacoraDynamicRoutesEnt>
                {
                    Errors = new List<ValidationFailure>(),
                    Success = false
                };
                try
                {
                    using (var bl = new VwDynamicRoutesBl(GetEfBitacoraAiConnection(), "system"))
                    {
                        var bitacoraIdDec = Encoding.UTF8.GetString(Convert.FromBase64String(bitacoraId));
                        response.Item = bl.GetByBitacora(bitacoraIdDec);
                        response.Message = Resources.Resource.Successful_Operation;
                        response.Success = true;
                        return Ok(response);
                    }
                }
                catch (Exception ex)
                {
                    response.Message = Resources.Resource.Generic_Error;
                    Logger.LogError(ex.ToString());
                    return Ok(response);
                }
            });
        }

    }
}
