﻿using System.Collections.Generic;
using Ilsp.BitacoraIvr.WebApi.Mq;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Ilsp.BitacoraIvr.WebApi.Utilities
{
    public class SenderMqBitacora
    {
        protected readonly IConfiguration Configuration;

        public SenderMqBitacora(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public string GetAdoBitacoraAiConnection()
        {
            return Configuration.GetConnectionString("AdoBitacoraAi");
        }

        public void SendToMqBitacoraStatus(string bitacoraId, string userName)
        {
            using (var bl =
                new Bl.BitacoraAiLq.BitacorasAxActivationBl(GetAdoBitacoraAiConnection(), userName))
            {
                

                var item = bl.GetBitacoraFolioById(bitacoraId);

                using (var sender = new MqSender(Configuration, "BitacoraStatus"))
                {
                    var hubMessage = new BitacoraFolioStatus
                    {
                        Hub = "changes", ClientMethod = "foliosChangesReceived",
                        Groups = new List<string>() {"allClients"}
                    };
                    hubMessage.Data = item;
                    var message = JsonConvert.SerializeObject(hubMessage);
                    sender.Send(message, "notification.message");
                }
            }
        }
    }
}
