﻿using System.Collections.Generic;

namespace Ilsp.BitacoraIvr.WebApi.Utilities
{
    public class BitacoraFolioStatus
    {

        public string Hub { get; set; }

        public string ClientMethod { get; set; }

        public List<string> Groups { get; set; }

        public object Data { get; set; }


    }
}
