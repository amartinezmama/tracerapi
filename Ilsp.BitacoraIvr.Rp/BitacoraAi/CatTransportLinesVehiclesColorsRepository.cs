﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatTransportLinesVehiclesColorsRepository : EfRepository<cat_transport_lines_vehicles_colors>
	{
		public CatTransportLinesVehiclesColorsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatTransportLinesVehiclesColorsRepository() : base()
		{
		}
	}
}
