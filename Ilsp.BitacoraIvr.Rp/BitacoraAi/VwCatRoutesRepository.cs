﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class VwCatRoutesRepository : EfRepository<vw_cat_routes>
	{
		public VwCatRoutesRepository(DbContext ctx) : base(ctx)
		{
		}
		public VwCatRoutesRepository() : base()
		{
		}
	}
}
