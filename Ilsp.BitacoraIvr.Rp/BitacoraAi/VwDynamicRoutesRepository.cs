﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class VwDynamicRoutesRepository : EfRepository<vw_DynamicRoutes>
	{
		public VwDynamicRoutesRepository(DbContext ctx) : base(ctx)
		{
		}
		public VwDynamicRoutesRepository() : base()
		{
		}
	}
}
