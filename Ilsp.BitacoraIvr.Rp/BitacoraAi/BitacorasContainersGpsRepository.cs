﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasContainersGpsRepository : EfRepository<bitacoras_containers_gps>
	{
		public BitacorasContainersGpsRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasContainersGpsRepository() : base()
		{
		}
	}
}
