﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacoraEvidencesRepository : EfRepository<BitacoraEvidences>
	{
		public BitacoraEvidencesRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacoraEvidencesRepository() : base()
		{
		}
	}
}
