﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatCustomersTransportLinesRepository : EfRepository<cat_customers_transport_lines>
	{
		public CatCustomersTransportLinesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatCustomersTransportLinesRepository() : base()
		{
		}
	}
}
