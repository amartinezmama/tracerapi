﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatColorsRepository : EfRepository<cat_colors>
	{
		public CatColorsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatColorsRepository() : base()
		{
		}
	}
}
