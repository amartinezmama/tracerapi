﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatSecurityKitsControlPointsRepository : EfRepository<cat_security_kits_control_points>
	{
		public CatSecurityKitsControlPointsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatSecurityKitsControlPointsRepository() : base()
		{
		}
	}
}
