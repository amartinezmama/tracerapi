﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class VwCatOriginDestinationsRepository : EfRepository<vw_cat_origin_destinations>
	{
		public VwCatOriginDestinationsRepository(DbContext ctx) : base(ctx)
		{
		}
		public VwCatOriginDestinationsRepository() : base()
		{
		}
	}
}
