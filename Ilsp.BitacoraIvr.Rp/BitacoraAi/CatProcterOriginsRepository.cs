﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatProcterOriginsRepository : EfRepository<cat_procter_origins>
	{
		public CatProcterOriginsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatProcterOriginsRepository() : base()
		{
		}
	}
}
