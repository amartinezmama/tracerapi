﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasCustodiesVehicleRepository : EfRepository<bitacoras_custodies_vehicle>
	{
		public BitacorasCustodiesVehicleRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasCustodiesVehicleRepository() : base()
		{
		}
	}
}
