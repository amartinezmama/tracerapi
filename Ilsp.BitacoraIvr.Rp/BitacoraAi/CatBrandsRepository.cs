﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatBrandsRepository : EfRepository<cat_brands>
	{
		public CatBrandsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatBrandsRepository() : base()
		{
		}
	}
}
