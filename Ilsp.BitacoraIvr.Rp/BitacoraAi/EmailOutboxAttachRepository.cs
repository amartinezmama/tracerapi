﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class EmailOutboxAttachRepository : EfRepository<EmailOutboxAttach>
	{
		public EmailOutboxAttachRepository(DbContext ctx) : base(ctx)
		{
		}
		public EmailOutboxAttachRepository() : base()
		{
		}
	}
}
