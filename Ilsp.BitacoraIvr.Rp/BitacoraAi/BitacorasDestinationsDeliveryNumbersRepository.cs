﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasDestinationsDeliveryNumbersRepository : EfRepository<bitacoras_destinations_delivery_numbers>
	{
		public BitacorasDestinationsDeliveryNumbersRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasDestinationsDeliveryNumbersRepository() : base()
		{
		}
	}
}
