﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatCusTransPhonesRepository : EfRepository<CatCusTransPhones>
	{
		public CatCusTransPhonesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatCusTransPhonesRepository() : base()
		{
		}
	}
}
