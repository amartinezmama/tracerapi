﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasRoutesRepository : EfRepository<BitacorasRoutes>
	{
		public BitacorasRoutesRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasRoutesRepository() : base()
		{
		}
	}
}
