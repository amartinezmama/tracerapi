﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasAlertsRepository : EfRepository<bitacoras_alerts>
	{
		public BitacorasAlertsRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasAlertsRepository() : base()
		{
		}
	}
}
