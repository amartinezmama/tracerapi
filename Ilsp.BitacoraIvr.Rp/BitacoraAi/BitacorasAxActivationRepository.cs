﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasAxActivationRepository : EfRepository<bitacoras_ax_activation>
	{
		public BitacorasAxActivationRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasAxActivationRepository() : base()
		{
		}
	}
}
