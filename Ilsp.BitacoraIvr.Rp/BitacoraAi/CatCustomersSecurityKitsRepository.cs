﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatCustomersSecurityKitsRepository : EfRepository<cat_customers_security_kits>
	{
		public CatCustomersSecurityKitsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatCustomersSecurityKitsRepository() : base()
		{
		}
	}
}
