﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class EmailOuboxEmailsRepository : EfRepository<EmailOuboxEmails>
	{
		public EmailOuboxEmailsRepository(DbContext ctx) : base(ctx)
		{
		}
		public EmailOuboxEmailsRepository() : base()
		{
		}
	}
}
