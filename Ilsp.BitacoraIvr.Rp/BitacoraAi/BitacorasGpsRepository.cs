﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasGpsRepository : EfRepository<bitacoras_gps>
	{
		public BitacorasGpsRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasGpsRepository() : base()
		{
		}
	}
}
