﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatCustomerFieldsRepository : EfRepository<CatCustomerFields>
	{
		public CatCustomerFieldsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatCustomerFieldsRepository() : base()
		{
		}
	}
}
