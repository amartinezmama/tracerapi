﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatCustomersRepository : EfRepository<cat_customers>
	{
		public CatCustomersRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatCustomersRepository() : base()
		{
		}
	}
}
