﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatSecurityKitsGeoreferencesRepository : EfRepository<cat_security_kits_georeferences>
	{
		public CatSecurityKitsGeoreferencesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatSecurityKitsGeoreferencesRepository() : base()
		{
		}
	}
}
