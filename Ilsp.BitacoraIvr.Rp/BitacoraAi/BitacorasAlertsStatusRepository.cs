﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasAlertsStatusRepository : EfRepository<bitacoras_alerts_status>
	{
		public BitacorasAlertsStatusRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasAlertsStatusRepository() : base()
		{
		}
	}
}
