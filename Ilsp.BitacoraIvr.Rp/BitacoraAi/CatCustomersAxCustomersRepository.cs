﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatCustomersAxCustomersRepository : EfRepository<cat_customers_ax_customers>
	{
		public CatCustomersAxCustomersRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatCustomersAxCustomersRepository() : base()
		{
		}
	}
}
