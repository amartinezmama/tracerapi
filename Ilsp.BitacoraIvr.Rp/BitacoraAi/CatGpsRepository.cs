﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatGpsRepository : EfRepository<cat_gps>
	{
		public CatGpsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatGpsRepository() : base()
		{
		}
	}
}
