﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatOperativeKeysRepository : EfRepository<cat_operative_keys>
	{
		public CatOperativeKeysRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatOperativeKeysRepository() : base()
		{
		}
	}
}
