﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatCustodianTypesRepository : EfRepository<cat_custodian_types>
	{
		public CatCustodianTypesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatCustodianTypesRepository() : base()
		{
		}
	}
}
