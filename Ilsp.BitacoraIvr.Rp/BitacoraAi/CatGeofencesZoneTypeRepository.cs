﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatGeofencesZoneTypeRepository : EfRepository<cat_geofences_zone_type>
	{
		public CatGeofencesZoneTypeRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatGeofencesZoneTypeRepository() : base()
		{
		}
	}
}
