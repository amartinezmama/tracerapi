﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatRegExpressionsRepository : EfRepository<CatRegExpressions>
	{
		public CatRegExpressionsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatRegExpressionsRepository() : base()
		{
		}
	}
}
