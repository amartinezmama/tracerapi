﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class VwCatRoutesIvrRepository : EfRepository<vw_cat_routes_ivr>
	{
		public VwCatRoutesIvrRepository(DbContext ctx) : base(ctx)
		{
		}
		public VwCatRoutesIvrRepository() : base()
		{
		}
	}
}
