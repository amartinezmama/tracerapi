﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatSituationsRepository : EfRepository<cat_situations>
	{
		public CatSituationsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatSituationsRepository() : base()
		{
		}
	}
}
