﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class EmailOutboxRepository : EfRepository<EmailOutbox>
	{
		public EmailOutboxRepository(DbContext ctx) : base(ctx)
		{
		}
		public EmailOutboxRepository() : base()
		{
		}
	}
}
