﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasTransportationsRepository : EfRepository<bitacoras_transportations>
	{
		public BitacorasTransportationsRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasTransportationsRepository() : base()
		{
		}
	}
}
