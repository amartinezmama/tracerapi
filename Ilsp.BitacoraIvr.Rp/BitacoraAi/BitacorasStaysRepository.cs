﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasStaysRepository : EfRepository<BitacorasStays>
	{
		public BitacorasStaysRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasStaysRepository() : base()
		{
		}
	}
}
