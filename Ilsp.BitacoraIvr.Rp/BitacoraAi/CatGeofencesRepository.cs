﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatGeofencesRepository : EfRepository<cat_geofences>
	{
		public CatGeofencesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatGeofencesRepository() : base()
		{
		}
	}
}
