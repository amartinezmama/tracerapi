﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class VwCatSecurityKitsRepository : EfRepository<vw_cat_security_kits>
	{
		public VwCatSecurityKitsRepository(DbContext ctx) : base(ctx)
		{
		}
		public VwCatSecurityKitsRepository() : base()
		{
		}
	}
}
