﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatEmailStatusRepository : EfRepository<CatEmailStatus>
	{
		public CatEmailStatusRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatEmailStatusRepository() : base()
		{
		}
	}
}
