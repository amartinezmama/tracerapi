﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatAlertsRepository : EfRepository<cat_alerts>
	{
		public CatAlertsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatAlertsRepository() : base()
		{
		}
	}
}
