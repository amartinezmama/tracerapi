﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatCustomersOriginsDestinationsRepository : EfRepository<cat_customers_origins_destinations>
	{
		public CatCustomersOriginsDestinationsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatCustomersOriginsDestinationsRepository() : base()
		{
		}
	}
}
