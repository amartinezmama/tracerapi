﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class VwCatTransportLinesVehiclesRepository : EfRepository<vw_cat_transport_lines_vehicles>
	{
		public VwCatTransportLinesVehiclesRepository(DbContext ctx) : base(ctx)
		{
		}
		public VwCatTransportLinesVehiclesRepository() : base()
		{
		}
	}
}
