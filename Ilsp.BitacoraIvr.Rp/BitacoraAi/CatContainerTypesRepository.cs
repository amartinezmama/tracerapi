﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatContainerTypesRepository : EfRepository<cat_container_types>
	{
		public CatContainerTypesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatContainerTypesRepository() : base()
		{
		}
	}
}
