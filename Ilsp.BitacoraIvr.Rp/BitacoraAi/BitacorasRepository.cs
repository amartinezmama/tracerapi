﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasRepository : EfRepository<bitacoras>
	{
		public BitacorasRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasRepository() : base()
		{
		}
	}
}
