﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatCustomersUsersRepository : EfRepository<cat_customers_users>
	{
		public CatCustomersUsersRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatCustomersUsersRepository() : base()
		{
		}
	}
}
