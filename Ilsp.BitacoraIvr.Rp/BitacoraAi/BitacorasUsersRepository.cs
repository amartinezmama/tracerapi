﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasUsersRepository : EfRepository<bitacoras_users>
	{
		public BitacorasUsersRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasUsersRepository() : base()
		{
		}
	}
}
