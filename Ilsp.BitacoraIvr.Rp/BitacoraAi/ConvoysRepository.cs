﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class ConvoysRepository : EfRepository<convoys>
	{
		public ConvoysRepository(DbContext ctx) : base(ctx)
		{
		}
		public ConvoysRepository() : base()
		{
		}
	}
}
