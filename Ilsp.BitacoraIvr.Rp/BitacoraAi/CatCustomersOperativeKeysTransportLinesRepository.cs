﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatCustomersOperativeKeysTransportLinesRepository : EfRepository<cat_customers_operative_keys_transport_lines>
	{
		public CatCustomersOperativeKeysTransportLinesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatCustomersOperativeKeysTransportLinesRepository() : base()
		{
		}
	}
}
