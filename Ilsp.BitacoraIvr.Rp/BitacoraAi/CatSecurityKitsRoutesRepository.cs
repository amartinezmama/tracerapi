﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatSecurityKitsRoutesRepository : EfRepository<cat_security_kits_routes>
	{
		public CatSecurityKitsRoutesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatSecurityKitsRoutesRepository() : base()
		{
		}
	}
}
