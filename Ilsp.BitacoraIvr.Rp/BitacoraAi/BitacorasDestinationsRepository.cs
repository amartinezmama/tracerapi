﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasDestinationsRepository : EfRepository<bitacoras_destinations>
	{
		public BitacorasDestinationsRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasDestinationsRepository() : base()
		{
		}
	}
}
