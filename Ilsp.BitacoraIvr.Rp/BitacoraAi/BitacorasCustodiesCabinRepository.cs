﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasCustodiesCabinRepository : EfRepository<bitacoras_custodies_cabin>
	{
		public BitacorasCustodiesCabinRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasCustodiesCabinRepository() : base()
		{
		}
	}
}
