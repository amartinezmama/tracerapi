﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatCatalogsCommonRepository : EfRepository<cat_catalogs_common>
	{
		public CatCatalogsCommonRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatCatalogsCommonRepository() : base()
		{
		}
	}
}
