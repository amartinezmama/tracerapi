﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatStatusRepository : EfRepository<cat_status>
	{
		public CatStatusRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatStatusRepository() : base()
		{
		}
	}
}
