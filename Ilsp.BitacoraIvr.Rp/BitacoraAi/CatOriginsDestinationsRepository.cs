﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatOriginsDestinationsRepository : EfRepository<cat_origins_destinations>
	{
		public CatOriginsDestinationsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatOriginsDestinationsRepository() : base()
		{
		}
	}
}
