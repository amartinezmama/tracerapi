﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasContainersRepository : EfRepository<bitacoras_containers>
	{
		public BitacorasContainersRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasContainersRepository() : base()
		{
		}
	}
}
