﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatSecurityKitsRepository : EfRepository<cat_security_kits>
	{
		public CatSecurityKitsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatSecurityKitsRepository() : base()
		{
		}
	}
}
