﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatCustomersTransportLineRepository : EfRepository<cat_customers_transport_line>
	{
		public CatCustomersTransportLineRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatCustomersTransportLineRepository() : base()
		{
		}
	}
}
