﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatMonitoringTypesRepository : EfRepository<cat_monitoring_types>
	{
		public CatMonitoringTypesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatMonitoringTypesRepository() : base()
		{
		}
	}
}
