﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class VwCatGeofencesIvrRepository : EfRepository<vw_cat_geofences_ivr>
	{
		public VwCatGeofencesIvrRepository(DbContext ctx) : base(ctx)
		{
		}
		public VwCatGeofencesIvrRepository() : base()
		{
		}
	}
}
