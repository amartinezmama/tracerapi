﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatUnitTypesRepository : EfRepository<cat_unit_types>
	{
		public CatUnitTypesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatUnitTypesRepository() : base()
		{
		}
	}
}
