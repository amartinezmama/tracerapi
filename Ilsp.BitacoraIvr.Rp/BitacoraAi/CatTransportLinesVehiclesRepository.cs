﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatTransportLinesVehiclesRepository : EfRepository<cat_transport_lines_vehicles>
	{
		public CatTransportLinesVehiclesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatTransportLinesVehiclesRepository() : base()
		{
		}
	}
}
