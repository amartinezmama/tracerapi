﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatEmailsTemplatesRepository : EfRepository<CatEmailsTemplates>
	{
		public CatEmailsTemplatesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatEmailsTemplatesRepository() : base()
		{
		}
	}
}
