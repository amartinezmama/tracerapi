﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatLogisticsOperatorsRepository : EfRepository<cat_logistics_operators>
	{
		public CatLogisticsOperatorsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatLogisticsOperatorsRepository() : base()
		{
		}
	}
}
