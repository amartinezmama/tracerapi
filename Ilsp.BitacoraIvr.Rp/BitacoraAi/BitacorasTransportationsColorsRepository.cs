﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasTransportationsColorsRepository : EfRepository<bitacoras_transportations_colors>
	{
		public BitacorasTransportationsColorsRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasTransportationsColorsRepository() : base()
		{
		}
	}
}
