﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasTransportationsOperatorsRepository : EfRepository<bitacoras_transportations_operators>
	{
		public BitacorasTransportationsOperatorsRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasTransportationsOperatorsRepository() : base()
		{
		}
	}
}
