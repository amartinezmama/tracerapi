﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatMonitoringFrecuenciesRepository : EfRepository<cat_monitoring_frecuencies>
	{
		public CatMonitoringFrecuenciesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatMonitoringFrecuenciesRepository() : base()
		{
		}
	}
}
