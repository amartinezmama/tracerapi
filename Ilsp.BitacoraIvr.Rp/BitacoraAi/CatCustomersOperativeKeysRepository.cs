﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatCustomersOperativeKeysRepository : EfRepository<cat_customers_operative_keys>
	{
		public CatCustomersOperativeKeysRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatCustomersOperativeKeysRepository() : base()
		{
		}
	}
}
