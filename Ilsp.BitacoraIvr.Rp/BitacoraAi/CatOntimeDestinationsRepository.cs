﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatOntimeDestinationsRepository : EfRepository<CatOntimeDestinations>
	{
		public CatOntimeDestinationsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatOntimeDestinationsRepository() : base()
		{
		}
	}
}
