﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasTransportationsGpsRepository : EfRepository<bitacoras_transportations_gps>
	{
		public BitacorasTransportationsGpsRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasTransportationsGpsRepository() : base()
		{
		}
	}
}
