﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatServiceTypesRepository : EfRepository<cat_service_types>
	{
		public CatServiceTypesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatServiceTypesRepository() : base()
		{
		}
	}
}
