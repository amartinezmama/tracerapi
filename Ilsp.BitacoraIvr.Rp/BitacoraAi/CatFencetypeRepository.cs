﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatFencetypeRepository : EfRepository<cat_fencetype>
	{
		public CatFencetypeRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatFencetypeRepository() : base()
		{
		}
	}
}
