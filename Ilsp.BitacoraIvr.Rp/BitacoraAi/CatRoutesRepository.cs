﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatRoutesRepository : EfRepository<cat_routes>
	{
		public CatRoutesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatRoutesRepository() : base()
		{
		}
	}
}
