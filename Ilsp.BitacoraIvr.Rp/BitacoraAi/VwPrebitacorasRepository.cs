﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class VwPrebitacorasRepository : EfRepository<vw_prebitacoras>
	{
		public VwPrebitacorasRepository(DbContext ctx) : base(ctx)
		{
		}
		public VwPrebitacorasRepository() : base()
		{
		}
	}
}
