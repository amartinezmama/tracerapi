﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatEmailsRepository : EfRepository<CatEmails>
	{
		public CatEmailsRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatEmailsRepository() : base()
		{
		}
	}
}
