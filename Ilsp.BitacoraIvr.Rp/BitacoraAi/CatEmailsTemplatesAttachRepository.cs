﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatEmailsTemplatesAttachRepository : EfRepository<CatEmailsTemplatesAttach>
	{
		public CatEmailsTemplatesAttachRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatEmailsTemplatesAttachRepository() : base()
		{
		}
	}
}
