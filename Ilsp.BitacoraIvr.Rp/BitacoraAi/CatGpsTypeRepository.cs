﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatGpsTypeRepository : EfRepository<cat_gps_type>
	{
		public CatGpsTypeRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatGpsTypeRepository() : base()
		{
		}
	}
}
