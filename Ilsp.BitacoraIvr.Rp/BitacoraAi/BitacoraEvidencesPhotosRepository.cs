﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacoraEvidencesPhotosRepository : EfRepository<BitacoraEvidencesPhotos>
	{
		public BitacoraEvidencesPhotosRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacoraEvidencesPhotosRepository() : base()
		{
		}
	}
}
