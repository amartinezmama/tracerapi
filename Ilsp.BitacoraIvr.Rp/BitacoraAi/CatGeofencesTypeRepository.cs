﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatGeofencesTypeRepository : EfRepository<cat_geofences_type>
	{
		public CatGeofencesTypeRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatGeofencesTypeRepository() : base()
		{
		}
	}
}
