﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class CatServicePrioritiesRepository : EfRepository<cat_service_priorities>
	{
		public CatServicePrioritiesRepository(DbContext ctx) : base(ctx)
		{
		}
		public CatServicePrioritiesRepository() : base()
		{
		}
	}
}
