﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasTransportationsStampsRepository : EfRepository<bitacoras_transportations_stamps>
	{
		public BitacorasTransportationsStampsRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasTransportationsStampsRepository() : base()
		{
		}
	}
}
