﻿using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.BitacoraAi
{
	public partial class BitacorasCustodiesRepository : EfRepository<bitacoras_custodies>
	{
		public BitacorasCustodiesRepository(DbContext ctx) : base(ctx)
		{
		}
		public BitacorasCustodiesRepository() : base()
		{
		}
	}
}
