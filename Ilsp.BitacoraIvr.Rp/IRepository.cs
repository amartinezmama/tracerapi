﻿using Ilsp.BitacoraIvr.Ent;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Ilsp.BitacoraIvr.Rp
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity GetFirst(Expression<Func<TEntity, bool>> predicate, string[] includes = null);
        TEntity GetFirstOrDefault(Expression<Func<TEntity, bool>> predicate, string include = null);
        IEnumerable<TEntity> GetAll(string[] includes = null);
        IEnumerable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate, string order, string[] includes = null);
        ResponseAction<TEntity> GetListPager(Expression<Func<TEntity, bool>> predicate, string order, int skip, int take, string[] includes = null);
        List<TEntity> GetListPager(Expression<Func<TEntity, bool>> predicate, string order, string[] includes = null);
        bool Save(TEntity entity);
        bool SaveWithOutTransaction(TEntity entity);
        bool Update(TEntity entity, Action<TEntity> updateAction);
        bool UpdateWithOutTransaction(TEntity entity, Action<TEntity> updateAction);
        bool Delete(TEntity entity);
        bool DeleteMassive(List<TEntity> entities);
        bool DeleteMassiveWithOutTransaction(List<TEntity> entities);
        bool DeleteWithOutTransaction(TEntity entity);
    }
}
