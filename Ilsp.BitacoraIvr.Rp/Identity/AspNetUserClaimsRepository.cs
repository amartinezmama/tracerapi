﻿using Ilsp.BitacoraIvr.EntIdentity;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.Identity
{
	public partial class AspNetUserClaimsRepository : EfRepository<AspNetUserClaims>
	{
		public AspNetUserClaimsRepository(DbContext ctx) : base(ctx)
		{
		}
		public AspNetUserClaimsRepository() : base()
		{
		}
	}
}
