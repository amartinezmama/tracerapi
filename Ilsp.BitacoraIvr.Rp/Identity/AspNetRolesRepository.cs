﻿using Ilsp.BitacoraIvr.EntIdentity;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.Identity
{
	public partial class AspNetRolesRepository : EfRepository<AspNetRoles>
	{
		public AspNetRolesRepository(DbContext ctx) : base(ctx)
		{
		}
		public AspNetRolesRepository() : base()
		{
		}
	}
}
