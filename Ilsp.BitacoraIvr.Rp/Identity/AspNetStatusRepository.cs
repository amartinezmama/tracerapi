﻿using Ilsp.BitacoraIvr.EntIdentity;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.Identity
{
	public partial class AspNetStatusRepository : EfRepository<AspNetStatus>
	{
		public AspNetStatusRepository(DbContext ctx) : base(ctx)
		{
		}
		public AspNetStatusRepository() : base()
		{
		}
	}
}
