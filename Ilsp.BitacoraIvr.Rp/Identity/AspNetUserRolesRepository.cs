﻿using Ilsp.BitacoraIvr.EntIdentity;
using System.Data.Entity;

namespace Ilsp.BitacoraIvr.Rp.Identity
{
	public partial class AspNetUserRolesRepository : EfRepository<AspNetUserRoles>
	{
		public AspNetUserRolesRepository(DbContext ctx) : base(ctx)
		{
		}
		public AspNetUserRolesRepository() : base()
		{
		}
	}
}
