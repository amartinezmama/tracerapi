﻿using Ilsp.BitacoraIvr.Ent;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;

namespace Ilsp.BitacoraIvr.Rp
{
    /// <summary>
    /// Operations CRUD to TEntity(table)
    /// </summary>
    /// <typeparam name="TEntity">Entity(table)</typeparam>
    public abstract class EfRepository<TEntity> : IDisposable, IRepository<TEntity> where TEntity : class
    {
        private DbContext _ctx;

        protected EfRepository(DbContext ctx)
        {
            _ctx = ctx;
        }

        protected EfRepository()
        {

        }

        public void Init(DbContext ctx)
        {
            _ctx = ctx;
        }

        /// <summary>
        /// Get first row of Entity(table) when predicate(filters) are met
        /// </summary>
        /// <param name="predicate">Filters</param>
        /// <param name="includes">Another related Entity (include = ["EntityName 1", "EntityName 2"])</param>
        /// <returns></returns>
        public TEntity GetFirst(Expression<Func<TEntity, bool>> predicate, string[] includes = null)
        {
            _ctx.Configuration.LazyLoadingEnabled = false;
            DbQuery<TEntity> query = _ctx.Set<TEntity>();
            if (includes != null)
            {
                foreach (var include in includes)
                {
                    query = query.Include(include);
                }
            }
            var items = query.Where(predicate).ToList();
            return items.Any() ? items.First() : null;
        }

        /// <summary>
        /// Get first row of Entity(table) when predicate(filters) are met
        /// </summary>
        /// <param name="predicate">Filters</param>
        /// <param name="include">Another related Entity (include = ["EntityName 1", "EntityName 2"])</param>
        /// <returns></returns>
        public TEntity GetFirstOrDefault(Expression<Func<TEntity, bool>> predicate, string include = null)
        {
            _ctx.Configuration.LazyLoadingEnabled = false;
            return include == null
                ? _ctx.Set<TEntity>().Where(predicate).FirstOrDefault()
                : _ctx.Set<TEntity>().Include(include).Where(predicate).FirstOrDefault();
        }

        /// <summary>
        /// Get list rows of Entity(table) when predicate(filters) are met
        /// </summary>
        /// <param name="predicate">Filters</param>
        /// <param name="order">Order Linq expression dynamic</param>
        /// <param name="includes">Another related Entity (include = ["EntityName 1", "EntityName 2"])</param>
        /// <returns></returns>
        public IEnumerable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate, string order, string[] includes = null)
        {
            _ctx.Configuration.LazyLoadingEnabled = false;
            DbQuery<TEntity> query = _ctx.Set<TEntity>();
            if (includes != null)
            {
                foreach (var include in includes)
                {
                    query = query.Include(include);
                }
            }
            if (string.IsNullOrEmpty(order))
            {
                order = "true";
            }
            return query.Where(predicate).OrderBy(order).ToList();
        }

        /// <summary>
        /// Get list rows of Entity(table) when predicate(filters) are met
        /// </summary>
        /// <param name="predicate">Filters</param>
        /// <param name="order">Order Linq expression dynamic</param>
        /// <param name="skip">Items to skip of collection</param>
        /// <param name="take">Items to take of collection</param>
        /// <param name="includes">Another related Entity (include = ["EntityName 1", "EntityName 2"])</param>
        /// <returns></returns>
        public ResponseAction<TEntity> GetListPager(Expression<Func<TEntity, bool>> predicate,
            string order, int skip, int take, string[] includes = null)
        {
            _ctx.Configuration.LazyLoadingEnabled = false;
            DbQuery<TEntity> query = _ctx.Set<TEntity>();
            if (includes != null)
            {
                query = includes.Aggregate(query, (current, include) => current.Include(include));
            }

            if (string.IsNullOrEmpty(order))
            {
                order = "true";
            }

            var data = query.Where(predicate).OrderBy(order).ToArray();
            take = take == 0 ? data.Length : take;
            return new ResponseAction<TEntity>
            {
                Items = data.Skip(skip).Take(take).ToArray(),
                Total = data.Length
            };
        }

        /// <summary>
        /// Get list rows of Entity(table) when predicate(filters) are met
        /// </summary>
        /// <param name="predicate">Filters</param>
        /// <param name="order">Order Linq expression dynamic</param>
        /// <param name="includes">Another related Entity (include = ["EntityName 1", "EntityName 2"])</param>
        /// <returns></returns>
        public List<TEntity> GetListPager(Expression<Func<TEntity, bool>> predicate,
            string order, string[] includes = null)
        {
            _ctx.Configuration.LazyLoadingEnabled = false;
            DbQuery<TEntity> query = _ctx.Set<TEntity>();
            if (includes != null)
            {
                query = includes.Aggregate(query, (current, include) => current.Include(include));
            }
            if (string.IsNullOrEmpty(order))
            {
                order = "true";
            }
            return query.Where(predicate).OrderBy(order).ToList();
        }

        /// <summary>
        /// Get all rows of Entity(table)
        /// </summary>
        /// <param name="includes">Another related Entity (include = ["EntityName 1", "EntityName 2"])</param>
        /// <returns></returns>
        public IEnumerable<TEntity> GetAll(string[] includes = null)
        {
            _ctx.Configuration.LazyLoadingEnabled = false;
            DbQuery<TEntity> query = _ctx.Set<TEntity>();

            if (includes != null)
            {
                query = includes.Aggregate(query, (current, include) => current.Include(include));
            }
            return query.ToList();
        }

        /// <summary>
        /// Save new row to Entity(table)
        /// </summary>
        /// <param name="entity">New row to save on Entity(table)</param>
        /// <returns></returns>
        public bool Save(TEntity entity)
        {
            var result = false;
            if (entity != null)
            {
                using (var t = _ctx.Database.BeginTransaction())
                {
                    _ctx.Configuration.LazyLoadingEnabled = false;
                    _ctx.Set<TEntity>().Add(entity);
                    result = _ctx.SaveChanges() > 0;
                    if (result)
                    {
                        t.Commit();
                    }
                    else
                    {
                        t.Rollback();
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Save new row to Entity(table)
        /// </summary>
        /// <param name="entity">New row to save on Entity(table)</param>
        /// <returns></returns>
        public bool SaveWithOutTransaction(TEntity entity)
        {
            var result = false;
            if (entity != null)
            {
                _ctx.Configuration.LazyLoadingEnabled = false;
                _ctx.Set<TEntity>().Add(entity);
                result = _ctx.SaveChanges() > 0;
            }
            return result;
        }

        /// <summary>
        /// Save multiple new rows to Entity(table)
        /// </summary>
        /// <param name="entities">New rows to save on Entity(table)</param>
        /// <returns></returns>
        public bool SaveMultiple(List<TEntity> entities)
        {
            var result = false;
            if (entities != null)
            {
                using (var t = _ctx.Database.BeginTransaction())
                {
                    foreach (var entity in entities)
                    {
                        _ctx.Configuration.LazyLoadingEnabled = false;
                        _ctx.Set<TEntity>().Add(entity);
                        _ctx.Entry(entity).State = EntityState.Added;
                    }
                    result = _ctx.SaveChanges() > 0;
                    if (result)
                    {
                        t.Commit();
                    }
                    else
                    {
                        t.Rollback();
                    }

                }
            }
            return result;
        }

        /// <summary>
        /// Delete row of Entity(table)
        /// </summary>
        /// <param name="entity">Row to delete on Entity(table)</param>
        /// <returns></returns>
        public bool Delete(TEntity entity)
        {
            var result = false;
            if (entity != null)
            {
                using (var t = _ctx.Database.BeginTransaction())
                {
                    _ctx.Configuration.LazyLoadingEnabled = false;
                    _ctx.Set<TEntity>().Attach(entity);
                    _ctx.Set<TEntity>().Remove(entity);
                    result = _ctx.SaveChanges() > 0;
                    if (result)
                    {
                        t.Commit();
                    }
                    else
                    {
                        t.Rollback();
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Delete row of Entity(table)
        /// </summary>
        /// <param name="entity">Row to delete on Entity(table)</param>
        /// <returns></returns>
        public bool DeleteWithOutTransaction(TEntity entity)
        {
            var result = false;
            if (entity != null)
            {
                _ctx.Configuration.LazyLoadingEnabled = false;
                _ctx.Set<TEntity>().Attach(entity);
                _ctx.Set<TEntity>().Remove(entity);
                result = _ctx.SaveChanges() > 0;
            }
            return result;
        }

        /// <summary>
        /// Delete row of Entity(table)
        /// </summary>
        /// <param name="entities">Rows to delete on Entity(table)</param>
        /// <returns></returns>
        public bool DeleteMassive(List<TEntity> entities)
        {
            var result = false;
            if (entities != null)
            {
                using (var t = _ctx.Database.BeginTransaction())
                {
                    foreach (var entity in entities)
                    {
                        _ctx.Configuration.LazyLoadingEnabled = false;
                        _ctx.Set<TEntity>().Attach(entity);
                        _ctx.Set<TEntity>().Remove(entity);
                        result = _ctx.SaveChanges() > 0;
                        if (result)
                        {
                            t.Commit();
                        }
                        else
                        {
                            t.Rollback();
                        }
                    }

                }
            }
            return result;
        }

        /// <summary>
        /// Delete row of Entity(table)
        /// </summary>
        /// <param name="entities">Rows to delete on Entity(table)</param>
        /// <returns></returns>
        public bool DeleteMassiveWithOutTransaction(List<TEntity> entities)
        {
            var result = false;
            if (entities != null)
            {
                foreach (var entity in entities)
                {
                    _ctx.Configuration.LazyLoadingEnabled = false;
                    _ctx.Set<TEntity>().Attach(entity);
                    _ctx.Set<TEntity>().Remove(entity);
                    result = _ctx.SaveChanges() > 0;
                }
            }
            return result;
        }

        /// <summary>
        /// Update row on Entity(table)
        /// </summary>
        /// <param name="entity">Row to update on Entity(table)</param>
        /// <param name="updateAction">List of properties to update on row</param>
        /// <returns></returns>
        public bool Update(TEntity entity, Action<TEntity> updateAction)
        {
            var t = _ctx.Database.BeginTransaction();
            _ctx.Configuration.LazyLoadingEnabled = false;
            _ctx.Set<TEntity>().Attach(entity);
            updateAction(entity);
            var result = _ctx.SaveChanges() > 0;
            if (result)
            {
                t.Commit();
            }
            else
            {
                t.Rollback();
            }
            return result;
        }

        /// <summary>
        /// Update row on Entity(table)
        /// </summary>
        /// <param name="entity">Row to update on Entity(table)</param>
        /// <param name="updateAction">List of properties to update on row</param>
        /// <returns></returns>
        public bool UpdateWithOutTransaction(TEntity entity, Action<TEntity> updateAction)
        {
            //var t = _ctx.Database.BeginTransaction();
            _ctx.Configuration.LazyLoadingEnabled = false;
            _ctx.Set<TEntity>().Attach(entity);
            updateAction(entity);
            var result = _ctx.SaveChanges() > 0;
            return result;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }

        ~EfRepository()
        {
            _ctx.Dispose();
        }
    }
}
