﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ilsp.BitacoraIvr.Ent.Common;

namespace Ilsp.BitacoraIvr.Tl
{
    public static class GooglePoints
    {
        /// <summary>
        /// Get point from string
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static string GetFirstPoint(string points)
        {
            var pts = points
                .Replace("LINESTRING", string.Empty)
                .Replace("POLYGON", string.Empty)
                .Replace("MULTILINESTRING", string.Empty)
                .Replace("POINT", string.Empty)
                .Replace("(", string.Empty)
                .Replace(")", string.Empty)
                .Split(',')
                .Select(x => new Point
                {
                    lat = double.Parse(x.Trim().Split(' ')[1].Trim()),
                    lng = double.Parse(x.Trim().Split(' ')[0].Trim())
                }).ToArray();
            if (pts.Any())
            {
                var point = pts.First();
                return $"{point.lat}, {point.lng}";
            }

            return string.Empty;
        }

        /// <summary>
        /// Get point from string
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static string GetLastPoint(string points)
        {
            var pts = points
                .Replace("LINESTRING", string.Empty)
                .Replace("POLYGON", string.Empty)
                .Replace("MULTILINESTRING", string.Empty)
                .Replace("POINT", string.Empty)
                .Replace("(", string.Empty)
                .Replace(")", string.Empty)
                .Split(',')
                .Select(x => new Point
                {
                    lat = double.Parse(x.Trim().Split(' ')[1].Trim()),
                    lng = double.Parse(x.Trim().Split(' ')[0].Trim())
                }).ToArray();
            if (pts.Any())
            {
                var point = pts.Last();
                return $"{point.lat}, {point.lng}";
            }

            return string.Empty;
        }

        /// <summary>
        /// Get points from string
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static IEnumerable<Point> GetPoints(string points)
        {
            return points
                .Replace("LINESTRING", string.Empty)
                .Replace("POLYGON", string.Empty)
                .Replace("MULTILINESTRING", string.Empty)
                .Replace("POINT", string.Empty)
                .Replace("(", string.Empty)
                .Replace(")", string.Empty)
                .Split(',')
                .Select(x => new Point
                {
                    lat = double.Parse(x.Trim().Split(' ')[1].Trim()),
                    lng = double.Parse(x.Trim().Split(' ')[0].Trim())
                }).ToArray();
        }

        /// <summary>
        /// Encode a gqography sql string geography data
        /// </summary>
        /// <param name="route"></param>
        /// <returns></returns>
        public static string GeographyEncode(string route)
        {
            return Encode(route
                            .Replace("LINESTRING", string.Empty)
                            .Replace("POLYGON", string.Empty)
                            .Replace("(", string.Empty)
                            .Replace(")", string.Empty)
                            .Split(',')
                            .Select(x => new Point
                            {
                                lat = double.Parse(x.Trim().Split(' ')[1].Trim()),
                                lng = double.Parse(x.Trim().Split(' ')[0].Trim())
                            }).ToArray());
        }

        /// <summary>
        /// Decode google style polyline coordinates.
        /// </summary>
        /// <param name="encodedPoints"></param>
        /// <returns></returns>
        public static IEnumerable<Point> Decode(string encodedPoints)
        {
            if (string.IsNullOrEmpty(encodedPoints))
                throw new ArgumentNullException(nameof(encodedPoints));

            var polylineChars = encodedPoints.ToCharArray();
            var index = 0;

            var currentLat = 0;
            var currentLng = 0;

            while (index < polylineChars.Length)
            {
                // calculate next latitude
                var sum = 0;
                var shifter = 0;
                int next5Bits;
                do
                {
                    next5Bits = polylineChars[index++] - 63;
                    sum |= (next5Bits & 31) << shifter;
                    shifter += 5;
                } while (next5Bits >= 32 && index < polylineChars.Length);

                if (index >= polylineChars.Length)
                    break;

                currentLat += (sum & 1) == 1 ? ~(sum >> 1) : sum >> 1;

                //calculate next longitude
                sum = 0;
                shifter = 0;
                do
                {
                    next5Bits = polylineChars[index++] - 63;
                    sum |= (next5Bits & 31) << shifter;
                    shifter += 5;
                } while (next5Bits >= 32 && index < polylineChars.Length);

                if (index >= polylineChars.Length && next5Bits >= 32)
                    break;

                currentLng += (sum & 1) == 1 ? ~(sum >> 1) : sum >> 1;

                yield return new Point
                {
                    lat = Convert.ToDouble(currentLat) / 1E5,
                    lng = Convert.ToDouble(currentLng) / 1E5
                };
            }
        }

        /// <summary>
        /// Encode it
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        private static string Encode(IEnumerable<Point> points)
        {
            var str = new StringBuilder();

            var encodeDiff = (Action<int>)(diff =>
            {
                var shifted = diff << 1;
                if (diff < 0)
                    shifted = ~shifted;

                var rem = shifted;

                while (rem >= 0x20)
                {
                    str.Append((char)((0x20 | (rem & 0x1f)) + 63));

                    rem >>= 5;
                }

                str.Append((char)(rem + 63));
            });

            var lastLat = 0;
            var lastLng = 0;

            foreach (var point in points)
            {
                var lat = (int)Math.Round(point.lat * 1E5);
                var lng = (int)Math.Round(point.lng * 1E5);

                encodeDiff(lat - lastLat);
                encodeDiff(lng - lastLng);

                lastLat = lat;
                lastLng = lng;
            }

            return str.ToString();
        }
    }
}
