﻿using System;

namespace Ilsp.BitacoraIvr.Tl
{
    public static class OptionsDateTime
    {
        /// <summary>
        /// Get date from format 'yyyy/MM/dd HH:mm:ss'
        /// </summary>
        /// <param name="dateString"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(string dateString)
        {
            var dateTimeParts = dateString.Split(' ');
            if (dateTimeParts.Length > 0)
            {
                var dateParts = dateTimeParts[0].Split('/');
                var year = Convert.ToInt32(dateParts[0]);
                var month = Convert.ToInt32(dateParts[1]);
                var day = Convert.ToInt32(dateParts[2]);
                var hour = 0;
                var minute = 0;
                if (dateTimeParts.Length > 1)
                {
                    var timeParts = dateTimeParts[1].Split(':');
                    hour = Convert.ToInt32(timeParts[0]);
                    minute = Convert.ToInt32(timeParts[1]);
                }
                return new DateTime(year, month, day, hour, minute, 0);
            }
            return DateTime.Now;
        }

        /// <summary>
        /// Get date from format 'dd/MM/yyyy HH:mm:ss'
        /// </summary>
        /// <param name="dateString"></param>
        /// <param name="delimiterSplit"></param>
        /// <returns></returns>
        public static DateTime ToDateTimeMx(string dateString, char delimiterSplit = '/')
        {
            var dateTimeParts = dateString.Split(' ');
            if (dateTimeParts.Length > 0)
            {
                var dateParts = dateTimeParts[0].Split(delimiterSplit);
                var year = Convert.ToInt32(dateParts[2]);
                var month = Convert.ToInt32(dateParts[1]);
                var day = Convert.ToInt32(dateParts[0]);
                var hour = 0;
                var minute = 0;
                if (dateTimeParts.Length > 1)
                {
                    var timeParts = dateTimeParts[1].Split(':');
                    hour = Convert.ToInt32(timeParts[0]);
                    minute = Convert.ToInt32(timeParts[1]);
                }
                return new DateTime(year, month, day, hour, minute, 0);
            }
            return DateTime.Now;
        }

        public static DateTime ToDateTimeYyyyMMdd(string dateString)
        {
            var dateTimeParts = dateString.Split(' ');
            if (dateTimeParts.Length > 0)
            {
                var dateParts = dateTimeParts[0].Split('/');
                var year = Convert.ToInt32(dateParts[0]);
                var month = Convert.ToInt32(dateParts[1]);
                var day = Convert.ToInt32(dateParts[2]);
                var hour = 0;
                var minute = 0;
                if (dateTimeParts.Length > 1)
                {
                    var timeParts = dateTimeParts[1].Split(':');
                    hour = Convert.ToInt32(timeParts[0]);
                    minute = Convert.ToInt32(timeParts[1]);
                }
                return new DateTime(year, month, day, hour, minute, 0);
            }
            return DateTime.Now;
        }

        public static DateTime? UtcToLocalDate(DateTime? utcDateTime)
        {
            if (utcDateTime != null)
            {
                var utcDateTimeTemp = utcDateTime.Value;
                var dateUtc = new DateTime(utcDateTimeTemp.Year, utcDateTimeTemp.Month, utcDateTimeTemp.Day, utcDateTimeTemp.Hour,
                    utcDateTimeTemp.Minute, utcDateTimeTemp.Second, utcDateTimeTemp.Millisecond, DateTimeKind.Utc);
                var universalDate = dateUtc.ToUniversalTime();
                return universalDate.ToLocalTime();
            }

            return null;

        }

    }
}
