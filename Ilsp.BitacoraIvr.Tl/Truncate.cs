﻿namespace Ilsp.BitacoraIvr.Tl
{
    public static class Truncate
    {
        public static string TruncateLatLng(string latLng, int limit = 9)
        {
            if (latLng.Contains("-"))
            {
                limit = 10;
            }
            return latLng.Substring(0, latLng.Length < limit ? latLng.Length : limit);
        }
    }
}
