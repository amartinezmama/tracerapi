﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ilsp.BitacoraIvr.Tl
{
    public class GenBitacoraIdentifier : IDisposable
    {
        readonly Dictionary<string, string> _dictionaryGen = new Dictionary<string, string>();
        readonly Dictionary<string, string> _dictionaryRev = new Dictionary<string, string>();

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public GenBitacoraIdentifier()
        {
            _dictionaryGen.Add("00", "0");
            _dictionaryGen.Add("01", "1");
            _dictionaryGen.Add("02", "2");
            _dictionaryGen.Add("03", "3");
            _dictionaryGen.Add("04", "4");
            _dictionaryGen.Add("05", "5");
            _dictionaryGen.Add("06", "6");
            _dictionaryGen.Add("07", "7");
            _dictionaryGen.Add("08", "8");
            _dictionaryGen.Add("09", "9");
            _dictionaryGen.Add("10", "A");
            _dictionaryGen.Add("11", "B");
            _dictionaryGen.Add("12", "C");
            _dictionaryGen.Add("13", "D");
            _dictionaryGen.Add("14", "E");
            _dictionaryGen.Add("15", "F");
            _dictionaryGen.Add("16", "G");
            _dictionaryGen.Add("17", "H");
            _dictionaryGen.Add("18", "I");
            _dictionaryGen.Add("19", "J");
            _dictionaryGen.Add("20", "K");
            _dictionaryGen.Add("21", "L");
            _dictionaryGen.Add("22", "M");
            _dictionaryGen.Add("23", "N");
            _dictionaryGen.Add("24", "O");
            _dictionaryGen.Add("25", "P");
            _dictionaryGen.Add("26", "Q");
            _dictionaryGen.Add("27", "R");
            _dictionaryGen.Add("28", "S");
            _dictionaryGen.Add("29", "T");
            _dictionaryGen.Add("30", "U");
            _dictionaryGen.Add("31", "V");
            _dictionaryGen.Add("32", "W");
            _dictionaryGen.Add("33", "X");
            _dictionaryGen.Add("34", "Y");
            _dictionaryGen.Add("35", "Z");

            _dictionaryRev.Add("0", "00");
            _dictionaryRev.Add("1", "01");
            _dictionaryRev.Add("2", "02");
            _dictionaryRev.Add("3", "03");
            _dictionaryRev.Add("4", "04");
            _dictionaryRev.Add("5", "05");
            _dictionaryRev.Add("6", "06");
            _dictionaryRev.Add("7", "07");
            _dictionaryRev.Add("8", "08");
            _dictionaryRev.Add("9", "09");
            _dictionaryRev.Add("A", "10");
            _dictionaryRev.Add("B", "11");
            _dictionaryRev.Add("C", "12");
            _dictionaryRev.Add("D", "13");
            _dictionaryRev.Add("E", "14");
            _dictionaryRev.Add("F", "15");
            _dictionaryRev.Add("G", "16");
            _dictionaryRev.Add("H", "17");
            _dictionaryRev.Add("I", "18");
            _dictionaryRev.Add("J", "19");
            _dictionaryRev.Add("K", "20");
            _dictionaryRev.Add("L", "21");
            _dictionaryRev.Add("M", "22");
            _dictionaryRev.Add("N", "23");
            _dictionaryRev.Add("O", "24");
            _dictionaryRev.Add("P", "25");
            _dictionaryRev.Add("Q", "26");
            _dictionaryRev.Add("R", "27");
            _dictionaryRev.Add("S", "28");
            _dictionaryRev.Add("T", "29");
            _dictionaryRev.Add("U", "30");
            _dictionaryRev.Add("V", "31");
            _dictionaryRev.Add("W", "32");
            _dictionaryRev.Add("X", "33");
            _dictionaryRev.Add("Y", "34");
            _dictionaryRev.Add("Z", "35");
        }

        private const string Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        private static readonly int Base = Alphabet.Length;

        private static string Encode(int num)
        {
            var sb = new StringBuilder();
            while (num > 0)
            {
                sb.Append(Alphabet[num % Base]);
                num /= Base;
            }

            var charArray = sb.ToString().ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public string GetCode()
        {
            var code = "";
            var date = DateTime.Now;
            //Year
            var year = date.Year.ToString("0000"); // Se obtiene el Año en cadena
            var yearSum = year.Sum(t => int.Parse(t.ToString())); // Se hace la sumatoria de todos los numeros del Año
            code += _dictionaryGen[yearSum.ToString("00")];
            // Se obtiene el codigo del numero generado de la sumatoria de los numeros del año
            //Months
            code += _dictionaryGen[date.Month.ToString("00")]; // Se obtiene el codigo del mes
            //Days
            code += _dictionaryGen[date.Day.ToString("00")]; // se obtiene el codigo del dia
            //Hour
            code += _dictionaryGen[(date.Hour + 10).ToString("00")];
            // A la Hora se le suma la cantidad de 10 y se obtiene su codigo
            //Minutes
            code += Encode(date.Minute); // Se obtiene el codigo de los minutos con Bijective Function
            //Seconds
            code += Encode(date.Second); // Se obtiene el codigo de los segundos con Bijective Function
            //Miliseconds
            code += Encode(date.Millisecond); // Se obtiene el codigo de los milisegundos con Bijective Function
            return code;
        }
    }
}
