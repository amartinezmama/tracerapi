﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.Tl.ExpressionBuilder.Common;

namespace Ilsp.BitacoraIvr.Tl.ExpressionBuilder.Builders
{
    public class ExpressionBuilder
    {

        public Expression<Func<T, bool>> GetFilterExpression<T>(List<Filter> filters, string condition = "AND") where T : class
        {
            var param = Expression.Parameter(typeof(T), "x");
            Expression expression = null;
            var connector = FilterStatementConnector.And;
            foreach (var filter in filters)
            {
                if (condition.ToUpper().Equals("AND"))
                {
                    connector = FilterStatementConnector.And;
                }
                if (condition.ToUpper().Equals("OR"))
                {
                    connector = FilterStatementConnector.Or;
                }
                var expr = GetFilterExpression(param, filter);
                expression = expression == null ? expr : CombineExpressions(expression, expr, connector);
            }
            expression = expression ?? Expression.Constant(true);

            return Expression.Lambda<Func<T, bool>>(expression, param);
        }

        public Expression<Func<T, bool>> GetFilterExpressionMany<T>(List<KendoFilter> kendoFilters) where T : class
        {
            var param = Expression.Parameter(typeof(T), "x");
            Expression expression = null;
            var connector = FilterStatementConnector.And;
            var parentConnector = FilterStatementConnector.And;
            foreach (var f in kendoFilters)
            {
                Expression expressionT = null;
                foreach (var filter in f.Filters)
                {
                    if (f.Logic.ToUpper().Equals("AND"))
                    {
                        connector = FilterStatementConnector.And;
                    }
                    if (f.Logic.ToUpper().Equals("OR"))
                    {
                        connector = FilterStatementConnector.Or;
                    }
                    var expr = GetFilterExpression(param, filter);
                    expressionT = expressionT == null ? expr : CombineExpressions(expressionT, expr, connector);
                }
                expressionT = expressionT ?? Expression.Constant(true);
                if (f.Parentlogic.ToUpper().Equals("AND"))
                {
                    parentConnector = FilterStatementConnector.And;
                }
                if (f.Parentlogic.ToUpper().Equals("OR"))
                {
                    parentConnector = FilterStatementConnector.Or;
                }
                expression = expression == null ? expressionT : CombineExpressions(expressionT, expression, parentConnector);
            }

            expression = expression ?? Expression.Constant(true);

            return Expression.Lambda<Func<T, bool>>(expression, param);
        }

        private Expression GetFilterExpression(Expression param, Filter filter)
        {
            //var cultureInfo = new System.Globalization.CultureInfo("es-MX");
            var property = Expression.Property(param, filter.Field);
            var typeParameter = property.Type.GenericTypeArguments.Any()
                ? property.Type.GenericTypeArguments[0].FullName ?? string.Empty
                : property.Type.FullName ?? string.Empty;

            if (typeParameter.Equals("System.Int32"))
            {
                return CallMethod(GetOperator(filter.Operator), param, filter.Field, Convert.ToInt32(filter.Value));
            }
            if (typeParameter.Equals("System.Int64"))
            {
                return CallMethod(GetOperator(filter.Operator), param, filter.Field, Convert.ToInt64(filter.Value));
            }
            if (typeParameter.Equals("System.Decimal"))
            {
                return CallMethod(GetOperator(filter.Operator), param, filter.Field, Convert.ToDecimal(filter.Value));
            }
            if (typeParameter.Equals("System.String"))
            {
                return CallMethod(GetOperator(filter.Operator), param, filter.Field, filter.Value);
            }
            if (typeParameter.Equals("System.Boolean"))
            {
                return CallMethod(GetOperator(filter.Operator), param, filter.Field, Convert.ToBoolean(int.Parse(filter.Value)));
            }
            if (typeParameter.Equals("System.DateTime"))
            {
                return CallMethod(GetOperator(filter.Operator), param, filter.Field, OptionsDateTime.ToDateTime(filter.Value));
            }
            if (typeParameter.Equals("System.Guid"))
            {
                return CallMethod(GetOperator(filter.Operator), param, filter.Field, Guid.Parse(filter.Value));
            }
            if (typeParameter.Equals("System.Byte"))
            {
                return CallMethod(GetOperator(filter.Operator), param, filter.Field, Convert.ToByte(filter.Value));
            }
            return null;
        }

        public Expression<Func<T, bool>> CombinePredicates<T>(Expression expr1, Expression expr2, FilterStatementConnector connector)
        {
            var param = Expression.Parameter(typeof(T), "x");
            var expression = connector == FilterStatementConnector.And ? Expression.And(expr1, expr2) : Expression.Or(expr1, expr2);
            return Expression.Lambda<Func<T, bool>>(expression, param);
        }

        private Expression CombineExpressions(Expression expr1, Expression expr2, FilterStatementConnector connector)
        {
            return connector == FilterStatementConnector.And ? Expression.And(expr1, expr2) : Expression.Or(expr1, expr2);
        }

        private static string GetOperator(string operatorName)
        {
            switch (operatorName.ToUpper())
            {
                case "EQUALS":
                    return "Equals";
                case "NOTEQUALS":
                    return "NotEquals";
                case "CONTAINS":
                    return "Contains";
                case "GREATERTHANOREQUAL":
                    return "GREATERTHANOREQUAL";
                case "LESSTHANOREQUAL":
                    return "LESSTHANOREQUAL";
                default:
                    return string.Empty;
            }
        }

        private static Expression CallMethod(string method, Expression parameter, string fieldName, object fieldValue)
        {
            var property = Expression.Property(parameter, fieldName);
            var value = Expression.Constant(fieldValue);
            var converted = Expression.Convert(value, property.Type);
            if (method.ToUpper().Equals("EQUALS"))
            {
                return Expression.Equal(property, converted);
            }
            if (method.ToUpper().Equals("NOTEQUALS"))
            {
                return Expression.NotEqual(property, converted);
            }
            if (method.ToUpper().Equals("GREATERTHANOREQUAL"))
            {
                return Expression.GreaterThanOrEqual(property, converted);
            }
            if (method.ToUpper().Equals("LESSTHANOREQUAL"))
            {
                return Expression.LessThanOrEqual(property, converted);
            }
            var dynamicMethod = typeof(string).GetMethod(GetOperator(method), new[] { property.Type });
            var expressionValue = Expression.Constant(fieldValue, property.Type);
            return Expression.Call(property, dynamicMethod, expressionValue);
        }

    }
}