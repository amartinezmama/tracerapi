﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.Tl.ExpressionBuilder.Helpers;

namespace Ilsp.BitacoraIvr.Tl.ExpressionBuilder.Builders
{
    public class OrderBuilder
    {
        readonly BuilderHelper helper;

        public OrderBuilder(BuilderHelper helper)
        {
            this.helper = helper;
        }

        public Expression<Func<T, bool>> GetExpression<T>(List<KendoSort> sorts) where T : class
        {
            var param = Expression.Parameter(typeof(T), "x");
            Expression expression = null;
            foreach (var sort in sorts)
            {
                Expression expr = null;
                expr = GetExpression(param, sort);
                //expression = expression == null ? expr : CombineExpressions(expression, expr, connector);
            }
            expression = expression ?? Expression.Constant(true);

            return Expression.Lambda<Func<T, bool>>(expression, param);
        }

        private Expression GetExpression(ParameterExpression param, KendoSort sort)
        {
            var propertyExp = Expression.Property(param, sort.Field);
            MethodInfo method = typeof(string).GetMethod("Contains", new[] { typeof(string) });
            return Expression.Call(propertyExp, method);
        }

        //private Expression CombineExpressions(Expression expr1, Expression expr2, sorttatementConnector connector)
        //{
        //    return connector == sorttatementConnector.And ? Expression.AndAlso(expr1, expr2) : Expression.OrElse(expr1, expr2);
        //}
    }
}
