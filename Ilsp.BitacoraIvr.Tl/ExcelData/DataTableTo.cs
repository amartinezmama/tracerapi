﻿using System.Data;
using System.IO;
using OfficeOpenXml;

namespace Ilsp.BitacoraIvr.Tl.ExcelData
{
    public static class DataTableTo
    {
        public static byte[] DtToExcelBytes(DataTable dt, bool printHeader)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Result");
                ws.Cells["A1"].LoadFromDataTable(dt, printHeader);
                ws.Cells.AutoFitColumns();
                var ms = new MemoryStream();
                pck.SaveAs(ms);
                return ms.ToArray();
            }
        }
    }
}
