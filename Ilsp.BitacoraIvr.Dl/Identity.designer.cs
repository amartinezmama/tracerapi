﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ilsp.BitacoraIvr.Dl
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="IdentityServer")]
	public partial class IdentityDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Definiciones de métodos de extensibilidad
    partial void OnCreated();
    #endregion
		
		public IdentityDataContext() : 
				base(global::Ilsp.BitacoraIvr.Dl.Properties.Settings.Default.IdentityServerConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public IdentityDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public IdentityDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public IdentityDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public IdentityDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.sp_get_users_by_role")]
		public ISingleResult<sp_get_users_by_roleResult> sp_get_users_by_role([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(100)")] string userRoles)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), userRoles);
			return ((ISingleResult<sp_get_users_by_roleResult>)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.ivr_sp_search_users")]
		public ISingleResult<ivr_sp_search_usersResult> ivr_sp_search_users([global::System.Data.Linq.Mapping.ParameterAttribute(DbType="NVarChar(50)")] string search)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), search);
			return ((ISingleResult<ivr_sp_search_usersResult>)(result.ReturnValue));
		}
	}
	
	public partial class sp_get_users_by_roleResult
	{
		
		private string _user;
		
		private string _role;
		
		public sp_get_users_by_roleResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="[user]", Storage="_user", DbType="NVarChar(256)")]
		public string user
		{
			get
			{
				return this._user;
			}
			set
			{
				if ((this._user != value))
				{
					this._user = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_role", DbType="NVarChar(256)")]
		public string role
		{
			get
			{
				return this._role;
			}
			set
			{
				if ((this._role != value))
				{
					this._role = value;
				}
			}
		}
	}
	
	public partial class ivr_sp_search_usersResult
	{
		
		private string _Id;
		
		private string _UserName;
		
		private string _RolName;
		
		public ivr_sp_search_usersResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Id", DbType="NVarChar(256)")]
		public string Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this._Id = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UserName", DbType="NVarChar(256)")]
		public string UserName
		{
			get
			{
				return this._UserName;
			}
			set
			{
				if ((this._UserName != value))
				{
					this._UserName = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_RolName", DbType="NVarChar(MAX) NOT NULL", CanBeNull=false)]
		public string RolName
		{
			get
			{
				return this._RolName;
			}
			set
			{
				if ((this._RolName != value))
				{
					this._RolName = value;
				}
			}
		}
	}
}
#pragma warning restore 1591
