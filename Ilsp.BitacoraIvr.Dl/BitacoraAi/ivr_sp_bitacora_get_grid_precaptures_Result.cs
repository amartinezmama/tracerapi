//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ilsp.BitacoraIvr.Dl.BitacoraAi
{
    using System;
    
    public partial class ivr_sp_bitacora_get_grid_precaptures_Result
    {
        public Nullable<decimal> Id { get; set; }
        public string Alias { get; set; }
        public string ServiceOrder { get; set; }
        public string Observations { get; set; }
        public string CreateDate { get; set; }
        public string TimeInactive { get; set; }
        public string Status { get; set; }
        public string Customer { get; set; }
        public string Origin { get; set; }
        public Nullable<int> Total { get; set; }
    }
}
