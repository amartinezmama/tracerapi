//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ilsp.BitacoraIvr.Dl.BitacoraAi
{
    using System;
    using System.Collections.Generic;
    
    public partial class convoys
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public convoys()
        {
            this.bitacoras_transportations = new HashSet<bitacoras_transportations>();
            this.bitacoras = new HashSet<bitacoras>();
        }
    
        public int id_convoy { get; set; }
        public string name { get; set; }
        public string color { get; set; }
        public string icon_path { get; set; }
        public string user_mod { get; set; }
        public System.DateTime create_date { get; set; }
        public Nullable<System.DateTime> mod_date { get; set; }
        public int id_status { get; set; }
    
        public virtual cat_status cat_status { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bitacoras_transportations> bitacoras_transportations { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<bitacoras> bitacoras { get; set; }
    }
}
