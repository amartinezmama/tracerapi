//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ilsp.BitacoraIvr.Dl.BitacoraAi
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_cat_transport_lines_vehicles
    {
        public int Id { get; set; }
        public string NumPlate { get; set; }
        public string UnitType { get; set; }
        public string Economic { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
    }
}
