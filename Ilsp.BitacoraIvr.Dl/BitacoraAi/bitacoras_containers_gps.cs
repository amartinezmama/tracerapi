//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ilsp.BitacoraIvr.Dl.BitacoraAi
{
    using System;
    using System.Collections.Generic;
    
    public partial class bitacoras_containers_gps
    {
        public int id_container_gps { get; set; }
        public int id_container { get; set; }
        public string id_gps { get; set; }
    
        public virtual bitacoras_containers bitacoras_containers { get; set; }
        public virtual cat_gps cat_gps { get; set; }
    }
}
