//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ilsp.BitacoraIvr.EntIdentity
{
    
    using System;
    using System.Collections.Generic;
    
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Ilsp.BitacoraIvr.Res;
    using Ilsp.BitacoraIvr.Dl.Identity;
    using System.Runtime.Serialization;
    
    [DataContract(Namespace = "Ilsp BitacoraAi")]
    [Table("AspNetStatus")]
    public partial class AspNetStatus
    {
        public AspNetStatus()
        {
    		Initialize();
            this.AspNetRoles = new HashSet<AspNetRoles>();
            this.AspNetUsers = new HashSet<AspNetUsers>();
        }
    
    	[DataMember]
    	[Key]
    	[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
    
    	[DataMember]
        [Required(ErrorMessageResourceName = ResourcesStrings.Entity_Generic_RequiredField, ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [StringLength(50, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string Name { get; set; }
    
    	[DataMember]
    	
        public virtual ICollection<AspNetRoles> AspNetRoles { get; set; }
    
    	[DataMember]
    	
        public virtual ICollection<AspNetUsers> AspNetUsers { get; set; }
    
    	partial void Initialize();
    	
    }
}
