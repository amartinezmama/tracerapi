﻿using System;
using System.Configuration;
using System.Text;
using RabbitMQ.Client;

namespace Ilsp.Services.LoadShipmentPlan.Mq
{
    public class MqSender : IDisposable
    {
        private string HostName { get; }
        private string UserName { get; }
        private string Password { get; }

        private ConnectionFactory _connectionFactory;
        private readonly IConnection _connection;
        private readonly IModel _model;
        private readonly string _exchangeName;

        public MqSender(string configPrefix)
        {
            HostName = ConfigurationManager.AppSettings[$"{configPrefix}.HostName"];
            UserName = ConfigurationManager.AppSettings[$"{configPrefix}.UserName"];
            Password = ConfigurationManager.AppSettings[$"{configPrefix}.Password"];
            _exchangeName = ConfigurationManager.AppSettings[$"{configPrefix}.ExchangeName"] ?? "notifications.exchange";

            _connectionFactory = new ConnectionFactory
            {
                HostName = HostName,
                UserName = UserName,
                Password = Password
            };

            _connection = _connectionFactory.CreateConnection();
            _model = _connection.CreateModel();
        }

        public MqSender(string hostName, string userName, string password)
        {
            HostName = hostName;
            UserName = userName;
            Password = password;

            _connectionFactory = new ConnectionFactory
            {
                HostName = hostName,
                UserName = userName,
                Password = password
            };

            _connection = _connectionFactory.CreateConnection();
            _model = _connection.CreateModel();
        }

        public void Send(string message)
        {
            //Setup properties
            var properties = _model.CreateBasicProperties();
            properties.Persistent = true;

            Send(properties, message);
        }

        public void Send(string message, string type)
        {
            //Setup properties
            var properties = _model.CreateBasicProperties();
            properties.Persistent = true;
            properties.Type = type;

            Send(properties, message);
        }

        private void Send(IBasicProperties properties, string message)
        {
            //Serialize
            var messageBuffer = Encoding.UTF8.GetBytes(message);

            //Send message
            _model.BasicPublish(_exchangeName, string.Empty, properties, messageBuffer);
        }

        public void Dispose()
        {
            _connection?.Close();

            if (_model != null && _model.IsOpen)
                _model.Abort();

            _connectionFactory = null;

            GC.SuppressFinalize(this);
        }
    }
}
