﻿using System;
using System.Configuration.Install;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;

namespace Ilsp.Services.LoadShipmentPlan
{
    static class Program
    {

        /// <summary>
        /// Punto de entrada principal de la aplicación.
        /// </summary>
        static void Main(string[] args)
        {
            // Inicialización del servicio para comenzar.
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new LoadShipmentPlan()
            };

            // ¿Estamos en modo interactivo?
            if (Environment.UserInteractive)
            {
                // ¿Estamos en modo de depuración?
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    // Simula la ejecución de servicios.
                    RunInteractiveServices(ServicesToRun);
                }
                else
                {
                    try
                    {
                        bool hasCommands = false;
                        // ¿Tenemos un comando para ejecutar los servicios en modo interactivo?
                        if (HasCommand(args, "run-services"))
                        {
                            RunInteractiveServices(ServicesToRun);
                            // No procesamos otros pedidos.
                            return;
                        }
                        // ¿Tenemos una orden para instalar e iniciar los servicios?
                        if (HasCommand(args, "start-services"))
                        {
                            // Instalación
                            ManagedInstallerClass.InstallHelper(new[] { typeof(Program).Assembly.Location });
                            // Empezando
                            foreach (var service in ServicesToRun)
                            {
                                ServiceController sc = new ServiceController(service.ServiceName);
                                sc.Start();
                                sc.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(10));
                            }
                            hasCommands = true;
                        }
                        // ¿Tenemos un comando para detener y desinstalar los servicios?
                        if (HasCommand(args, "stop-services"))
                        {
                            // Para
                            foreach (var service in ServicesToRun)
                            {
                                ServiceController sc = new ServiceController(service.ServiceName);
                                sc.Stop();
                                sc.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(10));
                            }
                            // Desinstalar
                            ManagedInstallerClass.InstallHelper(new[] { "/u", typeof(Program).Assembly.Location });
                            hasCommands = true;
                        }
                        // ¿Tenemos un comando de instalación?
                        if (HasCommand(args, "install"))
                        {
                            ManagedInstallerClass.InstallHelper(new[] { typeof(Program).Assembly.Location });
                            hasCommands = true;
                        }
                        // ¿Tenemos un comando de inicio?
                        if (HasCommand(args, "start"))
                        {
                            foreach (var service in ServicesToRun)
                            {
                                ServiceController sc = new ServiceController(service.ServiceName);
                                sc.Start();
                                sc.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(10));
                            }
                            hasCommands = true;
                        }
                        // ¿Tenemos un comando de parada?
                        if (HasCommand(args, "stop"))
                        {
                            foreach (var service in ServicesToRun)
                            {
                                ServiceController sc = new ServiceController(service.ServiceName);
                                sc.Stop();
                                sc.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(10));
                            }
                            hasCommands = false;
                        }
                        // ¿Tenemos un comando de desinstalación?
                        if (HasCommand(args, "uninstall"))
                        {
                            ManagedInstallerClass.InstallHelper(new[] { "/u", typeof(Program).Assembly.Location });
                            hasCommands = true;
                        }
                        // Si no tenemos comandos, mostramos un mensaje de ayuda
                        if (!hasCommands)
                        {
                            Console.WriteLine(@"Uso : {0} [command] [command ...]", Environment.GetCommandLineArgs());
                            Console.WriteLine(@"Comandos : ");
                            Console.WriteLine(@" - install : Instalacion de servicios");
                            Console.WriteLine(@" - uninstall : Desinstalar servicios");
                            Console.WriteLine(@" - start : Iniciar servicios");
                            Console.WriteLine(@" - stop : Detener los servicios");
                            Console.WriteLine(@" - start-services : Instalar e iniciar los servicios.");
                            Console.WriteLine(@" - stop-services : Detener y desinstalar servicios");
                            Console.WriteLine(@" - run-services : Ejecuta servicios en modo interactivo");
                        }
                    }
                    catch (Exception ex)
                    {
                        var oldColor = Console.ForegroundColor;
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(@"Error : {0}", ex.GetBaseException().Message);
                        Console.ForegroundColor = oldColor;
                    }
                }
            }
            else
            {
                // Ejecutar servicios normalmente
                ServiceBase.Run(ServicesToRun);
            }
        }

        /// <summary>
        /// Utilidad para determinar si tenemos un pedido en los argumentos de pedido en línea
        /// </summary>
        static bool HasCommand(string[] args, string command)
        {
            if (args == null || args.Length == 0 || string.IsNullOrWhiteSpace(command)) return false;
            return args.Any(a => string.Equals(a, command, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Ejecuta servicios en modo interactivo
        /// </summary>
        static void RunInteractiveServices(ServiceBase[] servicesToRun)
        {
            Console.WriteLine();
            Console.WriteLine(@"Inicio de servicios en modo interactivo.");
            Console.WriteLine();

            // Recuperación del método a ejecutar en cada servicio para iniciarlo
            MethodInfo onStartMethod = typeof(ServiceBase).GetMethod("OnStart", BindingFlags.Instance | BindingFlags.NonPublic);

            // Bucle de inicio de servicio
            foreach (ServiceBase service in servicesToRun)
            {
                Console.Write(@"Inicio de {0} ... ", service.ServiceName);
                onStartMethod?.Invoke(service, new object[] { new string[] { } });
                Console.WriteLine(@"Iniciado");
            }

            // Esperando el final
            Console.WriteLine();
            Console.WriteLine(@"Presione cualquier tecla para detener los servicios y completar el proceso...");
            Console.ReadKey();
            Console.WriteLine();

            // Recuperando el método a ejecutar en cada servicio para detenerlo
            MethodInfo onStopMethod = typeof(ServiceBase).GetMethod("OnStop", BindingFlags.Instance | BindingFlags.NonPublic);

            // Boucle d'arrêt
            foreach (ServiceBase service in servicesToRun)
            {
                Console.Write(@"Apagar {0} ... ", service.ServiceName);
                onStopMethod?.Invoke(service, null);
                Console.WriteLine(@"Apagado");
            }

            Console.WriteLine();
            Console.WriteLine(@"Todos los servicios están parados.");

            // Espere a que se presione una tecla para no regresar directamente a VS
            if (System.Diagnostics.Debugger.IsAttached)
            {
                Console.WriteLine();
                Console.Write(@"=== Presione cualquier tecla para salir ===");
                Console.ReadKey();
            }
        }

    }
}