﻿using System.Configuration;

namespace Ilsp.Services.LoadShipmentPlan
{
    public class CustomConfigSectionElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
        }

        [ConfigurationProperty("type", IsKey = true, IsRequired = true)]
        public string Type
        {
            get { return (string)this["type"]; }
        }

        [ConfigurationProperty("isRequired", IsKey = true, IsRequired = true)]
        public bool IsRequired
        {
            get { return (bool)this["isRequired"]; }
        }

        [ConfigurationProperty("description", IsKey = true, IsRequired = true)]
        public string Description
        {
            get { return (string)this["description"]; }
        }

        [ConfigurationProperty("notification", IsKey = true, IsRequired = true)]
        public bool Notification
        {
            get { return (bool)this["notification"]; }
        }
    }

    [ConfigurationCollection(typeof(CustomConfigSectionElement), AddItemName = "node")]
    public class CustomConfigSectionCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new CustomConfigSectionElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CustomConfigSectionElement)element).Name;
        }
    }

    public class CustomConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("NodeList", IsDefaultCollection = true)]
        public CustomConfigSectionCollection NodeList
        {
            get { return (CustomConfigSectionCollection)this["NodeList"]; }
        }
    }
}
