﻿using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.Common.Log;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Timers;
using System.Transactions;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.LoadShipmentPlan;
using Ilsp.BitacoraIvr.Ent.LspApp;
using Ilsp.BitacoraIvr.Tl;
using Ilsp.BitacoraIvr.Tl.ExcelData;
using Ilsp.BitacoraIvr.FluentValidators.BitacoraRules;
using Ilsp.Services.LoadShipmentPlan.Mq;

namespace Ilsp.Services.LoadShipmentPlan
{
    public partial class LoadShipmentPlan : ServiceBase
    {
        private Timer _timer;
        private readonly string _ftpServer;
        private readonly string _ftpUser;
        private readonly string _ftpPassword;
        private readonly string _pendingShipments;
        private readonly string _processShipments;
        private readonly string _errorShipments;
        private readonly string _tempPath;
        private readonly string _resultsShipments;
        private readonly char _delimiterSplit;
        private readonly char _separatorDate;
        private readonly int _seconds;
        private readonly ConcurrentDictionary<string, FilePath> _currentFiles = new ConcurrentDictionary<string, FilePath>();
        private List<ColumnMap> _columns = new List<ColumnMap>();
        private readonly string _lettersAccentsSpacePattern;
        private readonly string _lettersAccentsNumbersSpacePattern;
        private readonly string _platesPattern;
        private readonly string _economicPattern;
        private readonly string _numbersPattern;
        private readonly string _cnnStringBitacora;
        private readonly string _cnnStringIdentityServer;
        private readonly string _rolesToGetUsers;

        public LoadShipmentPlan()
        {
            _ftpServer = ConfigurationManager.AppSettings["sServer"];
            _ftpUser = ConfigurationManager.AppSettings["sUser"];
            _ftpPassword = ConfigurationManager.AppSettings["sPassword"];
            _pendingShipments = ConfigurationManager.AppSettings["PendingShipments"];
            _processShipments = ConfigurationManager.AppSettings["ProcessShipments"];
            _errorShipments = ConfigurationManager.AppSettings["ErrorShipments"];
            _tempPath = ConfigurationManager.AppSettings["tempPath"];
            _resultsShipments = ConfigurationManager.AppSettings["ResultsShipments"];
            _delimiterSplit = char.Parse(ConfigurationManager.AppSettings["DelimiterSplit"]);
            _separatorDate = char.Parse(ConfigurationManager.AppSettings["SeparatorDate"]);
            _seconds = int.Parse(ConfigurationManager.AppSettings["TimerSeconds"]);
            _cnnStringBitacora = ConfigurationManager.ConnectionStrings["BitacoraAiAdo"].ToString().Trim();
            _cnnStringIdentityServer = ConfigurationManager.ConnectionStrings["IdentityAdo"].ToString().Trim();
            _rolesToGetUsers = ConfigurationManager.AppSettings["RolesToGetUsers"];

            var configSection = (CustomConfigSection)ConfigurationManager.GetSection("CustomConfigSection");
            var nodeList = configSection.NodeList;

            foreach (CustomConfigSectionElement node in nodeList)
            {
                _columns.Add(new ColumnMap
                {
                    ColumnName = node.Name,
                    ColumnType = node.Type,
                    ColumnRequired = node.IsRequired,
                    Description = node.Description,
                    Notification = node.Notification
                });
            }

            _lettersAccentsSpacePattern = ConfigurationManager.AppSettings["LettersAccentsSpacePattern"];
            _lettersAccentsNumbersSpacePattern = ConfigurationManager.AppSettings["LettersAccentsNumbersSpacePattern"];
            _platesPattern = ConfigurationManager.AppSettings["PlatesPattern"];
            _economicPattern = ConfigurationManager.AppSettings["EconomicPattern"];
            _numbersPattern = ConfigurationManager.AppSettings["NumbersPattern"];

            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            LoggerManager.Instance.Info("Service was started");
            _timer = new Timer { Interval = _seconds * 1000 };
            _timer.Elapsed += timer_Tick;
            _timer.Enabled = true;
        }

        protected override void OnStop()
        {
            LoggerManager.Instance.Info("Service was stopped");
        }

        private void timer_Tick(object sender, ElapsedEventArgs e)
        {
            try
            {
                ReadAndMoveFiles();
            }
            catch (Exception aE)
            {
                LoggerManager.Instance.Error(aE);
            }
        }

        /// <summary>
        /// FTP Connecction
        /// Read files and move to process folder
        /// Call initial validation
        /// </summary>
        private void ReadAndMoveFiles()
        {
            _timer.Stop();

            if (!Directory.Exists(_tempPath))
            {
                Directory.CreateDirectory(_tempPath);
            }

            var ftpRequest = (FtpWebRequest)WebRequest.Create(new Uri($"ftp://{_ftpServer}/{_pendingShipments}/", UriKind.Absolute));
            ftpRequest.UsePassive = true;
            ftpRequest.KeepAlive = false;
            ftpRequest.Credentials = new NetworkCredential(_ftpUser, _ftpPassword);
            ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
            try
            {
                #region obtiene los archivos del origen

                using (var response = (FtpWebResponse)ftpRequest.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream ?? throw new InvalidOperationException(), true))
                        {
                            while (!reader.EndOfStream)
                            {
                                var line = reader.ReadLine();
                                if (!string.IsNullOrEmpty(line))
                                {
                                    var nowStr = DateTime.Now.ToString("LSP_ddMMyyyy_HHmmss_");
                                    var originalFileNme = line;
                                    var fileName = $"{nowStr}{(line.Contains(".xlsx") ? line : line.Replace(".xls", ".xlsx"))}";
                                    var filePath = new FilePath
                                    {
                                        OriginalFileName = originalFileNme,
                                        ImportedFileName = fileName,
                                        ProcessFileName = fileName,
                                        ErrorsFileName = $"Errores_{fileName}",
                                        ResultlFileName = $"Resultados_{fileName}"
                                    };
                                    LoggerManager.Instance.Info($"Archivo agregado al Diccionario: {line}");
                                    _currentFiles.TryAdd(fileName, filePath);

                                }
                            }
                        }
                    }
                }
                try
                {

                    ParalellValidate();

                }
                catch (Exception aE)
                {
                    LoggerManager.Instance.Error(aE.StackTrace);
                }

                #endregion
            }
            catch (WebException aE)
            {
                var status = ((FtpWebResponse)aE.Response).StatusDescription;
                LoggerManager.Instance.Error(status);
            }
            _timer.Start();
        }

        /// <summary>
        /// Get and process in parallel all files 
        /// </summary>
        private void ParalellValidate()
        {
            Parallel.ForEach(_currentFiles, currentElement =>
            {
                var file = currentElement.Value;
                LoggerManager.Instance.Info($"Archivo en parallel: {file.OriginalFileName}");
                try
                {
                    try
                    {
                        using (var bl = new BitacorasBl(_cnnStringBitacora, "system"))
                        {
                            bl.CreateUpdateBitacoraMassiveLoad(0, file.OriginalFileName, string.Empty, DateTime.Now, string.Empty, DateTime.Now, 'C');
                        }

                        LoggerManager.Instance.Info("Insert state to bitacoras:mmasive_load");

                        ProcessFile(file);

                        #region recoge y mueve los archivos de error

                        MoveErrorFiles(_tempPath, file);

                        //if (Directory.Exists(_tempPath))
                        //{
                        //    DeleteDirectory(_tempPath);
                        //}

                        #endregion recoge y mueve los archivos de error

                        _currentFiles.TryRemove(currentElement.Key, out _);
                        LoggerManager.Instance.Info($"Elimina archivo en parallel: {file.OriginalFileName}");
                    }
                    catch (Exception aE)
                    {
                        LoggerManager.Instance.Error(aE.StackTrace);
                    }
                }
                catch (WebException aE)
                {
                    LoggerManager.Instance.Error($"El archivo {file.OriginalFileName} no se movio a la carpeta PendingShipments. Error: \n{aE}");
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="originalFileName">File ready to be relocated</param>
        /// /// <param name="lastFilName"></param>
        private void MoveFilesToProcessOrError(string originalFileName, string lastFilName)
        {
            try
            {
                //move file to be process to be processed
                var request = (FtpWebRequest)WebRequest.Create(new Uri($"ftp://{_ftpServer}/{_pendingShipments}/{originalFileName}", UriKind.Absolute));
                request.Method = WebRequestMethods.Ftp.Rename;
                request.Credentials = new NetworkCredential(_ftpUser, _ftpPassword);
                request.RenameTo = $"../{_processShipments}/{originalFileName}";
                var response = (FtpWebResponse)request.GetResponse();
                response.Close();
                LoggerManager.Instance.Info($"The file {originalFileName} was read");

                var newRequest = (FtpWebRequest)WebRequest.Create(new Uri($"ftp://{_ftpServer}/{_processShipments}/{originalFileName}", UriKind.Absolute));
                newRequest.Method = WebRequestMethods.Ftp.Rename;
                newRequest.Credentials = new NetworkCredential(_ftpUser, _ftpPassword);
                newRequest.RenameTo = lastFilName;
                var response2 = (FtpWebResponse)newRequest.GetResponse();

                System.Threading.Thread.Sleep(250);
                response2.Close();
            }
            catch (Exception aE)
            {
                LoggerManager.Instance.Error($"File: {originalFileName} was not moved from the PendingShipments folder. Error: \n{aE}");
            }
        }

        /// <summary>
        /// Method that deletes files and temporary file processing directory
        /// </summary>
        /// <param name="path">Temporary path where the files are processed</param>
        private void DeleteDirectory(string path)
        {
            foreach (var filename in Directory.GetFiles(path))
            {
                File.Delete(filename);
            }

            foreach (var subfolder in Directory.GetDirectories(path))
            {
                DeleteDirectory(subfolder);
            }

            Directory.Delete(path);
        }

        /// <summary>
        /// Method that validates the generalities of the excel file
        /// </summary>
        /// <param name="file">Name of file</param>
        /// <returns>Returns a flag with the result of success or failure of the validation</returns> 
        private void ProcessFile(FilePath file)
        {
            try
            {
                if (Path.GetExtension(file.OriginalFileName)?.Trim().ToLower() == ".xls" || Path.GetExtension(file.OriginalFileName)?.Trim().ToLower() == ".xlsx")
                {
                    MoveFilesToProcessOrError(file.OriginalFileName, file.ProcessFileName);
                    LoggerManager.Instance.Info($"El archivo {file.OriginalFileName} esta en proceso");

                    string localErrorPath = $"{_tempPath}{file.ErrorsFileName}";
                    var ftpProccesedPath = $"ftp://{_ftpServer}/{_processShipments}/";

                    var nameTemporalFile = file.ResultlFileName;
                    var temporalPath = _tempPath + nameTemporalFile;
                    var ftpfullpath = ftpProccesedPath + file.ProcessFileName;

                    CreateProcessFile(ftpfullpath, temporalPath);

                    var importFileName = temporalPath;

                    try
                    {
                        using (var bl = new BitacorasBl(_cnnStringBitacora, "system"))
                        {
                            bl.CreateUpdateBitacoraMassiveLoad(0, file.OriginalFileName,
                                importFileName.Replace(_tempPath, ""), DateTime.Now, localErrorPath,
                                DateTime.Now, 'P');
                        }

                        LoggerManager.Instance.Info("Proceso actualizado para en analisis de la carga masiva.");
                    }
                    catch (Exception aE)
                    {
                        LoggerManager.Instance.Error(aE.StackTrace);
                    }

                    try
                    {
                        var fileExtension = Path.GetExtension(importFileName).Trim().ToLower();
                        if (fileExtension.Equals(".xls") || fileExtension.Equals(".xlsx"))
                        {
                            DataTable dt;
                            try
                            {
                                var fileBytes = File.ReadAllBytes(importFileName);
                                dt = ExcelTo.ExcelToDataTable(fileBytes, true);
                                if (!_columns.Where(c => c.ColumnRequired).All(c => dt.Columns.Contains(c.ColumnName)))
                                {
                                    var messageException = "El archivo no contiene el numero de columnas requeridas necesarias.";
                                    LoggerManager.Instance.Error(messageException);
                                    CreateErrorExcelFile(file, new List<FinalValidation>
                                    {
                                        new FinalValidation
                                        {
                                            NumberRow = 1,
                                            SourceFile = importFileName,
                                            Messages = new List<FluentMessage>
                                            {
                                                new FluentMessage
                                                {
                                                    Comments = messageException,
                                                    Success = false
                                                }
                                            }
                                        }
                                    });
                                    return;
                                }
                            }
                            catch (Exception aE)
                            {
                                LoggerManager.Instance.Error($"In file: {Path.GetFileName(file.OriginalFileName)}, the information is not charged to the datatable. Error: \n{aE}");
                                var messageException = $"No se consiguio leer el archivo Excel por error: {aE.Message}";
                                CreateErrorExcelFile(file, new List<FinalValidation>
                                {
                                    new FinalValidation
                                    {
                                        NumberRow = 1,
                                        SourceFile = importFileName,
                                        Messages = new List<FluentMessage>
                                        {
                                            new FluentMessage
                                            {
                                                Comments = messageException,
                                                Success = false
                                            }
                                        }
                                    }
                                });
                                return;
                            }
                            var dataColumns = dt.Columns;
                            var dataRows = dt.Rows;
                            UpdateProcessFileAddHeaderResult(temporalPath, dt.Columns.Count);
                            for (var index = 0; index < dataRows.Count; index++)
                            {
                                var rowNumber = index + 1;
                                var validationsItem = new FinalValidation
                                {
                                    NumberRow = rowNumber,
                                    SourceFile = importFileName,
                                    Messages = new List<FluentMessage>()
                                };
                                var listMessages = new List<FluentMessage>();
                                var dataRow = dataRows[index];
                                //var isDuplicateOnFile = false;

                                //#region Valida duplicados en archivo

                                ////verifica que el registro a insertar no sea identico a alguno previo
                                //// ReSharper disable once LoopVariableIsNeverChangedInsideLoop
                                //for (var j = 0; j < index; j++)
                                //{
                                //    isDuplicateOnFile = _columns.All(c => dataRows[j][c.ColumnName].Equals(dataRow[c.ColumnName]));
                                //    if (isDuplicateOnFile)
                                //    {
                                //        var messageException = "El registro No. " + rowNumber + " esta duplicado";
                                //        LoggerManager.Instance.Error("En el archivo: " + Path.GetFileName(file.OriginalFileName) + ", el numero file " + rowNumber + " esta duplicado.");
                                //        listMessages.Add(new FluentMessage { Comments = messageException, Success = false });
                                //        break;
                                //    }
                                //}

                                //#endregion

                                //if (!isDuplicateOnFile)
                                //{
                                var messageRequired = "No se encontro el registro de la columna: {0} -> dato requerido";
                                #region Mapeo de datos

                                var bitacoraItem = new BitacoraEnt
                                {
                                    Destinies = new List<DestinyEnt>(),
                                    Custody = null,
                                    Status = "P"
                                };
                                var isValidBitacora = true;

                                if (IsValidColumn(dataColumns, "CLIENTE ILSP", dataRow, listMessages, ref isValidBitacora))
                                {
                                    using (var bl = new CatCustomersBl(_cnnStringBitacora, "system"))
                                    {
                                        var columnName = "CLIENTE ILSP";
                                        var list = bl.Search(dataRow[columnName].ToString().Trim()).ToList();
                                        if (list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                        {
                                            bitacoraItem.Customer = list.First(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower()));
                                        }
                                        else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                        {
                                            isValidBitacora = false;
                                            listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                        }
                                    }
                                }

                                if (IsValidColumn(dataColumns, "AVANCE", dataRow, listMessages, ref isValidBitacora))
                                {
                                    var columnName = "AVANCE";
                                    if (!string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                    {
                                        if (dataRow[columnName].ToString().Trim().ToLower().Trim().Equals("Destino".Trim().ToLower()) || dataRow[columnName].ToString().Trim().ToLower().Trim().Equals("Avance".Trim().ToLower()))
                                        {
                                            bitacoraItem.Advance = dataRow[columnName].ToString().Trim();
                                        }
                                        else
                                        {
                                            var message = $"El valor de la columna '{columnName}' es invalido -> dato requerido";
                                            listMessages.Add(new FluentMessage { Comments = message, ColumnName = columnName, Success = false });
                                            isValidBitacora = false;
                                        }
                                    }
                                    else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                    {
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                    }
                                }

                                if (IsValidColumn(dataColumns, "ORIGEN", dataRow, listMessages, ref isValidBitacora))
                                {
                                    if (bitacoraItem.Customer != null)
                                    {
                                        using (var bl = new CatOriginsDestinationsBl(_cnnStringBitacora, "system"))
                                        {
                                            var columnName = "ORIGEN";
                                            var list = bl.SearchOrigins(bitacoraItem.Customer.Id, dataRow[columnName].ToString().Trim()).ToList();
                                            if (list.Any(item => item.Alias.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                            {
                                                bitacoraItem.Origin = list.First(item => item.Alias.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower()));
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Alias.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                            {
                                                SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString().Trim());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }

                                if (IsValidColumn(dataColumns, "LÍNEA", dataRow, listMessages, ref isValidBitacora))
                                {
                                    if (bitacoraItem.Customer != null)
                                    {
                                        using (var bl = new CatTransportLinesBl(_cnnStringBitacora, "system"))
                                        {
                                            var columnName = "LÍNEA";
                                            var list = bl.Search(bitacoraItem.Customer.Id, dataRow[columnName].ToString().Trim()).ToList();
                                            if (list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                            {
                                                bitacoraItem.TransportLine = list.First(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower()));
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                            {
                                                SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString().Trim());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }

                                if (IsValidColumn(dataColumns, "TIPO DE MONITOREO", dataRow, listMessages, ref isValidBitacora))
                                {
                                    using (var bl = new CatMonitoringTypesBl(_cnnStringBitacora, "system"))
                                    {
                                        var columnName = "TIPO DE MONITOREO";
                                        var list = bl.Search(dataRow[columnName].ToString().Trim()).ToList();
                                        if (list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                        {
                                            bitacoraItem.MonitoringType = list.First(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower()));
                                            bitacoraItem.IsMirrorAccount = bitacoraItem.MonitoringType.IsMirrorAccount ?? false;
                                        }
                                        else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                        {
                                            isValidBitacora = false;
                                            listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                        }
                                    }
                                }

                                if (IsValidColumn(dataColumns, "TIPO SERVICIO", dataRow, listMessages, ref isValidBitacora))
                                {
                                    using (var bl = new CatServiceTypesBl(_cnnStringBitacora, "system"))
                                    {
                                        var columnName = "TIPO SERVICIO";
                                        var list = bl.Search(dataRow[columnName].ToString().Trim()).ToList();
                                        if (list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                        {
                                            bitacoraItem.ServiceType = list.First(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower()));
                                        }
                                        else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                        {
                                            isValidBitacora = false;
                                            listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                        }
                                    }
                                }

                                #region Destinos

                                var isValidDestinations = true;
                                if (!IsValidColumn(dataColumns, "DESTINO", dataRow, listMessages, ref isValidBitacora))
                                {
                                    isValidDestinations = false;
                                    isValidBitacora = false;
                                }
                                if (!IsValidColumn(dataColumns, "FECHA DE CITA", dataRow, listMessages, ref isValidBitacora))
                                {
                                    isValidDestinations = false;
                                    isValidBitacora = false;
                                }
                                if (!IsValidColumn(dataColumns, "HORA DE CITA", dataRow, listMessages, ref isValidBitacora))
                                {
                                    isValidDestinations = false;
                                    isValidBitacora = false;
                                }

                                if (isValidDestinations)
                                {
                                    var destiniesSplit = dataRow["DESTINO"].ToString().Trim().Split(_delimiterSplit);
                                    var deliveryNumberSplit = dataRow["ENVÍO"].ToString().Trim().Split(_delimiterSplit);
                                    var dateSplit = dataRow["FECHA DE CITA"].ToString().Trim().Split(_delimiterSplit);
                                    var timeSplit = dataRow["HORA DE CITA"].ToString().Trim().Split(_delimiterSplit);
                                    var numberItems = destiniesSplit.Length;
                                    if (dateSplit.Length == numberItems && timeSplit.Length == numberItems && (string.IsNullOrEmpty(dataRow["ENVÍO"].ToString().Trim()) || deliveryNumberSplit.Length == numberItems))
                                    {
                                        for (int i = 0; i < destiniesSplit.Length; i++)
                                        {
                                            var destinyItem = destiniesSplit[i];
                                            var deliveryNumberItem = !string.IsNullOrEmpty(dataRow["ENVÍO"].ToString().Trim()) ? deliveryNumberSplit[i] : "SE";
                                            var dateItem = dateSplit[i];
                                            var timeItem = timeSplit[i];

                                            var destiny = new DestinyEnt();
                                            var date = "";
                                            var destinyValid = true;
                                            string columnName;

                                            if (bitacoraItem.Customer != null)
                                            {
                                                using (var bl = new CatOriginsDestinationsBl(_cnnStringBitacora, "system"))
                                                {
                                                    columnName = "DESTINO";
                                                    var list = bl.SearchDestinies(bitacoraItem.Customer.Id, destinyItem).ToList();
                                                    if (list.Any(item => item.Alias.Trim().ToLower().Equals(destinyItem.Trim().ToLower())))
                                                    {
                                                        destiny.Destiny = list.First(item => item.Alias.Trim().ToLower().Equals(destinyItem.Trim().ToLower()));
                                                    }
                                                    else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                                    {
                                                        destinyValid = false;
                                                        isValidBitacora = false;
                                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                                    }
                                                    if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Alias.Trim().ToLower().Equals(destinyItem.Trim().ToLower())))
                                                    {
                                                        SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString().Trim());
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                isValidBitacora = false;
                                            }

                                            columnName = "ENVÍO";
                                            if (!string.IsNullOrEmpty(deliveryNumberItem))
                                            {
                                                destiny.DeliveryNumber = deliveryNumberItem;
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                destinyValid = false;
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            else if (string.IsNullOrEmpty(deliveryNumberItem))
                                            {
                                                destiny.DeliveryNumber = string.Empty;
                                            }

                                            columnName = "FECHA DE CITA";
                                            if (!string.IsNullOrEmpty(dateItem))
                                            {
                                                var rgx = new Regex("^([0]{1}[1-9]{1}|[1]{1}[0-9]{1}|[2]{1}[0-9]{1}|[3]{1}[01]{1})[." + _separatorDate + "]([0]?[1-9]{1}|[1][0-2]{1})[." + _separatorDate + "]([0-9]{4})$");
                                                if (rgx.IsMatch(dateItem))
                                                {
                                                    date = dateItem;
                                                }
                                                else
                                                {
                                                    destinyValid = false;
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = $"El formato de la columna {columnName} es invalido", ColumnName = columnName, Success = false });
                                                }
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                destinyValid = false;
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }

                                            columnName = "HORA DE CITA";
                                            if (!string.IsNullOrEmpty(timeItem))
                                            {
                                                var rgx = new Regex("^(?:0?[0-9]|1[0-9]|2[0-3]):(00|30)$");
                                                if (rgx.IsMatch(timeItem))
                                                {
                                                    date += ' ' + timeItem;
                                                }
                                                else
                                                {
                                                    destinyValid = false;
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = $"El formato de la columna {columnName} es invalido", ColumnName = columnName, Success = false });
                                                }
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                destinyValid = false;
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }

                                            if (destinyValid)
                                            {
                                                try
                                                {
                                                    destiny.AppointmentDate = OptionsDateTime.ToDateTimeMx(date, _separatorDate).ToString("yyyy/MM/dd HH:mm:ss");
                                                    bitacoraItem.Destinies.Add(destiny);
                                                }
                                                catch (Exception)
                                                {
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = "Formato de fecha invalido", ColumnName = string.Empty, Success = false });
                                                }

                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = "Columnas de DESTINO, FECHA DE CITA, HORA DE CITA son invalidas.", ColumnName = string.Empty, Success = false });
                                    }
                                }
                                else
                                {
                                    isValidBitacora = false;
                                    listMessages.Add(new FluentMessage { Comments = "Columnas de DESTINO, FECHA DE CITA, HORA DE CITA son invalidas.", ColumnName = string.Empty, Success = false });
                                }



                                #endregion

                                #region Transporte

                                var transport = new TransportEnt
                                {
                                    Operators = new List<OperatorEnt>(),
                                    Gpss = new List<GpsEnt>(),
                                    Containers = new List<ContainerEnt>(),
                                    UnitStamps = new List<UnitStampEnt>()
                                };

                                #region Operadores
                                var isValidOperators = true;
                                if (!IsValidColumn(dataColumns, "NOMBRE OPERADOR", dataRow, listMessages, ref isValidBitacora))
                                {
                                    isValidBitacora = false;
                                    isValidOperators = false;
                                }

                                if (!IsValidColumn(dataColumns, "MEDIO DEL OPERADOR", dataRow, listMessages, ref isValidBitacora))
                                {
                                    isValidBitacora = false;
                                    isValidOperators = false;
                                }

                                if (isValidOperators)
                                {
                                    var operatorsSplit = dataRow["NOMBRE OPERADOR"].ToString().Trim().Split(_delimiterSplit);
                                    var cellPhonesSplit = dataRow["MEDIO DEL OPERADOR"].ToString().Trim().Split(_delimiterSplit);
                                    var numberItems = operatorsSplit.Length;
                                    if (operatorsSplit.Length > 0 && (string.IsNullOrEmpty(dataRow["MEDIO DEL OPERADOR"].ToString().Trim()) || cellPhonesSplit.Length == numberItems))
                                    {
                                        for (int i = 0; i < operatorsSplit.Length; i++)
                                        {
                                            var operatorItemSplit = operatorsSplit[i];
                                            var deliveryNumberItem = !string.IsNullOrEmpty(dataRow["MEDIO DEL OPERADOR"].ToString().Trim()) ? cellPhonesSplit[i] : "0";

                                            var operatorItem = new OperatorEnt();
                                            var operatorValid = true;
                                            string columnName;

                                            columnName = "NOMBRE OPERADOR";
                                            if (!string.IsNullOrEmpty(operatorItemSplit))
                                            {
                                                var rgx = new Regex(_lettersAccentsSpacePattern);
                                                if (rgx.IsMatch(operatorItemSplit))
                                                {
                                                    operatorItem.Name = operatorItemSplit;
                                                }
                                                else
                                                {
                                                    operatorValid = false;
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = $"El formato de la columna {columnName} es invalido", ColumnName = columnName, Success = false });
                                                }
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                operatorValid = false;
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }

                                            columnName = "MEDIO DEL OPERADOR";
                                            if (!string.IsNullOrEmpty(deliveryNumberItem))
                                            {
                                                var rgx = new Regex(_numbersPattern);
                                                if (rgx.IsMatch(deliveryNumberItem))
                                                {
                                                    operatorItem.CellPhoeOne = deliveryNumberItem;
                                                }
                                                else
                                                {
                                                    operatorValid = false;
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = $"El formato de la columna {columnName} es invalido", ColumnName = columnName, Success = false });
                                                }
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                operatorValid = false;
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            else if (string.IsNullOrEmpty(deliveryNumberItem))
                                            {
                                                operatorItem.CellPhoeOne = "0";
                                            }

                                            if (operatorValid)
                                            {
                                                transport.Operators.Add(operatorItem);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = "Columnas de NOMBRE OPERADOR y/o MEDIO DEL OPERADOR son invalidas.", ColumnName = string.Empty, Success = false });
                                    }
                                }
                                else
                                {
                                    isValidBitacora = false;
                                    listMessages.Add(new FluentMessage { Comments = "Columnas de NOMBRE OPERADOR y/o MEDIO DEL OPERADOR son invalidas.", ColumnName = string.Empty, Success = false });
                                }

                                #endregion

                                #region Vehiculo

                                var vehicleItem = new PlateEnt
                                {
                                    Colors = new List<ColorEnt>()
                                };
                                var vehicleValid = true;
                                if (IsValidColumn(dataColumns, "PLACAS", dataRow, listMessages, ref isValidBitacora, new Regex(_platesPattern)))
                                {
                                    if (bitacoraItem.Customer != null && bitacoraItem.TransportLine != null)
                                    {
                                        var columnName = "PLACAS";
                                        using (var bl = new CatTransportLinesVehiclesBl(_cnnStringBitacora, "system"))
                                        {
                                            var list = bl.Search(bitacoraItem.Customer.Id, bitacoraItem.TransportLine.Id, dataRow[columnName].ToString().Trim()).ToList();
                                            if (list.Any(item => item.NumPlate.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                            {
                                                vehicleItem = list.First(item => item.NumPlate.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower()));
                                            }
                                            else
                                            {
                                                vehicleItem.Id = null;
                                                vehicleItem.NumPlate = dataRow[columnName].ToString().Trim().ToUpper().Trim();
                                            }
                                            //else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            //{
                                            //    vehicleValid = false;
                                            //    isValidBitacora = false;
                                            //    listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            //}
                                            if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.NumPlate.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                            {
                                                SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString().Trim());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        vehicleValid = false;
                                        isValidBitacora = false;
                                    }
                                }
                                else
                                {
                                    vehicleValid = false;
                                }

                                if (IsValidColumn(dataColumns, "TIPO DE UNIDAD", dataRow, listMessages, ref isValidBitacora))
                                {
                                    if (bitacoraItem.Customer != null)
                                    {
                                        using (var bl = new CatUnitTypesBl(_cnnStringBitacora, "system"))
                                        {
                                            var columnName = "TIPO DE UNIDAD";
                                            var list = bl.Search(bitacoraItem.Customer.Id, dataRow[columnName].ToString().Trim()).ToList();
                                            if (list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                            {
                                                vehicleItem.UnitType = list.First(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower()));
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                            {
                                                SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString().Trim());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }
                                else
                                {
                                    vehicleValid = false;
                                }

                                if (IsValidColumn(dataColumns, "MARCA", dataRow, listMessages, ref isValidBitacora))
                                {
                                    if (bitacoraItem.Customer != null)
                                    {
                                        using (var bl = new CatBrandsBl(_cnnStringBitacora, "system"))
                                        {
                                            var columnName = "MARCA";
                                            var list = bl.Search(bitacoraItem.Customer.Id, dataRow[columnName].ToString().Trim())
                                                .ToList();
                                            if (list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                            {
                                                vehicleItem.Brand = list.First(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower()));
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                            {
                                                SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString().Trim());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }
                                else
                                {
                                    vehicleValid = false;
                                }

                                if (IsValidColumn(dataColumns, "COLOR", dataRow, listMessages, ref isValidBitacora))
                                {
                                    if (bitacoraItem.Customer != null)
                                    {
                                        var columnName = "COLOR";
                                        if (!string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                        {
                                            var collectionSplit = dataRow[columnName].ToString().Trim().Split(_delimiterSplit);
                                            using (var bl = new CatColorsBl(_cnnStringBitacora, "system"))
                                            {
                                                vehicleItem.Colors = new List<ColorEnt>();
                                                foreach (var itemSplit in collectionSplit)
                                                {
                                                    var list = bl.Search(bitacoraItem.Customer.Id, itemSplit).ToList();
                                                    if (list.Any(item => item.Name.Trim().ToLower().Equals(itemSplit.Trim().ToLower())))
                                                    {
                                                        vehicleItem.Colors.Add(list.First(item => item.Name.Trim().ToLower().Equals(itemSplit.Trim().ToLower())));
                                                    }
                                                    else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                                    {
                                                        isValidBitacora = false;
                                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                                    }
                                                    if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Name.Trim().ToLower().Equals(itemSplit.Trim().ToLower())))
                                                    {
                                                        SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), itemSplit);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }
                                else
                                {
                                    vehicleValid = false;
                                }

                                if (IsValidColumn(dataColumns, "NO. ECONÓMICO", dataRow, listMessages, ref isValidBitacora, new Regex(_economicPattern)))
                                {
                                    var columnName = "NO. ECONÓMICO";
                                    if (!string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                    {
                                        vehicleItem.Economic = dataRow[columnName].ToString().Trim();
                                    }
                                    else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                    {
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                    }
                                    else if (string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                    {
                                        vehicleItem.Economic = "SE";
                                    }
                                }
                                else
                                {
                                    vehicleValid = false;
                                }

                                if (IsValidColumn(dataColumns, "MODELO", dataRow, listMessages, ref isValidBitacora))
                                {
                                    if (bitacoraItem.Customer != null)
                                    {
                                        using (var bl = new CatModelsBl(_cnnStringBitacora, "system"))
                                        {
                                            var columnName = "MODELO";
                                            var list = bl.Search(bitacoraItem.Customer.Id, dataRow[columnName].ToString().Trim()).ToList();
                                            if (list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                            {
                                                var rgx = new Regex(_numbersPattern);
                                                if (rgx.IsMatch(dataRow[columnName].ToString().Trim()))
                                                {
                                                    vehicleItem.Model = list.First(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower()));
                                                }
                                                else
                                                {
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = $"El formato de la columna {columnName} es invalido", ColumnName = columnName, Success = false });
                                                }
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }
                                else
                                {
                                    vehicleValid = false;
                                }

                                if (vehicleValid)
                                {
                                    transport.Plate = vehicleItem;
                                }

                                #endregion

                                if (IsValidColumn(dataColumns, "GPS", dataRow, listMessages, ref isValidBitacora))
                                {
                                    if (bitacoraItem.Customer != null)
                                    {
                                        var columnName = "GPS";
                                        if (!string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                        {
                                            var collectionSplit = dataRow[columnName].ToString().Trim().Split(_delimiterSplit);
                                            if (collectionSplit.Any())
                                            {
                                                using (var bl = new CatGpsBl(_cnnStringBitacora, "system"))
                                                {
                                                    foreach (var itemSplit in collectionSplit)
                                                    {
                                                        var list = bl.Search(string.Empty, bitacoraItem.Customer.Id, itemSplit).ToList();
                                                        if (list.Any(item => item.Name.Trim().ToLower().Equals(itemSplit.Trim().ToLower())))
                                                        {
                                                            transport.Gpss.Add(list.First(item => item.Name.Trim().ToLower().Equals(itemSplit.Trim().ToLower())));
                                                        }
                                                        else
                                                        {
                                                            if (!bitacoraItem.MonitoringType.Name.Trim().ToLower().Equals("Web Service".Trim().ToLower()))
                                                            {
                                                                transport.Observations += $"No se encuentra el Gps {itemSplit} o no posiciona.\n";
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (transport.Gpss.Count == 0 && !bitacoraItem.MonitoringType.Name.Trim().ToLower().Equals("Activo".Trim().ToLower()))
                                        {
                                            if (!bitacoraItem.MonitoringType.Name.Trim().ToLower().Equals("Web Service".Trim().ToLower()))
                                            {
                                                using (var bl = new CatMonitoringTypesBl(_cnnStringBitacora, "system"))
                                                {
                                                    var list = bl.Search("Activo").ToList();
                                                    if (list.Any(item => item.Name.Trim().ToLower().Equals("Activo".Trim().ToLower())))
                                                    {
                                                        bitacoraItem.MonitoringType = list.First(item => item.Name.Trim().ToLower().Equals("Activo".Trim().ToLower()));
                                                        bitacoraItem.IsMirrorAccount = bitacoraItem.MonitoringType.IsMirrorAccount ?? false;
                                                    }
                                                }
                                                if (string.IsNullOrEmpty(transport.Observations))
                                                {
                                                    transport.Observations += $"La columna {columnName} se encuentra vacia.\n";
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }

                                var listSellos = new List<string>();

                                #region Contenedor 1

                                if (!string.IsNullOrEmpty(dataRow["TIPO DE REMOLQUE_1"].ToString().Trim()) ||
                                    !string.IsNullOrEmpty(dataRow["NÚMERO ECONÓMICO_1"].ToString().Trim()) ||
                                    !string.IsNullOrEmpty(dataRow["COLOR_1"].ToString().Trim()) ||
                                    !string.IsNullOrEmpty(dataRow["PLACAS_1"].ToString().Trim()))
                                {
                                    var containerValid1 = true;
                                    if (string.IsNullOrEmpty(dataRow["TIPO DE REMOLQUE_1"].ToString().Trim()))
                                    {
                                        containerValid1 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "TIPO DE REMOLQUE_1"), ColumnName = "TIPO DE REMOLQUE_1", Success = false });
                                    }
                                    if (string.IsNullOrEmpty(dataRow["NÚMERO ECONÓMICO_1"].ToString().Trim()))
                                    {
                                        containerValid1 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "NÚMERO ECONÓMICO_1"), ColumnName = "NÚMERO ECONÓMICO_1", Success = false });
                                    }
                                    if (string.IsNullOrEmpty(dataRow["COLOR_1"].ToString().Trim()))
                                    {
                                        containerValid1 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "COLOR_1"), ColumnName = "COLOR_1", Success = false });
                                    }
                                    if (string.IsNullOrEmpty(dataRow["PLACAS_1"].ToString().Trim()))
                                    {
                                        containerValid1 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "PLACAS_1"), ColumnName = "PLACAS_1", Success = false });
                                    }

                                    if (containerValid1)
                                    {
                                        var containerItem1 = new ContainerEnt
                                        {
                                            Gpss = new List<GpsEnt>()
                                        };

                                        if (IsValidColumn(dataColumns, "TIPO DE REMOLQUE_1", dataRow, listMessages, ref isValidBitacora))
                                        {
                                            using (var bl = new CatContainerTypesBl(_cnnStringBitacora, "system"))
                                            {
                                                var columnName = "TIPO DE REMOLQUE_1";
                                                var list = bl.Search(dataRow[columnName].ToString().Trim()).ToList();
                                                if (list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                                {
                                                    containerItem1.ContainerType = list.First(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower()));
                                                }
                                                else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                                {
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                                }
                                            }
                                        }
                                        else
                                        {
                                            containerValid1 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "NÚMERO ECONÓMICO_1", dataRow, listMessages, ref isValidBitacora, new Regex(_economicPattern)))
                                        {
                                            var columnName = "NÚMERO ECONÓMICO_1";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                            {
                                                containerItem1.Economic = dataRow[columnName].ToString().Trim();
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            else if (string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                            {
                                                containerItem1.Economic = "SE";
                                            }
                                        }
                                        else
                                        {
                                            containerValid1 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "COLOR_1", dataRow, listMessages, ref isValidBitacora, new Regex(_lettersAccentsSpacePattern)))
                                        {
                                            if (bitacoraItem.Customer != null)
                                            {
                                                using (var bl = new CatColorsBl(_cnnStringBitacora, "system"))
                                                {
                                                    var columnName = "COLOR_1";
                                                    var list = bl.Search(bitacoraItem.Customer.Id, dataRow[columnName].ToString().Trim()).ToList();
                                                    if (list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                                    {
                                                        containerItem1.Color = list.First(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower()));
                                                    }
                                                    else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                                    {
                                                        isValidBitacora = false;
                                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                                    }
                                                    if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                                    {
                                                        SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString().Trim());
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                isValidBitacora = false;
                                            }
                                        }
                                        else
                                        {
                                            containerValid1 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "PLACAS_1", dataRow, listMessages, ref isValidBitacora, new Regex(_platesPattern)))
                                        {
                                            var columnName = "PLACAS_1";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                            {
                                                var rgx = new Regex(_platesPattern);
                                                if (rgx.IsMatch(dataRow[columnName].ToString().Trim()))
                                                {
                                                    containerItem1.Plate = dataRow[columnName].ToString().Trim();
                                                }
                                                else
                                                {
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = $"El formato de la columna {columnName} es invalido", ColumnName = columnName, Success = false });
                                                }
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                        }
                                        else
                                        {
                                            containerValid1 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "SELLOS_1", dataRow, listMessages, ref isValidBitacora))
                                        {
                                            var columnName = "SELLOS_1";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                            {
                                                var collectionSplit = dataRow[columnName].ToString().Trim().Split(_delimiterSplit);
                                                var localSellos = new List<string>();
                                                foreach (var itemSplit in collectionSplit)
                                                {
                                                    var rgx = new Regex(_lettersAccentsNumbersSpacePattern);
                                                    if (!string.IsNullOrEmpty(itemSplit) && rgx.IsMatch(itemSplit))
                                                    {
                                                        listSellos.Add(itemSplit);
                                                        localSellos.Add(itemSplit);
                                                    }
                                                    else
                                                    {
                                                        var message = $"El formato de la columna {columnName} es invalido";
                                                        listMessages.Add(new FluentMessage { Comments = message, ColumnName = columnName, Success = false });
                                                        isValidBitacora = false;
                                                    }
                                                }

                                                if (localSellos.Any())
                                                {
                                                    containerItem1.UnitStamp = string.Join("/", localSellos);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            containerValid1 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "CAPACIDAD_1", dataRow, listMessages, ref isValidBitacora, new Regex(_lettersAccentsNumbersSpacePattern)))
                                        {
                                            var columnName = "CAPACIDAD_1";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                            {
                                                containerItem1.Capacity = dataRow[columnName].ToString().Trim();
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                        }
                                        else
                                        {
                                            containerValid1 = false;
                                        }

                                        if (containerValid1)
                                        {
                                            transport.Containers.Add(containerItem1);
                                        }
                                    }
                                }



                                #endregion

                                #region Contenedor 2

                                if (!string.IsNullOrEmpty(dataRow["TIPO DE REMOLQUE_2"].ToString().Trim()) ||
                                    !string.IsNullOrEmpty(dataRow["NÚMERO ECONÓMICO_2"].ToString().Trim()) ||
                                    !string.IsNullOrEmpty(dataRow["COLOR_2"].ToString().Trim()) ||
                                    !string.IsNullOrEmpty(dataRow["PLACAS_2"].ToString().Trim()))
                                {
                                    var containerValid2 = true;
                                    if (string.IsNullOrEmpty(dataRow["TIPO DE REMOLQUE_2"].ToString().Trim()))
                                    {
                                        containerValid2 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "TIPO DE REMOLQUE_2"), ColumnName = "TIPO DE REMOLQUE_2", Success = false });
                                    }
                                    if (string.IsNullOrEmpty(dataRow["NÚMERO ECONÓMICO_2"].ToString().Trim()))
                                    {
                                        containerValid2 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "NÚMERO ECONÓMICO_2"), ColumnName = "NÚMERO ECONÓMICO_2", Success = false });
                                    }
                                    if (string.IsNullOrEmpty(dataRow["COLOR_2"].ToString().Trim()))
                                    {
                                        containerValid2 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "COLOR_2"), ColumnName = "COLOR_2", Success = false });
                                    }
                                    if (string.IsNullOrEmpty(dataRow["PLACAS_2"].ToString().Trim()))
                                    {
                                        containerValid2 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "PLACAS_2"), ColumnName = "PLACAS_2", Success = false });
                                    }

                                    if (containerValid2)
                                    {
                                        var containerItem2 = new ContainerEnt
                                        {
                                            Gpss = new List<GpsEnt>()
                                        };
                                        if (IsValidColumn(dataColumns, "TIPO DE REMOLQUE_2", dataRow, listMessages, ref isValidBitacora))
                                        {
                                            using (var bl = new CatContainerTypesBl(_cnnStringBitacora, "system"))
                                            {
                                                var columnName = "TIPO DE REMOLQUE_2";
                                                var list = bl.Search(dataRow[columnName].ToString().Trim()).ToList();
                                                if (list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                                {
                                                    containerItem2.ContainerType = list.First(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower()));
                                                }
                                                else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                                {
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                                }
                                            }
                                        }
                                        else
                                        {
                                            containerValid2 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "NÚMERO ECONÓMICO_2", dataRow, listMessages, ref isValidBitacora, new Regex(_economicPattern)))
                                        {
                                            var columnName = "NÚMERO ECONÓMICO_2";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                            {
                                                containerItem2.Economic = dataRow[columnName].ToString().Trim();
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            else if (string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                            {
                                                containerItem2.Economic = "SE";
                                            }
                                        }
                                        else
                                        {
                                            containerValid2 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "COLOR_2", dataRow, listMessages, ref isValidBitacora, new Regex(_lettersAccentsSpacePattern)))
                                        {
                                            if (bitacoraItem.Customer != null)
                                            {
                                                using (var bl = new CatColorsBl(_cnnStringBitacora, "system"))
                                                {
                                                    var columnName = "COLOR_2";
                                                    var list = bl.Search(bitacoraItem.Customer.Id, dataRow[columnName].ToString().Trim()).ToList();
                                                    if (list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                                    {
                                                        containerItem2.Color = list.First(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower()));
                                                    }
                                                    else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                                    {
                                                        isValidBitacora = false;
                                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                                    }
                                                    if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                                    {
                                                        SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString().Trim());
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                isValidBitacora = false;
                                            }
                                        }
                                        else
                                        {
                                            containerValid2 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "PLACAS_2", dataRow, listMessages, ref isValidBitacora, new Regex(_platesPattern)))
                                        {
                                            var columnName = "PLACAS_2";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                            {
                                                var rgx = new Regex(_platesPattern);
                                                if (rgx.IsMatch(dataRow[columnName].ToString().Trim()))
                                                {
                                                    containerItem2.Plate = dataRow[columnName].ToString().Trim();
                                                }
                                                else
                                                {
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = $"El formato de la columna {columnName} es invalido", ColumnName = columnName, Success = false });
                                                }
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                        }
                                        else
                                        {
                                            containerValid2 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "SELLOS_2", dataRow, listMessages, ref isValidBitacora))
                                        {
                                            var columnName = "SELLOS_2";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                            {
                                                var collectionSplit = dataRow[columnName].ToString().Trim().Split(_delimiterSplit);
                                                var localSellos = new List<string>();
                                                foreach (var itemSplit in collectionSplit)
                                                {
                                                    var rgx = new Regex(_lettersAccentsNumbersSpacePattern);
                                                    if (!string.IsNullOrEmpty(itemSplit) && rgx.IsMatch(itemSplit))
                                                    {
                                                        listSellos.Add(itemSplit);
                                                        localSellos.Add(itemSplit);
                                                    }
                                                    else
                                                    {
                                                        var message = $"El formato de la columna {columnName} es invalido";
                                                        listMessages.Add(new FluentMessage { Comments = message, ColumnName = columnName, Success = false });
                                                        isValidBitacora = false;
                                                    }
                                                }

                                                if (localSellos.Any())
                                                {
                                                    containerItem2.UnitStamp = string.Join("/", localSellos);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            containerValid2 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "CAPACIDAD_2", dataRow, listMessages, ref isValidBitacora, new Regex(_lettersAccentsNumbersSpacePattern)))
                                        {
                                            var columnName = "CAPACIDAD_2";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                            {
                                                containerItem2.Capacity = dataRow[columnName].ToString().Trim();
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                        }
                                        else
                                        {
                                            containerValid2 = false;
                                        }

                                        if (containerValid2)
                                        {
                                            transport.Containers.Add(containerItem2);
                                        }
                                    }
                                }

                                #endregion

                                if (listSellos.Any())
                                {
                                    transport.UnitStamps = listSellos.Select(x => new UnitStampEnt
                                    {
                                        Id = 0,
                                        Name = x
                                    }).ToArray();
                                }

                                bitacoraItem.Transport = transport;

                                #endregion

                                if (IsValidColumn(dataColumns, "KIT TECNOLOGICO", dataRow, listMessages, ref isValidBitacora))
                                {
                                    if (bitacoraItem.Customer != null)
                                    {
                                        using (var bl = new CatSecurityKitsBl(_cnnStringBitacora, "system"))
                                        {
                                            var columnName = "KIT TECNOLOGICO";
                                            var list = bl.Search(bitacoraItem.Customer.Id, dataRow[columnName].ToString().Trim()).ToList();
                                            if (list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                            {
                                                bitacoraItem.SecurityKit = list.First(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower()));
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Name.Trim().ToLower().Equals(dataRow[columnName].ToString().Trim().ToLower())))
                                            {
                                                SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString().Trim());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }

                                #endregion

                                if (isValidBitacora)
                                {
                                    #region Validacion Fluent

                                    ValidationResult fluentResult;
                                    using (var bl = new BitacorasBl(_cnnStringBitacora, "system"))
                                    {
                                        using (var val = new PreBitacoraValidator(bl))
                                        {
                                            fluentResult = val.Validate(bitacoraItem);
                                        }
                                    }
                                    if (!fluentResult.IsValid)
                                    {
                                        LoggerManager.Instance.Error($"En el archivo: {Path.GetFileName(file.OriginalFileName)}, la fila numero {rowNumber} contiene errores.");
                                        foreach (var validationFailure in fluentResult.Errors)
                                        {
                                            listMessages.Add(new FluentMessage
                                            {
                                                Comments = validationFailure.ErrorMessage,
                                                Success = false
                                            });
                                        }
                                    }

                                    #endregion

                                    #region Guardar Bitacora, valida que no exista

                                    if (fluentResult.IsValid)
                                    {
                                        var result = SaveBitacora(bitacoraItem, localErrorPath, rowNumber);
                                        if (result.Item1)
                                        {
                                            validationsItem.Bitacora = result.Item2;
                                            listMessages.Add(new FluentMessage
                                            {
                                                Comments = result.Item2,
                                                Success = result.Item1
                                            });
                                        }
                                        else
                                        {
                                            listMessages.Add(new FluentMessage
                                            {
                                                Comments = result.Item2,
                                                Success = result.Item1
                                            });
                                        }
                                    }

                                    #endregion
                                }
                                //}

                                validationsItem.Messages = listMessages;
                                if (listMessages.Any(item => !item.Success))
                                {
                                    CreateErrorExcelFile(file, new List<FinalValidation> { validationsItem });
                                }

                                UpdateProcessFile(temporalPath, new List<FinalValidation> { validationsItem }, dt.Columns.Count);
                            }
                        }
                        else
                        {
                            LoggerManager.Instance.Error($"El archivo: {Path.GetFileName(file.OriginalFileName)} no es un archivo Excel.");
                            CreateErrorExcelFile(file, new List<FinalValidation>
                            {
                                new FinalValidation
                                {
                                    NumberRow = 1,
                                    SourceFile = importFileName,
                                    Messages = new List<FluentMessage>
                                    {
                                        new FluentMessage
                                        {
                                            Comments = "El archivo no tiene extensión xls o xlsx.",
                                            Success = false
                                        }
                                    }
                                }
                            });
                            try
                            {
                                using (var bl = new BitacorasBl(_cnnStringBitacora, "system"))
                                {
                                    bl.CreateUpdateBitacoraMassiveLoad(0, file.OriginalFileName, importFileName.Replace(_tempPath, ""), DateTime.Now, localErrorPath, DateTime.Now, 'F');
                                }
                            }
                            catch (Exception aE)
                            {
                                LoggerManager.Instance.Error(aE.StackTrace);
                            }

                            return;
                        }
                    }
                    catch (Exception aE)
                    {
                        LoggerManager.Instance.Error($"El archivo: {aE}\n{Path.GetFileName(file.OriginalFileName)} no se puede leer.");
                        CreateErrorExcelFile(file, new List<FinalValidation>
                        {
                            new FinalValidation
                            {
                                NumberRow = 1,
                                SourceFile = importFileName,
                                Messages = new List<FluentMessage>
                                {
                                    new FluentMessage
                                    {
                                        Comments = "No se consiguio leer la información del archivo Excel." + aE.Message + ", favor de subir nuevamente la plantilla",
                                        Success = false
                                    }
                                }
                            }
                        });
                    }

                    try
                    {
                        using (var bl = new BitacorasBl(_cnnStringBitacora, "system"))
                        {
                            bl.CreateUpdateBitacoraMassiveLoad(0, file.OriginalFileName,
                                importFileName.Replace(_tempPath, ""), DateTime.Now, localErrorPath,
                                DateTime.Now, 'F');
                        }

                        LoggerManager.Instance.Info($"Proceso de carga masiva finalizado para el archivo {file.OriginalFileName}");

                    }
                    catch (Exception aE)
                    {
                        LoggerManager.Instance.Error(aE.StackTrace);
                    }

                }
                else
                {
                    MoveFilesToProcessOrError(file.OriginalFileName, file.ProcessFileName);
                    LoggerManager.Instance.Error($"Archivo: {file.OriginalFileName} no es un archivo Excel.");
                    CreateErrorExcelFile(file, new List<FinalValidation>
                    {
                        new FinalValidation
                        {
                            NumberRow = 1,
                            Messages = new List<FluentMessage>
                            {
                                new FluentMessage
                                {
                                    Comments = "No es un archivo de Excel",
                                    Success = false
                                }
                            }
                        }
                    });
                }
                MoveResultsProcessFile(file);
            }
            catch (Exception ex)
            {
                LoggerManager.Instance.Error($"Archivo: {file.OriginalFileName} no fue movido a la carpeta PendingShipments. \nError: {ex}");
                CreateErrorExcelFile(file, new List<FinalValidation>
                {
                    new FinalValidation
                    {
                        NumberRow = 1,
                        Messages = new List<FluentMessage>
                        {
                            new FluentMessage
                            {
                                Comments = $"No se consiguio mover el archivo de la carpeta FTP  PendingShipments a FTP ProcessShipments: {ex.Message} Favor de volverlo a cargar.",
                                Success = false
                            }
                        }
                    }
                });
            }
        }

        private bool IsValidColumn(DataColumnCollection dataColumns, string columnName, DataRow dt, List<FluentMessage> lstMessages, ref bool isValidBitacora, Regex rgx = null)
        {
            if (dataColumns.Contains(columnName))
            {
                if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired) && string.IsNullOrEmpty(dt[columnName].ToString().Trim()))
                {
                    var message = $"El valor de la columna '{columnName}' es invalido -> dato requerido";
                    lstMessages.Add(new FluentMessage { Comments = message, ColumnName = columnName, Success = false });
                    isValidBitacora = false;
                    return false;
                }

                if (rgx != null && !string.IsNullOrEmpty(dt[columnName].ToString().Trim()))
                {
                    if (!rgx.IsMatch(dt[columnName].ToString().Trim()))
                    {
                        var message = $"El formato de la columna {columnName} es invalido";
                        lstMessages.Add(new FluentMessage { Comments = message, ColumnName = columnName, Success = false });
                        isValidBitacora = false;
                        return false;
                    }
                }

                if (_columns.Any(c => c.ColumnName.Equals(columnName) && !c.ColumnRequired) && string.IsNullOrEmpty(dt[columnName].ToString().Trim()))
                {
                    return true;
                }

                return true;
            }

            if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
            {
                var message = $"No se encontro la columna '{columnName}' en el archivo -> Columna requerida";
                lstMessages.Add(new FluentMessage { Comments = message, ColumnName = columnName, Success = false });
                isValidBitacora = false;
                return false;
            }

            if (!dataColumns.Contains(columnName))
            {
                return false;
            }

            return true;

        }

        private void SendNotificationEmail(ColumnMap dataColumn, string value)
        {
            //Proceso de envio de notificaciones
        }

        /// <summary>
        /// Method that constructs an item of bitacora, validates duplicity and inserts the record
        /// </summary>
        /// <param name="bitacoraItem">Bitacora</param>
        /// <param name="sourceFile">File that contains the record to be inserted</param>
        /// <param name="rowNumber">Row number of the File that contains the record to be inserted</param>
        /// <returns>Returns the flag indicating the success or failure of the insertion of the record</returns>
        private Tuple<bool, string> SaveBitacora(BitacoraEnt bitacoraItem, string sourceFile, int rowNumber)
        {
            string bitacoraId = null;
            LoggerManager.Instance.Info($"New register: \n{JsonConvert.SerializeObject(bitacoraItem)}");
            try
            {
                bool result;
                #region revision para no duplicacion de bitacoras

                try
                {
                    using (var bl = new BitacorasBl(_cnnStringBitacora, "system"))
                    {
                        if (bl.ValidDuplicateBitacora(bitacoraItem.Customer.Id,
                            bitacoraItem.Destinies.First().Destiny.Id, bitacoraItem.Destinies.First().DeliveryNumber,
                            bitacoraItem.Transport.Plate.NumPlate))
                        {
                            var errorDescription = "BITÁCORA DUPLICADA: ya se encuentra registrado en la base de datos.";
                            LoggerManager.Instance.Error($"In file: {sourceFile} the register #{rowNumber} is duplicated.");
                            return new Tuple<bool, string>(false, errorDescription);
                        }
                    }
                }
                catch (Exception aE)
                {
                    var errorDescription = "Error en la búsqueda de bitacora duplicada";
                    LoggerManager.Instance.Error($"In file: {sourceFile} and in the search for duplicity, an error occurred \n{aE}");
                    return new Tuple<bool, string>(false, errorDescription);

                }

                #endregion revision para no duplicacion de bitacoras

                #region insertar

                using (var scope = new TransactionScope())
                {
                    using (var bl = new BitacorasBl(_cnnStringBitacora, "system"))
                    {
                        result = bl.Create(bitacoraItem, ref bitacoraId);
                        if (result)
                        {
                            scope.Complete();
                            LoggerManager.Instance.Info($"Bitacora was created: \n{JsonConvert.SerializeObject(bitacoraItem)}");
                        }
                    }
                }

                #region Envio de mensaje de edicion de Bitacora a Rabbit

                if (result)
                {
                    try
                    {
                        SendMessage(bitacoraId, "create");
                    }
                    catch (Exception ex)
                    {
                        LoggerManager.Instance.Info(ex.ToString().Trim());
                    }
                }

                if (result)
                {
                    return new Tuple<bool, string>(true, bitacoraItem.Alias);
                }

                return new Tuple<bool, string>(false, "Intento fallido de inserción del registro.");

                #endregion

                #endregion insertar
            }
            catch (Exception aE)
            {
                var errorDescription = "Intento fallido de inserción del registro.";
                LoggerManager.Instance.Error($"Bitacora create: \n{aE}\nIn file: {sourceFile}");
                return new Tuple<bool, string>(false, errorDescription);
            }

        }

        /// <summary>
        /// Method that generates the excel file for the error log
        /// </summary>
        /// <param name="fileItem">Error file path</param>
        /// <param name="lstErrors">Error list</param>
        private void CreateErrorExcelFile(FilePath fileItem, List<FinalValidation> lstErrors)
        {
            try
            {
                var localErrorPath = $"{_tempPath}{fileItem.ErrorsFileName}";
                if (!Directory.Exists(_tempPath))
                {
                    Directory.CreateDirectory(_tempPath);
                }
                var file = new FileInfo(localErrorPath);
                if (!File.Exists(localErrorPath))
                {
                    using (var excelPackage = new ExcelPackage(file))
                    {
                        var worksheet = excelPackage.Workbook.Worksheets.Add("Errores");
                        worksheet.Cells[1, 1].Value = "No.";
                        worksheet.Cells[1, 2].Value = "Fecha, Hora y Archivo procesado";
                        worksheet.Cells[1, 3].Value = "Descripción del error";
                        worksheet.Cells[1, 4].Value = "Línea errónea";
                        worksheet.Cells[1, 5].Value = "Columna errónea";
                        worksheet.Cells[1, 6].Value = "Información errónea ";
                        var row = 2;
                        foreach (var errorItem in lstErrors)
                        {
                            foreach (var itemMessage in errorItem.Messages)
                            {
                                worksheet.Cells[row, 1].Value = errorItem.NumberRow;
                                worksheet.Cells[row, 2].Value = Path.GetFileName(errorItem.SourceFile);
                                worksheet.Cells[row, 3].Value = itemMessage.Comments;
                                worksheet.Cells[row, 4].Value = errorItem.NumberRow.ToString().Trim();
                                worksheet.Cells[row, 5].Value = itemMessage.ColumnName;
                                worksheet.Cells[row, 6].Value = itemMessage.ColumnInformation;
                                row++;
                            }
                        }
                        worksheet.Cells.AutoFitColumns();
                        excelPackage.SaveAs(new FileInfo(localErrorPath));
                    }
                }
                else
                {
                    using (var excelPackage = new ExcelPackage(file))
                    {
                        var worksheet = excelPackage.Workbook.Worksheets[0];
                        var row = worksheet.Dimension.Rows + 1;
                        foreach (var errorItem in lstErrors)
                        {
                            foreach (var itemMessage in errorItem.Messages)
                            {
                                worksheet.Cells[row, 1].Value = errorItem.NumberRow;
                                worksheet.Cells[row, 2].Value = Path.GetFileName(errorItem.SourceFile);
                                worksheet.Cells[row, 3].Value = itemMessage.Comments;
                                worksheet.Cells[row, 4].Value = errorItem.NumberRow.ToString().Trim();
                                worksheet.Cells[row, 5].Value = itemMessage.ColumnName;
                                worksheet.Cells[row, 6].Value = itemMessage.ColumnInformation;
                                row++;
                            }
                        }
                        worksheet.Cells.AutoFitColumns();
                        excelPackage.Save();
                    }
                }
            }
            catch (Exception e)
            {
                LoggerManager.Instance.Info(e);
                throw;
            }
        }

        private void CreateProcessFile(string ftpfullpath, string inputfilepath)
        {
            try
            {
                if (!Directory.Exists(_tempPath))
                {
                    Directory.CreateDirectory(_tempPath);
                }
                using (var request = new WebClient())
                {
                    request.Credentials = new NetworkCredential(_ftpUser, _ftpPassword);
                    var fileData = request.DownloadData(ftpfullpath);
                    using (var fileNew = File.Create(inputfilepath))
                    {
                        fileNew.Write(fileData, 0, fileData.Length);
                        fileNew.Close();
                    }
                }
            }
            catch (Exception e)
            {
                LoggerManager.Instance.Info(e);
                throw;
            }
        }

        private void UpdateProcessFileAddHeaderResult(string pathFile, int columns)
        {
            try
            {
                if (!Directory.Exists(_tempPath))
                {
                    Directory.CreateDirectory(_tempPath);
                }
                var file = new FileInfo(pathFile);
                if (File.Exists(pathFile))
                {
                    using (var excelPackage = new ExcelPackage(file))
                    {
                        var worksheet = excelPackage.Workbook.Worksheets[0];
                        var numberColumns = columns; //worksheet.Dimension.Columns;
                        worksheet.Cells[1, numberColumns + 1].Value = "Registrado/No Registrado";
                        worksheet.Cells[1, numberColumns + 2].Value = "Comentarios";
                        worksheet.Cells[1, numberColumns + 3].Value = "Bitácora";
                        //Se rellena los encabezados
                        using (var range = worksheet.Cells[1, numberColumns + 1, 1, numberColumns + 3])
                        {
                            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            range.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Crimson);
                            range.Style.Font.Color.SetColor(System.Drawing.Color.White);
                        }

                        worksheet.Column(numberColumns + 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Column(numberColumns + 1).Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        worksheet.Column(numberColumns + 2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Column(numberColumns + 2).Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        worksheet.Column(numberColumns + 3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Column(numberColumns + 3).Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        worksheet.Column(numberColumns + 1).Width = 20;
                        worksheet.Column(numberColumns + 2).Width = 40;
                        worksheet.Column(numberColumns + 3).Width = 10;
                        excelPackage.Save();
                    }
                }
            }
            catch (Exception e)
            {
                LoggerManager.Instance.Info(e);
                throw;
            }
        }

        private void UpdateProcessFile(string pathFile, List<FinalValidation> list, int columns)
        {
            try
            {
                if (!Directory.Exists(_tempPath))
                {
                    Directory.CreateDirectory(_tempPath);
                }
                var file = new FileInfo(pathFile);
                if (File.Exists(pathFile))
                {
                    using (var excelPackage = new ExcelPackage(file))
                    {
                        var worksheet = excelPackage.Workbook.Worksheets[0];
                        var numberColumns = columns;//worksheet.Dimension.Columns;
                        //Se rellenan los resultados
                        foreach (var rowItem in list)
                        {
                            var comment = string.Empty;
                            foreach (var message in rowItem.Messages)
                            {
                                comment += $"{message.Comments}\n";
                            }
                            worksheet.Cells[rowItem.NumberRow + 1, numberColumns + 1].Value = string.IsNullOrEmpty(rowItem.Bitacora) ? "NO" : "SI";
                            worksheet.Cells[rowItem.NumberRow + 1, numberColumns + 2].Value = string.IsNullOrEmpty(comment) ? "NA" : comment;
                            worksheet.Cells[rowItem.NumberRow + 1, numberColumns + 3].Value = string.IsNullOrEmpty(rowItem.Bitacora) ? "NA" : rowItem.Bitacora;
                        }
                        var totalRows = worksheet.Dimension.Rows;
                        using (var range = worksheet.Cells[1, numberColumns + 1, totalRows, numberColumns + 3])
                        {
                            range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            range.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        }
                        excelPackage.Save();
                    }
                }
            }
            catch (Exception e)
            {
                LoggerManager.Instance.Info(e);
                throw;
            }
        }

        ///  <summary>
        /// Method that moves via FTP the error files from temporal path to the folder ErrorShipments 
        ///  </summary>
        ///  <param name="localErrorDirectory">Temporary path where the files are processed</param>
        ///  <param name="file"></param>
        private void MoveErrorFiles(string localErrorDirectory, FilePath file)
        {
            const int bufferSize = 2048;
            var errorFiles = Directory.GetFiles(localErrorDirectory, "*.*", SearchOption.AllDirectories).Where(s => s.Contains(file.ErrorsFileName)).ToArray();
            if (errorFiles.Length > 0)
            {
                foreach (var fileItem in errorFiles)
                {
                    var request = (FtpWebRequest)WebRequest.Create(new Uri($"ftp://{_ftpServer}//{_errorShipments}//{Path.GetFileName(fileItem)}", UriKind.Absolute));
                    request.Credentials = new NetworkCredential(_ftpUser, _ftpPassword);
                    request.UseBinary = true;
                    request.UsePassive = true;
                    request.KeepAlive = true;
                    request.Method = WebRequestMethods.Ftp.UploadFile;
                    var ftpStream = request.GetRequestStream();
                    var localFileStream = new FileStream(fileItem ?? throw new InvalidOperationException(), FileMode.OpenOrCreate);
                    var byteBuffer = new byte[bufferSize];
                    var bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                    try
                    {
                        while (bytesSent != 0)
                        {
                            ftpStream.Write(byteBuffer, 0, bytesSent);
                            bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerManager.Instance.Error(ex);
                        LoggerManager.Instance.Info(ex.ToString().Trim());
                    }

                    localFileStream.Close();
                    ftpStream.Close();
                    File.Delete(fileItem);
                }
            }
        }

        private void MoveResultsProcessFile(FilePath file)
        {
            try
            {
                const int bufferSize = 2048;
                var files = Directory.GetFiles(_tempPath, "*.*", SearchOption.AllDirectories).Where(s => s.Contains(file.ResultlFileName)).ToArray();
                foreach (var path in files)
                {
                    var request = (FtpWebRequest)WebRequest.Create(new Uri($"ftp://{_ftpServer}//{_resultsShipments}//{Path.GetFileName(path)}", UriKind.Absolute));
                    request.Credentials = new NetworkCredential(_ftpUser, _ftpPassword);
                    request.UseBinary = true;
                    request.UsePassive = true;
                    request.KeepAlive = true;
                    request.Method = WebRequestMethods.Ftp.UploadFile;
                    var ftpStream = request.GetRequestStream();
                    var localFileStream = new FileStream(path ?? throw new InvalidOperationException(), FileMode.OpenOrCreate);
                    var byteBuffer = new byte[bufferSize];
                    var bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                    try
                    {
                        while (bytesSent != 0)
                        {
                            ftpStream.Write(byteBuffer, 0, bytesSent);
                            bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerManager.Instance.Error(ex);
                        LoggerManager.Instance.Info(ex.ToString().Trim());
                    }

                    localFileStream.Close();
                    ftpStream.Close();
                }
            }
            catch (Exception e)
            {
                LoggerManager.Instance.Info(e);
                throw;
            }
        }

        private void SendMessage(string bitacoraId, string mode)
        {
            using (var bi = new BitacorasBl(_cnnStringBitacora, "system"))
            {
                var bitacora = bi.GetBitacoraMessage(bitacoraId, _cnnStringIdentityServer, _rolesToGetUsers, "create".Equals(mode));
                // Send message to Mq            
                using (var sender = new MqSender("Mq"))
                {
                    //send assignment
                    var message = JsonConvert.SerializeObject(bitacora);
                    sender.Send(message, "bitacora." + mode);
                    LoggerManager.Instance.Info("Send message Bitacora Edited with Id: " + bitacoraId);
                }
            }
        }

    }
}