﻿namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraCustodyAbandonEnt
    {
        public string Id { get; set; }
        public string Alias { get; set; }
        public string Customer { get; set; }
        public string VehicleType { get; set; }
        public string Brand { get; set; }
        public string Color { get; set; }
        public string Plate { get; set; }
        public string Economic { get; set; }
        public string OperatorName { get; set; }
        public string OperatorPhone { get; set; }
        public string CustodyName { get; set; }
        public string CustodyPhone { get; set; }
    }
}
