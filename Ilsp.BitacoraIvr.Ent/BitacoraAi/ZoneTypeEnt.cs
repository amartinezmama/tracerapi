﻿namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class ZoneTypeEnt
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
    }
}
