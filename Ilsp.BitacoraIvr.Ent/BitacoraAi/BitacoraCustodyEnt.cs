﻿namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraCustodyEnt
    {
        public string BitacoraId { get; set; }
        public string Alias { get; set; }
        public string GpsId { get; set; }
        public string GpsAlias { get; set; }
        public string StartService { get; set; }
        public string EndService { get; set; }
    }
}