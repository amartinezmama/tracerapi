﻿using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class MonitoringTypeEnt
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool? IsMirrorAccount { get; set; }
        [DataMember]
        public string User { get; set; }
        [DataMember]
        public bool? Gpsrequired { get; set; }
        [DataMember]
        public int Gpsallow { get; set; }
        [DataMember]
        public bool? Custodyrequired { get; set; }
        [DataMember]
        public int Custodiesallow { get; set; }
        [DataMember]
        public bool? Securitykitrequired { get; set; }
        [DataMember]
        public int Securitykitallow { get; set; }
        [DataMember]
        public bool? Lockrequired { get; set; }
        [DataMember]
        public bool? Trabapatinrequired { get; set; }
        [DataMember]
        public bool IsCustodyAbandon { get; set; }
        [DataMember]
        public bool IsLoadingWitness { get; set; }
    }
}
