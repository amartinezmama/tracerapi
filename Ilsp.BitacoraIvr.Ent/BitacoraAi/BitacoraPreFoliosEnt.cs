﻿using System;
using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraPreFoliosEnt
    {
        [DataMember]
        public decimal Id { get; set; }
        [DataMember]
        public string Alias { get; set; }
        [DataMember]
        public string CustodyFolio { get; set; }
        [DataMember]
        public DateTime? CreateDate { get; set; }
        [DataMember]
        public int CustomerId { get; set; }
        [DataMember]
        public string CustomerName { get; set; }
        [DataMember]
        public string StatusId { get; set; }
        [DataMember]
        public string StatusName { get; set; }
        [DataMember]
        public int? ServiceTypeId { get; set; }
        [DataMember]
        public string ServiceTypeName { get; set; }
        [DataMember]
        public int? ReasonCancellationId { get; set; }
        [DataMember]
        public string ReasonCancellationName { get; set; }
        [DataMember]
        public string ReasonCancellationComment { get; set; }
        [DataMember]
        public DateTime? ReasonCancellationDate { get; set; }
        [DataMember]
        public bool? ReasonCancellationStatus { get; set; }
        [DataMember]
        public int Stays { get; set; }
    }
}
