﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraFolioSatusDto
    {
        public string BitacoraId { get; set; }
        public string BitacoraStatusId { get; set; }
        public string BitacoraStatusDescription { get; set; }
        public string SentToBilling { get; set; }
        public string CancellationFolioStatusId { get; set; }
        public string IsFalseMovement { get; set; }
        public string EvidenceCount { get; set; }
        public string EvidencePhotoCount { get; set; }
        public string ServiceOrder { get; set; }
        public string FolioNumber { get; set; }


    }
}
