﻿using System.Collections.Generic;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class SetFrecuencyMonitoring
    {
        public IEnumerable<string> Bitacoras { get; set; }
        public int Frecuency { get; set; }
        public string Comment { get; set; }
    }
}
