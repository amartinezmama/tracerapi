﻿namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraGps
    {
        public long BitacoraId { get; set; }
        public string Alias { get; set; }
        public int HeaderId { get; set; }
        public string GpsId { get; set; }
        public string From { get; set; }
        public string Type { get; set; }
        public bool Assigned { get; set; }
    }
}
