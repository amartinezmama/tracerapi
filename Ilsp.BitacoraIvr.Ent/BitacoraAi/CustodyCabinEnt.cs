﻿using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class CustodyCabinEnt
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public CustodianEnt Custodian { get; set; }
        [DataMember]
        public string ShortNumber { get; set; }
        [DataMember]
        public string LargeNumber { get; set; }
        [DataMember]
        public bool IsCustodyAbandon { get; set; }

    }
}
