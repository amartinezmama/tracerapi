﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraFolioGridDto
    {
        public int Id;

        public string BitacoraId;

        public string BitacoraAlias;

        public string BitacoraStatusId;

        public string BitacoraStatusDescription;

        public string BitacoraMonitoringTypeId;

        public string BitacoraMonitoringTypeDesc;

        public string CustomerId;

        public string CustomerName;

        public string AxCustomerId;

        public string AxCustomerName;

        public string ProjectId;

        public string AgreementId;

        public string Folio;

        public string ServiceOrder;

        public string CreateDate;

        public string UpdateUser;

        public string UpdateDate;

        public string DynamicsMessage;

        public string ServiceTypeId;

        public string ServiceTypeDescription;

        public string CancellationFolioStatusId;

        public string CancellationFolioStatusDescription;

        public string ReasonCancellationId;

        public string ReasonCancellationDescription;

        public string ReasonCancellationComment;

        public string ReasonCancellationDate;

        public string IdStay;

        public string TimeStay;

        public string PlaceSaty;

        public string SentToBilling;

        public string SentToBillingDate;

        public string EvidenceCount;

        public string EvidencePhotoCount;

        public string IsFalseMovement;

        public string EndServiceDate;

        public string ServiceOrderStatus;

        public int Total;
    }
}
