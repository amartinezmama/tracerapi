﻿namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacorasAssigment
    {
        public string Id { get; set; }
        public string BitacoraName { get; set; }
        public string AssignedUsers { get; set; }
        public int? CustomerId { get; set; }
        public string AccountName { get; set; }
        public int? Alerts { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string TransportLineName { get; set; }
        public string Economic { get; set; }
        public string Plate { get; set; }
        public string Priority { get; set; }
        public string AppointmentDate { get; set; }
        public string CreateDate { get; set; }
    }
}