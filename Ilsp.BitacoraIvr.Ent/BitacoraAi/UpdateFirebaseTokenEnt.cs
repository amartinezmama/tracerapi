﻿using System.ComponentModel.DataAnnotations;
using Ilsp.BitacoraIvr.Res;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class UpdateFirebaseTokenEnt
    {
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_RequiredField, ErrorMessageResourceType = typeof(Resource))]
        public string Token { get; set; }
    }
}
