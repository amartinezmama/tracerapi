﻿namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class MonitoringFrecuencyEnt
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
