﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraDynamicRoutesEnt
    {
        public string BitacoraId { get; set; }
        public string Alias { get; set; }
        public IEnumerable<GpsEnt> Gpss { get; set; }
        public IEnumerable<RouteEnt> Routes { get; set; }
        public IEnumerable<AlertEnt> Alerts { get; set; }
    }
}
