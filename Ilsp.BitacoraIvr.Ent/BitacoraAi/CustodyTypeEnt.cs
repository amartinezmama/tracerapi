﻿using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class CustodyTypeEnt
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool ShowCabin { get; set; }
        [DataMember]
        public bool ShowVehicle { get; set; }
    }
}
