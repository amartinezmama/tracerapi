﻿namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraFalseMovementEnt
    {
        public string Id { get; set; }
        public string IdBitacora { get; set; }
        public string ServiceOrder { get; set; }
    }
}
