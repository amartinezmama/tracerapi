﻿using System.Collections.Generic;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class CatCusTransPhonesEnt
    {
        public CustomerEnt Customer { get; set; }
        public TransportLineEnt TransportLine { get; set; }
        public IEnumerable<string> Phones { get; set; }
    }
}
