﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Ilsp.BitacoraIvr.Ent.Common;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class RouteGrid
    {
        [DataMember]
        public int? Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string OriginAddress { get; set; }
        [DataMember]
        public string OriginPointStr { get; set; }
        [DataMember]
        public string OriginPoint { get; set; }
        [DataMember]
        public string DestinyAddress { get; set; }
        [DataMember]
        public string DestinyPointStr { get; set; }
        [DataMember]
        public string DestinyPoint { get; set; }
        [DataMember]
        public string Relevance { get; set; }
        [DataMember]
        public string Distance { get; set; }
        [DataMember]
        public string Time { get; set; }
        [DataMember]
        public int Total { get; set; }
    }

    public class RouteEnt
    {
        [DataMember]
        public int? Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int Radio { get; set; }
        [DataMember]
        public RelevanceEnt Relevance { get; set; }
        [DataMember]
        public double Distance { get; set; }
        [DataMember]
        public string Time { get; set; }
        [DataMember]
        public string PointsEncoded { get; set; }
        [DataMember]
        public string OriginAddress { get; set; }
        [DataMember]
        public Point OriginPoint { get; set; }
        [DataMember]
        public string DestinyAddress { get; set; }
        [DataMember]
        public Point DestinyPoint { get; set; }
        [DataMember]
        public IEnumerable<Point> Waypoints { get; set; }
        [DataMember]
        public IEnumerable<Point> Points { get; set; }
    }

    public class RoutePsql
    {
        [DataMember]
        public int idroute { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public int? radio { get; set; }
        [DataMember]
        public string updateuser { get; set; }
        [DataMember]
        public DateTime? date { get; set; }
        [DataMember]
        public string geom { get; set; }
        [DataMember]
        public int action { get; set; }
    }
}
