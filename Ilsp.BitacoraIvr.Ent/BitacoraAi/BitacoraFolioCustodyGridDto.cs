﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraFolioCustodyGridDto
    {
        public int Id;

        public string BitacoraId;

        public string BitacoraAlias;

        public string BitacoraStatusId;

        public string BitacoraStatusDescription;

        public string CustomerId;

        public string CustomerName;

        public string Folio;

        public string ServiceOrder;

        public string CreateDate;

        public string ServiceTypeId;

        public string ServiceTypeDescription;

        public string Origin;

        public string MonitoringType;

        public int Total;
    }
}
