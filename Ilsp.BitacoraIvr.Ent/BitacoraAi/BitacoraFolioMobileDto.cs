﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraFolioMobileDto
    {
        public string Id { get; set; }
        public string BitacoraId { get; set; }
        public string BitacoraAlias { get; set; }
        public string BitacoraStatusId { get; set; }
        public string BitacoraStatusDescription { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Folio { get; set; }
        public string ServiceOrder { get; set; }
        public string CreateDate { get; set; }
        public string ServiceTypeId { get; set; }
        public string ServiceTypeDescription { get; set; }
        public string Origin { get; set; }
        public string Total { get; set; }
    }
}
