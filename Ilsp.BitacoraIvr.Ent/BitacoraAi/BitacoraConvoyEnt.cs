﻿using System;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraConvoyEnt
    {
        public string Id { get; set; }
        public string Alias { get; set; }
        public int? ConvoyId { get; set; }
        public string ConvoyName { get; set; }
        public string ConvoyColor { get; set; }
        public DateTime? CreateDate { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}
