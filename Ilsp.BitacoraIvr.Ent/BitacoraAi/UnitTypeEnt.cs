﻿using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class UnitTypeEnt
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int UnitsAllow { get; set; }
        [DataMember]
        public int MinUnitsAllow { get; set; }
        [DataMember]
        public bool IsValid { get; set; }
    }
}
