﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class CustomerEnt
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string FiscalName { get; set; }
        [DataMember]
        public string FiscalAddress { get; set; }
        [DataMember]
        public string Webpage { get; set; }
        [DataMember]
        public bool Email { get; set; }
        [DataMember]
        public bool Sms { get; set; }
        [DataMember]
        public string Logo { get; set; }
        [DataMember]
        public bool Event { get; set; }
        [DataMember]
        public bool Daily { get; set; }
        [DataMember]
        public bool Weekly { get; set; }
        [DataMember]
        public bool Monthly { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public string CreationDate { get; set; }
        [DataMember]
        public string ModifiedDate { get; set; }
        [DataMember]
        public bool OnTime { get; set; }
        [DataMember]
        public bool ValidPreBitacora { get; set; }
        [DataMember]
        public long BitacoraId { get; set; }
        [DataMember]
        public bool FolioProccess { get; set; }
        [DataMember]
        public IEnumerable<EvidenceFieldEnt> EvidenceFields { get; set; }
        [DataMember]
        public IEnumerable<EvidencePhotoEnt> Photos { get; set; }
        [DataMember]
        public bool Selected { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public bool? SendGeneralReport { get; set; }
        public IEnumerable<EmailEnt> MailsGeneralReport { get; set; }
        [DataMember]
        public IEnumerable<OperativeKeyEnt> OpKeyTransportLines { get; set; }
        [DataMember]
        public IEnumerable<AxCustomerEnt> FiscalNames { get; set; }
        [DataMember]
        public IEnumerable<CustomerOpKeyEnt> OperativeKeys { get; set; }
        [DataMember]
        public bool? Status { get; set; }
    }
}
