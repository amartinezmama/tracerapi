﻿namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class GpsInfo
    {
        public string GpsAlias { get; set; }
        public string GpsStatus { get; set; }
        public string GpsStatusName { get; set; }
        public int GpsType { get; set; }
        public string GpsTypeName { get; set; }
        public string BitacoraAlias { get; set; }
        public string BitacoraStatus { get; set; }
        public string BitacoraStatusName { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int TimeLastLocation { get; set; }
        public int TimeMaxLastLocation { get; set; }
    }
}
