﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class ContainerEnt
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public ContainerTypeEnt ContainerType { get; set; }
        [DataMember]
        public string Economic { get; set; }
        [DataMember]
        public ColorEnt Color { get; set; }
        [DataMember]
        public string Plate { get; set; }
        [DataMember]
        public string Capacity { get; set; }
        [DataMember]
        public string UnitStamp { get; set; }
        [DataMember]
        public LockEnt Lock { get; set; }
        [DataMember]
        public PadLockEnt PadLock { get; set; }
        [DataMember]
        public KeyEnt Key { get; set; }
        [DataMember]
        public virtual ICollection<GpsEnt> Gpss { get; set; }
    }
}
