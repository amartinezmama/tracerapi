﻿using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class EvidencePhotoEnt
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Photo { get; set; }
        [DataMember]
        public string PhotoFilePath { get; set; }
    }
}
