﻿namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class CustomerOpKeyEnt
    {
        public int Id { get; set; }
        public OperativeKeyEnt OperativeKey { get; set; }
        public string Mail { get; set; }
    }
}