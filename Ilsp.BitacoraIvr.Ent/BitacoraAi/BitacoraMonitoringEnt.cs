﻿namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraMonitoringEnt
    {
        public string status { get; set; }
        public int statusDetail { get; set; }
        public int? nextMonitoring { get; set; }
    }
}