﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class PlateEnt
    {
        [DataMember]
        public int? Id { get; set; }
        [DataMember]
        public string NumPlate { get; set; }
        [DataMember]
        public UnitTypeEnt UnitType { get; set; }
        [DataMember]
        public BrandEnt Brand { get; set; }
        [DataMember]
        public string Economic { get; set; }
        [DataMember]
        public UnitModelEnt Model { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public virtual ICollection<ColorEnt> Colors { get; set; }
        [DataMember]
        public TransportLineEnt TransportLine { get; set; }
    }

    public class PlateGrid
    {
        [DataMember]
        public int? Id { get; set; }
        [DataMember]
        public string NumPlate { get; set; }
        [DataMember]
        public string UnitType { get; set; }
        [DataMember]
        public string Brand { get; set; }
        [DataMember]
        public string Economic { get; set; }
        [DataMember]
        public string Model { get; set; }
        [DataMember]
        public string Comments { get; set; }
    }
}