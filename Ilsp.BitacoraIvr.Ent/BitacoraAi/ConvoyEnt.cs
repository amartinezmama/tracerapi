﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class ConvoyEnt
    {
        [DataMember]
        public int? Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public ColorEnt Color { get; set; }
        [DataMember]
        public IEnumerable<string> BitacoraList { get; set; }
    }
}
