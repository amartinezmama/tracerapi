﻿using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class EvidenceFieldEnt
    {
        [DataMember]
        public int? FieldId { get; set; }
        [DataMember]
        public string Field { get; set; }
        [DataMember]
        public bool IsRequired { get; set; }
        [DataMember]
        public string Value { get; set; }
    }
}
