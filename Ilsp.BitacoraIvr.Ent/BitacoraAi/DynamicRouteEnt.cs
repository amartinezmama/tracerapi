﻿using System.ComponentModel.DataAnnotations;
using Ilsp.BitacoraIvr.Res;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class DynamicRouteEnt
    {
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_RequiredField, ErrorMessageResourceType = typeof(Resource))]
        public string BitacoraId { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_RequiredField, ErrorMessageResourceType = typeof(Resource))]
        public string EncodeRoute { get; set; }
    }
}
