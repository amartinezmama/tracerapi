﻿using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraTurnOffAlertEnt
    {
        [DataMember] public string Id { get; set; }
        [DataMember] public string BitacoraId { get; set; }
        [DataMember] public string BitacoraAlias { get; set; }
        [DataMember] public string GpsId { get; set; }
        [DataMember] public string GpsAlias { get; set; }
        [DataMember] public string AlertId { get; set; }
        [DataMember] public string AlertName { get; set; }
        [DataMember] public string MinutesDisabled { get; set; }
        [DataMember] public string Observations { get; set; }
        [DataMember] public string CreateUser { get; set; }
        [DataMember] public string CreateDate { get; set; }
        [DataMember] public string CancelUser { get; set; }
        [DataMember] public string CancelDate { get; set; }
        [DataMember] public string Status { get; set; }
        [DataMember] public string StatusDescription { get; set; }
    }
}
