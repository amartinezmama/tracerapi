﻿namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class CustomerGrid
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FiscalName { get; set; }
        public int Total { get; set; }
    }
}