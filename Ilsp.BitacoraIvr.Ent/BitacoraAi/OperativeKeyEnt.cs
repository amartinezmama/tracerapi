﻿using System.Collections.Generic;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class OperativeKeyAction
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    public class OperativeKeyEnt
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public List<OperativeKeyAction> Actions { get; set; }
    }
}
