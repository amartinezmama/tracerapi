﻿namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class GeofenceTypeEnt
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
