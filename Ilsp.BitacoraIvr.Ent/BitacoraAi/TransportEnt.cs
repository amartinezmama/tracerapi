﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class TransportEnt
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public virtual ICollection<OperatorEnt> Operators { get; set; }
        [DataMember]
        public PlateEnt Plate { get; set; }
        [DataMember]
        public virtual ICollection<UnitStampEnt> UnitStamps { get; set; }
        [DataMember]
        public virtual ICollection<GpsEnt> Gpss { get; set; }
        [DataMember]
        public ConvoyEnt Convoy { get; set; }
        [DataMember]
        public virtual ICollection<ContainerEnt> Containers { get; set; }
        [DataMember]
        public string Observations { get; set; }
    }
}
