﻿namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraCurrentlyEdited
    {
        public decimal BitacoraId { get; set; }
        public string Alias { get; set; }
        public string UserName { get; set; }
    }
}
