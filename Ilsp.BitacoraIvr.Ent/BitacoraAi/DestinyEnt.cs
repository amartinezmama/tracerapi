﻿using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class DestinyEnt
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int BitacoraDestinyId { get; set; }
        [DataMember]
        public string DeliveryNumber { get; set; }
        [DataMember]
        public string AppointmentDate { get; set; }
        [DataMember]
        public OriginAndDestinyEnt Destiny { get; set; }
    }
}