﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraEnt
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Alias { get; set; }
        [DataMember]
        public CustomerEnt Customer { get; set; }
        [DataMember]
        public AxCustomerEnt BusinessCustomer { get; set; }
        [DataMember]
        public TransportLineEnt TransportLine { get; set; }
        [DataMember]
        public ServiceTypeEnt ServiceType { get; set; }
        [DataMember]
        public LogisticOperatorEnt LogisticOperator { get; set; }
        [DataMember]
        public ProjectEnt Project { get; set; }
        [DataMember]
        public AgreementEnt Agreement { get; set; }
        [DataMember]
        public string CustodyFolio { get; set; }
        [DataMember]
        public OriginAndDestinyEnt Origin { get; set; }
        [DataMember]
        public string Advance { get; set; }
        [DataMember]
        public ProductTypeEnt ProductType { get; set; }
        [DataMember]
        public MonitoringTypeEnt MonitoringType { get; set; }
        [DataMember]
        public virtual ICollection<DestinyEnt> Destinies { get; set; }
        [DataMember]
        public TransportEnt Transport { get; set; }
        [DataMember]
        public SecurityKitEnt SecurityKit { get; set; }
        [DataMember]
        public CustodyEnt Custody { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public bool EnableBitacora { get; set; }
        [DataMember]
        public bool GenerateOrder { get; set; }
        [DataMember]
        public bool IsMirrorAccount { get; set; }
        [DataMember]
        public string MirrorAccountUrl { get; set; }
        [DataMember]
        public string MirrorAccountUser { get; set; }
        [DataMember]
        public string MirrorAccountPass { get; set; }
        [DataMember]
        public string LoadingWitness { get; set; }
        [DataMember]
        public string MirrorAccountClientCode { get; set; }
        [DataMember]
        public bool IsAxActive { get; set; }
        [DataMember]
        public string ServiceOrder { get; set; }
        [DataMember]
        public string CreateDate { get; set; }
        [DataMember]
        public int? IlspUserId { get; set; }

    }

}
