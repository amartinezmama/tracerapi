﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class CustodyEnt
    {
        [DataMember]
        public int? Id { get; set; }
        [DataMember]
        public CustodyTypeEnt CustodyType { get; set; }
        [DataMember]
        public virtual ICollection<CustodyCabinEnt> Cabins { get; set; }
        [DataMember]
        public virtual ICollection<CustodyVehicleEnt> Vehicles { get; set; }
    }
}
