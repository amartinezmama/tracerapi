﻿using System.Collections.Generic;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class CatCusTransPhonesGrid
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string TransportLineId { get; set; }
        public string TransportLineName { get; set; }
        public string Phone { get; set; }
        public IEnumerable<string> Phones { get; set; }
    }
}
