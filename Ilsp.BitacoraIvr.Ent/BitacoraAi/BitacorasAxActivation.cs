﻿namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacorasAxActivation
    {
        
        public int Id { get; set; }
        public decimal IdBitacora { get; set; }        
        public string AxCustomerId { get; set; }        
        public string AxProyectId { get; set; }                
        public long AxAgreementId { get; set; }        
        public string CustodyFolio { get; set; }
        public string AxServiceOrder { get; set; }
        public string Status { get; set; }        
        public System.DateTime CreateDate { get; set; }                
        public string UpdateUser { get; set; }        
        public System.DateTime UpdateDate { get; set; }
        public string Message { get; set; }
        public string ReasonCancellationComment { get; set; }
        public int ReasonCancellationId { get; set; }
        public System.DateTime ReasonCancellationDate { get; set; }
        public bool SentToBilling { get; set; }
        public System.DateTime SentToBillingDate { get; set; }
        public int CancellationFolioStatusId { get; set; }
        public int ServiceTypeId { get; set; }
        public string CancellationRejectedComment { get; set; }
        public string CancellationRequestUser { get; set; }



    }
}
