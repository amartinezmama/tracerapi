﻿using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class CustodyUnitEnt
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string AxId { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
}
