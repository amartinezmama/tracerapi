﻿using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class OriginAndDestinyEnt
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Alias { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Street { get; set; }
        [DataMember]
        public string Town { get; set; }
        [DataMember]
        public string Municipality { get; set; }
        [DataMember]
        public string Number { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Postcode { get; set; }
        [DataMember]
        public string Latitude { get; set; }
        [DataMember]
        public string Longitude { get; set; }
        [DataMember]
        public string Typeid { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public bool IsOriginProcter { get; set; }
        [DataMember]
        public bool Selected { get; set; }
        [DataMember]
        public int CustomerId { get; set; }
    }

    public class OriginAndDestinyGrid
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Location { get; set; }
    }
    
}
