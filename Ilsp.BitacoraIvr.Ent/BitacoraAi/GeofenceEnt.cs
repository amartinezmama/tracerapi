﻿using System.Collections.Generic;
using Ilsp.BitacoraIvr.Ent.Common;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class GeofenceEnt
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ZoneTypeEnt ZoneType { get; set; }
        public GeofenceTypeEnt GeofenceType { get; set; }
        public double? Radio { get; set; }
        public double Time { get; set; }
        public string Color { get; set; }
        public IEnumerable<Point> Points { get; set; }
    }

    public class GeofenceGridEnt
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ZoneType { get; set; }
        public string GeofenceType { get; set; }
        public string StartPoint { get; set; }
    }
}
