﻿using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class CustodyVehicleEnt
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public CustodianEnt Custodian { get; set; }
        [DataMember]
        public string ShortNumber { get; set; }
        [DataMember]
        public string LargeNumber { get; set; }
        [DataMember]
        public CustodyUnitEnt Vehicle { get; set; }
        [DataMember]
        public bool IsCustodyAbandon { get; set; }
    }
}