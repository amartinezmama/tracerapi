﻿namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraGrid
    {
        public string Id { get; set; }
        public string Alias { get; set; }
        public string Observations { get; set; }
        public string CreateDate { get; set; }
        public string TimeInactive { get; set; }
        public string StatusId { get; set; }
        public string Status { get; set; }
        public string CustomerId { get; set; }
        public string Customer { get; set; }
        public string AxCustomer { get; set; }
        public string ServiceTypeName { get; set; }
        public string ProjectName { get; set; }
        public string AgreementName { get; set; }
        public string Origin { get; set; }
        public string CustodyFolio { get; set; }
        public string ProductName { get; set; }
        public string MonitoringTypeName { get; set; }
        public string TransportLineName { get; set; }
        public string SecurityKitName { get; set; }
        public string ServiceOrder { get; set; }
        public int Duplicates { get; set; }
    }
}
