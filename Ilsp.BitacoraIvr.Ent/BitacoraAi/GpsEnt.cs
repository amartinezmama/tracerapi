﻿using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class GpsEnt
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string TimeLastPosition { get; set; }
    }
}
