﻿using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class CustodianEnt
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
}
