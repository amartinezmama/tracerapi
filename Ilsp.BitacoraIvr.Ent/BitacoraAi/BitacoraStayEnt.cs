﻿using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class BitacoraStayEnt
    {
        [DataMember]
        public decimal BitacoraId { get; set; }
        [DataMember]
        public string Alias { get; set; }
        [DataMember]
        public int? CustomerId { get; set; }
        [DataMember]
        public string CustomerName { get; set; }
        [DataMember]
        public string AxCustomerId { get; set; }
        [DataMember]
        public string AxCustomerName { get; set; }
        [DataMember]
        public string ProjectId { get; set; }
        [DataMember]
        public string ProjectName { get; set; }
        [DataMember]
        public string CustodyFolio { get; set; }
        [DataMember]
        public int OriginId { get; set; }
        [DataMember]
        public string OriginName { get; set; }
        [DataMember]
        public string Destiny { get; set; }
    }

    public class StayEnt
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public decimal BitacoraId { get; set; }
        [DataMember]
        public long AgreementId { get; set; }
        [DataMember]
        public int TimeStay { get; set; }
        [DataMember]
        public string PlaceStay { get; set; }
    }
}
