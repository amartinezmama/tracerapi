﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.BitacoraAi
{
    public class SecurityKitEnt
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public IEnumerable<CustomerEnt> Customers { get; set; }
        public IEnumerable<FenceTechnologicalKitEnt> Geofences { get; set; }
        public IEnumerable<ControlPointEnt> ControlPoints { get; set; }
        public IEnumerable<RouteEnt> Routes { get; set; }
    }

    public class SecurityKitGridEnt
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Geofences { get; set; }
        [DataMember]
        public string Routes { get; set; }
    }

    public class FenceTechnologicalKitEnt
    {
        [DataMember]
        public int? Id { get; set; }
        [DataMember]
        public FenceTypeEnt FenceType { get; set; }
        [DataMember]
        public GeofenceEnt Geofence { get; set; }
        [DataMember]
        public bool? NotificationInGeofence { get; set; }
        [DataMember]
        public bool? NotificationOutGeofence { get; set; }
        [DataMember]
        public int? NotificationTimeWait { get; set; }
    }

    public class ControlPointEnt
    {
        [DataMember]
        public int? Id { get; set; }
        [DataMember]
        public GeofenceEnt GeofenceInit { get; set; }
        [DataMember]
        public GeofenceEnt GeofenceEnd { get; set; }
        [DataMember]
        public int TravelTime { get; set; }
    }

    public class FenceTypeEnt
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
    }

}
