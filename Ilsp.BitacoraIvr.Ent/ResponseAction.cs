﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ilsp.BitacoraIvr.Ent
{
    public class ResponseAction<TEntity>
    {
        public int Total { get; set; }
        public IEnumerable<TEntity> Items { get; set; }
    }
}
