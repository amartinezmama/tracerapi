﻿namespace Ilsp.BitacoraIvr.Ent.OntimeAi
{
    public class Shipment
    {
        public string ShipmentId { get; set; }
        public string ShipmentNumber { get; set; }
        public string AppointmentDate { get; set; }
    }
}
