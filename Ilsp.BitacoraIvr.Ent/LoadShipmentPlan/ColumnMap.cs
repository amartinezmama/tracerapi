﻿namespace Ilsp.BitacoraIvr.Ent.LoadShipmentPlan
{
    public class ColumnMap
    {
        public string ColumnName { get; set; }
        public string ColumnType { get; set; }
        public bool ColumnRequired { get; set; }
        public string Description { get; set; }
        public bool Notification { get; set; }
    }
}
