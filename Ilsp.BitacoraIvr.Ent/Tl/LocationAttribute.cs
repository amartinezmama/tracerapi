﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ilsp.BitacoraIvr.Ent.Tl
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    sealed public class LocationAttribute : ValidationAttribute
    {
        /// <summary>
        /// Validates latitude longitude with next format: 19.370747,-99.264081
        /// </summary>
        /// <param name="value">Value</param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            bool result = true;
            double aux;
            if (!string.IsNullOrEmpty(value as string))
            {
                string location = value.ToString();
                string[] splitData = location.Split(separator: new char[] { ',' });
                if (splitData.Length != 2 || !double.TryParse(splitData[0], out aux) || !double.TryParse(splitData[1], out aux))
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
