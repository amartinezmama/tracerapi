﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Ilsp.BitacoraIvr.Ent.Identity
{
    public class UserEnt
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public IEnumerable<string> Roles { get; set; }
        [DataMember]
        public bool Selected { get; set; }
        [DataMember]
        public string CustomerId { get; set; }
    }
}
