﻿using System.ComponentModel.DataAnnotations;

namespace Ilsp.BitacoraIvr.Ent.Common
{
    public class BasicModel
    {
        [Required(ErrorMessageResourceName = "Model_Required", ErrorMessageResourceType = typeof(Resources.Resource))]
        public int Id { get; set; }
        [Required(ErrorMessageResourceName = "Model_Required", ErrorMessageResourceType = typeof(Resources.Resource))]
        public string Description { get; set; }
    }
}
