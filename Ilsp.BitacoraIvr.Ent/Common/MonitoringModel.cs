﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Ilsp.BitacoraIvr.Ent.Tl;

namespace Ilsp.BitacoraIvr.Ent.Common
{
    public class MonitoringModel
    {
        [Required(ErrorMessageResourceName = "Model_Required", ErrorMessageResourceType = typeof(Resources.Resource))]
        public string BitacoraId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        [Display(Name = "LineGps", ResourceType = typeof(Resources.Resource))]
        [MaxLength(50, ErrorMessageResourceName = "Model_MaxStringLength", ErrorMessageResourceType = typeof(Resources.Resource))]
        [Location(ErrorMessageResourceName = "Location_NotValid", ErrorMessageResourceType = typeof(Resources.Resource))]
        public string LineGps { get; set; }
        [Display(Name = "Observations", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "Model_Required", ErrorMessageResourceType = typeof(Resources.Resource))]
        [MaxLength(500, ErrorMessageResourceName = "Model_MaxStringLength", ErrorMessageResourceType = typeof(Resources.Resource))]
        public string Observations { get; set; }
        [Display(Name = "Situation", ResourceType = typeof(Resources.Resource))]
        public BasicModel Situation { get; set; }
        [Display(Name = "Status", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "Model_Required", ErrorMessageResourceType = typeof(Resources.Resource))]
        public BasicModel Status { get; set; }
        public List<OpKeyAlertModel> OpKeyAlerts { get; set; }
        [Display(Name = "MonitoringFrequency", ResourceType = typeof(Resources.Resource))]
        public int? MonitoringFrequency { get; set; }
        public bool RequiredCallByFrequency { get; set; }
        public int? ContactId { get; set; }
    }
}
