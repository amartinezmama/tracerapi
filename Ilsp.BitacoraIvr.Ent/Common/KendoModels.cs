﻿using System;
using System.Collections.Generic;

namespace Ilsp.BitacoraIvr.Ent.Common
{
    public class KendoRequest
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public int Page { get; set; } = 1;
        public List<KendoSort> Sort { get; set; }

        public KendoFilter Filter { get; set; }
    }

    public class KendoSort
    {
        public string Field { get; set; }
        public string Dir { get; set; }
    }

    public class KendoFilter
    {
        public List<Filter> Filters { get; set; }
        public string Logic { get; set; }
        public string Parentlogic { get; set; }
    }

    public class Filter
    {
        public string Operator { get; set; }
        public string Value { get; set; }
        public string Field { get; set; }
    }

    public class KendoResponse
    {
        public Array Data { get; set; }
        public int Count { get; set; }
        public string Errors { get; set; }

        public KendoResponse(Array data, int count)
        {
            Data = data;
            Count = count;
        }

        public KendoResponse(string errors)
        {
            Errors = errors;
        }

        public KendoResponse()
        {

        }
    }
}
