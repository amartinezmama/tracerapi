﻿using System.ComponentModel.DataAnnotations;

namespace Ilsp.BitacoraIvr.Ent.Common
{
    public class OpKeyAlertModel
    {
        [Display(Name = "Alert", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "Model_Required", ErrorMessageResourceType = typeof(Resources.Resource))]
        public string AlertId { get; set; }
        [Display(Name = "OperativeKey", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceName = "Model_Required", ErrorMessageResourceType = typeof(Resources.Resource))]
        public BasicModel OpKeyAlert { get; set; }

    }
}
