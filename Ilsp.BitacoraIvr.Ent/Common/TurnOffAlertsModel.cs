﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Ilsp.BitacoraIvr.Ent.Common
{
    public class TurnOffAlertsModel
    {
        public int id;
        public decimal id_bitacora;
        public string alias_bitacora;
        public string id_gps;
        public string alias;
        public int id_alert;
        public string name_alert;
        public int minutes_disabled;
        public string observations;
        public string user_create;
        public System.DateTime create_date;
        public string user_cancel;
        public System.Nullable<System.DateTime> cancel_date;
        public short status;
        public string status_description;
    }

    public class AlertsBitacora
    {
        public string id_bitacora { get; set; }
        public string alias { get; set; }
        public System.Nullable<char> status { get; set; }
    }

    public class GetGpsBitacoraModel
    {
        public System.Nullable<decimal> bitacora_id { get; set; }
        public string gps_id { get; set; }
        public string alias { get; set; }
        public string type { get; set; }
    }

    public class GetAlertsModel
    {
        public int id_alert { get; set; }
        public string description { get; set; }
    }

    public class DisabledAlertModel
    {
        public int Id { get; set; }
        public string BitacoraId { get; set; }
        public string GpsId { get; set; }
        public int AlertId { get; set; }
        public int Minutes { get; set; }
        public string Status { get; set; }
        public Timer Timer { get; set; }
    }
}
