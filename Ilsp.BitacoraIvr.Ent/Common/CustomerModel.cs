﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ilsp.BitacoraIvr.Ent.Common
{
    public class CustomerModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public bool selected { get; set; }
    }

    public class SaveUpdateModel
    {
        public bool sucess { get; set; }
        public string message { get; set; }
        public bool busyName { get; set; }
    }
}
