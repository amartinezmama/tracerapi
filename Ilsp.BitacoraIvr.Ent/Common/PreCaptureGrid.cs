﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ilsp.BitacoraIvr.Ent.Common
{
    public class PreCaptureGrid
    {
        public string Id { get; set; }
        public string Alias { get; set; }
        public string Frecuency { get; set; }
        public string ServiceOrder { get; set; }
        public string Observations { get; set; }
        public string CreateDate { get; set; }
        public string TimeInactive { get; set; }
        public string StatusId { get; set; }
        public string Status { get; set; }
        public string Customer { get; set; }
        public string TransportLine { get; set; }
        public string BussinesName { get; set; }
        public string Proyect { get; set; }
        public string Agreement { get; set; }
        public string CustodyFolio { get; set; }
        public string Origin { get; set; }
        public string Destiny { get; set; }
        public string DeliveryNumber { get; set; }
        public string AppointmentDate { get; set; }
        public string ServiceType { get; set; }
        public string MonitoringType { get; set; }
        public string Plate { get; set; }
        public string Economic { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string VehicleType { get; set; }
        public string CustodyType { get; set; }
        public string UserCreate { get; set; }
        public string UserUpdate { get; set; }
        public string AssignedUser { get; set; }
        public bool Selected { get; set; }
        public int Total { get; set; }
    }
}
