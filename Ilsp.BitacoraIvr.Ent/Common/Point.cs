﻿namespace Ilsp.BitacoraIvr.Ent.Common
{
    public class Point
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }
}
