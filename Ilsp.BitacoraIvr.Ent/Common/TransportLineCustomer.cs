﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ilsp.BitacoraIvr.Ent.Common
{
    public class TransportLineCustomer
    {
        public int CustomerId { get; set; }
        public string TransportLinesIds { get; set; }
    }

    public class TypeDataDualList
    {
        public string id { get; set; }
        public string name { get; set; }
        public bool selected { get; set; }
    }

    public class GenericDualList
    {
        public bool Success { get; set; }
        public string Message { get; set; }    
        public IEnumerable<TypeDataDualList> Items { get; set; }
        public int Total { get; set; }
    }
}
