﻿namespace Ilsp.BitacoraIvr.Ent.Common
{
    public class BitacoraReactionViewModel
    {
        public string Id { get; set; }
        public string Comments { get; set; }
    }
}
