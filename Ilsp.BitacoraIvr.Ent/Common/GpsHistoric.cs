﻿namespace Ilsp.BitacoraIvr.Ent.Common
{
    public class GpsHistoric
    {
        public string GpsId { get; set; }
        public string GpsAlias { get; set; }
        public string IlspEventId { get; set; }
        public string IlspEventName { get; set; }
        public string ProviderEventId { get; set; }
        public string ProviderEventName { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Altitude { get; set; }
        public string Speed { get; set; }
        public string KmOdometer { get; set; }
        public string Degrees { get; set; }
        public string Satellite { get; set; }
        public string Temperature { get; set; }
        public string Battery { get; set; }
        public string SignalValid { get; set; }
        public string RawData { get; set; }
        public string Street { get; set; }
        public string Colony { get; set; }
        public string Municipality { get; set; }
        public string District { get; set; }
        public string Country { get; set; }
        public string PostCode { get; set; }
        public string GpsDate { get; set; }
        public string CreateDate { get; set; }
        public string ProviderId { get; set; }
    }
}
