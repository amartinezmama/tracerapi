﻿using System.Collections.Generic;

namespace Ilsp.BitacoraIvr.Ent.LspApp
{
    public class FinalValidation
    {
        public int NumberRow { get; set; }
        public string Bitacora { get; set; }
        public string SourceFile { get; set; }
        public IEnumerable<FluentMessage> Messages { get; set; }
    }

    public class FluentMessage
    {
        public string Comments { get; set; }
        public string ColumnName { get; set; }
        public string ColumnInformation { get; set; }
        public bool Success { get; set; }
    }
}
