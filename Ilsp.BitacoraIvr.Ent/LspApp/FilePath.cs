﻿namespace Ilsp.BitacoraIvr.Ent.LspApp
{
    public class FilePath
    {
        public string OriginalFileName { get; set; }
        public string ImportedFileName { get; set; }
        public string ProcessFileName { get; set; }
        public string ErrorsFileName { get; set; }
        public string ResultlFileName { get; set; }
        public bool IsProcess { get; set; }
    }
}
