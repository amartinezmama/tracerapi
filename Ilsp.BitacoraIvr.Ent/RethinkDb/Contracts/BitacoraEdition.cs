﻿using System.Collections.Generic;

namespace Ilsp.BitacoraIvr.Ent.RethinkDb.Contracts
{
    public class BitacoraEdition
    {
        public bool acceptCallByOpKey { get; set; }
        public IEnumerable<string> alerts { get; set; }
        public IEnumerable<ControlPoint> controlPoints { get; set; }
        public int? convoyId { get; set; }
        public int customerId { get; set; }
        public IEnumerable<string> disabledAlerts { get; set; }
        public IEnumerable<Geofence> geofences { get; set; }
        public IEnumerable<Gps> gpsList { get; set; }
        public string id { get; set; }
        public int reactionStatus { get; set; }
        public IEnumerable<int> routes { get; set; }
        public string status { get; set; }
        public int statusDetail { get; set; }
        public decimal theftPercent { get; set; }
        public IEnumerable<string> users { get; set; }
    }

    public class ControlPoint
    {
        public int id { get; set; }
        public int type { get; set; }
        public int time { get; set; }
    }

    public class Geofence
    {
        public int id { get; set; }
        public int type { get; set; }
    }

    public class Gps
    {
        public string alias { get; set; }
        public string deviceId { get; set; }
        public string type { get; set; }
    }
}
