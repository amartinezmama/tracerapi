﻿namespace Ilsp.BitacoraIvr.Ent.RethinkDb
{
    public class RethinkConnection
    {
        public string HostName { get; set; }
        public string DbName { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
