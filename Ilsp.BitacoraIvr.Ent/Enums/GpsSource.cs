﻿namespace Ilsp.BitacoraIvr.Ent.Enums
{
    public enum GpsSource
    {
        TRAILER = 1,
        CONTAINER = 2,
        CUSTODY = 3
    }
}
