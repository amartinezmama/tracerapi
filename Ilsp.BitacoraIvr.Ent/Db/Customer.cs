﻿using System;

namespace Ilsp.BitacoraIvr.Ent.Db
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FiscalName { get; set; }
        public string FiscalAddress { get; set; }
        public string Webpage { get; set; }
        public bool Email { get; set; }
        public bool Sms { get; set; }
        public string Logo { get; set; }
        public bool Event { get; set; }
        public bool Daily { get; set; }
        public bool Weekly { get; set; }
        public bool Monthly { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
