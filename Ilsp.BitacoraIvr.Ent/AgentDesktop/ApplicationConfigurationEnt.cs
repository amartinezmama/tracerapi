﻿namespace Ilsp.BitacoraIvr.Ent.AgentDesktop
{
    public class ApplicationConfigurationEnt
    {
        public string Name { get; set; }
        public string Screen_pop_url { get; set; }
        public string Report_name { get; set; }
        public string Report_parameter_name { get; set; }
    }
}
