﻿namespace Ilsp.BitacoraIvr.Ent.AgentDesktop
{
    public class ContactPhoneEnt
    {
        public string Type { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public short? Order { get; set; }
    }
}
