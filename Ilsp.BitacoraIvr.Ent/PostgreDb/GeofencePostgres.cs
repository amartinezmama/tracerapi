﻿using System;

namespace Ilsp.BitacoraIvr.Ent.PostgreDb
{
    public class GeofencePostgre
    {
        public int idgeofence { get; set; }
        public int idzonetype { get; set; }
        public int idgeofencestype { get; set; }
        public string name { get; set; }
        public double? radio { get; set; }
        public string updateuser { get; set; }
        public DateTime? date { get; set; }
        public string geom { get; set; }
        public int action { get; set; }
    }
}
