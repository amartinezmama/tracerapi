﻿using System;
using Ilsp.BitacoraIvr.Rp;

namespace Ilsp.BitacoraIvr.Bl
{
    public class BaseBl<E, R> where R : EfRepository<E> where E : class
    {

        public BaseBl(string connection)
        {
            Connection = connection;
        }

        public BaseBl()
        {
            Repository = Activator.CreateInstance<R>();
        }

        protected R Repository { get; set; }
        protected string Connection { get; set; }

    }
}
