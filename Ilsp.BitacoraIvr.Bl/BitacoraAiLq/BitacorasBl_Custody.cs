﻿using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class BitacorasBl
    {

        public IEnumerable<BitacoraCustodyEnt> GetCustodiesOfServiceByServiceOrder(string serviceOrder)
        {
            return _ctx.sp_getCustodiesBySo(serviceOrder).Select(x => new BitacoraCustodyEnt
            {
                BitacoraId = x.BitacoraId.ToString(),
                Alias = x.Alias,
                GpsId = x.GpsId,
                GpsAlias = x.GpsAlias,
                StartService = x.StartService != null ? x.StartService?.ToString("yyyy/MM/dd HH:mm:ss") : DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"),
                EndService = x.EndService != null ? x.EndService?.ToString("yyyy/MM/dd HH:mm:ss") : DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
            }).ToArray();
        }

    }
}
