﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatProjectsBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public CatProjectsBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public IEnumerable<ProjectEnt> Search(string axCustomerId, string search)
        {
            return _ctx.ivr_sp_search_projects(axCustomerId, search).Select(x => new ProjectEnt
            {
                Id = x.Id,
                Name = x.Name
            }).ToArray();
        }

        public ProjectEnt First(string axCustomerId, string projectId)
        {
            var response = _ctx.ivr_sp_one_projects(axCustomerId, projectId).Select(x => new ProjectEnt
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
