﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public class BitacoraCustodyCabinBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public BitacoraCustodyCabinBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Create Cabins of Trailer in Bitacora
        /// </summary>
        /// <param name="cabins">Collection of Cabins</param>
        /// <param name="custodyHeaderId">Custody Identifier</param>
        /// <returns></returns>
        public bool Create(IEnumerable<CustodyCabinEnt> cabins, int custodyHeaderId)
        {
            //add Custodian of cabin to Custody
            foreach (var cabin in cabins)
            {
                int? cabinId = 0;
                _ctx.Sp_bitacora_custody_cabin_create(custodyHeaderId, cabin.Custodian.Id,
                    cabin.ShortNumber, cabin.LargeNumber, cabin.IsCustodyAbandon, ref cabinId);
                if (cabinId == 0)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Update Cabins of Trailer in Bitacora
        /// </summary>
        /// <param name="cabins">Collection of Cabins</param>
        /// <param name="custodyId">Custody Identifier</param>
        /// <returns></returns>
        public bool Update(IEnumerable<CustodyCabinEnt> cabins, int custodyId)
        {
            //update Custodian of cabin to Custody
            return
                cabins.All(
                    cabin =>
                        _ctx.Sp_bitacora_custody_cabin_update(cabin.Id, custodyId, cabin.Custodian.Id,
                            cabin.ShortNumber.ToString(), cabin.LargeNumber.ToString()) != 0);
        }

        /// <summary>
        /// Delete Cabins of Trailer in Bitacora
        /// </summary>
        /// <param name="cabins">Collection of Cabins</param>
        /// <param name="custodyId">Custody Identifier</param>
        /// <returns></returns>
        public bool Delete(IEnumerable<CustodyCabinEnt> cabins, int custodyId)
        {
            //delete Custodian of cabin to Custody
            return
                cabins.All(
                    cabin => _ctx.Sp_bitacora_custody_cabin_delete(cabin.Id, custodyId) != 0);
        }

    }
}