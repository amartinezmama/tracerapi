﻿using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.Tl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public class DynamicRoutesBl : IDisposable
    {
        private readonly string _userName;
        private readonly string _connection;
        public readonly BitacoraAiDataContext Ctx;

        public DynamicRoutesBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            Ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public Task<IsAuthorizedCellphoneForDynamicRoutesResult> IsValidPhone(string phone)
        {
            return Task.Run(() => Ctx.IsAuthorizedCellphoneForDynamicRoutes(phone).FirstOrDefault());
        }

        public Task<bool> CreateRoute(DynamicRouteEnt model, int appId)
        {
            return Task.Run(() =>
            {
                var routeId = 0;
                if (!decimal.TryParse(model.BitacoraId, out var bitacoraId)) return false;
                using (var gbl = new CatRoutesBl(_connection, _userName))
                {
                    var points = GooglePoints.Decode(model.EncodeRoute)
                        .ToArray();
                    gbl.Create(new RouteEnt
                    {
                        Name = $"Ruta Bitácora: {DateTime.Now:yyyy-MM-dd HH:mm}",
                        Description = "Ruta creada desde el sistema",
                        PointsEncoded = model.EncodeRoute,
                        Radio = 100,
                        Relevance = new RelevanceEnt
                        {
                            Id = "1"
                        },
                        Waypoints = new List<Point>
                        {
                            points[0],
                            points[points.Length-1]
                        },
                        Time = "00:00:00",
                        DestinyAddress = string.Empty,
                        OriginAddress = string.Empty
                    }, appId, ref routeId);
                    Ctx.sp_create_bitacora_route(bitacoraId, routeId, _userName);
                }
                return true;
            });
        }

        public Task<int> SetFirebaseToken(string phone, string firebaseToken)
        {
            return Task.Run(() => Ctx.SaveFirebaseToken(phone, firebaseToken));
        }

        public async Task<IEnumerable<object>> GetAuthorizedGeofencesForRoute(decimal bitacoraId)
        {
            return await Task.Run(() =>
            {
                var items = Ctx.GetAuthorizedGeofencesForOneRoute(bitacoraId).Select(x => new
                {
                    x.name,
                    data = GooglePoints.GeographyEncode(x.geom),
                    x.color,
                }).ToArray();
                return items;
            });

        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
