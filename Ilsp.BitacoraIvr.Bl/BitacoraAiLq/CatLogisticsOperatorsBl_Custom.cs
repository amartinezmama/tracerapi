﻿using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using System.Collections.Generic;
using System.Linq;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatLogisticsOperatorsBl
    {
        public IEnumerable<LogisticOperatorEnt> Search(string search)
        {
            return _ctx.ivr_sp_search_logistic_operator(search).Select(x => new LogisticOperatorEnt
            {
                Id = x.Id,
                Name = x.Description,
                Contact = x.Contact
            }).ToArray();
        }

        public LogisticOperatorEnt First(int id)
        {
            var response = _ctx.ivr_sp_one_logistic_operator(id).Select(x => new LogisticOperatorEnt
            {
                Id = x.Id,
                Name = x.Description,
                Contact = x.Contact
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        /// <summary>
        /// Create Brand
        /// </summary>
        /// <param name="entity">Brand Data</param>
        /// <param name="id">BrandId</param>
        /// <returns>TRUE when operation is successful</returns>
        public bool Create(LogisticOperatorEnt entity, ref int id)
        {
            id = _ctx.sp_logistic_operators_create(entity.Name, null);
            return id > 0;
        }
    }

}
