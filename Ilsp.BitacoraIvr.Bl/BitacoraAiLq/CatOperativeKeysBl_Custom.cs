﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Tl;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatOperativeKeysBl
    {
        public IEnumerable<OperativeKeyEnt> Search(string search)
        {
            return _ctx.ivr_sp_search_opkeys(search).Select(x => new OperativeKeyEnt
            {
                Id = x.Id,
                Description = x.Name
            }).ToArray();
        }

        /// <summary>
        /// Method to get one item
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        public OperativeKeyEnt FirstCustom(int id)
        {
            var response = _ctx.ivr_sp_one_opkeys(id).Select(item => new OperativeKeyEnt
            {
                Id = item.Id,
                Description = item.Name
            }).ToArray();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        public IEnumerable<OperativeKeyEnt> GetOpKeysByRoleId(string roleId)
        {
            return _ctx.sp_get_operative_keys(roleId).Select(x => new OperativeKeyEnt
            {
                Id = x.id,
                Description = x.description,
                Actions = string.IsNullOrEmpty(x.action) ? new List<OperativeKeyAction>() :
                    x.action.Split(',')
                        .Select(s =>
                        {
                            var r = s.Split('|');
                            return new OperativeKeyAction
                            {
                                Value = r[1],
                                Key = r[0]
                            };
                        }).ToList()
            }).ToArray();
        }

        public IEnumerable<sp_get_operative_keys_by_alertResult> GetOpKeysByRoleIdAndAlertId(decimal bitacoraId, int alertId)
        {
            return _ctx.sp_get_operative_keys_by_alert(bitacoraId, alertId).Select(x => x.Cast<sp_get_operative_keys_by_alertResult>()).ToArray();
        }

    }
}
