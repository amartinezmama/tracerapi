﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatUnitTypesBl
    {

        public IEnumerable<UnitTypeEnt> Search(int customerId, string search)
        {
            return _ctx.ivr_sp_search_unit_type(customerId, search).Select(x => new UnitTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                UnitsAllow = x.UnitsAllow,
                MinUnitsAllow = x.MinUnitsAllow,
                IsValid = x.IsValid
            }).ToArray();
        }

        public IEnumerable<UnitTypeEnt> SearchOnly(string search)
        {
            return _ctx.ivr_sp_search_cat_unit_type(search).Select(x => new UnitTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                UnitsAllow = x.UnitsAllow,
                MinUnitsAllow = x.MinUnitsAllow,
                IsValid = x.IsValid
            }).ToArray();
        }

        public UnitTypeEnt First(int unitTypeId)
        {
            var response = _ctx.ivr_sp_one_unit_type(unitTypeId).Select(x => new UnitTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                UnitsAllow = x.UnitsAllow,
                MinUnitsAllow = x.MinUnitsAllow,
                IsValid = x.IsValid
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

    }
}
