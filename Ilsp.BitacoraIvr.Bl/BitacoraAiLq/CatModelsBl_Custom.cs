﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatModelsBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public CatModelsBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public IEnumerable<UnitModelEnt> Search(int customerId, string search)
        {
            return _ctx.ivr_sp_search_models(customerId, search).Select(x => new UnitModelEnt
            {
                Id = x.Id ?? 0,
                Name = x.Name
            }).ToArray();
        }

        public IEnumerable<UnitModelEnt> SearchOnly(string search)
        {
            return _ctx.ivr_sp_search_cat_models(search).Select(x => new UnitModelEnt
            {
                Id = x.Id ?? 0,
                Name = x.Name
            }).ToArray();
        }

        public UnitModelEnt First(int modelUnitId)
        {
            var response = _ctx.ivr_sp_one_models(modelUnitId).Select(x => new UnitModelEnt
            {
                Id = x.Id ?? 0,
                Name = x.Name
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
