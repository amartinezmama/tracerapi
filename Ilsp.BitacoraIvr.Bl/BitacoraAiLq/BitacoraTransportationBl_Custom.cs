﻿using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public class BitacoraTransportationBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public BitacoraTransportationBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Create Transportation Data
        /// </summary>
        /// <param name="transport">Transportation Data</param>
        /// <param name="bitacoraId">Bitacora Identifier</param>
        /// <returns></returns>
        public bool Create(TransportEnt transport, long bitacoraId)
        {
            int transportId = _ctx.Sp_bitacora_transportation_create(bitacoraId, transport.Plate?.Id,
                transport.Plate != null ? string.Join(",", transport.UnitStamps.Select(u => u.Name).ToList()) : string.Empty,
                transport.Plate != null ? string.Join(",", transport.Plate.Colors.Select(c => c.Id).ToList()) : string.Empty,
                transport.Observations, null,
                transport.Plate?.UnitType.Id, transport.Plate?.Economic, transport.Plate?.Brand.Id, transport.Plate?.Model.Id);
            if (transportId == 0)
            {
                return false;
            }
            transport.Id = transportId;
            using (var blo = new BitacoraTransportationOperatorBl(_connection, _userName, _ctx))
            {
                //add Operators to Transportation Data
                return blo.Create(transport.Operators ?? new List<OperatorEnt>(), transport.Id);
            }
        }

        /// <summary>
        /// Update Transportation Data
        /// </summary>
        /// <param name="transport">Transportation Data</param>
        /// <param name="bitacoraId">Bitacora Identifier</param>
        /// <returns></returns>
        public bool UpdatePreFolios(TransportEnt transport, long bitacoraId)
        {
            //update Transportation Data
            int transportId = _ctx.Sp_bitacora_transportation_update(transport.Id, bitacoraId,
                transport.Plate?.Id,
                transport.Plate != null ? string.Join(",", transport.UnitStamps.Select(u => u.Name).ToList()) : string.Empty,
                transport.Plate != null ? string.Join(",", transport.Plate.Colors.Select(c => c.Id).ToList()) : string.Empty,
                transport.Observations,
                null, transport.Plate?.UnitType.Id, transport.Plate?.Economic, transport.Plate?.Brand.Id,
                transport.Plate?.Model.Id);
            if (transportId == 0)
            {
                return false;

            }

            //Operators
            var listOperatorsExist = _ctx.sp_bitacora_transportation_operator_get(transport.Id).ToArray();
            using (var bl = new BitacoraTransportationOperatorBl(_connection, _userName, _ctx))
            {
                //delete Operators to Transportation Data
                if (!bl.Delete(listOperatorsExist.Select(o => new OperatorEnt
                {
                    Id = o.operatorid
                }), transport.Id))
                {
                    return false;
                }

                //add Operators to Transportation Data
                if (!bl.Create(transport.Operators, transport.Id))
                {
                    return false;
                }
            }


            return true;
        }

        /// <summary>
        /// Update Transportation Data
        /// </summary>
        /// <param name="transport">Transportation Data</param>
        /// <param name="bitacoraId">Bitacora Identifier</param>
        /// <returns></returns>
        public bool Update(TransportEnt transport, long bitacoraId)
        {
            //update Transportation Data
            int transportId = _ctx.Sp_bitacora_transportation_update(transport.Id, bitacoraId,
                transport.Plate?.Id,
                transport.Plate != null ? string.Join(",", transport.UnitStamps.Select(u => u.Name).ToList()) : string.Empty,
                transport.Plate != null ? string.Join(",", transport.Plate.Colors.Select(c => c.Id).ToList()) : string.Empty,
                transport.Observations,
                null, transport.Plate?.UnitType.Id, transport.Plate?.Economic, transport.Plate?.Brand.Id,
                transport.Plate?.Model.Id);
            if (transportId == 0)
            {
                return false;
            }

            //Operators
            var listOperatorsExist = _ctx.sp_bitacora_transportation_operator_get(transport.Id).ToArray();
            using (var bl = new BitacoraTransportationOperatorBl(_connection, _userName, _ctx))
            {
                //delete Operators to Transportation Data
                if (!bl.Delete(listOperatorsExist.Select(o => new OperatorEnt
                {
                    Id = o.operatorid
                }), transport.Id))
                {
                    return false;
                }

                //add Operators to Transportation Data
                if (!bl.Create(transport.Operators, transport.Id))
                {
                    return false;
                }
            }
            return true;
        }

        public TransportEnt Get(long bitacoraId)
        {
            var result = _ctx.sp_BitacoraTransportationGet(bitacoraId).ToList();
            if (result.Any())
            {
                var item = result.First();
                var transportItem = new TransportEnt
                {
                    Id = item.transportationheaderid,
                    Observations = item.commnets
                };
                using (var bl = new BitacoraTransportationOperatorBl(_connection, _userName, _ctx))
                {
                    transportItem.Operators = bl.Get(transportItem.Id);
                }

                if (item.vehicleid != null)
                {
                    transportItem.Plate = new PlateEnt
                    {
                        Id = item.vehicleid,
                        Economic = !string.IsNullOrEmpty(item.economic) ? item.economic : null,
                        Comments = !string.IsNullOrEmpty(item.commnets) ? item.commnets : null
                    };
                    using (var bl = new CatTransportLinesVehiclesBl(_connection, _userName, _ctx))
                    {
                        var vehicle = bl.First(null, item.vehicleid ?? 0);
                        if (vehicle != null)
                        {
                            transportItem.Plate.NumPlate = vehicle.NumPlate;
                        }
                    }
                    if (item.unittypeid != null)
                    {
                        using (var bl = new CatUnitTypesBl(_connection, _userName, _ctx))
                        {
                            transportItem.Plate.UnitType = bl.First(item.unittypeid ?? 0);
                        }
                    }
                    if (item.brandid != null)
                    {
                        using (var bl = new CatBrandsBl(_connection, _userName, _ctx))
                        {
                            transportItem.Plate.Brand = bl.First(item.brandid ?? 0);
                        }
                    }
                    if (item.model != null)
                    {
                        using (var bl = new CatModelsBl(_connection, _userName, _ctx))
                        {
                            transportItem.Plate.Model = bl.First(item.model ?? 0);
                        }
                    }

                    if (!string.IsNullOrEmpty(item.colorsid))
                    {
                        var colorsId = item.colorsid.Split(',');
                        transportItem.Plate.Colors = new List<ColorEnt>();
                        using (var bl = new CatColorsBl(_connection, _userName, _ctx))
                        {
                            foreach (var c in colorsId)
                            {
                                transportItem.Plate.Colors.Add(bl.First(c));
                            }
                        }
                    }
                    else
                    {
                        transportItem.Plate.Colors = new List<ColorEnt>();
                    }
                }

                if (!string.IsNullOrEmpty(item.stamps))
                {
                    var stampsResult = item.stamps.Split(',');
                    transportItem.UnitStamps = stampsResult.Select(u => new UnitStampEnt
                    {
                        Id = int.Parse(u.Split('|')[0]),
                        Name = u.Split('|')[1]
                    }).ToList();
                }
                else
                {
                    transportItem.UnitStamps = new List<UnitStampEnt>();
                }

                using (var bl = new BitacorasTransportationsGpsBl(_connection, _userName, _ctx))
                {
                    transportItem.Gpss = bl.Get(transportItem.Id);
                }

                using (var bl = new BitacoraContainerBl(_connection, _userName, _ctx))
                {
                    transportItem.Containers = bl.Get(bitacoraId);
                }

                return transportItem;
            }

            return new TransportEnt();
        }

    }
}
