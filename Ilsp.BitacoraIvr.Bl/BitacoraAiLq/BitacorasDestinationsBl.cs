﻿using System;
using Ilsp.BitacoraIvr.Dl;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
	public partial class BitacorasDestinationsBl : IDisposable
	{
        private readonly string _connection;
        private readonly string _userName;
        public readonly BitacoraAiDataContext _ctx;

        public BitacorasDestinationsBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
	}
}
