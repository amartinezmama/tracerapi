﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.Tl;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatSituationsBl
    {

        public IEnumerable<sp_get_situations_by_operative_key_Result> GetSituationsByOpKey(int operativeKey)
        {
            return _ctx.sp_get_situations_by_operative_key(operativeKey).Select(x => x.Cast<sp_get_situations_by_operative_key_Result>()).ToArray();
        }

    }
}
