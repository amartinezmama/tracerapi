﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public class BitacoraContainerBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public BitacoraContainerBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Create new container on Bitacora
        /// </summary>
        /// <param name="containers">Collections of Containers</param>
        /// <param name="bitacoraId">Bitacora identifier</param>
        /// <returns></returns>
        public bool Create(IEnumerable<ContainerEnt> containers, long bitacoraId)
        {
            //add Containers
            foreach (var container in containers)
            {
                int? containerId = 0;
                _ctx.Sp_bitacora_container_create(bitacoraId,
                    container.ContainerType.Id, container.Economic, container.Color.Id, container.Plate,
                    container.Capacity, container.UnitStamp,
                    container.Lock?.Id,
                    container.PadLock?.Id,
                    container.Key?.Id,
                    ref containerId);
                if (containerId == null || containerId == 0)
                {
                    return false;
                }
                container.Id = (int)containerId;
            }
            return true;
        }

        /// <summary>
        /// Update container of Bitacora
        /// </summary>
        /// <param name="containers">Collections of Containers</param>
        /// <param name="bitacoraId">Bitacora identifier</param>
        /// <returns></returns>
        public bool Update(IEnumerable<ContainerEnt> containers, long bitacoraId)
        {
            //update Containers
            foreach (var container in containers)
            {
                if (
                    _ctx.Sp_bitacora_container_update(container.Id, bitacoraId, container.ContainerType.Id,
                        container.Economic, container.Color.Id, container.Plate, container.Capacity, container.UnitStamp,
                        container.Lock?.Id,
                        container.PadLock?.Id,
                        container.Key?.Id
                        ) == 0)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Delete container of Bitacora
        /// </summary>
        /// <param name="containers">Collections of Containers</param>
        /// <param name="bitacoraId">Bitacora Identifier</param>
        /// <returns></returns>
        public bool Delete(IEnumerable<ContainerEnt> containers, long bitacoraId)
        {
            //delete Containers
            foreach (var container in containers)
            {
                if (_ctx.Sp_bitacora_container_delete(container.Id, bitacoraId, _userName) == 0)
                {
                    return false;
                }
            }
            return true;
        }

        public List<ContainerEnt> Get(long bitacoraId)
        {
            using (var bl = new CatContainerTypesBl(_connection, _userName, _ctx))
            {
                using (var colorbl = new CatColorsBl(_connection, _userName, _ctx))
                {
                    using (var lockBl = new CatLocksBl(_connection, _userName, _ctx))
                    {
                        using (var padLockBl = new CatPadLocksBl(_connection, _userName, _ctx))
                        {
                            using (var keyBl = new CatKeysBl(_connection, _userName, _ctx))
                            {
                                return _ctx.sp_bitacora_container_get(bitacoraId).Select(c => new ContainerEnt
                                {
                                    Id = c.containerheaderid,
                                    ContainerType = bl.First(c.containertypeid),
                                    Economic = c.economic,
                                    Color = colorbl.First(c.colorid),
                                    Plate = c.plates,
                                    Capacity = c.capacity,
                                    UnitStamp = c.stamp,
                                    Lock = !string.IsNullOrEmpty(c.axlockid) ? lockBl.First(c.axlockid) : null,
                                    PadLock = !string.IsNullOrEmpty(c.axpadlockid) ? padLockBl.First(c.axpadlockid) : null,
                                    Key = !string.IsNullOrEmpty(c.axkeypadlockid) ? keyBl.First(c.axkeypadlockid) : null,
                                    Gpss = _ctx.sp_bitacora_container_gps_get(c.containerheaderid).Select(x => new GpsEnt
                                    {
                                        Id = x.gpsid.Trim(),
                                        Name = x.GpsName.Trim(),
                                        Type = x.Type ?? 0,
                                        TimeLastPosition = string.Empty
                                    }).ToList()
                                }).ToList();
                            }
                        }
                    }
                }
            }
        }

    }
}
