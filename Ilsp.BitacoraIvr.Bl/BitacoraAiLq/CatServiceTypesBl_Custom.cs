﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatServiceTypesBl
    {

        public IEnumerable<ServiceTypeEnt> All()
        {
            return _ctx.ivr_sp_all_service_type().Select(x => new ServiceTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                Destinations = x.Destinations
            }).ToArray();
        }

        public IEnumerable<ServiceTypeEnt> Search(string search)
        {
            return _ctx.ivr_sp_search_service_type(search).Select(x => new ServiceTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                Destinations = x.Destinations
            }).ToArray();
        }

        public ServiceTypeEnt First(int serviceTypeId)
        {
            var response = _ctx.ivr_sp_one_service_type(serviceTypeId).Select(x => new ServiceTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                Destinations = x.Destinations
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

    }
}
