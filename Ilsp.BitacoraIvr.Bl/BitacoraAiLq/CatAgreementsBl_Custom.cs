﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatAgreementsBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public CatAgreementsBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public IEnumerable<AgreementEnt> Search(int customerId, string search)
        {
            return _ctx.ivr_sp_search_agreements(customerId, search).Select(x => new AgreementEnt
            {
                Id = x.Id ?? 0,
                Name = x.Name
            }).ToArray();
        }

        public AgreementEnt First(int customerId, long agreementId)
        {
            var response = _ctx.ivr_sp_one_agreements(customerId, agreementId).Select(x => new AgreementEnt
            {
                Id = x.Id ?? 0,
                Name = x.Name
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
