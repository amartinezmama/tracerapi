﻿using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.Tl;
using Ilsp.Common.Geocoding.BusinessEntities;
using Ilsp.Common.MessageContracts;
using Ilsp.Common.MessageContracts.Rethinkdb;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic;
using System.Xml.Linq;
using Ilsp.BitacoraIvr.Bl.IdentityLq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.Enums;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class BitacorasBl
    {

        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <param name="canSeeBitacoras">Allow all bitacoras</param>
        /// <returns></returns>
        public KendoResponse GetGridPreBitacoras(KendoRequest model, bool canSeeBitacoras)
        {
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join("|", model.Sort.Select(x => $"{x.Field},{(x.Dir.ToLower() == "asc" ? "1" : "0")}").ToArray());
            }

            // compose the filter 
            var filter = string.Empty;
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("|", model.Filter.Filters.Select(f => $"{f.Field},{f.Value.ToLower()}").ToArray());
            }

            var response = _ctx.ivr_sp_bitacora_get_grid_prebitacoras(filter, order, model.Page, model.Take, _userName, canSeeBitacoras).ToList();
            var totalData = 0;
            if (response.Any())
            {
                totalData = response.First().Total ?? 0;
            }
            var items = response.Select(item => new BitacoraGrid
            {
                Id = item.Id.ToString(),
                Alias = item.Alias,
                Observations = item.Observations,
                CreateDate = item.CreateDate,
                TimeInactive = item.TimeInactive,
                StatusId = item.StatusId,
                Status = item.Status,
                Customer = item.Customer,
                AxCustomer = item.AxCustomer,
                ServiceTypeName = item.ServiceTypeName,
                ProjectName = item.ProjectName,
                AgreementName = item.AgreementName,
                Origin = item.Origin,
                CustodyFolio = item.CustodyFolio,
                ProductName = item.ProductName,
                MonitoringTypeName = item.MonitoringTypeName,
                TransportLineName = item.TransportLineName,
                SecurityKitName = item.SecurityKitName,
                ServiceOrder = item.ServiceOrder
            }).ToList();
            var data = items.AsQueryable();
            return new KendoResponse(data.ToArray(), totalData);
        }

        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <param name="canSeeBitacoras">Allow all bitacoras</param>
        /// <returns></returns>
        public KendoResponse GetGridBitacorasCustodyAbandon(KendoRequest model, bool canSeeBitacoras)
        {
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join("|", model.Sort.Select(x => $"{x.Field},{(x.Dir.ToLower() == "asc" ? "1" : "0")}").ToArray());
            }

            // compose the filter 
            var filter = string.Empty;
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("|", model.Filter.Filters.Select(f => $"{f.Field},{f.Value.ToLower()}").ToArray());
            }

            var response = _ctx.sp_bitacora_get_grid_bitacoras_is_custody_abandon(filter, order, model.Page, model.Take, _userName, canSeeBitacoras).ToList();
            var totalData = 0;
            if (response.Any())
            {
                totalData = response.First().Total ?? 0;
            }
            var items = response.Select(x => new BitacoraCustodyAbandonEnt
            {
                Id = x.Id.ToString(),
                Alias = x.Alias,
                Customer = x.Customer,
                VehicleType = x.VehicleType,
                Brand = x.Brand,
                Color = x.Color,
                Plate = x.Plate,
                Economic = x.Economic,
                OperatorName = string.IsNullOrEmpty(x.Operator) ? string.Empty : x.Operator.Split(',').First().Split('|')[0],
                OperatorPhone = string.IsNullOrEmpty(x.Operator) ? string.Empty : x.Operator.Split(',').First().Split('|')[1],
                CustodyName = string.IsNullOrEmpty(x.Custody) ? string.Empty : x.Custody.Split(',').First().Split('|')[0],
                CustodyPhone = string.IsNullOrEmpty(x.Custody) ? string.Empty : x.Custody.Split(',').First().Split('|')[1]
            }).ToList();
            var data = items.AsQueryable();
            return new KendoResponse(data.ToArray(), totalData);
        }

        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <param name="canSeeBitacoras">Allow all bitacoras</param>
        /// <returns></returns>
        public KendoResponse GetGridPreCaptures(KendoRequest model, bool canSeeBitacoras)
        {
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join("|", model.Sort.Select(x => $"{x.Field},{(x.Dir.ToLower() == "asc" ? "1" : "0")}").ToArray());
            }

            // compose the filter 
            var filter = string.Empty;
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("|", model.Filter.Filters.Select(f => $"{f.Field},{f.Value.ToLower()}").ToArray());
            }

            var response = _ctx.ivr_sp_bitacora_get_grid_precaptures(filter, order, model.Page, model.Take, _userName, canSeeBitacoras).ToList();
            var totalData = 0;
            if (response.Any())
            {
                totalData = response.First().Total ?? 0;
            }
            var items = response.Select(item => new BitacoraGrid
            {
                Id = item.Id.ToString(),
                Alias = item.Alias,
                Observations = item.Observations,
                CreateDate = item.CreateDate,
                TimeInactive = item.TimeInactive,
                StatusId = item.StatusId,
                Status = item.Status,
                Customer = item.Customer,
                AxCustomer = item.AxCustomer,
                ServiceTypeName = item.ServiceTypeName,
                ProjectName = item.ProjectName,
                AgreementName = item.AgreementName,
                Origin = item.Origin,
                CustodyFolio = item.CustodyFolio,
                ProductName = item.ProductName,
                MonitoringTypeName = item.MonitoringTypeName,
                TransportLineName = item.TransportLineName,
                SecurityKitName = item.SecurityKitName,
                ServiceOrder = item.ServiceOrder
            }).ToList();
            var data = items.AsQueryable();
            return new KendoResponse(data.ToArray(), totalData);
        }

        /// <summary>
        /// Get Bitacora to edit from Monitoring
        /// </summary>
        /// <param name="bitacoraid">Bitacora Identifier</param>
        /// <returns></returns>
        public BitacoraEnt GetToEdit(long bitacoraid)
        {
            var result = _ctx.sp_bitacora_get_by_id(bitacoraid).ToArray();
            if (result.Any())
            {
                var bitacoraHeader = result.First();
                var bitacoraItem =
                    new BitacoraEnt
                    {
                        Id = bitacoraHeader.id.ToString(CultureInfo.CurrentCulture),
                        Alias = bitacoraHeader.alias,
                        CustodyFolio = !string.IsNullOrEmpty(bitacoraHeader.custodyfolio)
                            ? bitacoraHeader.custodyfolio
                            : null,
                        Advance = bitacoraHeader.advance,
                        ServiceOrder = !string.IsNullOrEmpty(bitacoraHeader.axserviceorder)
                            ? bitacoraHeader.axserviceorder
                            : null,
                        IsAxActive = bitacoraHeader.isaxactive ?? false,
                        IsMirrorAccount = bitacoraHeader.isMirrorAccount,
                        MirrorAccountUrl = bitacoraHeader.mirrorAccountUrl,
                        MirrorAccountUser = bitacoraHeader.mirrorAccountUser,
                        MirrorAccountPass = bitacoraHeader.mirrorAccountPass,
                        LoadingWitness = bitacoraHeader.loadingWitness,
                        MirrorAccountClientCode = bitacoraHeader.mirrorAccountClientCode,
                        Status = bitacoraHeader.statusid.ToString(),
                        CreateDate = bitacoraHeader.createdate?.ToString("yyyy/MM/dd HH:mm:ss")
                    };
                using (var bl = new CatCustomersBl(_connection, _userName, _ctx))
                {
                    bitacoraItem.Customer = bl.First(bitacoraHeader.customerid ?? 0);
                }

                if (!string.IsNullOrEmpty(bitacoraHeader.axcustomerid))
                {
                    using (var bl = new CatAxCustomerBl(_connection, _userName, _ctx))
                    {
                        bitacoraItem.BusinessCustomer = bl.First(bitacoraHeader.customerid ?? 0, bitacoraHeader.axcustomerid);
                    }
                }

                if (!string.IsNullOrEmpty(bitacoraHeader.axtransportlineid))
                {
                    using (var bl = new CatTransportLinesBl(_connection, _userName, _ctx))
                    {
                        bitacoraItem.TransportLine = bl.First(bitacoraHeader.customerid ?? 0,
                            bitacoraHeader.axtransportlineid);
                    }
                }

                if (bitacoraHeader.servicetypeid != null)
                {
                    using (var bl = new CatServiceTypesBl(_connection, _userName, _ctx))
                    {
                        bitacoraItem.ServiceType = bl.First(bitacoraHeader.servicetypeid ?? 0);
                    }
                }

                if (bitacoraHeader.logisticoperatorid != null)
                {
                    using (var bl = new CatLogisticsOperatorsBl(_connection, _userName, _ctx))
                    {
                        bitacoraItem.LogisticOperator = bl.First(bitacoraHeader.logisticoperatorid ?? 0);
                    }
                }

                if (!string.IsNullOrEmpty(bitacoraHeader.axcustomerid) && !string.IsNullOrEmpty(bitacoraHeader.axproyectid))
                {
                    using (var bl = new CatProjectsBl(_connection, _userName, _ctx))
                    {
                        bitacoraItem.Project = bl.First(bitacoraHeader.axcustomerid, bitacoraHeader.axproyectid);
                    }
                }

                if (!string.IsNullOrEmpty(bitacoraHeader.axproyectid) && bitacoraHeader.axagreementid != null)
                {
                    using (var bl = new CatAgreementsBl(_connection, _userName, _ctx))
                    {
                        bitacoraItem.Agreement = bl.First(bitacoraItem.Customer.Id, bitacoraHeader.axagreementid ?? 0);
                    }
                }

                if (bitacoraHeader.originid != null)
                {
                    using (var bl = new CatOriginsDestinationsBl(_connection, _userName, _ctx))
                    {
                        bitacoraItem.Origin = bl.FirstOrigin(bitacoraHeader.customerid ?? 0, bitacoraHeader.originid ?? 0);
                    }
                }

                if (!string.IsNullOrEmpty(bitacoraHeader.producttypeid))
                {
                    using (var bl = new CatProductTypesBl(_connection, _userName, _ctx))
                    {
                        bitacoraItem.ProductType = bl.First(bitacoraHeader.producttypeid);
                    }
                }

                if (bitacoraHeader.monitoringtypeid != null)
                {
                    using (var bl = new CatMonitoringTypesBl(_connection, _userName, _ctx))
                    {
                        bitacoraItem.MonitoringType = bl.First(bitacoraHeader.monitoringtypeid ?? 0);
                    }
                }

                using (var bl = new BitacoraDestinyBl(_connection, _userName, _ctx))
                {
                    bitacoraItem.Destinies = bl.Get(long.Parse(bitacoraItem.Id), bitacoraHeader.customerid ?? 0);
                }

                using (var bl = new BitacoraTransportationBl(_connection, _userName, _ctx))
                {
                    bitacoraItem.Transport = bl.Get(long.Parse(bitacoraItem.Id));
                }

                if (bitacoraHeader.securitykitid != null)
                {
                    using (var bl = new CatSecurityKitsBl(_connection, _userName, _ctx))
                    {
                        bitacoraItem.SecurityKit =
                            bl.First(bitacoraHeader.customerid ?? 0, bitacoraHeader.securitykitid ?? 0);
                    }
                }

                using (var bl = new BitacoraCustodyBl(_connection, _userName, _ctx))
                {
                    bitacoraItem.Custody = bl.Get(long.Parse(bitacoraItem.Id));
                }
                return bitacoraItem;
            }
            return null;
        }

        /// <summary>
        /// Create Bitacora
        /// </summary>
        /// <param name="bitacora">Bitacora Data</param>
        /// <param name="bitacoraId">return Bitacora Identifier Created</param>
        /// <param name="isPreFolios">When come from PreFolios</param>
        /// <returns></returns>
        public bool Create(BitacoraEnt bitacora, ref string bitacoraId, bool isPreFolios = false)
        {
            if (bitacora.LogisticOperator != null && bitacora.LogisticOperator.Id == 0)
            {
                using (var loBl = new CatLogisticsOperatorsBl(_connection, _userName, _ctx))
                {
                    var loId = 0;
                    if (loBl.Create(bitacora.LogisticOperator, ref loId))
                    {
                        bitacora.LogisticOperator = loBl.First(loId);
                    }
                    else
                    {
                        bitacora.LogisticOperator = null;
                    }
                }
            }
            var id = DateTime.Now.Ticks;
            string alias;
            using (var dic = new GenBitacoraIdentifier())
            {
                alias = dic.GetCode();
            }

            var result = 0;
            if (isPreFolios)
            {

                result = _ctx.Sp_bitacora_create(id, alias.Substring(0, 2), bitacora.Customer.Id, bitacora.BusinessCustomer?.Id,
                    bitacora.ServiceType?.Id, bitacora.Project?.Id, bitacora.Agreement?.Id, bitacora.Origin?.Id,
                    bitacora.Advance,
                    bitacora.CustodyFolio, bitacora.ProductType?.Id, bitacora.MonitoringType?.Id,
                    bitacora.TransportLine?.Id,
                    _userName, bitacora.SecurityKit?.Id,
                    bitacora.GenerateOrder ? 1 : 0, // to generate order service
                    bitacora.Status,
                    //isBoardingPlan ? "R" : bitacora.Source == "C" ? "R" : "P",
                    string.Empty,
                    DateTime.Now.Ticks.ToString(), bitacora.IsMirrorAccount, bitacora.LogisticOperator?.Id,
                    bitacora.IlspUserId,
                    isPreFolios,
                    bitacora.MirrorAccountUrl,
                    bitacora.MirrorAccountUser,
                    bitacora.MirrorAccountPass,
                    bitacora.LoadingWitness,
                    bitacora.MirrorAccountClientCode);
            }
            else
            {

                result = _ctx.Sp_bitacora_create(id, alias.Substring(0, 2), bitacora.Customer.Id,
                    bitacora.BusinessCustomer?.Id,
                    bitacora.ServiceType?.Id, bitacora.Project?.Id, bitacora.Agreement?.Id, bitacora.Origin?.Id,
                    bitacora.Advance,
                    bitacora.CustodyFolio, bitacora.ProductType?.Id, bitacora.MonitoringType?.Id,
                    bitacora.TransportLine?.Id,
                    _userName, bitacora.SecurityKit?.Id,
                    bitacora.GenerateOrder ? 1 : 0, // to generate order service
                    bitacora.Status,
                    //isBoardingPlan ? "R" : bitacora.Source == "C" ? "R" : "P",
                    string.Empty,
                    DateTime.Now.Ticks.ToString(), bitacora.IsMirrorAccount, bitacora.LogisticOperator?.Id,
                    bitacora.IlspUserId,
                    isPreFolios,
                    bitacora.MirrorAccountUrl,
                    bitacora.MirrorAccountUser,
                    bitacora.MirrorAccountPass,
                    bitacora.LoadingWitness,
                    bitacora.MirrorAccountClientCode);
            }

            if (result != -1)
            {
                bitacora.Alias = alias.Substring(0, 2) + result.ToString().PadLeft(7, '0');
                bitacora.Id = id.ToString();
                bitacoraId = id.ToString();

                #region Destinations

                // add destinations of bitacora
                using (var bl = new BitacoraDestinyBl(_connection, _userName, _ctx))
                {
                    if (!bl.Create(bitacora.Destinies, long.Parse(bitacora.Id)))
                    {
                        return false;
                    }
                }

                #endregion

                #region Transportation Data

                if (bitacora.Transport != null)
                {
                    if (bitacora.Transport.Plate != null)
                    {
                        //create Brand if not exist
                        using (var brandBl = new CatBrandsBl(_connection, _userName, _ctx))
                        {
                            if (bitacora.Transport.Plate.Brand?.Id == null && bitacora.Transport.Plate.Brand?.Name != null)
                            {
                                var brandId = 0;
                                if (!brandBl.Create(bitacora.Transport.Plate.Brand, ref brandId))
                                {
                                    return false;
                                }

                                bitacora.Transport.Plate.Brand.Id = brandId;
                            }
                        }

                        //create or update vehicle
                        using (var vehicleBl = new CatTransportLinesVehiclesBl(_connection, _userName, _ctx))
                        {
                            if (bitacora.Transport.Plate != null)
                            {
                                if (bitacora.Transport.Plate.Id == null)
                                {
                                    var vehicleId = 0;
                                    bitacora.Transport.Plate.TransportLine = bitacora.TransportLine;
                                    if (!vehicleBl.Create(bitacora.Transport.Plate, ref vehicleId))
                                    {
                                        return false;
                                    }

                                    bitacora.Transport.Plate.Id = vehicleId;
                                }
                                else if (bitacora.Transport.Plate.Id != null)
                                {
                                    bitacora.Transport.Plate.TransportLine = bitacora.TransportLine;
                                    if (!vehicleBl.Update(bitacora.Transport.Plate))
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                    }

                    using (var bl = new BitacoraTransportationBl(_connection, _userName, _ctx))
                    {
                        //add Transportation Data
                        if (!bl.Create(bitacora.Transport, id))
                        {
                            return false;
                        }
                    }

                }
                #endregion

                #region Containers/Trailers
                if (bitacora.Transport != null)
                {
                    using (var bl = new BitacoraContainerBl(_connection, _userName, _ctx))
                    {
                        //add Containers
                        if (!bl.Create(bitacora.Transport.Containers ?? new List<ContainerEnt>(), id))
                        {
                            return false;
                        }
                    }
                }

                #endregion

                #region Custodies

                if (bitacora.Custody != null)
                {
                    using (var bl = new BitacoraCustodyBl(_connection, _userName, _ctx))
                    {
                        // add custodies to Bitacora
                        if (!bl.Create(bitacora.Custody, id))
                        {
                            return false;
                        }
                    }
                }

                #endregion

                #region Gps on Bitacora

                if (bitacora.Transport != null)
                {
                    var gpsFront = new List<BitacoraGps>();
                    //add gps of Transport
                    foreach (var gt in bitacora.Transport.Gpss)
                    {
                        gpsFront.Add(new BitacoraGps
                        {
                            BitacoraId = long.Parse(bitacora.Id),
                            HeaderId = bitacora.Transport.Id,
                            GpsId = gt.Id,
                            From = GpsSource.TRAILER.ToString()
                        });
                    }

                    if (bitacora.Transport.Containers != null)
                    {

                        foreach (var container in bitacora.Transport.Containers)
                        {
                            gpsFront.AddRange(
                                container.Gpss.Where(x => !gpsFront.Any(y => y.GpsId.Equals(x.Id))).Select(x =>
                                    new BitacoraGps
                                    {
                                        BitacoraId = long.Parse(bitacora.Id),
                                        HeaderId = container.Id,
                                        GpsId = x.Id,
                                        From = GpsSource.CONTAINER.ToString()
                                    }).ToList());
                        }
                    }

                    //insert gps on transport
                    using (var blg = new BitacoraTransportationGpsBl(_connection, _userName, _ctx))
                    {
                        foreach (var gt in gpsFront.Where(x => x.From.Equals(GpsSource.TRAILER.ToString())))
                        {
                            if (!blg.Create(gt.GpsId, gt.HeaderId))
                            {
                                return false;
                            }
                        }
                    }
                    //insert gps on containers
                    using (var bl = new BitacoraContainerGpsBl(_connection, _userName, _ctx))
                    {
                        foreach (var gt in gpsFront.Where(x => x.From.Equals(GpsSource.CONTAINER.ToString())))
                        {
                            //add Gps´s by Container
                            if (!bl.Create(gt.GpsId, gt.HeaderId))
                            {
                                return false;
                            }
                        }
                    }

                }
                #endregion
            }
            return true;
        }

        /// <summary>
        /// Create Bitacora
        /// </summary>
        /// <param name="lasBitacoraId"></param>
        /// <param name="bitacora">Bitacora Data</param>
        /// <param name="userName">User name that modifies the Registry</param>
        /// <param name="bitacoraId">return Bitacora Identifier Created</param>
        /// <param name="isBoardingPlan">To indicate if come from Import Boarding Plan</param>
        /// <returns></returns>
        public bool CreateCloneBitacoraWithServiceOrder(long lasBitacoraId, BitacoraEnt bitacora, ref string bitacoraId)
        {
            var id = DateTime.Now.Ticks;
            string alias;
            using (var dic = new GenBitacoraIdentifier())
            {
                alias = dic.GetCode();
            }

            var result = _ctx.Sp_bitacora_create_clone_with_service_order(lasBitacoraId, id, alias.Substring(0, 2), bitacora.Customer.Id, bitacora.BusinessCustomer?.Id,
                bitacora.ServiceType?.Id, bitacora.Project?.Id, bitacora.Agreement?.Id, bitacora.Origin?.Id,
                bitacora.Advance,
                bitacora.CustodyFolio, bitacora.ProductType?.Id, bitacora.MonitoringType?.Id, bitacora.TransportLine?.Id,
                _userName, bitacora.SecurityKit?.Id,
                1, // to generate order service
                "O", //with service order default
                string.Empty,
                DateTime.Now.Ticks.ToString(), bitacora.IsMirrorAccount, bitacora.LogisticOperator?.Id);
            if (result != -1)
            {
                bitacora.Alias = alias.Substring(0, 2) + result.ToString().PadLeft(7, '0');
                bitacora.Id = id.ToString();
                bitacoraId = id.ToString();

                #region Destinations

                // add destinations of bitacora
                using (var bl = new BitacoraDestinyBl(_connection, _userName, _ctx))
                {
                    if (!bl.Create(bitacora.Destinies, long.Parse(bitacora.Id)))
                    {
                        return false;
                    }
                }

                #endregion

                #region Transportation Data

                if (bitacora.Transport.Plate != null)
                {
                    //create Brand if not exist
                    using (var brandBl = new CatBrandsBl(_connection, _userName, _ctx))
                    {
                        if (bitacora.Transport.Plate.Brand?.Id == null && bitacora.Transport.Plate.Brand?.Name != null)
                        {
                            var brandId = 0;
                            if (!brandBl.Create(bitacora.Transport.Plate.Brand, ref brandId))
                            {
                                return false;
                            }

                            bitacora.Transport.Plate.Brand.Id = brandId;
                        }
                    }

                    //create or update vehicle
                    using (var vehicleBl = new CatTransportLinesVehiclesBl(_connection, _userName, _ctx))
                    {
                        if (bitacora.Transport.Plate != null)
                        {
                            if (bitacora.Transport.Plate.Id == null)
                            {
                                var vehicleId = 0;
                                bitacora.Transport.Plate.TransportLine = bitacora.TransportLine;
                                if (!vehicleBl.Create(bitacora.Transport.Plate, ref vehicleId))
                                {
                                    return false;
                                }

                                bitacora.Transport.Plate.Id = vehicleId;
                            }
                            else if (bitacora.Transport.Plate.Id != null)
                            {
                                bitacora.Transport.Plate.TransportLine = bitacora.TransportLine;
                                if (!vehicleBl.Update(bitacora.Transport.Plate))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }

                using (var bl = new BitacoraTransportationBl(_connection, _userName, _ctx))
                {
                    //add Transportation Data
                    if (!bl.Create(bitacora.Transport, id))
                    {
                        return false;
                    }
                }

                #endregion

                #region Containers/Trailers

                using (var bl = new BitacoraContainerBl(_connection, _userName, _ctx))
                {
                    //add Containers
                    if (!bl.Create(bitacora.Transport.Containers, id))
                    {
                        return false;
                    }
                }

                #endregion

                #region Custodies

                if (bitacora.Custody?.Id != null)
                {
                    using (var bl = new BitacoraCustodyBl(_connection, _userName, _ctx))
                    {
                        // add custodies to Bitacora
                        if (!bl.Create(bitacora.Custody, id))
                        {
                            return false;
                        }
                    }
                }

                #endregion

                #region Gps on Bitacora

                var gpsFront = new List<BitacoraGps>();
                //add gps of Transport
                foreach (var gt in bitacora.Transport.Gpss)
                {
                    gpsFront.Add(new BitacoraGps
                    {
                        BitacoraId = long.Parse(bitacora.Id),
                        HeaderId = bitacora.Transport.Id,
                        GpsId = gt.Id,
                        From = GpsSource.TRAILER.ToString()
                    });
                }

                foreach (var container in bitacora.Transport.Containers)
                {
                    gpsFront.AddRange(
                        container.Gpss.Where(x => !gpsFront.Any(y => y.GpsId.Equals(x.Id))).Select(x => new BitacoraGps
                        {
                            BitacoraId = long.Parse(bitacora.Id),
                            HeaderId = container.Id,
                            GpsId = x.Id,
                            From = GpsSource.CONTAINER.ToString()
                        }).ToList());
                }

                //insert gps on transport
                using (var blg = new BitacoraTransportationGpsBl(_connection, _userName, _ctx))
                {
                    foreach (var gt in gpsFront.Where(x => x.From.Equals(GpsSource.TRAILER.ToString())))
                    {
                        if (!blg.Create(gt.GpsId, gt.HeaderId))
                        {
                            return false;
                        }
                    }
                }
                //insert gps on containers
                using (var bl = new BitacoraContainerGpsBl(_connection, _userName, _ctx))
                {
                    foreach (var gt in gpsFront.Where(x => x.From.Equals(GpsSource.CONTAINER.ToString())))
                    {
                        //add Gps´s by Container
                        if (!bl.Create(gt.GpsId, gt.HeaderId))
                        {
                            return false;
                        }
                    }
                }

                #endregion
            }
            return true;
        }

        /// <summary>
        /// Update Bitacora
        /// </summary>
        /// <param name="bitacora">Bitacora Data</param>
        /// <returns></returns>
        public bool Update(BitacoraEnt bitacora, bool isPreFolios = false)
        {
            if (bitacora.LogisticOperator != null && bitacora.LogisticOperator.Id == 0)
            {
                using (var loBl = new CatLogisticsOperatorsBl(_connection, _userName, _ctx))
                {
                    var loId = 0;
                    if (loBl.Create(bitacora.LogisticOperator, ref loId))
                    {
                        bitacora.LogisticOperator = loBl.First(loId);
                    }
                    else
                    {
                        bitacora.LogisticOperator = null;
                    }
                }
            }
            //add bitacora header
            var success = _ctx.Sp_bitacora_update(long.Parse(bitacora.Id), string.Empty, bitacora.Customer.Id,
                bitacora.BusinessCustomer?.Id,
                bitacora.ServiceType?.Id, bitacora.Project?.Id, bitacora.Agreement?.Id, bitacora.Origin?.Id,
                bitacora.Advance,
                bitacora.CustodyFolio, bitacora.ProductType?.Id, bitacora.MonitoringType?.Id, bitacora.TransportLine?.Id,
                _userName, bitacora.SecurityKit?.Id,
                bitacora.GenerateOrder ? 1 : 0, // to generate order service
                bitacora.EnableBitacora ? 1 : 0, // to change from pre-capture to bitacora
                bitacora.IsMirrorAccount,
                bitacora.LogisticOperator?.Id,
                isPreFolios,
                bitacora.MirrorAccountUrl,
                bitacora.MirrorAccountUser,
                bitacora.MirrorAccountPass,
                bitacora.LoadingWitness,
                bitacora.MirrorAccountClientCode
                );
            if (success > 0)
            {
                #region Destinatios

                using (var bl = new BitacoraDestinyBl(_connection, _userName, _ctx))
                {
                    //Destinatios
                    var listDestinyExiste = _ctx.sp_bitacora_destiny_get(long.Parse(bitacora.Id)).ToArray();
                    // delete destinations of bitacora
                    if (!bl.Delete(listDestinyExiste.Select(d => new DestinyEnt
                    {
                        BitacoraDestinyId = d.destinyheaderid,
                        Id = d.destinyid
                    }), long.Parse(bitacora.Id)))
                    {
                        return false;
                    }

                    // add destinations of bitacora
                    if (!bl.Create(bitacora.Destinies, long.Parse(bitacora.Id)))
                    {
                        return false;
                    }
                }

                #endregion
                #region Transportation Data
                if (bitacora.Transport != null)
                {
                    if (bitacora.Transport.Plate != null)
                    {
                        //create Brand if not exist
                        using (var brandBl = new CatBrandsBl(_connection, _userName, _ctx))
                        {
                            if (bitacora.Transport.Plate.Brand?.Id == null &&
                                bitacora.Transport.Plate.Brand?.Name != null)
                            {
                                var brandId = 0;
                                if (!brandBl.Create(bitacora.Transport.Plate.Brand, ref brandId))
                                {
                                    return false;
                                }

                                bitacora.Transport.Plate.Brand.Id = brandId;
                            }
                        }

                        //create or update vehicle
                        using (var vehicleBl = new CatTransportLinesVehiclesBl(_connection, _userName, _ctx))
                        {
                            if (bitacora.Transport.Plate != null)
                            {
                                if (bitacora.Transport.Plate.Id == null)
                                {
                                    var vehicleId = 0;
                                    bitacora.Transport.Plate.TransportLine = bitacora.TransportLine;
                                    if (!vehicleBl.Create(bitacora.Transport.Plate, ref vehicleId))
                                    {
                                        return false;
                                    }

                                    bitacora.Transport.Plate.Id = vehicleId;
                                }
                                else if (bitacora.Transport.Plate.Id != null)
                                {
                                    bitacora.Transport.Plate.TransportLine = bitacora.TransportLine;
                                    if (!vehicleBl.Update(bitacora.Transport.Plate))
                                    {
                                        return false;
                                    }
                                }
                            }
                        }


                        using (var bl = new BitacoraTransportationBl(_connection, _userName, _ctx))
                        {
                            //update Transportation Data
                            if (!bl.Update(bitacora.Transport, long.Parse(bitacora.Id)))
                            {
                                return false;
                            }
                        }
                    }

                    #region Container/Trailers

                    using (var bl = new BitacoraContainerBl(_connection, _userName, _ctx))
                    {
                        //Containers
                        var listContainersExist = _ctx.sp_bitacora_container_get(long.Parse(bitacora.Id)).ToList();
                        //delete Containers
                        if (!bl.Delete(listContainersExist.Select(c => new ContainerEnt
                        {
                            Id = c.containerheaderid
                        }), long.Parse(bitacora.Id)))
                        {
                            return false;
                        }

                        //add Containers
                        if (bitacora.Transport.Operators != null && !bl.Create(bitacora.Transport.Containers, long.Parse(bitacora.Id)))
                        {
                            return false;
                        }
                    }

                    #endregion
                }
                #endregion

                #region Custodies

                using (var bl = new BitacoraCustodyBl(_connection, _userName, _ctx))
                {
                    //Custodies
                    var listCustodiesExist = _ctx.sp_bitacora_custody_get(long.Parse(bitacora.Id)).ToArray();
                    //delete custody
                    if (!bl.Delete(listCustodiesExist.Select(c => new CustodyEnt
                    {
                        Id = c.custodyheaderid
                    }), long.Parse(bitacora.Id)))
                    {
                        return false;
                    }

                    if (bitacora.Custody?.Id != null && bitacora.Custody.CustodyType != null)
                    {
                        //add custody
                        if (!bl.Create(bitacora.Custody, long.Parse(bitacora.Id)))
                        {
                            return false;
                        }
                    }
                }

                #endregion

                #region Gps on Bitacora

                var gpsFront = new List<BitacoraGps>();
                var gpsOnBitacora = _ctx.sp_gps_get_all_in_bitacora(long.Parse(bitacora.Id)).Select(x => new BitacoraGps
                {
                    BitacoraId = long.Parse(x.bitacoraid),
                    GpsId = x.gpsid,
                    From = x.from
                }).ToList();

                //add gps of Transport
                if (bitacora.Transport.Gpss != null)
                {
                    var tg = new List<BitacoraGps>();
                    foreach (var gt in bitacora.Transport.Gpss)
                    {
                        tg.Add(new BitacoraGps
                        {
                            BitacoraId = long.Parse(bitacora.Id),
                            HeaderId = bitacora.Transport.Id,
                            GpsId = gt.Id,
                            From = GpsSource.TRAILER.ToString()
                        });
                    }

                    gpsFront.AddRange(tg.GroupBy(g => g.GpsId).Select(grp => grp.First()).ToList());
                }

                //add gps by trailer
                if (bitacora.Transport.Containers != null)
                {
                    foreach (var container in bitacora.Transport.Containers)
                    {
                        var gpsByContainer = container.Gpss.GroupBy(x => x.Id).Select(grp => grp.First()).ToList();
                        gpsFront.AddRange(
                            gpsByContainer.Where(x => !gpsFront.Any(y => y.GpsId.Equals(x.Id))).Select(x =>
                                new BitacoraGps
                                {
                                    BitacoraId = long.Parse(bitacora.Id),
                                    HeaderId = container.Id,
                                    GpsId = x.Id,
                                    From = GpsSource.CONTAINER.ToString()
                                }).ToList());
                    }
                }

                //delete always gps
                //delete gps on transport
                using (var blg = new BitacoraTransportationGpsBl(_connection, _userName, _ctx))
                {
                    foreach (var gt in gpsOnBitacora.Where(x => x.From.Equals(GpsSource.TRAILER.ToString())))
                    {
                        if (!blg.Delete(gt.GpsId, long.Parse(bitacora.Id)))
                        {
                            return false;
                        }

                    }
                }
                //delete gps on containers
                using (var bl = new BitacoraContainerGpsBl(_connection, _userName, _ctx))
                {
                    foreach (var gt in gpsOnBitacora.Where(x => x.From.Equals(GpsSource.CONTAINER.ToString())))
                    {
                        //add Gps´s by Container
                        if (!bl.Delete(gt.GpsId, long.Parse(bitacora.Id)))
                        {
                            return false;
                        }
                    }
                }

                //insert gps always all gps
                //insert gps always on transport
                using (var blg = new BitacoraTransportationGpsBl(_connection, _userName, _ctx))
                {
                    foreach (var gt in gpsFront.Where(x => x.From.Equals(GpsSource.TRAILER.ToString())))
                    {
                        if (!blg.Create(gt.GpsId, gt.HeaderId))
                        {
                            return false;
                        }
                    }
                }
                //insert gps always on containers
                using (var bl = new BitacoraContainerGpsBl(_connection, _userName, _ctx))
                {
                    foreach (var gt in gpsFront.Where(x => x.From.Equals(GpsSource.CONTAINER.ToString())))
                    {
                        //add Gps´s by Container
                        if (!bl.Create(gt.GpsId, gt.HeaderId))
                        {
                            return false;
                        }
                    }
                }

                #endregion
            }
            return true;
        }

        /// <summary>
        /// Update Bitacora
        /// </summary>
        /// <param name="bitacora">Bitacora Data</param>
        /// <returns></returns>
        public bool UpdateAddTransportToPreFolios(BitacoraEnt bitacora, bool isPreFolios = false)
        {
            if (bitacora.LogisticOperator != null && bitacora.LogisticOperator.Id == 0)
            {
                using (var loBl = new CatLogisticsOperatorsBl(_connection, _userName, _ctx))
                {
                    var loId = 0;
                    if (loBl.Create(bitacora.LogisticOperator, ref loId))
                    {
                        bitacora.LogisticOperator = loBl.First(loId);
                    }
                    else
                    {
                        bitacora.LogisticOperator = null;
                    }
                }
            }
            //add bitacora header
            var success = _ctx.Sp_bitacora_update(long.Parse(bitacora.Id), string.Empty, bitacora.Customer.Id,
                bitacora.BusinessCustomer?.Id,
                bitacora.ServiceType?.Id, bitacora.Project?.Id, bitacora.Agreement?.Id, bitacora.Origin?.Id,
                bitacora.Advance,
                bitacora.CustodyFolio, bitacora.ProductType?.Id, bitacora.MonitoringType?.Id, bitacora.TransportLine?.Id,
                _userName, bitacora.SecurityKit?.Id,
                bitacora.GenerateOrder ? 1 : 0, // to generate order service
                bitacora.EnableBitacora ? 1 : 0, // to change from pre-capture to bitacora
                bitacora.IsMirrorAccount, bitacora.LogisticOperator?.Id,
                isPreFolios,
                bitacora.MirrorAccountUrl,
                bitacora.MirrorAccountUser,
                bitacora.MirrorAccountPass,
                bitacora.LoadingWitness,
                bitacora.MirrorAccountClientCode);
            if (success > 0)
            {

                #region Transportation Data

                if (bitacora.Transport.Plate != null)
                {
                    //create Brand if not exist
                    using (var brandBl = new CatBrandsBl(_connection, _userName, _ctx))
                    {
                        if (bitacora.Transport.Plate.Brand?.Id == null && bitacora.Transport.Plate.Brand?.Name != null)
                        {
                            var brandId = 0;
                            if (!brandBl.Create(bitacora.Transport.Plate.Brand, ref brandId))
                            {
                                return false;
                            }

                            bitacora.Transport.Plate.Brand.Id = brandId;
                        }
                    }

                    //create or update vehicle
                    using (var vehicleBl = new CatTransportLinesVehiclesBl(_connection, _userName, _ctx))
                    {
                        if (bitacora.Transport.Plate != null)
                        {
                            if (bitacora.Transport.Plate.Id == null)
                            {
                                var vehicleId = 0;
                                bitacora.Transport.Plate.TransportLine = bitacora.TransportLine;
                                if (!vehicleBl.Create(bitacora.Transport.Plate, ref vehicleId))
                                {
                                    return false;
                                }

                                bitacora.Transport.Plate.Id = vehicleId;
                            }
                            else if (bitacora.Transport.Plate.Id != null)
                            {
                                bitacora.Transport.Plate.TransportLine = bitacora.TransportLine;
                                if (!vehicleBl.Update(bitacora.Transport.Plate))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }

                using (var bl = new BitacoraTransportationBl(_connection, _userName, _ctx))
                {
                    //update Transportation Data
                    if (!bl.UpdatePreFolios(bitacora.Transport, long.Parse(bitacora.Id)))
                    {
                        return false;
                    }
                }



                #endregion

                #region Container/Trailers

                using (var bl = new BitacoraContainerBl(_connection, _userName, _ctx))
                {
                    //Containers
                    var listContainersExist = _ctx.sp_bitacora_container_get(long.Parse(bitacora.Id)).ToList();
                    //delete Containers
                    if (!bl.Delete(listContainersExist.Select(c => new ContainerEnt
                    {
                        Id = c.containerheaderid
                    }), long.Parse(bitacora.Id)))
                    {
                        return false;
                    }

                    //add Containers
                    if (!bl.Create(bitacora.Transport.Containers, long.Parse(bitacora.Id)))
                    {
                        return false;
                    }
                }

                #endregion

                #region Gps on Bitacora

                var gpsFront = new List<BitacoraGps>();
                var gpsOnBitacora = _ctx.sp_gps_get_all_in_bitacora(long.Parse(bitacora.Id)).Select(x => new BitacoraGps
                {
                    BitacoraId = long.Parse(x.bitacoraid),
                    GpsId = x.gpsid,
                    From = x.from
                }).ToList();

                //add gps of Transport
                var tg = new List<BitacoraGps>();
                foreach (var gt in bitacora.Transport.Gpss)
                {
                    tg.Add(new BitacoraGps
                    {
                        BitacoraId = long.Parse(bitacora.Id),
                        HeaderId = bitacora.Transport.Id,
                        GpsId = gt.Id,
                        From = GpsSource.TRAILER.ToString()
                    });
                }
                gpsFront.AddRange(tg.GroupBy(g => g.GpsId).Select(grp => grp.First()).ToList());
                //add gps by trailer
                foreach (var container in bitacora.Transport.Containers)
                {
                    var gpsByTrailer = container?.Gpss?.GroupBy(x => x.Id).Select(grp => grp.First()).ToList();
                    if (gpsByTrailer != null)
                    {
                        gpsFront?.AddRange(
                            gpsByTrailer?.Where(x => !gpsFront.Any(y => y.GpsId.Equals(x.Id))).Select(x =>
                                new BitacoraGps
                                {
                                    BitacoraId = long.Parse(bitacora.Id),
                                    HeaderId = container.Id,
                                    GpsId = x.Id,
                                    From = GpsSource.CONTAINER.ToString()
                                }).ToList());
                    }
                }

                //delete always gps
                //delete gps on transport
                using (var blg = new BitacoraTransportationGpsBl(_connection, _userName, _ctx))
                {
                    foreach (var gt in gpsOnBitacora.Where(x => x.From.Equals(GpsSource.TRAILER.ToString())))
                    {
                        if (!blg.Delete(gt.GpsId, long.Parse(bitacora.Id)))
                        {
                            return false;
                        }

                    }
                }
                //delete gps on containers
                using (var bl = new BitacoraContainerGpsBl(_connection, _userName, _ctx))
                {
                    foreach (var gt in gpsOnBitacora.Where(x => x.From.Equals(GpsSource.CONTAINER.ToString())))
                    {
                        //add Gps´s by Container
                        if (!bl.Delete(gt.GpsId, long.Parse(bitacora.Id)))
                        {
                            return false;
                        }
                    }
                }

                //insert gps always all gps
                //insert gps always on transport
                using (var blg = new BitacoraTransportationGpsBl(_connection, _userName, _ctx))
                {
                    foreach (var gt in gpsFront.Where(x => x.From.Equals(GpsSource.TRAILER.ToString())))
                    {
                        if (!blg.Create(gt.GpsId, gt.HeaderId))
                        {
                            return false;
                        }
                    }
                }
                //insert gps always on containers
                using (var bl = new BitacoraContainerGpsBl(_connection, _userName, _ctx))
                {
                    foreach (var gt in gpsFront.Where(x => x.From.Equals(GpsSource.CONTAINER.ToString())))
                    {
                        //add Gps´s by Container
                        if (!bl.Create(gt.GpsId, gt.HeaderId))
                        {
                            return false;
                        }
                    }
                }

                #endregion
            }
            return true;
        }

        /// <summary>
        /// Update Bitacora Custody Abandon
        /// </summary>
        /// <param name="bitacora">Bitacora Data</param>
        /// <returns></returns>
        public bool UpdateCustodyAbandon(BitacoraEnt bitacora)
        {
            #region Custodies

            using (var bl = new BitacoraCustodyBl(_connection, _userName, _ctx))
            {
                //Custodies
                var listCustodiesExist = _ctx.sp_bitacora_custody_get(long.Parse(bitacora.Id)).ToArray();
                //delete custody
                if (!bl.Delete(listCustodiesExist.Select(c => new CustodyEnt
                {
                    Id = c.custodyheaderid
                }), long.Parse(bitacora.Id)))
                {
                    return false;
                }

                if (bitacora.Custody?.Id != null && bitacora.Custody.CustodyType != null)
                {
                    //add custody
                    if (!bl.Create(bitacora.Custody, long.Parse(bitacora.Id)))
                    {
                        return false;
                    }
                }
            }

            #endregion
            return true;
        }

        public BitacoraMessage GetBitacoraMessage(string bitacoraId, string identity, string rolesSection, bool includeMonitorist = false)
        {
            XElement userRoles;
            using (var catAspNetUsersBl = new CatAspNetUsersBl(identity, _userName))
            {
                userRoles = catAspNetUsersBl.GetUserByRoles(rolesSection);
            }
            //var roles = identity.sp_get_users_by_role("administrador,supervisor cm");
            //var userRoles = new XElement("users", roles.Select(u => new XElement("user", new XAttribute("name", u.user), new XAttribute("role", u.role))));
            var b = _ctx.sp_get_bitacoras_for_message_by_id(bitacoraId, userRoles).FirstOrDefault();
            var bitacora = new BitacoraMessage
            {
                id = b?.id.Value.ToString(),
                bitacora = b.bitacora,
                alerts = new List<Alert>(),
                customer = b.customer,
                monitoringFrequency = b.monitoringFrequency ?? 10,
                monitoringType = b.monitoringType.Value,
                priority = b.priority,
                transportLine = b.transportLine,
                plates = b.plates,
                economic = b.economic,
                phone = b.phone,
                status = b.status.ToString(),
                statusDetail = b.statusDetail.Value,
                reactionStatus = b.reactionStatus.Value,
                isMirrorAccount = b.isMirrorAccount.Value,
                nextMonitoring = b.nextMonitoring ?? 10,
                createDate = b.createDate.Value,
                gpsList = new List<Gps>(),
                destinations = new List<string>(),
                appointmentDate = b.appointmentDate,
                toUsers = new List<string>(),
                news = b.news,
                theftPercent = (b.theftPercent ?? 0) / 100,
                monitoringTypeDescription = b.monitoringTypeDescription,
                custodianFolio = b.custodyFolio,
                custodianNames = b.custodies,
                monitoringComments = GetStringSection(b.monitoringInfo, ';', 4),
                monitoringLocation = GetStringSection(b.monitoringInfo, ';', 3),
                monitoringSituation = GetStringSection(b.monitoringInfo, ';', 1),
                monitoringStatus = GetStringSection(b.monitoringInfo, ';', 0),
                monitoringUser = GetStringSection(b.monitoringInfo, ';', 2),
                operators = b.operators,
                origin = b.origin,
                hideNextMonitoring = b.hideNextMonitoring.Value
            };
            if (!string.IsNullOrEmpty(b.alerts))
            {
                bitacora.alerts.AddRange(b.alerts.Trim().Split(',').Select(a =>
                {
                    var alert = a.Trim().Split(';');
                    return new Alert
                    {
                        id = int.Parse(alert[0]),
                        name = alert[1],
                        latitude = double.Parse(alert[2]),
                        longitude = double.Parse(alert[3]),
                        info = $"<b>{Resources.Resource.Alert}:</b> {alert[1]} <br>" +
                               $"<b>{Resources.Resource.Speed}:</b> {Math.Round(double.Parse(alert[7]), 0)} Km/h <br>" +
                        $"<b>{Resources.Resource.GpsAlias}:</b> {alert[4]} <br>" +
                        $"<b>{Resources.Resource.Datetime}:</b> {alert[6]} <br>",
                        gps = alert[4],
                        priority = short.Parse(alert[5]),
                        datetime = alert[6],
                        speed = double.Parse(alert[7])
                    };
                }).ToList());
            }
            if (!string.IsNullOrEmpty(b.gpsList))
            {
                bitacora.gpsList.AddRange(b.gpsList.Split(',').Select(g =>
                {
                    var gps = g.Trim().Split(';');
                    return new Gps
                    {
                        Id = gps[0],
                        Alias = gps[1],
                        Type = gps[2]
                    };
                }).ToList());
            }
            if (!string.IsNullOrEmpty(b.destinations))
            {
                bitacora.destinations.AddRange(b.destinations.Split(',').ToList());
            }
            if (!string.IsNullOrEmpty(b.toUsers))
            {
                bitacora.toUsers.AddRange(b.toUsers.Split(',').ToList());
            }
            if (includeMonitorist)
            {
                var users = _ctx.sp_get_monitorist_of_customer_by_bitacora(decimal.Parse(bitacoraId));
                bitacora.toUsers.AddRange(users.Select(u => u.username));
            }
            return bitacora;
        }

        public BitacoraMessage GetBitacoraMessage(string bitacoraId, string identityCnn, bool includeMonitorist = false)
        {
            IdentityDataContext identity = new IdentityDataContext(identityCnn);
            var roles = identity.sp_get_users_by_role("administrador,supervisor cm");
            var userRoles = new XElement("users", roles.Select(u => new XElement("user", new XAttribute("name", u.user), new XAttribute("role", u.role))));
            var b = _ctx.sp_get_bitacoras_for_message_by_id(bitacoraId, userRoles).FirstOrDefault();
            var bitacora = new BitacoraMessage
            {
                id = b?.id.Value.ToString(),
                bitacora = b.bitacora,
                alerts = new List<Alert>(),
                customer = b.customer,
                monitoringFrequency = b.monitoringFrequency ?? 10,
                monitoringType = b.monitoringType.Value,
                priority = b.priority,
                transportLine = b.transportLine,
                plates = b.plates,
                economic = b.economic,
                phone = b.phone,
                status = b.status.ToString(),
                statusDetail = b.statusDetail.Value,
                reactionStatus = b.reactionStatus.Value,
                isMirrorAccount = b.isMirrorAccount.Value,
                nextMonitoring = b.nextMonitoring ?? 10,
                createDate = b.createDate.Value,
                gpsList = new List<Gps>(),
                destinations = new List<string>(),
                appointmentDate = b.appointmentDate,
                toUsers = new List<string>(),
                news = b.news,
                theftPercent = (b.theftPercent ?? 0) / 100,
                monitoringTypeDescription = b.monitoringTypeDescription,
                custodianFolio = b.custodyFolio,
                custodianNames = b.custodies,
                monitoringComments = GetStringSection(b.monitoringInfo, ';', 4),
                monitoringLocation = GetStringSection(b.monitoringInfo, ';', 3),
                monitoringSituation = GetStringSection(b.monitoringInfo, ';', 1),
                monitoringStatus = GetStringSection(b.monitoringInfo, ';', 0),
                monitoringUser = GetStringSection(b.monitoringInfo, ';', 2),
                operators = b.operators,
                origin = b.origin,
                hideNextMonitoring = b.hideNextMonitoring.Value
            };
            if (!string.IsNullOrEmpty(b.alerts))
            {
                bitacora.alerts.AddRange(b.alerts.Trim().Split(',').Select(a =>
                {
                    var alert = a.Trim().Split(';');
                    return new Alert
                    {
                        id = int.Parse(alert[0]),
                        name = alert[1],
                        latitude = double.Parse(alert[2]),
                        longitude = double.Parse(alert[3]),
                        info = $"<b>{Resources.Resource.Alert}:</b> {alert[1]} <br>" +
                               $"<b>{Resources.Resource.Speed}:</b> {Math.Round(double.Parse(alert[7]), 0)} Km/h <br>" +
                        $"<b>{Resources.Resource.GpsAlias}:</b> {alert[4]} <br>" +
                        $"<b>{Resources.Resource.Datetime}:</b> {alert[6]} <br>",
                        gps = alert[4],
                        priority = short.Parse(alert[5]),
                        datetime = alert[6],
                        speed = double.Parse(alert[7])
                    };
                }).ToList());
            }

            if (!string.IsNullOrEmpty(b.monitoringInfo))
            {
                var alertsInfo = b.monitoringInfo.Split(';');
                bitacora.monitoringDatetime = DateTime.Parse(alertsInfo[5]);

                if (alertsInfo.Length == 7)
                {
                    var opAlerts = alertsInfo[6].Split(',');
                    if (opAlerts.Length > 0)
                    {
                        bitacora.alertOpKeys = opAlerts.Select(oa =>
                        {
                            var splitData = oa.Split('|');
                            return new AlertOperativeKey
                            {
                                alert = splitData[0],
                                operativeKey = splitData[1]
                            };
                        }).ToList();
                    }
                }
            }

            if (!string.IsNullOrEmpty(b.location))
            {
                var l = b.location.Split(',');
                if (l.Length == 4)
                {
                    if (double.TryParse(l[0], out double lat) && double.TryParse(l[1], out double lng))
                    {
                        bitacora.location = new Location
                        {
                            Latitude = lat,
                            Longitude = lng
                        };
                    }

                    if (double.TryParse(l[2], out double speed))
                    {
                        bitacora.speed = speed;
                    }

                    if (double.TryParse(l[3], out double battery))
                    {
                        bitacora.battery = battery;
                    }
                }
            }
            if (!string.IsNullOrEmpty(b.gpsList))
            {
                bitacora.gpsList.AddRange(b.gpsList.Split(',').Select(g =>
                {
                    var gps = g.Trim().Split(';');
                    return new Gps
                    {
                        Id = gps[0],
                        Alias = gps[1],
                        Type = gps[2]
                    };
                }).ToList());
            }
            if (!string.IsNullOrEmpty(b.destinations))
            {
                bitacora.destinations.AddRange(b.destinations.Split(',').ToList());
            }
            if (!string.IsNullOrEmpty(b.toUsers))
            {
                bitacora.toUsers.AddRange(b.toUsers.Split(',').ToList());
            }
            if (includeMonitorist)
            {
                var users = _ctx.sp_get_monitorist_of_customer_by_bitacora(decimal.Parse(bitacoraId));
                bitacora.toUsers.AddRange(users.Select(u => u.username));
            }
            return bitacora;
        }

        private string GetStringSection(string source, char separator, int start)
        {
            var result = string.Empty;
            if (!string.IsNullOrEmpty(source))
            {
                var split = source.Split(separator);
                if (split.Length > start)
                {
                    result = split[start];
                }
            }
            return result;
        }

        /// <summary>
        /// Valid permission to edit Bitacora
        /// </summary>
        /// <returns></returns>
        public BitacoraCurrentlyEdited ValidPersmissionToEdit(long bitacoraId, string ip)
        {
            var result = _ctx.sp_currently_edited(bitacoraId, _userName, ip, 1).Select(x => new BitacoraCurrentlyEdited
            {
                BitacoraId = x.ID_BITACORA ?? 0,
                Alias = x.ALIAS,
                UserName = x.USERNAME
            }).ToList();
            if (result.Any())
            {
                return result.First();
            }
            return null;
        }

        /// <summary>
        /// Finalize or Cancel edition Bitacora
        /// </summary>
        /// <returns></returns>
        public bool FinalizeEditionBitacora(long bitacoraId, string ip)
        {
            _ctx.sp_currently_edited(bitacoraId, _userName, ip, 3).ToList();
            return true;
        }

        /// <summary>
        /// Finalize or Cancel all edition Bitacora
        /// </summary>
        /// <returns></returns>
        public bool FinalizeEditionAllBitacora(string ip)
        {
            _ctx.sp_currently_edited(null, _userName, ip, 4);
            return true;
        }

        /// <summary>
        /// Update Bitacora Status when pass from Pre-Capture to Pre-Bitacora
        /// </summary>
        /// <param name="bitacora">Bitacora Data</param>
        /// <returns></returns>
        public bool UpdateStatus(BitacoraEnt bitacora, bool isPreFolios = false)
        {
            if (bitacora.LogisticOperator != null && bitacora.LogisticOperator.Id == 0)
            {
                using (var loBl = new CatLogisticsOperatorsBl(_connection, _userName))
                {
                    var loId = 0;
                    if (loBl.Create(bitacora.LogisticOperator, ref loId))
                    {
                        bitacora.LogisticOperator = loBl.First(loId);
                    }
                    else
                    {
                        bitacora.LogisticOperator = null;
                    }
                }
            }
            var success = _ctx.Sp_bitacora_update(
                long.Parse(bitacora.Id),
                bitacora.Alias,
                bitacora.Customer.Id,
                bitacora.BusinessCustomer.Id,
                bitacora.ServiceType.Id,
                bitacora.Project.Id,
                bitacora.Agreement.Id,
                bitacora.Origin.Id,
                bitacora.Advance?.Trim(),
                bitacora.CustodyFolio,
                bitacora.ProductType.Id,
                bitacora.MonitoringType.Id,
                bitacora.TransportLine.Id,
                _userName,
                bitacora.SecurityKit?.Id,
                bitacora.GenerateOrder ? 1 : 0, // to generate order service
                bitacora.EnableBitacora ? 1 : 0, // to change from pre-capture to bitacora
                bitacora.IsMirrorAccount,
                bitacora.LogisticOperator?.Id,
                isPreFolios,
                bitacora.MirrorAccountUrl,
                bitacora.MirrorAccountUser,
                bitacora.MirrorAccountPass,
                bitacora.LoadingWitness,
                bitacora.MirrorAccountClientCode);

            if (success > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Change Status Bitacora
        /// </summary>
        /// <param name="bitacoraId">Bitacora Identifier</param>
        /// <param name="status">New status of Bitacora</param>
        /// <returns></returns>
        public bool ChangeStatus(long bitacoraId, string status)
        {
            var bitacora = _ctx.sp_bitacora_exixts(bitacoraId, 2, _userName, false).ToArray();
            if (bitacora.Length > 0)
            {
                var result = _ctx.Sp_bitacora_change_status(bitacoraId, char.Parse(status), _userName);
                return result > 0;
            }
            return false;
        }

        /// <summary>
        /// Create register when User is editing Bitacora
        /// </summary>
        /// <returns></returns>
        public bool InsertEditingUserName(long bitacoraId, string ip)
        {
            _ctx.sp_currently_edited(bitacoraId, _userName, ip, 2);
            return true;
        }

        public ReactionEventMessage GetReactionEvent(string deviceId, string bitacoraId)
        {
            var e = _ctx.sp_get_reaction_event_info_by_gps(deviceId, decimal.Parse(bitacoraId)).FirstOrDefault();
            var message = new ReactionEventMessage
            {
                Alerts = new List<Alert>(e.alerts.Trim().Split(',').Where(a => !string.IsNullOrEmpty(a)).Select(a =>
                {
                    var alert = a.Trim().Split(';');
                    return new Alert
                    {
                        id = int.Parse(alert[0]),
                        name = alert[1],
                        latitude = double.Parse(alert[2]),
                        longitude = double.Parse(alert[3]),
                        info = $"{alert[1]} - {alert[4]}",
                        gps = alert[4],
                        priority = short.Parse(alert[5])
                    };
                })),
                Bitacora = e.bitacora,
                Customer = e.customer,
                Economic = e.economic,
                Folio = e.folio,
                Gps = new List<Gps>(e.gps.Split(',').Where(g => !string.IsNullOrEmpty(g)).Select(g =>
                {
                    var gps = g.Trim().Split(';');
                    return new Gps
                    {
                        Id = gps[0],
                        Alias = gps[1]
                    };
                })),
                Id = e.id.ToString(CultureInfo.InvariantCulture),
                Phone = e.phone,
                Plates = e.plates,
                Status = e.statusDetail ?? 3,
                TransportLine = e.transportLine,
                IsAutomatic = false,
                Location = new Location
                {
                    Latitude = e.latitude,
                    Longitude = e.longitude
                },
                IsPermanentReaction = e.isPermanentReaction ?? false
            };

            return message;
        }

        public ReactionEventMessage GetReactionEvent(string bitacoraId)
        {
            var e = _ctx.sp_get_reaction_event_info(decimal.Parse(bitacoraId)).FirstOrDefault();
            var message = new ReactionEventMessage
            {
                Alerts = new List<Alert>(e.alerts.Trim().Split(',').Where(a => !string.IsNullOrEmpty(a)).Select(a =>
                {
                    var alert = a.Trim().Split(';');
                    return new Alert
                    {
                        id = int.Parse(alert[0]),
                        name = alert[1],
                        latitude = double.Parse(alert[2]),
                        longitude = double.Parse(alert[3]),
                        info = $"{alert[1]} - {alert[4]}",
                        gps = alert[4],
                        priority = short.Parse(alert[5])
                    };
                })),
                Bitacora = e.bitacora,
                Customer = e.customer,
                Economic = e.economic,
                Folio = e.folio,
                Gps = new List<Gps>(e.gps.Split(',').Where(g => !string.IsNullOrEmpty(g)).Select(g =>
                {
                    var gps = g.Trim().Split(';');
                    return new Gps
                    {
                        Id = gps[0],
                        Alias = gps[1]
                    };
                })),
                Id = e.id.ToString(CultureInfo.InvariantCulture),
                Phone = e.phone,
                Plates = e.plates,
                Status = e.statusDetail,
                TransportLine = e.transportLine,
                IsAutomatic = false,
                Location = new Location
                {
                    Latitude = e.latitude,
                    Longitude = e.longitude
                },
                IsPermanentReaction = e.isPermanentReaction ?? false
            };

            return message;
        }

        public int UpdateBitacoraStatusDetail(long bitacoraId, int statusDetail, string type)
        {
            return int.Parse(_ctx.sp_update_bitacora_status_detail(bitacoraId, statusDetail, type).ToString());
        }

        public BitacoraRethinkdb GetBitacoraRethinkdb(long bitacoraId)
        {
            var result = _ctx.sp_get_active_bitacoras(bitacoraId).ToList();
            if (result.Any())
            {
                var item = result.First();
                var controlPoints = !string.IsNullOrEmpty(item.controlPoints) ? item.controlPoints.Split(',') : new string[0];
                var geofences = !string.IsNullOrEmpty(item.geofences) ? item.geofences.Split(',') : new string[0];
                var routes = !string.IsNullOrEmpty(item.routes) ? item.routes.Split(',') : new string[0];
                var gpsList = !string.IsNullOrEmpty(item.gpsList) ? item.gpsList.Split(',') : new string[0];
                var alerts = !string.IsNullOrEmpty(item.alerts) ? item.alerts.Split(',') : new string[0];
                var disabledAlerts = !string.IsNullOrEmpty(item.disabledAlerts) ? item.disabledAlerts.Split(',') : new string[0];
                return new BitacoraRethinkdb
                {
                    id = item.id,
                    status = item.status,
                    statusDetail = item.statusDetail,
                    reactionStatus = item.reactionStatus,
                    controlPoints = controlPoints.Select(cp => new ControlPointRethinkdb
                    {
                        id = int.Parse(cp.Split(';')[0].ToString()),
                        type = int.Parse(cp.Split(';')[1].ToString()),
                        time = int.Parse(cp.Split(';')[2].ToString())
                    }).ToArray(),
                    geofences = geofences.Select(g => new GeofenceRethinkdb
                    {
                        id = int.Parse(g.Split(';')[0].ToString()),
                        type = int.Parse(g.Split(';')[1].ToString())
                    }).ToArray(),
                    routes = routes.Select(int.Parse).ToArray(),
                    gpsList = gpsList.Select(g => new GpsRethinkdb
                    {
                        deviceId = g.Split(';')[0].ToString(),
                        alias = g.Split(';')[1].ToString(),
                        type = g.Split(';')[2].ToString()
                    }).ToArray(),
                    customerId = item.customerId ?? 0,
                    users = !string.IsNullOrEmpty(item.users)
                        ? item.users.Split(',').Select(u => u).ToArray()
                        : new string[0],
                    convoyId = item.convoyId,
                    acceptCallByOpKey = item.acceptCallByOpKey ?? false,
                    alerts = alerts.Select(a => new AlertRethinkdb
                    {
                        id = int.Parse(a.Split(';')[0].ToString()),
                        name = a.Split(';')[1].ToString()
                    }).ToArray(),
                    disabledAlerts = disabledAlerts.Select(da => new DisabledAlert
                    {
                        Id = int.Parse(da.Split(';')[0].ToString()),
                        BitacoraId = da.Split(';')[1].ToString(),
                        GpsId = da.Split(';')[2].ToString(),
                        AlertId = int.Parse(da.Split(';')[3].ToString()),
                        Minutes = int.Parse(da.Split(';')[4].ToString()),
                        Status = short.Parse(da.Split(';')[5].ToString()),
                        Timer = null
                    }).ToArray(),
                    theftPercent = item.theftPercent
                };
            }

            return null;
        }

        public BitacoraMonitoringEnt CreateMonitoring(MonitoringModel model, double? latitude, double? longitude, Address address, XElement opKeyAlerts = null)
        {
            model.MonitoringFrequency = model.MonitoringFrequency == null ? null : (model.MonitoringFrequency == 0 ? null : model.MonitoringFrequency);
            var result = _ctx.sp_create_bitacora_monitoring(
                decimal.Parse(model.BitacoraId), model.Status.Id, model.MonitoringFrequency, model.Situation?.Id,
                _userName, latitude, longitude, address.Street,
                address.District, address.City, address.State, address.Country, address.PostalCode, model.Observations,
                opKeyAlerts?.ToString()).ToList();
            if (result.Any())
            {
                var item = result.First();
                return new BitacoraMonitoringEnt
                {
                    status = item.status.ToString(),
                    statusDetail = item.statusDetail,
                    nextMonitoring = item.nextMonitoring
                };
            }

            return null;
        }


        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <param name="canSeeBitacoras">Allow all bitacoras</param>
        /// <returns></returns>
        public KendoResponse GetGridBitacorasAssigment(KendoRequest model, bool canSeeBitacoras)
        {
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(",", model.Sort.Select(x => $"{x.Field} {x.Dir.ToLower()}").ToArray());
            }

            var filter = "1 == 1";
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("&&", model.Filter.Filters.Select(f =>
                        $"{f.Field}.ToLower().Contains(\"{f.Value.ToLower()}\")").ToArray());
            }

            var response = _ctx.sp_get_bitacoras_to_assignment(_userName, canSeeBitacoras).ToList();
            var items = response.Select(item => new BitacorasAssigment
            {
                Id = item.Id.ToString(CultureInfo.InvariantCulture),
                BitacoraName = item.BitacoraName,
                AssignedUsers = item.AssignedUser,
                CustomerId = item.CustomerId,
                AccountName = item.AccountName,
                Alerts = item.Alerts,
                Origin = item.Origin,
                Destination = item.Destination,
                TransportLineName = item.TransportLineName,
                Economic = item.Economic,
                Plate = item.Plate,
                Priority = item.ServicePriorityName,
                AppointmentDate = item.AppoimentDate?.ToString("dd/MM/yyyy HH:mm:ss"),
                CreateDate = item.CreateDate?.ToString("dd/MM/yyyy HH:mm:ss")
            }).ToList();
            if (string.IsNullOrEmpty(filter))
            {
                filter = "1 == 1";
            }
            var data = items.AsQueryable().Where(filter);
            return new KendoResponse(data.OrderBy(string.IsNullOrEmpty(order) ? "true": order)
                .Skip(model.Skip)
                .Take(model.Take == 0 ? data.Count() : model.Take)
                .ToArray(), data.Count());
        }

        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <returns></returns>
        public KendoResponse GetGridBitacorasForReassigment(KendoRequest model)
        {
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(",", model.Sort.Select(x => $"{x.Field} {x.Dir.ToLower()}").ToArray());
            }

            var userName = string.Empty;
            if (model.Filter.Filters.Any(f => f.Field.ToLower().Equals("username")))
            {
                userName = model.Filter.Filters.First(f => f.Field.ToLower().Equals("username")).Value;
            }

            var response = _ctx.sp_get_bitacoras_by_user_to_reassignment(userName).ToList();
            var items = response.Select(item => new BitacorasAssigment
            {
                Id = item.Id.ToString(CultureInfo.InvariantCulture),
                BitacoraName = item.BitacoraName,
                AccountName = item.AccountName,
                Alerts = item.Alerts,
                Origin = item.Origin,
                Destination = item.Destination,
                TransportLineName = item.TransportLineName,
                Priority = item.ServicePriorityName,
                AppointmentDate = item.AppoimentDate?.ToString("dd/MM/yyyy HH:mm:ss"),
                CreateDate = item.CreateDate?.ToString("dd/MM/yyyy HH:mm:ss")
            }).ToList();

            var data = items.AsQueryable();
            return new KendoResponse(data.OrderBy(string.IsNullOrEmpty(order) ? "true": order)
                .Skip(model.Skip)
                .Take(model.Take == 0 ? data.Count() : model.Take)
                .ToArray(), data.Count());
        }

        public bool ValidGpsOnPreBitacora(decimal? bitacoraId, string gpsId)
        {
            return _ctx.Sp_bitacora_valid_gps_prebitacora(bitacoraId, gpsId) == 0;
        }

        public bool ValidDuplicateBitacora(int customerId, int destinyId, string deliveryNumber, string plate)
        {
            return _ctx.sp_ValidDuplicateBitacora(customerId, destinyId, deliveryNumber, plate) > 0;
        }

        public bool CreateUpdateBitacoraMassiveLoad(int id, string originalFileNameIn, string fileNameIn, DateTime beginDate, string fileNameOut, DateTime endDate, char massiveLoadState)
        {
            return _ctx.sp_create_or_update_bitacora_state_massive_load(id, originalFileNameIn, fileNameIn, beginDate, fileNameOut, endDate, massiveLoadState, _userName) > 0;
        }

    }
}
