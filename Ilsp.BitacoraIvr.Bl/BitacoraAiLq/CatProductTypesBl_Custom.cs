﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatProductTypesBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public CatProductTypesBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public IEnumerable<ProductTypeEnt> All()
        {
            return _ctx.ivr_sp_all_producttypes().Select(x => new ProductTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                Type = x.Type,
                Color = x.Color
            }).ToArray();
        }

        public IEnumerable<ProductTypeEnt> Search(string search)
        {
            return _ctx.ivr_sp_search_producttypes(search).Select(x => new ProductTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                Type = x.Type,
                Color = x.Color
            }).ToArray();
        }

        public ProductTypeEnt First(string productTypeId)
        {
            var response = _ctx.ivr_sp_one_producttypes(productTypeId).Select(x => new ProductTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                Type = x.Type,
                Color = x.Color
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
