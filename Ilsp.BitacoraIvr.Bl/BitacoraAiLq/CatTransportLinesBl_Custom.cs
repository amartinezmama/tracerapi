﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatTransportLinesBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public CatTransportLinesBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public IEnumerable<TransportLineEnt> Search(int customerId, string search)
        {
            return _ctx.ivr_sp_search_transportlines(customerId, search).Select(x => new TransportLineEnt
            {
                Id = x.Id,
                Name = x.Name
            }).ToArray();
        }

        public IEnumerable<TransportLineEnt> SearchOnly(string search)
        {
            return _ctx.ivr_sp_search_cat_transportlines(search).Select(x => new TransportLineEnt
            {
                Id = x.Id,
                Name = x.Name
            }).ToArray();
        }

        public TransportLineEnt First(int customerId, string transportLineId)
        {
            var response = _ctx.ivr_sp_one_transportlines(customerId, transportLineId).Select(x => new TransportLineEnt
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
