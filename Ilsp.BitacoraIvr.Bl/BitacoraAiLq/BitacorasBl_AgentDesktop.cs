﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.AgentDesktop;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class BitacorasBl
    {
        public IEnumerable<ContactPhoneEnt> GetPhonesByIlspContactId(string id, string type)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException(@"Null or empty", nameof(id));
            }

            if (string.IsNullOrEmpty(type))
            {
                throw new ArgumentException(@"Null or empty", nameof(type));
            }

            if (!long.TryParse(id, out var idResult))
            {
                throw new ArgumentException(@"Is not a long valid value", nameof(id));
            }

            return _ctx.sp_get_contact_phones_by_id(idResult, type).Select(c => new ContactPhoneEnt
            {
                Type = c.Type,
                Phone = c.Phone,
                Name = c.Name,
                Order = c.Order
            }).ToArray();
        }

        public ApplicationConfigurationEnt GetApplicationConfiguration(int id)
        {
            var response = _ctx.sp_get_application_configuration(id).ToList();
            if (response.Any())
            {
                var item = response.First();
                return new ApplicationConfigurationEnt
                {
                    Name = item.name,
                    Screen_pop_url = item.screen_pop_url,
                    Report_name = item.report_name,
                    Report_parameter_name = item.report_parameter_name
                };
            }

            return null;
        }

        public string GetScript(int appId, string serviceId, string type, string situation)
        {
            return _ctx.spGetScript(appId, type, situation, decimal.Parse(serviceId)).Select(s => s.script).FirstOrDefault();
        }

        public string GetPhone(string phone)
        {
            return _ctx.fnGetPhoneNumber(phone);
        }

        public bool CanCloseContact(string contactId, string type)
        {
            return _ctx.fnCanContactClose(contactId, type) ?? false;
        }
    }
}
