﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatCustomersAxCustomersBl
    {

        public IEnumerable<AxCustomerEnt> Search(int customerId, string search)
        {
            return _ctx.ivr_sp_search_axcustomers(customerId, search).Select(x => new AxCustomerEnt
            {
                Id = x.AXCUSTOMERID,
                Name = x.SOCIALREASON
            }).ToArray();
        }

        public AxCustomerEnt FirstByCustomer(int customerId, string axCustomerId)
        {
            var response = _ctx.ivr_sp_one_axcustomers(customerId, axCustomerId).Select(x => new AxCustomerEnt
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        public IEnumerable<AxCustomerEnt> SearchFiscalNames(string search)
        {
            return _ctx.ivr_sp_search_fiscal_names(search).Select(x => new AxCustomerEnt
            {
                Id = x.AXCUSTOMERID,
                Name = x.SOCIALREASON
            }).ToArray();
        }

        public IEnumerable<AxCustomerEnt> GetFiscalNamesByCustomer(int customerId)
        {
            return _ctx.ivr_sp_get_axcustomers_by_customer(customerId).Select(x => new AxCustomerEnt
            {
                Id = x.AXCUSTOMERID,
                Name = x.SOCIALREASON
            }).ToArray();
        }

        public AxCustomerEnt First(string axCustomerId)
        {
            var response = _ctx.ivr_sp_one_fiscal_name(axCustomerId).Select(x => new AxCustomerEnt
            {
                Id = x.AXCUSTOMERID,
                Name = x.SOCIALREASON
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

    }
}
