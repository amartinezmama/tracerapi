﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class BitacorasTransportationsGpsBl
    {

        public List<GpsEnt> Get(int transportId)
        {
            return _ctx.sp_bitacora_transportation_gps_get(transportId).Select(g => new GpsEnt
            {
                Id = g.GpsId,
                Name = g.GpsName,
                Type = g.TypeId ?? 0
            }).ToList();
        }

    }
}
