﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatCustomersOriginsDestinationsBl
    {

        public List<OriginAndDestinyEnt> GetOriginsByCustomer(int customerId)
        {
            return _ctx.sp_get_origins_destinations_with_customer(customerId, "1").Select(g => new OriginAndDestinyEnt
            {
                Id = g.Id,
                Name = g.Name,
                Selected = g.Selected ?? false
            }).ToList();
        }

        public List<OriginAndDestinyEnt> GetDestinationsByCustomer(int customerId)
        {
            return _ctx.sp_get_origins_destinations_with_customer(customerId, "0").Select(g => new OriginAndDestinyEnt
            {
                Id = g.Id,
                Name = g.Name,
                Selected = g.Selected ?? false
            }).ToList();
        }

    }
}
