﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatMonitoringTypesBl
    {

        public IEnumerable<MonitoringTypeEnt> All()
        {
            return _ctx.ivr_sp_all_monitoring_types().Select(x => new MonitoringTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                IsMirrorAccount = x.isMirrorAccount,
                User = x.User,
                Gpsrequired = x.gpsrequired,
                Gpsallow = x.gpsallow,
                Custodyrequired = x.custodyrequired,
                Custodiesallow = x.custodiesallow,
                Securitykitrequired = x.securitykitrequired,
                Securitykitallow = x.securitykitallow,
                Lockrequired = x.lockrequired,
                Trabapatinrequired = x.trabapatinrequired,
                IsCustodyAbandon = x.isCustodyAbandon,
                IsLoadingWitness = x.isLoadingWitness ?? false
            }).ToArray();
        }

        public IEnumerable<MonitoringTypeEnt> Search(string search)
        {
            return _ctx.ivr_sp_search_monitoring_types(search).Select(x => new MonitoringTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                IsMirrorAccount = x.isMirrorAccount,
                User = x.User,
                Gpsrequired = x.gpsrequired,
                Gpsallow = x.gpsallow,
                Custodyrequired = x.custodyrequired,
                Custodiesallow = x.custodiesallow,
                Securitykitrequired = x.securitykitrequired,
                Securitykitallow = x.securitykitallow,
                Lockrequired = x.lockrequired,
                Trabapatinrequired = x.trabapatinrequired,
                IsCustodyAbandon = x.isCustodyAbandon,
                IsLoadingWitness = x.isLoadingWitness ?? false
            }).ToArray();
        }

        public MonitoringTypeEnt First(int monitoringTypeId)
        {
            var response = _ctx.ivr_sp_one_monitoring_types(monitoringTypeId).Select(x => new MonitoringTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                IsMirrorAccount = x.isMirrorAccount,
                User = x.User,
                Gpsrequired = x.gpsrequired,
                Gpsallow = x.gpsallow,
                Custodyrequired = x.custodyrequired,
                Custodiesallow = x.custodiesallow,
                Securitykitrequired = x.securitykitrequired,
                Securitykitallow = x.securitykitallow,
                Lockrequired = x.lockrequired,
                Trabapatinrequired = x.trabapatinrequired,
                IsCustodyAbandon = x.isCustodyAbandon,
                IsLoadingWitness = x.isLoadingWitness ?? false
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

    }
}
