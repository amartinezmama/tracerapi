﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public class BitacoraCustodyVehicleBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public BitacoraCustodyVehicleBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Create Vehicles of Custody
        /// </summary>
        /// <param name="vehicles">Collection of Vehicles</param>
        /// <param name="custodyId">Custody Identifier</param>
        /// <param name="isCustodyAbandon">isCustodyAbandon</param>
        /// <returns></returns>
        public bool Create(IEnumerable<CustodyVehicleEnt> vehicles, int custodyId, bool? isCustodyAbandon)
        {
            //add Vehicles to Custody
            foreach (var vehicle in vehicles)
            {
                int? vehicleId = 0;
                _ctx.Sp_bitacora_custody_vehicle_create(custodyId, vehicle.Custodian.Id,
                    vehicle.Vehicle.Id, vehicle.ShortNumber,
                    vehicle.LargeNumber, vehicle.Vehicle.Id, vehicle.Vehicle.AxId, _userName, isCustodyAbandon, ref vehicleId);
                if (vehicleId == 0)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Update Vehicles of Custody
        /// </summary>
        /// <param name="vehicles">Collection of Vehicles</param>
        /// <param name="custodyId">Custody Identifier</param>
        /// <param name="isCustodyAbandon">IsCustodyAbandon</param>
        /// <returns></returns>
        public bool Update(IEnumerable<CustodyVehicleEnt> vehicles, int custodyId, bool? isCustodyAbandon)
        {
            //update Vehicles to Custody
            return
                vehicles.All(
                    vehicle =>
                        _ctx.Sp_bitacora_custody_vehicle_update(vehicle.Id, custodyId,
                            vehicle.Custodian.Id, vehicle.Vehicle.Id, vehicle.ShortNumber.ToString(),
                            vehicle.LargeNumber.ToString(), vehicle.Vehicle.Id, vehicle.Vehicle.AxId, _userName, isCustodyAbandon) != 0);
        }

        /// <summary>
        /// Delete Vehicles of Custody
        /// </summary>
        /// <param name="vehicles">Collection of Vehicles</param>
        /// <param name="custodyId">Custody Identifier</param>
        /// <returns></returns>
        public bool Delete(IEnumerable<CustodyVehicleEnt> vehicles, int custodyId)
        {
            //delete Vehicles to Custody
            return
                vehicles.All(
                    vehicle =>
                        _ctx.Sp_bitacora_custody_vehicle_delete(vehicle.Id, custodyId, _userName) != 0);
        }

    }
}
