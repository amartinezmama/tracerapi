﻿using System;
using Ilsp.BitacoraIvr.Dl;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class AuditBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public AuditBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public int CreateAlertNotification(long bitacoraId, string notification)
        {
            return int.Parse(_ctx.sp_create_or_update_bitacora_notification(bitacoraId, notification, "alert", _userName).ToString());
        }

        public int CreateReactionNotification(long bitacoraId, string notification)
        {
            return int.Parse(_ctx.sp_create_or_update_bitacora_notification(bitacoraId, notification, "reaction", _userName).ToString());
        }

        public int CreateNewsNotification(long bitacoraId, string notification)
        {
            return int.Parse(_ctx.sp_create_or_update_bitacora_notification(bitacoraId, notification, "news", _userName).ToString());
        }

        public int CreateNotificationsNotification(long bitacoraId, string notification)
        {
            return int.Parse(_ctx.sp_create_or_update_bitacora_notification(bitacoraId, notification, "notifications", _userName).ToString());
        }

        public int DisableBitacoraNews(long bitacoraId, string news)
        {
            return int.Parse(_ctx.sp_disabled_bitacora_news(bitacoraId, news).ToString());
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
