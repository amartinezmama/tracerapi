﻿using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatTransportLinesVehiclesBl
    {

        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <returns></returns>
        public KendoResponse GetGridCustom(KendoRequest model)
        {
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join("|", model.Sort.Select(x => $"{x.Field},{(x.Dir.ToLower() == "asc" ? "1" : "0")}").ToArray());
            }

            // compose the filter 
            var filter = string.Empty;
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("|", model.Filter.Filters.Select(f => $"{f.Field},{f.Value.ToLower()}").ToArray());
            }

            var response = _ctx.ivr_sp_search_plates_grid(filter, order, model.Page, model.Take, _userName).ToList();
            var totalData = 0;
            if (response.Any())
            {
                totalData = response.First().Total ?? 0;
            }
            var items = response.Select(item => new PlateGrid
            {
                Id = item.Id,
                NumPlate = item.Name.ToUpper().Trim(),
                UnitType = item.UnitName?.ToUpper().Trim(),
                Economic = item.Economic?.ToUpper().Trim(),
                Brand = item.BrandName?.ToUpper().Trim(),
                Model = item.Model?.ToUpper().Trim()
            }).ToList();
            var data = items.AsQueryable();
            return new KendoResponse(data.ToArray(), totalData);
        }

        public IEnumerable<PlateEnt> Search(int customerId, string transportLine, string search)
        {
            var response = _ctx.ivr_sp_search_plates(customerId, transportLine, search).ToList();
            var list = new List<PlateEnt>();
            response.ForEach(x =>
            {
                list.Add(new PlateEnt
                {
                    Id = x.Id,
                    NumPlate = x.Name,
                    UnitType = x.UnitId == null ? null : new UnitTypeEnt
                    {
                        Id = x.UnitId ?? 0,
                        Name = x.UnitName,
                        UnitsAllow = x.UnitsAllow ?? 0,
                        MinUnitsAllow = x.MinUnitsAllow ?? 0,
                        IsValid = x.UnitIsValid ?? false
                    },
                    Brand = x.BrandId == null ? null : new BrandEnt
                    {
                        Id = x.BrandId ?? 0,
                        Name = x.BrandName
                    },
                    Economic = x.Economic,
                    Model = string.IsNullOrEmpty(x.Model) ? null : new UnitModelEnt
                    {
                        Id = int.Parse(x.Model.Trim()),
                        Name = x.Model
                    },
                    Comments = x.Observations ?? string.Empty,
                    Colors = !string.IsNullOrEmpty(x.Color) ? x.Color?.Split(',').Select(c => new ColorEnt
                    {
                        Id = c.Split('|')[0],
                        Name = c.Split('|')[1],
                        IsValid = Convert.ToBoolean(int.Parse(c.Split('|')[2]))
                    }).ToArray() : new ColorEnt[0],
                    TransportLine = !string.IsNullOrEmpty(x.AxTransportLine) ? new TransportLineEnt
                    {
                        Id = x.AxTransportLine.Trim(),
                        Name = x.TransportLineName
                    } : null
                });
            });
            return list;
        }

        public PlateEnt First(string transportLine, int plateId)
        {
            var response = _ctx.ivr_sp_one_plates(transportLine, plateId).Select(x => new PlateEnt
            {
                Id = x.Id,
                NumPlate = x.Name,
                UnitType = x.UnitId == null ? null : new UnitTypeEnt
                {
                    Id = x.UnitId ?? 0,
                    Name = x.UnitName,
                    UnitsAllow = x.UnitsAllow ?? 0,
                    MinUnitsAllow = x.MinUnitsAllow ?? 0,
                    IsValid = x.UnitIsValid ?? false
                },
                Brand = x.BrandId == null ? null : new BrandEnt
                {
                    Id = x.BrandId ?? 0,
                    Name = x.BrandName
                },
                Economic = x.Economic,
                Model = string.IsNullOrEmpty(x.Model) ? null : new UnitModelEnt
                {
                    Id = int.Parse(x.Model.Trim()),
                    Name = x.Model
                },
                Comments = x.Observations ?? string.Empty,
                Colors = !string.IsNullOrEmpty(x.Color) ? x.Color?.Split(',').Select(c => new ColorEnt
                {
                    Id = c.Split('|')[0],
                    Name = c.Split('|')[1],
                    IsValid = Convert.ToBoolean(int.Parse(c.Split('|')[2]))
                }).ToArray() : new ColorEnt[0],
                TransportLine = !string.IsNullOrEmpty(x.AxTransportLine) ? new TransportLineEnt
                {
                    Id = x.AxTransportLine.Trim(),
                    Name = x.TransportLineName
                } : null
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        /// <summary>
        /// Create Vehicle
        /// </summary>
        /// <param name="vehicle">Transportation Data</param>
        /// <param name="vehicleId">Id created</param>
        /// <returns></returns>
        public bool Create(PlateEnt vehicle, ref int vehicleId)
        {
            vehicleId = _ctx.sp_transport_lines_vehicles_create(vehicle.TransportLine.Id,
                vehicle.UnitType.Id,
                vehicle.NumPlate.ToUpper(),
                vehicle.Economic.ToUpper(),
                vehicle.Model.Id.ToString(),
                vehicle.Comments?.ToUpper(),
                vehicle.Brand.Id,
                vehicle.Colors != null ? string.Join(",", vehicle.Colors.Select(m => m.Id).ToList()) : null,
                _userName);
            if (vehicleId == 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Update Vehicle
        /// </summary>
        /// <param name="vehicle">Transportation Data</param>
        /// <returns></returns>
        public bool Update(PlateEnt vehicle)
        {
            int? vehicleId = _ctx.sp_transport_lines_vehicles_update(vehicle.Id,
                vehicle.TransportLine?.Id,
                vehicle.UnitType.Id,
                vehicle.NumPlate.ToUpper(),
                vehicle.Economic.ToUpper(),
                vehicle.Model.Id.ToString(),
                vehicle.Comments?.ToUpper(),
                vehicle.Brand.Id,
                vehicle.Colors != null ? string.Join(",", vehicle.Colors.Select(m => m.Id).ToList()) : null,
                _userName);
            if (vehicleId == 0)
            {
                return false;
            }
            return true;
        }

    }
}
