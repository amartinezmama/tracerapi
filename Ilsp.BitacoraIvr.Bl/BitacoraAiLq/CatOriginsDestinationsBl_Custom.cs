﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatOriginsDestinationsBl
    {

        public IEnumerable<OriginAndDestinyEnt> SearchOrigins(int customerId, string search)
        {
            return _ctx.ivr_sp_search_origins(customerId, search).Select(x => new OriginAndDestinyEnt
            {
                Id = x.Id,
                Alias = x.Alias,
                Address = x.Address,
                Street = x.Street,
                Town = x.Town,
                Municipality = x.Municipality,
                Number = x.Number,
                State = x.State,
                Postcode = x.Postcode,
                Latitude = x.Latitude.ToString(),
                Longitude = x.Longitude.ToString(),
                Typeid = x.Typeid.ToString(),
                Type = x.Type,
                IsOriginProcter = x.IsOriginProcter > 0
            }).ToArray();
        }

        public OriginAndDestinyEnt FirstOrigin(int customerId, int originId)
        {
            var response = _ctx.ivr_sp_one_origins(customerId, originId).Select(x => new OriginAndDestinyEnt
            {
                Id = x.Id,
                Alias = x.Alias,
                Address = x.Address,
                Street = x.Street,
                Town = x.Town,
                Municipality = x.Municipality,
                Number = x.Number,
                State = x.State,
                Postcode = x.Postcode,
                Latitude = x.Latitude.ToString(),
                Longitude = x.Longitude.ToString(),
                Typeid = x.Typeid.ToString(),
                Type = x.Type,
                IsOriginProcter = x.IsOriginProcter > 0
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        public IEnumerable<OriginAndDestinyEnt> SearchDestinies(int customerId, string search)
        {
            return _ctx.ivr_sp_search_destinies(customerId, search).Select(x => new OriginAndDestinyEnt
            {
                Id = x.Id,
                Alias = x.Alias,
                Address = x.Address,
                Street = x.Street,
                Town = x.Town,
                Municipality = x.Municipality,
                Number = x.Number,
                State = x.State,
                Postcode = x.Postcode,
                Latitude = x.Latitude.ToString(),
                Longitude = x.Longitude.ToString(),
                Typeid = x.Typeid.ToString(),
                Type = x.Type,
                IsOriginProcter = x.IsOriginProcter > 0,
            }).ToArray();
        }

        public OriginAndDestinyEnt FirstDestiny(int customerId, int destinyId)
        {
            var response = _ctx.ivr_sp_one_destinies(customerId, destinyId).Select(x => new OriginAndDestinyEnt
            {
                Id = x.Id,
                Alias = x.Alias,
                Address = x.Address,
                Street = x.Street,
                Town = x.Town,
                Municipality = x.Municipality,
                Number = x.Number,
                State = x.State,
                Postcode = x.Postcode,
                Latitude = x.Latitude.ToString(),
                Longitude = x.Longitude.ToString(),
                Typeid = x.Typeid.ToString(),
                Type = x.Type,
                IsOriginProcter = x.IsOriginProcter > 0
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

    }
}
