﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatCustomersBl
    {
        public IEnumerable<CustomerEnt> GetAllCustom()
        {
            return _ctx.sp_get_customers().Select(x => new CustomerEnt
            {
                Id = x.Id,
                Name = x.Name,
                FiscalName = x.FiscalName,
                FiscalAddress = x.FiscalAddress,
                Webpage = x.Webpage,
                Email = x.Email ?? false,
                Sms = x.Sms ?? false,
                Logo = x.Logo,
                Event = x.Event ?? false,
                Daily = x.Daily ?? false,
                Weekly = x.Weekly ?? false,
                Monthly = x.Monthly ?? false,
                ModifiedBy = x.ModifiedBy,
                CreationDate = x.CreationDate.ToString("dd/MMM/yyyy HH:mm:ss"),
                ModifiedDate = x.ModifiedDate?.ToString("dd/MMM/yyyy HH:mm:ss"),
                OnTime = x.OnTime,
                ValidPreBitacora = x.ValidPreBitacora
            }).ToArray();
        }

        public IEnumerable<CustomerEnt> Search(string search)
        {
            return _ctx.ivr_sp_search_customers(search).Select(x => new CustomerEnt
            {
                Id = x.Id,
                Name = x.Name,
                FiscalName = x.FiscalName,
                FiscalAddress = x.FiscalAddress,
                Webpage = x.Webpage,
                Email = x.Email ?? false,
                Sms = x.Sms ?? false,
                Logo = x.Logo,
                Event = x.Event ?? false,
                Daily = x.Daily ?? false,
                Weekly = x.Weekly ?? false,
                Monthly = x.Monthly ?? false,
                ModifiedBy = x.ModifiedBy,
                CreationDate = x.CreationDate.ToString("dd/MMM/yyyy HH:mm:ss"),
                ModifiedDate = x.ModifiedDate?.ToString("dd/MMM/yyyy HH:mm:ss"),
                OnTime = x.OnTime,
                ValidPreBitacora = x.ValidPreBitacora
            }).ToArray();
        }

        public CustomerEnt First(int customerId)
        {
            var response = _ctx.ivr_sp_one_customers(customerId).Select(x => new CustomerEnt
            {
                Id = x.Id,
                Name = x.Name,
                FiscalName = x.FiscalName,
                FiscalAddress = x.FiscalAddress,
                Webpage = x.Webpage,
                Email = x.Email ?? false,
                Sms = x.Sms ?? false,
                Logo = x.Logo,
                Event = x.Event ?? false,
                Daily = x.Daily ?? false,
                Weekly = x.Weekly ?? false,
                Monthly = x.Monthly ?? false,
                ModifiedBy = x.ModifiedBy,
                CreationDate = x.CreationDate.ToString("dd/MMM/yyyy HH:mm:ss"),
                ModifiedDate = x.ModifiedDate?.ToString("dd/MMM/yyyy HH:mm:ss"),
                OnTime = x.OnTime,
                ValidPreBitacora = x.ValidPreBitacora
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        public IEnumerable<string> GetUsersAssignedByCustomer(int customerId)
        {
            return _ctx.sp_get_users_by_customer(customerId).Select(x => x.user_id).ToArray();
        }

        public IEnumerable<int> GetCustomersAssignedByUser(string userName)
        {
            return _ctx.sp_get_customers_by_user(userName).Select(x => x.customer_id).ToArray();
        }

        /// <summary>
        /// Get Items
        /// </summary>
        /// <param name="model">Filters, Orders and Pages</param>
        /// <returns></returns>
        public KendoResponse GetGridCustom(KendoRequest model)
        {
            var filterSp = string.Empty;
            var orderSp = string.Empty;

            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filterSp = string.Join("|",
                    model.Filter.Filters.Select(f => $"{f.Field},{f.Value.ToLower()}")
                        .ToArray());
            }

            if (model.Sort != null && model.Sort.Count > 0)
            {
                orderSp = string.Join("|",
                    model.Sort.Select(x => $"{x.Field},{(x.Dir == "asc" ? "1" : "0")}").ToArray());
            }
            var datatemp = _ctx.ivr_sp_get_customers(filterSp, orderSp, model.Page, model.Take).Select(x => new CustomerGrid
            {
                Id = x.id ?? 0,
                Name = x.name,
                FiscalName = x.fiscalname,
                Total = x.Total ?? 0
            }).ToList();

            var count = 0;
            if (datatemp.Count > 0)
            {
                count = datatemp.First().Total;
            }
            return new KendoResponse(datatemp.ToArray(), count);
        }

        public bool CreateCustom(CustomerEnt entity)
        {
            var id = _ctx.ivr_sp_customer_create(entity.Name, entity.SendGeneralReport,
            string.Join(",", entity.MailsGeneralReport?.Select(x => x.Name) ?? new string[0]), _userName, entity.Status);
            var result = id > 0;
            entity.Id = id;
            if (result)
            {
                foreach (var opKeyTl in entity.OpKeyTransportLines ?? new OperativeKeyEnt[0])
                {
                    if (_ctx.ivr_sp_customer_opkeys_trans_lines_create(entity.Id, opKeyTl.Id, _userName) == 0)
                    {
                        result = false;
                        break;
                    }
                }
            }
            if (result)
            {
                foreach (var fiscalName in entity.FiscalNames ?? new AxCustomerEnt[0])
                {
                    if (_ctx.ivr_sp_customer_axcustomer_create(entity.Id, fiscalName.Id, _userName) == 0)
                    {
                        result = false;
                        break;
                    }
                }
            }
            if (result)
            {
                foreach (var opKey in entity.OperativeKeys ?? new CustomerOpKeyEnt[0])
                {
                    if (_ctx.ivr_sp_customer_opkeys_create(entity.Id, opKey.OperativeKey.Id, opKey.Mail, _userName) == 0)
                    {
                        result = false;
                        break;
                    }
                }
            }

            return result;
        }

        public bool UpdateCustom(CustomerEnt entity, CustomerEnt customerBefore)
        {
            var result = _ctx.ivr_sp_customer_update(entity.Id, entity.Name, entity.SendGeneralReport,
                string.Join(",", entity.MailsGeneralReport?.Select(x => x.Name) ?? new string[0]), _userName, entity.Status) > 0;
            if (result)
            {
                foreach (var opKeyTl in customerBefore.OpKeyTransportLines.Where(b => !entity.OpKeyTransportLines.Any(a => a.Id.Equals(b.Id))))
                {
                    if (_ctx.ivr_sp_customer_opkeys_trans_lines_delete(entity.Id, opKeyTl.Id) == 0)
                    {
                        result = false;
                        break;
                    }
                }
            }
            if (result)
            {
                foreach (var opKeyTl in entity.OpKeyTransportLines.Where(a => !customerBefore.OpKeyTransportLines.Any(b => b.Id.Equals(a.Id))))
                {
                    if (_ctx.ivr_sp_customer_opkeys_trans_lines_create(entity.Id, opKeyTl.Id, _userName) == 0)
                    {
                        result = false;
                        break;
                    }
                }
            }
            if (result)
            {
                foreach (var fiscalName in customerBefore.FiscalNames.Where(b => !entity.FiscalNames.Any(a => a.Id.Equals(b.Id))))
                {
                    if (_ctx.ivr_sp_customer_axcustomer_delete(entity.Id, fiscalName.Id) == 0)
                    {
                        result = false;
                        break;
                    }
                }
            }
            if (result)
            {
                foreach (var fiscalName in entity.FiscalNames.Where(a => !customerBefore.FiscalNames.Any(b => b.Id.Equals(a.Id))))
                {
                    if (_ctx.ivr_sp_customer_axcustomer_create(entity.Id, fiscalName.Id, _userName) == 0)
                    {
                        result = false;
                        break;
                    }
                }
            }
            if (result)
            {
                foreach (var opKey in customerBefore.OperativeKeys.Where(b => !entity.OperativeKeys.Any(a => a.OperativeKey.Id.Equals(b.OperativeKey.Id) && a.Mail.ToLower().Equals(b.Mail.ToLower()))))
                {
                    if (_ctx.ivr_sp_customer_opkeys_delete(entity.Id, opKey.OperativeKey.Id, opKey.Mail) == 0)
                    {
                        result = false;
                        break;
                    }
                }
            }
            if (result)
            {
                foreach (var opKey in entity.OperativeKeys.Where(a => !customerBefore.OperativeKeys.Any(b => b.OperativeKey.Id.Equals(a.OperativeKey.Id) && b.Mail.ToLower().Trim().Equals(a.Mail.ToLower().Trim()))))
                {
                    if (_ctx.ivr_sp_customer_opkeys_create(entity.Id, opKey.OperativeKey.Id, opKey.Mail, _userName) == 0)
                    {
                        result = false;
                        break;
                    }
                }

            }
            return result;
        }

        public IEnumerable<CustomerEnt> GetCustomersByUser()
        {
            return _ctx.sp_getCustomersByUser(_userName).Select(x => new CustomerEnt
            {
                Id = x.Id,
                Name = x.Name,
                FiscalName = x.FiscalName,
                FiscalAddress = x.FiscalAddress,
                Webpage = x.Webpage,
                Email = x.Email ?? false,
                Sms = x.Sms ?? false,
                Logo = x.Logo,
                Event = x.Event ?? false,
                Daily = x.Daily ?? false,
                Weekly = x.Weekly ?? false,
                Monthly = x.Monthly ?? false,
                ModifiedBy = x.ModifiedBy,
                CreationDate = x.CreationDate.ToString("dd/MMM/yyyy HH:mm:ss"),
                ModifiedDate = x.ModifiedDate?.ToString("dd/MMM/yyyy HH:mm:ss"),
                OnTime = x.OnTime,
                ValidPreBitacora = x.ValidPreBitacora
            }).ToArray();
        }

    }
}
