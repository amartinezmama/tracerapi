﻿using System;
using Ilsp.BitacoraIvr.Dl;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public class BitacoraTransportationGpsBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public BitacoraTransportationGpsBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Create gps in Transportation Data on Bitacora
        /// </summary>
        /// <param name="gpsId">GpsId</param>
        /// <param name="transportId">Transportation identifier</param>
        /// <returns></returns>
        public bool Create(string gpsId, int transportId)
        {
            //add gps´s to Transportation Data
            int? transportGpsId = 0;
            _ctx.Sp_bitacora_transportation_gps_create(transportId, gpsId, _userName, ref transportGpsId);
            if (transportGpsId == 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Delete Gps's in Transportation Data on Bitacora
        /// </summary>
        /// <param name="gpsId">GpsId</param>
        /// <param name="bitacoraId">BitacoraId</param>
        /// <returns></returns>
        public bool Delete(string gpsId, long bitacoraId)
        {
            //delete gps´s to Transportation Data
            if (_ctx.Sp_bitacora_transportation_gps_delete(bitacoraId, gpsId, _userName) == 0)
            {
                return false;
            }
            return true;
        }

    }
}
