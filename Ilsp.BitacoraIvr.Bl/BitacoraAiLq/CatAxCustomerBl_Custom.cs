﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatAxCustomerBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public CatAxCustomerBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public IEnumerable<AxCustomerEnt> Search(int customerId, string search)
        {
            return _ctx.ivr_sp_search_axcustomers(customerId, search).Select(x => new AxCustomerEnt
            {
                Id = x.AXCUSTOMERID,
                Name = x.SOCIALREASON
            }).ToArray(); 
        }

        public AxCustomerEnt First(int customerId, string axCustomerId)
        {
            var response = _ctx.ivr_sp_one_axcustomers(customerId, axCustomerId).Select(x => new AxCustomerEnt
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}