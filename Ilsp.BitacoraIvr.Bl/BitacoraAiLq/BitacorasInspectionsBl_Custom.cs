﻿using System;
using Ilsp.BitacoraIvr.Dl;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class BitacorasInspectionsBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public BitacorasInspectionsBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public int ValidateThreeGpsLineLocation(long bitacoraId, double? latitude, double? longitude)
        {
            return int.Parse(_ctx.sp_validate_three_gps_line_location(bitacoraId, latitude, longitude).ToString());
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
