﻿using System;
using Ilsp.BitacoraIvr.Dl;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public class BitacoraContainerGpsBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public BitacoraContainerGpsBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Create Gps's of Container by Bitacora
        /// </summary>
        /// <param name="gpsId">GpsId</param>
        /// <param name="containerId">Container Identifier</param>
        /// <returns></returns>
        public bool Create(string gpsId, int containerId)
        {
            //add Gps´s by Container
            int? containerGpsId = 0;
            _ctx.Sp_bitacora_container_gps_create(containerId, gpsId, _userName, ref containerGpsId);
            if (containerGpsId == 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Delete Gps's of Container by Bitacora
        /// </summary>
        /// <param name="gpsId">GpsId</param>
        /// <param name="bitacoraId">BitacoraId</param>
        /// <returns></returns>
        public bool Delete(string gpsId, long bitacoraId)
        {
            if (_ctx.Sp_bitacora_container_gps_delete(bitacoraId, gpsId, _userName) == 0)
            {
                return false;
            }
            return true;
        }

    }
}
