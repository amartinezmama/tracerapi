﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatCustodianTypesBl
    {

        public IEnumerable<CustodyTypeEnt> All()
        {
            return _ctx.ivr_sp_all_custody_types().Select(x => new CustodyTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                ShowCabin = x.ShowCabin,
                ShowVehicle = x.ShowVehicle
            }).ToArray();
        }

        public IEnumerable<CustodyTypeEnt> Search(string search)
        {
            return _ctx.ivr_sp_search_custody_types(search).Select(x => new CustodyTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                ShowCabin = x.ShowCabin,
                ShowVehicle = x.ShowVehicle
            }).ToArray();
        }

        public CustodyTypeEnt First(int custodyTypeId)
        {
            var response = _ctx.ivr_sp_one_custody_types(custodyTypeId).Select(x => new CustodyTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                ShowCabin = x.ShowCabin,
                ShowVehicle = x.ShowVehicle
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

    }
}
