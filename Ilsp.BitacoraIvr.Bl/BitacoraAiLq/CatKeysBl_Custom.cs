﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatKeysBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public CatKeysBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public IEnumerable<KeyEnt> Search(string search)
        {
            return _ctx.ivr_sp_search_container_keys(search).Select(x => new KeyEnt
            {
                Id = x.Id,
                Name = x.Name
            }).ToArray();
        }

        public KeyEnt First(string keyId)
        {
            var response = _ctx.ivr_sp_one_container_keys(keyId).Select(x => new KeyEnt
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
