﻿using System;
using Ilsp.BitacoraIvr.Dl;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
	public partial class BitacorasAlertsBl : IDisposable
	{
        private readonly string _connection;
        private readonly string _userName;
        public readonly BitacoraAiDataContext _ctx;

        public BitacorasAlertsBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
	}
}
