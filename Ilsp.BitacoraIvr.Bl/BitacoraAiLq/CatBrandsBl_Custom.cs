﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatBrandsBl
    {

        public IEnumerable<BrandEnt> Search(int customerId, string search)
        {
            return _ctx.ivr_sp_search_brands(customerId, search).Select(x => new BrandEnt
            {
                Id = x.Id,
                Name = x.Name,
                IsValid = x.IsValid
            }).ToArray();
        }

        public IEnumerable<BrandEnt> SearchOnly(string search)
        {
            return _ctx.ivr_sp_search_cat_brands(search).Select(x => new BrandEnt
            {
                Id = x.Id,
                Name = x.Name,
                IsValid = x.IsValid
            }).ToArray();
        }

        public BrandEnt First(int brandId)
        {
            var response = _ctx.ivr_sp_one_brands(brandId).Select(x => new BrandEnt
            {
                Id = x.Id,
                Name = x.Name,
                IsValid = x.IsValid
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        /// <summary>
        /// Create Brand
        /// </summary>
        /// <param name="brand">Brand Data</param>
        /// <param name="brandId">BrandId</param>
        /// <returns>TRUE when operation is successful</returns>
        public bool Create(BrandEnt brand, ref int brandId)
        {
            brandId = _ctx.sp_brands_create(brand.Name.ToUpper(), _userName);
            return brandId > 0;
        }

    }
}
