﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Tl;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatGeofencesBl
    {

        public IEnumerable<object> GetGeofencesWithZone()
        {
            return _ctx.sp_get_zone_geofences().Select(x => new
            {
                x.name,
                data = GooglePoints.GeographyEncode(x.data),
                x.color,
                type = x.data.Contains("LINESTRING") ? "polyline" : "polygon"
            }).ToArray();
        }

        public bool CreateCustom(GeofenceEnt geofence, int appId)
        {
            geofence.Id = _ctx.sp_geofences_create(geofence.Name, geofence.Description, geofence.ZoneType.Id,
                geofence.GeofenceType.Id, string.Join(",", geofence.Points.Select(x => x.lng + " " + x.lat).ToList()),
                geofence.Radio, geofence.Time, geofence.Color, _userName, appId);
            return geofence.Id > 0;
        }

        public bool UpdateCustom(GeofenceEnt geofence)
        {
            return _ctx.sp_geofences_update(geofence.Id, geofence.Name, geofence.Description, geofence.ZoneType.Id,
                geofence.GeofenceType.Id, string.Join(",", geofence.Points.Select(x => x.lng + " " + x.lat).ToList()),
                geofence.Radio, geofence.Time, geofence.Color, _userName) > 0;
        }

        public IEnumerable<GeofenceEnt> Search(int applicationId, string search)
        {
            return _ctx.ivr_sp_search_geofences(applicationId, search).Select(x => new GeofenceEnt
            {
                Id = x.Id,
                Name = x.Name
            }).ToArray();
        }

    }
}