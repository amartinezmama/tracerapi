﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Tl;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public class BitacoraDestinyBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public BitacoraDestinyBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// add Destiny of destinations to Bitacora
        /// </summary>
        /// <param name="destinations">Collection of Destiny</param>
        /// <param name="bitacoraId">Bitacora Identifier</param>
        /// <returns></returns>
        public bool Create(IEnumerable<DestinyEnt> destinations, long bitacoraId)
        {
            foreach (var destiny in destinations)
            {
                int? destinyId = _ctx.Sp_bitacora_destiny_create(bitacoraId, destiny.Destiny.Id,
                    OptionsDateTime.ToDateTime(destiny.AppointmentDate), destiny.DeliveryNumber.ToUpper());
                if (destinyId == 0)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// add, update or delete Destiny of destinations to Bitacora
        /// </summary>
        /// <param name="destinations">Collection of Destiny</param>
        /// <param name="bitacoraId">Bitacora Identifier</param>
        /// <returns></returns>
        public bool Update(IEnumerable<DestinyEnt> destinations, long bitacoraId)
        {
            return
                destinations.All(
                    destiny =>
                        _ctx.Sp_bitacora_destiny_update(destiny.Id, bitacoraId, destiny.Id,
                            OptionsDateTime.ToDateTime(destiny.AppointmentDate), destiny.DeliveryNumber.ToUpper()) !=
                        0);
        }

        /// <summary>
        /// add, update or delete Destiny of destinations to Bitacora
        /// </summary>
        /// <param name="destinations">Collection of Destiny</param>
        /// <param name="bitacoraId">Bitacora Identifier</param>
        /// <returns></returns>
        public bool Delete(IEnumerable<DestinyEnt> destinations, long bitacoraId)
        {
            return
                destinations.All(
                    destiny =>
                        _ctx.Sp_bitacora_destiny_delete(destiny.BitacoraDestinyId, bitacoraId, destiny.Id) != 0);
        }

        public List<DestinyEnt> Get(long bitacoraId, int customerId)
        {
            using (var bl = new CatOriginsDestinationsBl(_connection, _userName, _ctx))
            {
                return _ctx.sp_bitacora_destiny_get(bitacoraId).Select(d => new DestinyEnt
                {
                    Id = d.destinyheaderid,
                    Destiny = bl.FirstDestiny(customerId, d.destinyid),
                    AppointmentDate = d.meetingdate.ToString("yyyy/MM/dd HH:mm:ss"),
                    DeliveryNumber = d.devilverynumber
                }).ToList();
            }
        }

    }
}
