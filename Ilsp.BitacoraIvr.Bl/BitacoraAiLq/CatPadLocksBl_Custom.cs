﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatPadLocksBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public CatPadLocksBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public IEnumerable<PadLockEnt> Search(string search)
        {
            return _ctx.ivr_sp_search_container_padlocks(search).Select(x => new PadLockEnt
            {
                Id = x.Id,
                Name = x.Name
            }).ToArray();
        }

        public PadLockEnt First(string padLockId)
        {
            var response = _ctx.ivr_sp_one_container_padlocks(padLockId).Select(x => new PadLockEnt
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
