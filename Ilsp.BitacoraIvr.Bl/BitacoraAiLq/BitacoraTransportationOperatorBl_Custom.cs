﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public class BitacoraTransportationOperatorBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public BitacoraTransportationOperatorBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Create operators in Transportation Data on Bitacora
        /// </summary>
        /// <param name="transportationOperators">List of Operators</param>
        /// <param name="transportationheaderid">Transportation identifier</param>
        /// <returns></returns>
        public bool Create(IEnumerable<OperatorEnt> transportationOperators, int transportationheaderid)
        {
            foreach (var operators in transportationOperators)
            {
                int? operatorId = 0;
                _ctx.Sp_bitacora_transportation_operator_create(transportationheaderid, operators.Name,
                    operators.CellPhoeOne, operators.CellPhoeTwo, ref operatorId);
                if (operatorId == 0)
                {

                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Delete operators in Transportation Data on Bitacora
        /// </summary>
        /// <param name="transportationOperators">List of Operators</param>
        /// <param name="transportId"></param>
        /// <returns></returns>
        public bool Delete(IEnumerable<OperatorEnt> transportationOperators, int transportId)
        {
            if (
                transportationOperators.Select(
                    operators =>
                        _ctx.Sp_bitacora_transportation_operator_delete(transportId,
                            operators.Id)).Any(operatorId => operatorId == 0))
            {
                return false;
            }
            return true;
        }

        public List<OperatorEnt> Get(int transportId)
        {
            return _ctx.sp_bitacora_transportation_operator_get(transportId).Select(o => new OperatorEnt
            {
                Id = o.operatorid,
                Name = o.name,
                CellPhoeOne = o.cellphone,
                CellPhoeTwo = o.secondphone
            }).ToList();
        }

    }
}
