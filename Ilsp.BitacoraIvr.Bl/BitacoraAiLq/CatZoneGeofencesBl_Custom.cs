﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Tl;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatZoneGeofencesBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public CatZoneGeofencesBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public IEnumerable<object> GetZoneGeofences()
        {
            return _ctx.sp_get_zone_geofences().Select(r => new
            {
                r.name,
                data = GooglePoints.GeographyEncode(r.data),
                r.color,
                type = "zone"
            }).ToArray();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
