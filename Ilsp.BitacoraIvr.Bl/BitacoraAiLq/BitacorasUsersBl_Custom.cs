﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.Identity;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class BitacorasUsersBl
    {

        public IEnumerable<UserEnt> GetUsersAssignOrUnassignUsersOnBitacora(int customerId, long bitacoraId)
        {
            return _ctx.sp_get_users_assigned_to_bitacora(customerId, bitacoraId).Select(x => new UserEnt
            {
                Id = x.UserName,
                UserName = x.UserName,
                Selected = x.Selected ?? false
            }).ToArray();
        }

        public bool AssignOrUnassignUsersToBitacora(long bitacoraId, string users)
        {
            return _ctx.sp_get_assign_or_unassign_users_to_bitacora(bitacoraId, users, _userName) > 0;
        }

        public bool ReassignOrUnassignUsersToBitacora(string unassignUsers, string bitacoras, string assignUsers)
        {
            return _ctx.sp_reassign_or_unassign_users_to_bitacora(bitacoras, unassignUsers, assignUsers, _userName) >
                   0;
        }

    }
}
