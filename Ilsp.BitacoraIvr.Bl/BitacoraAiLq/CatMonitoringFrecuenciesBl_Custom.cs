﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.Common;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatMonitoringFrecuenciesBl
    {

        public KendoResponse GetGridBitacoras(KendoRequest model, bool all)
        {
            var filterSp = string.Empty;
            var orderSp = string.Empty;

            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filterSp = string.Join("|",
                    model.Filter.Filters.Select(f => $"{f.Field},{f.Value.ToLower()}")
                        .ToArray());
            }

            if (model.Sort != null && model.Sort.Count > 0)
            {
                orderSp = string.Join("|",
                    model.Sort.Select(x => $"{x.Field},{(x.Dir == "asc" ? "1" : "0")}").ToArray());
            }

            var datatemp =
                _ctx.sp_bitacora_get_grid_bitacoras(filterSp, orderSp, model.Page, model.Take, _userName, all).ToList()
                    .Select(x => new PreCaptureGrid
                    {
                        Id = x.Id.ToString(),
                        Alias = x.Alias,
                        Frecuency = x.Frecuency,
                        Status = x.Status,
                        CreateDate = x.CreateDate,
                        Customer = x.Customer,
                        TransportLine = x.TransportLine,
                        Origin = x.Origin,
                        Destiny = x.Destiny,
                        DeliveryNumber = x.DeliveryNumber,
                        AppointmentDate = x.AppointmentDate,
                        ServiceType = x.ServiceType,
                        MonitoringType = x.MonitoringType,
                        Plate = x.Plate,
                        Economic = x.Economic,
                        Brand = x.Brand,
                        Model = x.Model,
                        Color = x.Color,
                        VehicleType = x.VehicleType,
                        CustodyType = x.CustodyType,
                        Selected = false,
                        Total = x.Total ?? 0
                    }).ToList();

            var count = 0;
            if (datatemp.Count > 0)
            {
                count = datatemp.First().Total;
            }

            return new KendoResponse(datatemp.ToArray(), count);
        }

        /// <summary>
        /// Set new Frecuency Monitoting
        /// </summary>
        /// <returns></returns>
        public bool SetFrecuencyMonitoring(IEnumerable<string> bitacoras, int frecuency, string comment)
        {
            return _ctx.sp_bitacora_change_frecuency_monitoring(string.Join(",", bitacoras), frecuency, comment, _userName) > 0;
        }

    }
}