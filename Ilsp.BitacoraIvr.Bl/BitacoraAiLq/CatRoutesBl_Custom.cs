﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Tl;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatRoutesBl
    {

        public IEnumerable<object> GetRoutesByBitacora(long bitacoraId)
        {
            return _ctx.sp_get_routes_by_bitacora(bitacoraId).Select(r => new
            {
                r.name,
                data = GooglePoints.GeographyEncode(r.data),
                r.color,
                r.type
            }).ToArray();
        }

        public IEnumerable<RouteEnt> Search(int applicationId, string search)
        {
            return _ctx.ivr_sp_search_routes(applicationId, search).Select(x => new RouteEnt
            {
                Id = x.Id,
                Name = x.Name
            }).ToArray();
        }

        /// <summary>
        /// Create Item
        /// </summary>
        /// <param name="entity">Item</param>
        /// <param name="applicationId">Application Id</param>
        /// <param name="routeId">Return Route Identifier Created</param>
        /// <returns>TRUE when operation is successful</returns>
        public bool Create(RouteEnt entity, int applicationId, ref int routeId)
        {
            entity.Points = GooglePoints.Decode(entity.PointsEncoded);
            routeId = _ctx.sp_route_create(entity.Name, entity.Description, entity.Radio, entity.Relevance.Id,
                entity.Distance, entity.Time, entity.OriginAddress, entity.DestinyAddress,
                string.Join(",", entity.Waypoints.Select(x => x.lng + " " + x.lat).ToList()),
                string.Join(",", entity.Points.Select(x => x.lng + " " + x.lat).ToList()), _userName, applicationId);
            return routeId > 0;
        }

        /// <summary>
        /// Update Bitacora
        /// </summary>
        /// <param name="route">Route Data</param>
        /// <param name="applicationId">Application Id</param>
        /// <returns>TRUE when operation is successful</returns>
        public bool Update(RouteEnt route, int applicationId)
        {
            route.Points = GooglePoints.Decode(route.PointsEncoded);
            var result = _ctx.sp_route_update(route.Id, route.Name, route.Description, route.Radio, route.Relevance.Id,
                route.Distance, route.Time, route.OriginAddress, route.DestinyAddress,
                string.Join(",", route.Waypoints.Select(x => x.lng + " " + x.lat).ToList()),
                string.Join(",", route.Points.Select(x => x.lng + " " + x.lat).ToList()), _userName);
            return result > 0;
        }

        /// <summary>
        /// Detele Item
        /// </summary>
        /// <param name="logic">Especify if the registry is eliminated logic o physical</param>
        /// <param name="route">Route Identifier</param>
        /// <param name="userName">User name that modifies the Registry</param>
        /// <returns></returns>
        public bool Delete(bool logic, int route, string userName)
        {
            return _ctx.sp_route_delete(route, logic, userName) > 0;
        }

    }
}
