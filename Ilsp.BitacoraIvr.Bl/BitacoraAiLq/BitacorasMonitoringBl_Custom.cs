﻿using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.Tl;
using Ilsp.Common.Geocoding.BusinessEntities;
using Ilsp.Common.MessageContracts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public class BitacorasMonitoringBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public BitacorasMonitoringBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public IEnumerable<BitacoraMessage> GetCustom(string monitoringUser, string userRole, bool canSeeReactionItems, long? bitacoraId)
        {
            var results = new List<BitacoraMessage>();
            if (bitacoraId == null)
            {
                var items = _ctx.sp_get_bitacoras_for_monitoring_all(monitoringUser, userRole, canSeeReactionItems, bitacoraId).ToList();
                items.ForEach(item =>
                {
                    results.Add(GetBitacoraMessageAll(item));
                });
                //Parallel.ForEach(items, item =>
                //{
                //    try
                //    {
                //        results.Add(GetBitacoraMessageAll(item));
                //    }
                //    catch (Exception ex)
                //    {
                //        throw ex;
                //    }
                //});
            }
            else
            {
                var items = _ctx.sp_get_bitacoras_for_monitoring(monitoringUser, userRole, canSeeReactionItems, bitacoraId).ToList();
                items.ForEach(item =>
                {
                    results.Add(GetBitacoraMessage(item));
                });
                //Parallel.ForEach(items, item =>
                //{
                //    results.Add(GetBitacoraMessage(item));
                //});
            }

            return results;
        }

        public IEnumerable<BitacoraMessage> GetAssociatedBitacoras(decimal? bitacoraId)
        {
            var result = new List<BitacoraMessage>();
            var bitacoraList = _ctx.fnGetAssociatedBitacorasForMonitoring(bitacoraId);
            bitacoraList.ToList().ForEach(b =>
                {
                    var bitDb = _ctx.sp_get_bitacoras_for_monitoring(string.Empty, string.Empty, false, b.BitacoraId)
                        .FirstOrDefault();
                    if (bitDb != null)
                    {
                        result.Add(GetBitacoraMessage(bitDb));
                    }
                });

            return result;
        }

        public IEnumerable<object> Massive(string monitoringUser, string userRole, bool canSeeReactionItems)
        {
            return _ctx.sp_get_bitacoras_for_massive_monitoring(monitoringUser, userRole, canSeeReactionItems)
                .Select(b =>
                {
                    var bitacora = new
                    {
                        b.bitacora,
                        alerts = new List<string>(),
                        b.customer,
                        monitoringType = b.monitoringType ?? 0,
                        b.priority,
                        b.transportLine,
                        b.plates,
                        b.economic,
                        b.phone,
                        status = b.status.ToString(),
                        b.statusDetail,
                        destinations = new List<string>(),
                        b.appoinmentDate,
                        b.latitude,
                        b.longitude
                    };
                    if (!string.IsNullOrEmpty(b.alerts))
                    {
                        bitacora.alerts.AddRange(b.alerts.Trim().Split(',').ToList());
                    }

                    if (!string.IsNullOrEmpty(b.destinations))
                    {
                        bitacora.destinations.AddRange(b.destinations.Split(',').ToList());
                    }

                    return bitacora;
                }).ToArray();
        }

        public sp_get_last_location_of_bitacora_Result LastLocation(long bitacoraId)
        {
            return _ctx.sp_get_last_location_of_bitacora(bitacoraId).Select(x => x.Cast<sp_get_last_location_of_bitacora_Result>()).FirstOrDefault();
        }

        public IEnumerable<IGrouping<string, sp_get_last_locations_of_bitacora_Result>> LastLocations(long? bitacoraId)
        {
            return _ctx.sp_get_last_locations_of_bitacora(bitacoraId)
                .Select(x => x.Cast<sp_get_last_locations_of_bitacora_Result>()).GroupBy(ll => ll.GpsId).ToArray();
        }

        public IEnumerable<sp_get_last_location_of_bitacoras_gps_Result> LastLocationBitacoraGps(long? bitacoraId)
        {

            return _ctx.sp_get_last_location_of_bitacoras_gps(bitacoraId).Select(x => new sp_get_last_location_of_bitacoras_gps_Result
            {
                alias = x.alias,
                battery = x.battery.HasValue? Convert.ToDecimal(x.battery.Value) : 0,
                colony = x.colony,
                createDate = x.createDate,
                degrees = x.degrees,
                latitude = x.latitude,
                longitude = x.longitude,
                district = x.district,
                gpsId = x.gpsId,
                municipality = x.municipality,
                postcode = x.postcode,
                speed = x.speed,
                street = x.street,
                type = x.type
            }).ToArray();
        }

        public IEnumerable<sp_get_last_locations_of_bitacora_report_Result> LastLocationBitacoraReport(long? bitacoraId)
        {
            return _ctx.sp_get_last_locations_of_bitacora_report(bitacoraId).Select(x => x.Cast<sp_get_last_locations_of_bitacora_report_Result>()).ToArray();
        }

        public sp_create_bitacora_monitoring_Result CreateMonitoring(MonitoringModel model, double? latitude, double? longitude, Address address, XElement opKeyAlerts = null)
        {
            var result = _ctx.sp_create_bitacora_monitoring(
                decimal.Parse(model.BitacoraId), model.Status.Id, model.MonitoringFrequency, model.Situation?.Id, _userName, latitude, longitude, address.Street,
                address.District, address.City, address.State, address.Country, address.PostalCode, model.Observations, opKeyAlerts?.ToString()).FirstOrDefault();
            return result.Cast<sp_create_bitacora_monitoring_Result>();
        }

        private BitacoraMessage GetBitacoraMessage(sp_get_bitacoras_for_monitoringResult b)
        {
            var bitacora = new BitacoraMessage
            {
                id = b.id.ToString(),
                bitacora = b.bitacora,
                alerts = new List<Alert>(),
                customer = b.customer,
                monitoringFrequency = b.monitoringFrequency ?? 10,
                monitoringType = b.monitoringType.Value,
                priority = b.priority,
                transportLine = b.transportLine,
                plates = b.plates,
                economic = b.economic,
                phone = b.phone,
                status = b.status,
                statusDetail = b.statusDetail.Value,
                reactionStatus = b.reactionStatus.Value,
                isMirrorAccount = b.isMirrorAccount.Value,
                nextMonitoring = b.nextMonitoring ?? 10,
                createDate = b.createDate.Value,
                gpsList = new List<Gps>(),
                destinations = new List<string>(),
                appointmentDate = b.appointmentDate,
                toUsers = new List<string>(),
                news = b.news,
                theftPercent = (b.theftPercent ?? 0) / 100,
                monitoringTypeDescription = b.monitoringTypeDescription,
                custodianFolio = b.custodyFolio,
                custodianNames = b.custodies,
                monitoringComments = GetStringSection(b.monitoringInfo, '~', 4),
                monitoringLocation = GetStringSection(b.monitoringInfo, '~', 3),
                monitoringSituation = GetStringSection(b.monitoringInfo, '~', 1),
                monitoringStatus = GetStringSection(b.monitoringInfo, '~', 0),
                monitoringUser = GetStringSection(b.monitoringInfo, '~', 2),
                convoy = b.convoy,
                operators = b.operators,
                origin = b.origin,
                hideNextMonitoring = b.hideNextMonitoring.Value,
                monitoringFrequencyId = b.monitoringFrequencyId ?? 3,
            };
            if (!string.IsNullOrEmpty(b.location))
            {
                var l = b.location.Split(',');
                if (l.Length == 4)
                {
                    if (double.TryParse(l[0], out double lat) && double.TryParse(l[1], out double lng))
                    {
                        bitacora.location = new Location
                        {
                            Latitude = lat,
                            Longitude = lng
                        };
                    }

                    if (double.TryParse(l[2], out double speed))
                    {
                        bitacora.speed = speed;
                    }

                    if (double.TryParse(l[3], out double battery))
                    {
                        bitacora.battery = battery;
                    }
                }
            }

            if (!string.IsNullOrEmpty(b.monitoringInfo))
            {
                var alertsInfo = b.monitoringInfo.Split('~');
                if (DateTime.TryParse(alertsInfo[5], out var dateTime))
                {
                    bitacora.monitoringDatetime = dateTime;
                }
                if (alertsInfo.Length == 7)
                {
                    var opAlerts = alertsInfo[6].Split(',');
                    if (opAlerts.Length > 0)
                    {
                        bitacora.alertOpKeys = opAlerts.Select(oa =>
                        {
                            var splitData = oa.Split('|');
                            if (splitData.Length == 2)
                            {
                                return new AlertOperativeKey
                                {
                                    alert = splitData[0],
                                    operativeKey = splitData[1]
                                };
                            }

                            return null;
                        }).Where(op => op != null).ToList();
                    }
                }
            }

            if (!string.IsNullOrEmpty(b.alerts))
            {
                bitacora.alerts.AddRange(b.alerts.Trim().Split(',').Select(a =>
                {
                    var alert = a.Trim().Split(';');
                    if (!double.TryParse(alert[2], out var latitude))
                    {
                        latitude = 0;
                    }
                    if (!double.TryParse(alert[3], out var longitude))
                    {
                        longitude = 0;
                    }

                    if (!double.TryParse(alert[7], out var speed))
                    {
                        speed = 0;
                    }

                    return new Alert
                    {
                        id = int.Parse(alert[0]),
                        name = alert[1],
                        latitude = latitude,
                        longitude = longitude,
                        info = $"<b>{Resources.Resource.Alert}:</b> {alert[1]} <br>" +
                               $"<b>{Resources.Resource.Speed}:</b> {Math.Round(speed, 0)} Km/h <br>" +
                        $"<b>{Resources.Resource.GpsAlias}:</b> {alert[4]} <br>" +
                        $"<b>{Resources.Resource.Datetime}:</b> {alert[6]} <br>",
                        gps = alert[4],
                        priority = short.Parse(alert[5]),
                        datetime = alert[6],
                        speed = speed
                    };
                }).ToList());
            }
            if (!string.IsNullOrEmpty(b.gpsList))
            {
                bitacora.gpsList.AddRange(b.gpsList.Split(',').Select(g =>
                {
                    var gps = g.Trim().Split(';');
                    return new Gps
                    {
                        Id = gps[0],
                        Alias = gps[1],
                        Type = gps[2]
                    };
                }).ToList());
            }
            if (!string.IsNullOrEmpty(b.destinations))
            {
                bitacora.destinations.AddRange(b.destinations.Split(',').ToList());
            }
            return bitacora;
        }

        private BitacoraMessage GetBitacoraMessageAll(sp_get_bitacoras_for_monitoring_allResult b)
        {
            try
            {
                var bitacora = new BitacoraMessage
                {
                    id = b.id.ToString(),
                    bitacora = b.bitacora,
                    alerts = new List<Alert>(),
                    customer = b.customer,
                    monitoringFrequency = b.monitoringFrequency ?? 10,
                    monitoringType = b.monitoringType.Value,
                    priority = b.priority,
                    transportLine = b.transportLine,
                    plates = b.plates,
                    economic = b.economic,
                    phone = b.phone,
                    status = b.status.ToString(),
                    statusDetail = b.statusDetail,
                    reactionStatus = b.reactionStatus,
                    isMirrorAccount = b.isMirrorAccount,
                    nextMonitoring = b.nextMonitoring ?? 10,
                    createDate = b.createDate.Value,
                    gpsList = new List<Gps>(),
                    destinations = new List<string>(),
                    appointmentDate = null,
                    toUsers = new List<string>(),
                    news = b.news,
                    theftPercent = b.theftPercent / 100,
                    monitoringTypeDescription = b.monitoringTypeDescription,
                    custodianFolio = b.custodyFolio,
                    custodianNames = b.custodies,
                    //monitoringComments = GetStringSection(b.monitoringInfo, '~', 4),
                    //monitoringLocation = GetStringSection(b.monitoringInfo, '~', 3),
                    //monitoringSituation = GetStringSection(b.monitoringInfo, '~', 1),
                    //monitoringStatus = GetStringSection(b.monitoringInfo, '~', 0),
                    //monitoringUser = GetStringSection(b.monitoringInfo, '~', 2),
                    convoy = b.convoy,
                    operators = b.operators,
                    origin = b.origin,
                    hideNextMonitoring = b.hideNextMonitoring.Value,
                    monitoringFrequencyId = b.monitoringFrequencyId ?? 3,
                };
                if (!string.IsNullOrEmpty(b.location))
                {
                    var l = b.location.Split(',');
                    if (l.Length == 4)
                    {
                        if (double.TryParse(l[0], out double lat) && double.TryParse(l[1], out double lng))
                        {
                            bitacora.location = new Location
                            {
                                Latitude = lat,
                                Longitude = lng
                            };
                        }

                        if (double.TryParse(l[2], out double speed))
                        {
                            bitacora.speed = speed;
                        }

                        if (double.TryParse(l[3], out double battery))
                        {
                            bitacora.battery = battery;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(b.monitoringInfo))
                {
                    var alertsInfo = b.monitoringInfo.Split('~');
                    if (DateTime.TryParse(alertsInfo[5], out var dateTime))
                    {
                        bitacora.monitoringDatetime = dateTime;
                    }
                    if (alertsInfo.Length == 7)
                    {
                        var opAlerts = alertsInfo[6].Split(',');
                        if (opAlerts.Length > 0)
                        {
                            bitacora.alertOpKeys = opAlerts.Select(oa =>
                            {
                                var splitData = oa.Split('|');
                                if (splitData.Length == 2)
                                {
                                    return new AlertOperativeKey
                                    {
                                        alert = splitData[0],
                                        operativeKey = splitData[1]
                                    };
                                }

                                return null;
                            }).Where(op => op != null).ToList();
                        }
                    }
                }

                if (!string.IsNullOrEmpty(b.alerts))
                {
                    bitacora.alerts.AddRange(b.alerts.Trim().Split(',').Select(a =>
                    {
                        var alert = a.Trim().Split(';');
                        if (!double.TryParse(alert[2], out var latitude))
                        {
                            latitude = 0;
                        }
                        if (!double.TryParse(alert[3], out var longitude))
                        {
                            longitude = 0;
                        }

                        if (!double.TryParse(alert[7], out var speed))
                        {
                            speed = 0;
                        }

                        return new Alert
                        {
                            id = int.Parse(alert[0]),
                            name = alert[1],
                            latitude = latitude,
                            longitude = longitude,
                            info = $"<b>{Resources.Resource.Alert}:</b> {alert[1]} <br>" +
                                   $"<b>{Resources.Resource.Speed}:</b> {Math.Round(speed, 0)} Km/h <br>" +
                            $"<b>{Resources.Resource.GpsAlias}:</b> {alert[4]} <br>" +
                            $"<b>{Resources.Resource.Datetime}:</b> {alert[6]} <br>",
                            gps = alert[4],
                            priority = short.Parse(alert[5]),
                            datetime = alert[6],
                            speed = speed
                        };
                    }).ToList());
                }
                if (!string.IsNullOrEmpty(b.gpsList))
                {
                    bitacora.gpsList.AddRange(b.gpsList.Split(',').Select(g =>
                    {
                        var gps = g.Trim().Split(';');
                        return new Gps
                        {
                            Id = gps[0],
                            Alias = gps[1],
                            Type = gps[2]
                        };
                    }).ToList());
                }
                if (!string.IsNullOrEmpty(b.destinations))
                {
                    bitacora.destinations.AddRange(b.destinations.Split(',').ToList());
                }
                return bitacora;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        private string GetStringSection(string source, char separator, int start)
        {
            var result = string.Empty;
            if (!string.IsNullOrEmpty(source))
            {
                var split = source.Split(separator);
                if (split.Length > start)
                {
                    result = split[start];
                }
            }
            return result;
        }

        public IEnumerable<BitacoraConvoyEnt> GetBitacorasOnConvoy(long bitacoraId)
        {
            return _ctx.sp_get_bitacoras_on_convoy_by_bitacoraid(bitacoraId).Select(item => new BitacoraConvoyEnt
            {
                Id = item.Id.ToString(CultureInfo.CurrentCulture),
                Alias = item.Alias,
                ConvoyId = item.ConvoyId,
                ConvoyName = item.ConvoyName,
                ConvoyColor = item.ConvoyColor,
                Latitude = item.Latitude,
                Longitude = item.Longitude,
                CreateDate = item.CreateDate
            }).ToArray();
        }

        public bool SituationRequiredCall(string situation)
        {
            return _ctx.fnCallSituationRequireCall(situation) ?? true;
        }

        public spGetAutomatedScriptResult SituationAutomatedCall(string bitacoraId, string situation)
        {
            if (decimal.TryParse(bitacoraId, out var id))
            {
                return _ctx.spGetAutomatedScript(situation, id)
                    .FirstOrDefault();
            }

            return null;
        }

        public IEnumerable<string> GetBitacorasReadyForStartingRoad(string user, string role, decimal? bitacoraId)
        {
            return _ctx.sp_get_bitacoras_ready_for_starting_road(user, role, bitacoraId)
                .Select(b => b.BitacoraId?.ToString())
                .ToArray();
        }

        public GetAlexaNotificationByProcessResult GetAlexaNotificationByProcess(string bitacoraId,
            int processId)
        {
            if (decimal.TryParse(bitacoraId, out var id))
            {
                return _ctx.GetAlexaNotificationByProcess(id, processId)
                    .FirstOrDefault();
            }

            return null;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
