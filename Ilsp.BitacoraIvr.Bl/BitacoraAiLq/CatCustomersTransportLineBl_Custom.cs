﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatCustomersTransportLineBl
    {

        public List<TransportLineEnt> GetTransportLinesByCustomer(int customerId)
        {
            return _ctx.sp_get_transport_lines_with_customer(customerId).Select(g => new TransportLineEnt
            {
                Id = g.Id,
                Name = g.NAME,
                Selected = g.Selected ?? false
            }).ToList();
        }

    }
}
