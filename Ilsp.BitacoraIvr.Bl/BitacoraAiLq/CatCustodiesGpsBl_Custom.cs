﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatCustodiesGpsBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public CatCustodiesGpsBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public IEnumerable<CustodyUnitEnt> Search(string search)
        {
            return _ctx.ivr_sp_search_custodies_gps(search).Select(x => new CustodyUnitEnt
            {
                AxId = x.AxId,
                Id = x.Id,
                Name = x.Name
            }).ToArray();
        }

        public CustodyUnitEnt First(string unitGpsId)
        {
            var response = _ctx.ivr_sp_one_custodies_gps(unitGpsId).Select(x => new CustodyUnitEnt
            {
                AxId = x.AxId,
                Id = x.Id,
                Name = x.Name
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
