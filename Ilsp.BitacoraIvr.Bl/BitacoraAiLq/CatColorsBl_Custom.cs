﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatColorsBl
    {

        public IEnumerable<ColorEnt> Search(int customerId, string search)
        {
            return _ctx.ivr_sp_search_colors(customerId, search).Select(x => new ColorEnt
            {
                Id = x.Id,
                Name = x.Name,
                IsValid = x.IsValid
            }).ToArray();
        }

        public IEnumerable<ColorEnt> SearchOnly(string search)
        {
            return _ctx.ivr_sp_search_cat_colors(search).Select(x => new ColorEnt
            {
                Id = x.Id,
                Name = x.Name,
                IsValid = x.IsValid
            }).ToArray();
        }

        public ColorEnt First(string colorId)
        {
            var response = _ctx.ivr_sp_one_colors(colorId).Select(x => new ColorEnt
            {
                Id = x.Id,
                Name = x.Name,
                IsValid = x.IsValid
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

    }
}