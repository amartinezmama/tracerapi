﻿using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CustomerBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public CustomerBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public IEnumerable<CustomerEnt> GetCustomers()
        {
            return _ctx.sp_get_customers().Select(x => new CustomerEnt
            {
                Name = x.Name,
                Id = x.Id,
            }).ToArray();
        }


        public SaveUpdateModel saveOUpdateBl()
        {

            return null;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
