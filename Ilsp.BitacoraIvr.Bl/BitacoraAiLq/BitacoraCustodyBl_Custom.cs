﻿using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public class BitacoraCustodyBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly BitacoraAiDataContext _ctx;

        public BitacoraCustodyBl(string connection, string userName, BitacoraAiDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new BitacoraAiDataContext(connection);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Create Custody of Bitacora
        /// </summary>
        /// <param name="custody">Custody</param>
        /// <param name="bitacoraId">Bitacora Identifier</param>
        /// <returns></returns>
        public bool Create(CustodyEnt custody, long bitacoraId)
        {
            int? custodyId = 0;
            _ctx.Sp_bitacora_custody_create(bitacoraId, custody.CustodyType.Id, ref custodyId);
            if (custodyId == 0)
            {
                return false;
            }

            custody.Id = custodyId ?? 0;

            if (custody.Cabins != null)
            {
                using (var bl = new BitacoraCustodyCabinBl(_connection, _userName, _ctx))
                {
                    //add Custodian of cabin to Custody
                    if (!bl.Create(custody.Cabins, custodyId ?? 0))
                    {
                        return false;
                    }
                }
            }

            if (custody.Vehicles != null)
            {
                using (var bl = new BitacoraCustodyVehicleBl(_connection, _userName, _ctx))
                {
                    var isCustodyAbandon = custody.Vehicles.Any(item => item.IsCustodyAbandon.Equals(true));
                    //add Vehicles to Custody
                    if (!bl.Create(custody.Vehicles, custodyId ?? 0, isCustodyAbandon))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Update Custody of Bitacora
        /// </summary>
        /// <param name="custody">Custody</param>
        /// <param name="bitacoraId">Bitacora Identifier</param>
        /// <param name="isCustodyAbandon"></param>
        /// <returns></returns>
        public bool Update(CustodyEnt custody, long bitacoraId, bool? isCustodyAbandon)
        {
            if (_ctx.Sp_bitacora_custody_update(bitacoraId, custody.Id, custody.CustodyType.Id) == 0)
            {
                return false;
            }

            //Cabins
            var listCabinsExist = _ctx.sp_bitacora_custody_cabin_get(custody.Id).ToArray();
            using (var bl = new BitacoraCustodyCabinBl(_connection, _userName, _ctx))
            {
                //delete Custodian of cabin to Custody
                if (!bl.Delete(listCabinsExist.Select(c => new CustodyCabinEnt
                {
                    Id = c.cabinheaderid
                }), custody.Id ?? 0))
                {
                    return false;
                }
                //add Custodian of cabin to Custody
                if (!bl.Create(custody.Cabins, custody.Id ?? 0))
                {
                    return false;
                }
            }

            //Vehicles
            var listCustodyVehiclesExist = _ctx.sp_bitacora_custody_vehicle_get(custody.Id).ToArray();
            using (var bl = new BitacoraCustodyVehicleBl(_connection, _userName, _ctx))
            {
                //delete Vehicles to Custody
                if (!bl.Delete(listCustodyVehiclesExist.Select(v => new CustodyVehicleEnt
                {
                    Id = v.vehicleheaderid
                }), custody.Id ?? 0))
                {
                    return false;
                }
                //add Vehicles to Custody
                if (
                    !bl.Create(custody.Vehicles, custody.Id ?? 0, isCustodyAbandon))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Delete Custody of Bitacora
        /// </summary>
        /// <param name="custodies">Collection of Custody</param>
        /// <param name="bitacoraId">Bitacora Identifier</param>
        /// <returns></returns>
        public bool Delete(IEnumerable<CustodyEnt> custodies, long bitacoraId)
        {
            return custodies.All(custody => _ctx.Sp_bitacora_custody_delete(bitacoraId, custody.Id, _userName) != 0);
        }

        public CustodyEnt Get(long bitacoraId)
        {
            var result = _ctx.sp_bitacora_custody_get(bitacoraId).ToList();
            if (result.Any())
            {
                var item = result.First();
                using (var bl = new CatCustodianTypesBl(_connection, _userName, _ctx))
                {
                    using (var custodianBl = new CatCustodiansBl(_connection, _userName, _ctx))
                    {
                        return new CustodyEnt
                        {
                            Id = item.custodyheaderid,
                            CustodyType = bl.First(item.custodiantypeid),
                            Cabins = _ctx.sp_bitacora_custody_cabin_get(item.custodyheaderid).Select(c =>
                                new CustodyCabinEnt
                                {
                                    Id = c.cabinheaderid,
                                    Custodian = custodianBl.First(c.axcustodianid),
                                    ShortNumber = c.shortnumber,
                                    LargeNumber = c.largenumber,
                                    IsCustodyAbandon = c.isCustodyAbandon
                                }).ToList(),
                            Vehicles = _ctx.sp_bitacora_custody_vehicle_get(item.custodyheaderid).Select(v =>
                                new CustodyVehicleEnt
                                {
                                    Id = v.vehicleheaderid,
                                    Custodian = custodianBl.First(v.axcustodianid),
                                    ShortNumber = v.shortnumber,
                                    LargeNumber = v.largenumber,
                                    Vehicle = _ctx.ivr_sp_one_custodies_gps(v.gpsaxid).Select(g => new CustodyUnitEnt
                                    {
                                        Id = g.Id,
                                        AxId = g.AxId,
                                        Name = g.Name
                                    }).First(),
                                    IsCustodyAbandon = v.isCustodyAbandon
                                }).ToList()
                        };
                    }
                }
            }

            return null;
        }

    }
}
