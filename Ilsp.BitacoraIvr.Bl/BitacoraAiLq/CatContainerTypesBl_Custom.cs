﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatContainerTypesBl
    {

        public IEnumerable<ContainerTypeEnt> Search(string search)
        {
            return _ctx.ivr_sp_search_container_types(search).Select(x => new ContainerTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                IsValid = x.IsValid
            }).ToArray();
        }

        public ContainerTypeEnt First(int containerTypeId)
        {
            var response = _ctx.ivr_sp_one_container_types(containerTypeId).Select(x => new ContainerTypeEnt
            {
                Id = x.Id,
                Name = x.Name,
                IsValid = x.IsValid
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

    }
}
