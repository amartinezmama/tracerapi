﻿using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using System;
using System.Linq;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    partial class BitacorasAxActivationBl
    {

        public KendoResponse GetGridPagedList(KendoRequest model)
        {

            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join("|", model.Sort.Select(x => $"{x.Field},{(x.Dir.ToLower() == "asc" ? "1" : "0")}").ToArray());
            }

            // compose the filter 
            var filter = string.Empty;
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("|", model.Filter.Filters.Select(f => $"{f.Field},{f.Value.ToLower()}").ToArray());
            }

            var response = _ctx.sp_get_bitacoras_folios(filter, order, model.Page, model.Take, _userName).ToList();

            var totalData = 0;
            if (response.Any())
            {
                totalData = response.First().Total ?? 0;
            }

            //var data = response.AsQueryable();
            var data = response.Select(m => new BitacoraFolioGridDto
            {
                Id = m.Id.HasValue ? Convert.ToInt32(m.Id.Value) : 0,
                BitacoraId = m.BitacoraId,
                BitacoraAlias = m.BitacoraAlias,
                BitacoraStatusId = m.BitacoraStatusId,
                BitacoraStatusDescription = m.BitacoraStatusDescription,
                BitacoraMonitoringTypeId = m.BitacoraMonitoringTypeId,
                BitacoraMonitoringTypeDesc = m.BitacoraMonitoringTypeDesc,
                CustomerId = m.CustomerId,
                CustomerName = m.CustomerName,
                AxCustomerId = m.AxCustomerId,
                AxCustomerName = m.AxCustomerName,
                ProjectId = m.ProjectId,
                AgreementId = m.AgreementId,
                Folio = m.Folio,
                ServiceOrder = m.ServiceOrder,
                CreateDate = m.CreateDate,
                UpdateUser = m.UpdateUser,
                UpdateDate = m.UpdateDate,
                DynamicsMessage = m.DynamicsMessage,
                ServiceTypeId = m.ServiceTypeId,
                ServiceTypeDescription = m.ServiceTypeDescription,
                CancellationFolioStatusId = m.CancellationFolioStatusId,
                CancellationFolioStatusDescription = m.CancellationFolioStatusDescription,
                ReasonCancellationId = m.ReasonCancellationId,
                ReasonCancellationDescription = m.ReasonCancellationDescription,
                ReasonCancellationComment = m.ReasonCancellationComment,
                ReasonCancellationDate = m.ReasonCancellationDate,
                IdStay = m.IdStay,
                TimeStay = m.TimeStay,
                PlaceSaty = m.PlaceSaty,
                SentToBilling = m.SentToBilling,
                SentToBillingDate = m.SentToBillingDate,
                EvidenceCount = m.EvidenceCount,
                EvidencePhotoCount = m.EvidencePhotoCount,
                IsFalseMovement = m.IsFalseMovement,
                EndServiceDate = m.EndServiceDate,
                ServiceOrderStatus =  m.ServiceOrderStatus,
                Total = m.Total ?? 0

            }).ToList();

            return new KendoResponse(data.ToArray(), totalData);

        }


        public KendoResponse GetGridCustody(KendoRequest model)
        {

            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join("|", model.Sort.Select(x => $"{x.Field},{(x.Dir.ToLower() == "asc" ? "1" : "0")}").ToArray());
            }

            // compose the filter 
            var filter = string.Empty;
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("|", model.Filter.Filters.Select(f => $"{f.Field},{f.Value.ToLower()}").ToArray());
            }

            var response = _ctx.sp_get_bitacora_folio_by_custody(filter, order, model.Page, model.Take, _userName).ToList();

            var totalData = 0;
            if (response.Any())
            {
                totalData = response.First().Total ?? 0;
            }

            var data = response.Select(m => new BitacoraFolioCustodyGridDto
            {
                Id = m.Id.HasValue ? Convert.ToInt32(m.Id.Value) : 0,
                BitacoraId = m.BitacoraId,
                BitacoraAlias = m.BitacoraAlias,
                BitacoraStatusId = m.BitacoraStatusId,
                BitacoraStatusDescription = m.BitacoraStatusDescription,
                CustomerId = m.CustomerId,
                CustomerName = m.CustomerName,
                Folio = m.Folio,
                ServiceOrder = m.ServiceOrder,
                CreateDate = m.CreateDate,
                ServiceTypeId = m.ServiceTypeId,
                ServiceTypeDescription = m.ServiceTypeDescription,
                Origin = m.Origin,
                MonitoringType =  m.MonitoringType,
                Total = m.Total ?? 0


            }).ToList();
            return new KendoResponse(data.ToArray(), totalData);

        }


        public BitacoraFolioSatusDto GetBitacoraFolioById(string bitacoraId)
        {   
            var response = _ctx.sp_get_bitacoras_folios_by_id(Convert.ToDecimal(bitacoraId)).ToList();
            
            var data = response.Select(m => new BitacoraFolioSatusDto()
            {
                BitacoraId = m.BitacoraId.ToString(),
                BitacoraStatusId = m.BitacoraStatusId.ToString(),
                BitacoraStatusDescription = m.BitacoraStatusDescription,
                CancellationFolioStatusId = m.CancellationFolioStatusId.ToString(),
                SentToBilling = m.SentToBilling.ToString(),
                IsFalseMovement = m.IsFalseMovement.ToString(),
                EvidencePhotoCount = m.EvidencePhotoCount.ToString(),
                EvidenceCount =  m.EvidenceCount.ToString(),
                FolioNumber = m.FolioNumber.ToString(),
                ServiceOrder = m.ServiceOrder.ToString()

            }).FirstOrDefault();

            return data;

        }


    }
}
