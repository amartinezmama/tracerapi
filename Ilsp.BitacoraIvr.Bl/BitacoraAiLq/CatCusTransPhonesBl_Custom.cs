﻿using System.Linq;
using System.Linq.Dynamic;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatCusTransPhonesBl
    {

        /// <summary>
        /// Method to get list of rows in pages
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <returns></returns>
        public KendoResponse GetListPagerCustom(KendoRequest model)
        {
            var customerId = 0;
            if (model.Filter.Filters.Any(x => x.Field.ToLower().Equals("customerid")))
            {
                customerId = int.Parse(model.Filter.Filters.First(x => x.Field.ToLower().Equals("customerid")).Value);
            }
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(",", model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => $"{x.Field} {(x.Dir?.ToLower() == "asc" ? "asc" : "desc")}").ToArray());
            }

            // compose the filter 
            var filter = string.Empty;
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("&&", model.Filter.Filters.Select(f =>
                        $"{f.Field}.ToString().ToLower().Contains(\"{f.Value.ToString().ToLower()}\")").ToArray());
            }

            var response = _ctx.ivr_sp_get_phones_trans_by_customer(customerId).Select(x => new CatCusTransPhonesGrid
            {
                CustomerId = x.CustomerId,
                CustomerName = x.CustomerName.Trim(),
                TransportLineId = x.TransportLineId.Trim(),
                TransportLineName = x.TransportLineName.Trim(),
                Phone = x.Phones,
                Phones = !string.IsNullOrEmpty(x.Phones) ? x.Phones.Split(',') : new string[0]
            }).ToList();
            var items = response.AsQueryable();
            if (string.IsNullOrEmpty(filter))
            {
                filter = "1 == 1";
            }
            if (string.IsNullOrEmpty(order))
            {
                order = "true";
            }
            var data = items.AsQueryable().Where(filter);
            return new KendoResponse(data.OrderBy(string.IsNullOrEmpty(order) ? "true": order)
                .Skip(model.Skip)
                .Take(model.Take == 0 ? data.Count() : model.Take)
                .ToArray(), data.Count());
        }

        public CatCusTransPhonesGrid First(int customerId, string transportLineId)
        {
            var response = _ctx.ivr_sp_one_phones_trans_by_customer(customerId, transportLineId).Select(x => new CatCusTransPhonesGrid
            {
                CustomerId = x.CustomerId,
                CustomerName = x.CustomerName.Trim(),
                TransportLineId = x.TransportLineId.Trim(),
                TransportLineName = x.TransportLineName.Trim(),
                Phone = x.Phones,
                Phones = !string.IsNullOrEmpty(x.Phones) ? x.Phones.Split(',') : new string[0]
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

    }
}
