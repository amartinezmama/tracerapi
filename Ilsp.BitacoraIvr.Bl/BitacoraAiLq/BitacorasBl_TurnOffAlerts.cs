﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.Tl;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class BitacorasBl
    {

        /// <summary>
        /// Method to get list of rows in pages
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <returns></returns>
        public KendoResponse GetGridBitacorasTurnOffAlerts(KendoRequest model)
        {
            var noFilters = new[] { "dateInit", "dateEnd" };
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join("|", model.Sort.Select(x => $"{x.Field},{(x.Dir.ToLower() == "asc" ? "1" : "0")}").ToArray());
            }

            // compose the filter 
            var filter = string.Empty;
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("|",
                    model.Filter.Filters.Where(f => !noFilters.Any(nf => nf.Equals(f.Field)))
                        .Select(f => $"{f.Field},{f.Value.ToLower()}").ToArray());
            }

            var dateInit = DateTime.Now;
            var dateEnd = DateTime.Now;
            if (model.Filter != null && model.Filter.Filters.Any(f => f.Field.Equals("dateInit")))
            {
                var item = model.Filter.Filters.First(f => f.Field.Equals("dateInit"));
                dateInit = OptionsDateTime.ToDateTimeYyyyMMdd(item.Value);
            }

            if (model.Filter != null && model.Filter.Filters.Any(f => f.Field.Equals("dateEnd")))
            {
                var item = model.Filter.Filters.First(f => f.Field.Equals("dateEnd"));
                dateEnd = OptionsDateTime.ToDateTimeYyyyMMdd(item.Value);
                dateEnd = dateEnd.AddDays(1);
            }

            var response = _ctx.sp_get_grid_bitacoras_turn_off_alerts(filter, order, model.Page, model.Take, dateInit, dateEnd).ToList();
            var totalData = 0;
            if (response.Any())
            {
                totalData = response.First().Total ?? 0;
            }

            var items = response.Select(item => new BitacoraTurnOffAlertEnt
            {
                Id = item.Id.ToString(),
                BitacoraId = item.bitacoraId.ToString(),
                BitacoraAlias = item.bitacoraAlias,
                GpsId = item.gpsId,
                GpsAlias = item.gpsAlias,
                AlertId = item.alertId.ToString(),
                AlertName = item.alertName,
                MinutesDisabled = item.minutesDisabled.ToString(),
                Observations = item.observations,
                CreateUser = item.createUser,
                CreateDate = item.createDate?.ToString("yyyy/MM/dd HH:mm:ss"),
                CancelUser = item.cancelUser,
                CancelDate = item.cancelDate?.ToString("yyyy/MM/dd HH:mm:ss"),
                Status = item.status,
                StatusDescription = item.statusDescription
            }).ToList();
            var data = items.AsQueryable();
            return new KendoResponse(data.ToArray(), totalData);
        }

        public IEnumerable<sp_get_bitacoras_alerts_status_for_mqResult> GetBitacorasAlertsForMq(string bitacoraId)
        {
            return _ctx.sp_get_bitacoras_alerts_status_for_mq(bitacoraId).ToList();
        }

        public IEnumerable<BitacoraEnt> GetBitacoras(string search)
        {
            return _ctx.sp_get_bitacoras_for_alerts_status(search).Select(item => new BitacoraEnt
            {
                Id = item.id_bitacora,
                Alias = item.alias
            }).ToList();
        }

        public IEnumerable<BitacoraGps> GetGpsByBitacoras(string bitacoraId)
        {
            return _ctx.sp_get_gps_by_bitacoras_alerts_status(bitacoraId).Select(item => new BitacoraGps
            {
                BitacoraId = item.bitacora_id != null ? long.Parse(item.bitacora_id.ToString()) : 0,
                GpsId = item.gps_id,
                Alias = item.alias,
                Type = item.type
            }).ToList();
        }

        public int CreateBitacorasAlertsForMq(string bitacoraId, string gpsId, string alertName, int minutesDisabled, string observations)
        {
            return _ctx.sp_create_bitacora_alert_status(bitacoraId, gpsId, alertName, minutesDisabled, observations, _userName);
        }

    }
}
