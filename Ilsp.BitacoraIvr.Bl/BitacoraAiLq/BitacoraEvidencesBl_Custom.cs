﻿using Ilsp.BitacoraIvr.Dl;
using System.Collections.Generic;
using System.Linq;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    partial class BitacoraEvidencesBl
    {

        public List<sp_get_bitacora_evidences_customerResult> GetEvidencesFields(string bitacoraId, int customerId)
        {
            var response = this._ctx.sp_get_bitacora_evidences_customer(bitacoraId, customerId);

            var data = response.AsQueryable();

            return data.ToList();

        }


        public int ValidEvidences(decimal bitacoraId)
        {
            var response = this._ctx.sp_validation_bitacora_evidences(bitacoraId);

            var result = response.Select(m => m.ValidEvidences).FirstOrDefault();

            return result;
        }

    }
}
