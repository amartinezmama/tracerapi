﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatSecurityKitsBl
    {

        public IEnumerable<SecurityKitEnt> Search(int customerId, string search)
        {
            return _ctx.ivr_sp_search_security_kits(customerId, search).Select(x => new SecurityKitEnt
            {
                Id = x.Id,
                Name = x.Name
            }).ToArray();
        }

        public SecurityKitEnt First(int customerId, int securityKitId)
        {
            var response = _ctx.ivr_sp_one_security_kits(customerId, securityKitId).Select(x => new SecurityKitEnt
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            if (response.Any())
            {
                return response.First();
            }

            return null;
        }

        /// <summary>
        /// Create Item
        /// </summary>
        /// <param name="entity">Data</param>
        /// <returns>TRUE when operation is successful</returns>
        public bool Create(SecurityKitEnt entity)
        {
            var id = _ctx.sp_technologicalkits_create(entity.Name, entity.Description, null, null, null, _userName);
            if (id > 0)
            {
                entity.Id = id;
                if (entity.Customers
                    .Select(item => _ctx.sp_technologicalkits_customers_create(entity.Id, item.Id, _userName))
                    .Any(idCustomer => idCustomer == 0))
                {
                    return false;
                }

                if (entity.Geofences.Select(item => _ctx.sp_technologicalkits_geofence_create(entity.Id,
                    item.FenceType.Id, item.Geofence.Id, item.NotificationInGeofence, item.NotificationOutGeofence,
                    item.NotificationTimeWait, _userName)).Any(idGeofence => idGeofence == 0))
                {
                    return false;
                }

                if (entity.ControlPoints.Select(item => _ctx.sp_technologicalkits_control_points_create(entity.Id,
                    item.GeofenceInit.Id, item.GeofenceEnd.Id, item.TravelTime, _userName)).Any(idCp => idCp == 0))
                {
                    return false;
                }

                if (entity.Routes.Select(item => _ctx.sp_technologicalkits_routes_create(entity.Id, item.Id ?? 0)).Any(idCp => idCp == 0))
                {
                    return false;
                }
            }
            return id > 0;
        }

        /// <summary>
        /// Update Item
        /// </summary>
        /// <param name="entity">Data</param>
        /// <returns>TRUE when operation is successful</returns>
        public bool Update(SecurityKitEnt entity)
        {
            var id = _ctx.sp_technologicalkits_update(entity.Id, entity.Name, entity.Description, null, null, null, _userName);
            if (id > 0)
            {
                _ctx.sp_technologicalkits_customers_delete(entity.Id, 0, true);
                if (entity.Customers
                    .Select(item => _ctx.sp_technologicalkits_customers_create(entity.Id, item.Id, _userName))
                    .Any(idCustomer => idCustomer == 0))
                {
                    return false;
                }

                _ctx.sp_technologicalkits_geofence_delete(entity.Id, 0);
                if (entity.Geofences.Select(item => _ctx.sp_technologicalkits_geofence_create(entity.Id,
                    item.FenceType.Id, item.Geofence.Id, item.NotificationInGeofence, item.NotificationOutGeofence,
                    item.NotificationTimeWait, _userName)).Any(idGeofence => idGeofence == 0))
                {
                    return false;
                }

                _ctx.sp_technologicalkits_control_points_delete(0, entity.Id);
                if (entity.ControlPoints.Select(item => _ctx.sp_technologicalkits_control_points_create(entity.Id,
                    item.GeofenceInit.Id, item.GeofenceEnd.Id, item.TravelTime, _userName)).Any(idCp => idCp == 0))
                {
                    return false;
                }

                _ctx.sp_technologicalkits_routes_delete(entity.Id, 0, true);
                if (entity.Routes.Select(item => _ctx.sp_technologicalkits_routes_create(entity.Id, item.Id ?? 0)).Any(idCp => idCp == 0))
                {
                    return false;
                }
            }
            return id > 0;
        }

        /// <summary>
        /// Delete Item
        /// </summary>
        /// <param name="id">technologicalKit Identifier</param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            return _ctx.sp_technologicalkits_delete(id, _userName) > 0;
        }

        /// <summary>
        /// Clone Item
        /// </summary>
        /// <param name="id">Data</param>
        /// <returns>TRUE when operation is successful</returns>
        public bool CloneSecurityKit(int id)
        {
            return _ctx.sp_technologicalkits_clone(id, _userName) > 0;
        }

    }
}
