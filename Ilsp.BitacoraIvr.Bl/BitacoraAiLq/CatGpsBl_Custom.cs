﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiLq
{
    public partial class CatGpsBl
    {

        public IEnumerable<GpsEnt> Search(string gpsType, int customerId, string search)
        {
            return _ctx.ivr_sp_search_gps(gpsType, customerId, search).Select(x => new GpsEnt
            {
                Id = x.Id,
                Name = x.Name,
                Type = x.Type,
                TimeLastPosition = x.TimeLastPosition.ToString()
            }).ToArray();
        }

        public void CreateGpsWsBitacora(long bitacoraId)
        {
            _ctx.sp_create_gps_to_ws_by_bitacora(bitacoraId);
        }

        /// <summary>
        /// Get all Gps assigned to a bitacora
        /// </summary>
        /// <param name="bitacoraId">BitacoraId</param>
        /// <returns></returns>
        public List<BitacoraGps> GetAllInBitacora(long bitacoraId)
        {
            return _ctx.sp_gps_get_all_in_bitacora_for_pretobit(bitacoraId).Select(x => new BitacoraGps
            {
                BitacoraId = long.Parse(x.bitacoraid),
                Alias = x.Alias,
                GpsId = x.gpsid,
                From = x.from,
                Assigned = x.Assigned > 0
            }).ToList();
        }

        /// <summary>
        /// Method to get all of rows
        /// </summary>
        /// <param name="gpsId">Gps</param>
        /// <param name="dateInit">Start Date</param>
        /// <param name="dateEnd">End Date</param>
        /// <param name="orderBy">Order By 0 = DESC, 1 = ASC</param>
        /// <returns></returns>
        public List<GpsHistoric> GetListSameMonth(string gpsId, DateTime dateInit, DateTime dateEnd, bool orderBy)
        {
            var res = _ctx.sp_dasvo_history_events_get_same_month(_userName, gpsId, dateInit, dateEnd, orderBy)
                .ToList();
            return mapHistorics(res);
        }

        private List<GpsHistoric> mapHistorics<TE>(List<TE> res)
        {
            return res.Select(x => new GpsHistoric
            {
                GpsId = GetPropValue(x, "GpsId"),
                GpsAlias = GetPropValue(x, "GpsAlias"),
                IlspEventId = GetPropValue(x, "IlspEventId"),
                IlspEventName = GetPropValue(x, "IlspEventName"),
                ProviderEventId = GetPropValue(x, "ProviderEventId"),
                ProviderEventName = GetPropValue(x, "ProviderEventName"),
                Latitude = GetPropValue(x, "Latitude") ?? string.Empty,
                Longitude = GetPropValue(x, "Longitude") ?? string.Empty,
                //Latitude = Truncate.TruncateLatLng(GetPropValue(x, "Latitude") ?? string.Empty),
                //Longitude = Truncate.TruncateLatLng(GetPropValue(x, "Longitude") ?? string.Empty),
                Altitude = GetPropValue(x, "Altitude"),
                Speed = GetPropValue(x, "Speed") != null
                    ? int.Parse(GetPropValue(x, "Speed")) <= 5 ? "0" : GetPropValue(x, "Speed")
                    : string.Empty,
                KmOdometer = GetPropValue(x, "KmOdometer"),
                Degrees = GetPropValue(x, "Degrees"),
                Satellite = GetPropValue(x, "Satellite"),
                Temperature = GetPropValue(x, "Temperature"),
                Battery = GetPropValue(x, "Battery"),
                SignalValid = GetPropValue(x, "SignalValid"),
                RawData = GetPropValue(x, "RawData"),
                Street = GetPropValue(x, "Street"),
                Colony = GetPropValue(x, "Colony"),
                Municipality = GetPropValue(x, "Municipality"),
                District = GetPropValue(x, "District"),
                Country = GetPropValue(x, "Country"),
                PostCode = GetPropValue(x, "PostCode"),
                GpsDate = GetPropValue(x, "GpsDate") != null
                    ? Convert.ToDateTime(GetPropValue(x, "GpsDate")).ToString("yyyy/MM/dd HH:mm:ss")
                    : string.Empty,
                CreateDate = GetPropValue(x, "CreateDate") != null
                    ? Convert.ToDateTime(GetPropValue(x, "CreateDate")).ToString("yyyy/MM/dd HH:mm:ss")
                    : string.Empty,
                ProviderId = string.Empty
            }).ToList();
        }

        private static string GetPropValue(object src, string propName)
        {
            try
            {
                var value = src.GetType().GetProperty(propName)?.GetValue(src, null)?.ToString();
                return value;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        //public GpsEnt First(string customerId)
        //{
        //    var response = _ctx.ivr_sp_one_gps(customerId).Select(x => new GpsEnt
        //    {
        //        Id = x.Id,
        //        Name = x.Name,
        //        Type = x.Type.ToString(),
        //        TimeLastPosition = x.TimeLastPosition.ToString()
        //    }).ToList();
        //    if (response.Any())
        //    {
        //        return response.First();
        //    }

        //    return null;
        //}

        public IEnumerable<BitacoraCustodyEnt> GetCustodiesOfServiceByGps(string search)
        {
            return _ctx.sp_getCustodiesByGps(search).Select(x => new BitacoraCustodyEnt
            {
                BitacoraId = string.Empty,
                Alias = string.Empty,
                GpsId = x.GpsId,
                GpsAlias = x.GpsAlias,
                StartService = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"),
                EndService = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
            }).ToArray();
        }

        /// <summary>
        /// Search Gps by Alias
        /// </summary>
        /// <param name="alias">Alias</param>
        /// <param name="customerid">Customer Id</param>
        /// <returns></returns>
        public IEnumerable<GpsInfo> ValidateDisponibilityGpsByAlias(string alias, int customerid)
        {
            var result = _ctx.sp_gps_get_info_bitacora(alias, customerid).ToArray();
            if (result.Any())
            {
                return result.Select(x => new GpsInfo
                {
                    GpsAlias = x.GpsAlias,
                    GpsStatus = x.GpsStatus?.ToString(),
                    GpsStatusName =
                        x.GpsStatus.ToString() == "A" ? Resources.Resource.Active : Resources.Resource.Inactiva,
                    GpsType = x.GpsType,
                    GpsTypeName = x.GpsTypeName,
                    BitacoraAlias = x.BitacoraAlias,
                    BitacoraStatus = x.BitacoraStatus?.ToString(),
                    BitacoraStatusName = x.BitacoraStatus != null
                        ? x.BitacoraStatus.ToString() == "A"
                            ? Resources.Resource.Binnacle
                            : x.BitacoraStatus.ToString() == "P"
                                ? Resources.Resource.Pre_Bitacora
                                : x.BitacoraStatus.ToString() == "R"
                                    ? Resources.Resource.Pre_Capture
                                    : x.BitacoraStatus.ToString() == "O"
                                        ? Resources.Resource.Pending_Service_Order
                                        : Resources.Resource.Unknown
                        : string.Empty,
                    CustomerId = x.CustomerId,
                    CustomerName = x.CustomerName,
                    TimeLastLocation = x.TimeLastPosition ?? 0,
                    TimeMaxLastLocation = x.TimeMaxLastPosition ?? 0
                }).ToArray();
            }
            return new GpsInfo[0];
        }

        /// <summary>
        /// Search Gps by Alias for Pre-Captures
        /// </summary>
        /// <param name="alias">Alias</param>
        /// <param name="customerid">Customer Id</param>
        /// <returns></returns>
        public IEnumerable<GpsInfo> ValidateDisponibilityGpsByAliasForPreCaptures(string alias, int customerid)
        {
            var result = _ctx.sp_gps_get_info_bitacora_for_precaptures(alias, customerid).ToArray();
            if (result.Any())
            {
                return result.Select(x => new GpsInfo
                {
                    GpsAlias = x.GpsAlias,
                    GpsStatus = x.GpsStatus?.ToString(),
                    GpsStatusName =
                        x.GpsStatus.ToString() == "A" ? Resources.Resource.Active : Resources.Resource.Inactiva,
                    GpsType = x.GpsType,
                    GpsTypeName = x.GpsTypeName,
                    BitacoraAlias = x.BitacoraAlias,
                    BitacoraStatus = x.BitacoraStatus?.ToString(),
                    BitacoraStatusName = x.BitacoraStatus != null
                        ? x.BitacoraStatus.ToString() == "A"
                            ? Resources.Resource.Binnacle
                            : x.BitacoraStatus.ToString() == "P"
                                ? Resources.Resource.Pre_Bitacora
                                : x.BitacoraStatus.ToString() == "R"
                                    ? Resources.Resource.Pre_Capture
                                    : x.BitacoraStatus.ToString() == "O"
                                        ? Resources.Resource.Pending_Service_Order
                                        : Resources.Resource.Unknown
                        : string.Empty,
                    CustomerId = x.CustomerId,
                    CustomerName = x.CustomerName,
                    TimeLastLocation = x.TimeLastPosition ?? 0,
                    TimeMaxLastLocation = x.TimeMaxLastPosition ?? 0
                }).ToArray();
            }
            return new GpsInfo[0];
        }

    }
}
