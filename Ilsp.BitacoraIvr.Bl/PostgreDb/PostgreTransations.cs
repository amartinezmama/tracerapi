﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using Npgsql;

namespace Ilsp.BitacoraIvr.Bl.PostgreDb
{
    public class PostgreTransations<T> : IDisposable where T : class, new()
    {
        private readonly string _connectionString;

        public PostgreTransations(string connectionString)
        {
            _connectionString = connectionString;
        }

        public bool ExecNonQuery(T parameters, string sp)
        {
            int result = 0;
            using (var conn = new NpgsqlConnection(_connectionString))
            {
                conn.Open();
                NpgsqlTransaction transaction = conn.BeginTransaction();
                try
                {
                    using (var cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = sp;
                        cmd.CommandType = CommandType.StoredProcedure;
                        AssignParameters(GetParameters(parameters), cmd);
                        result = cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    conn.Close();
                    transaction.Rollback();
                    return false;
                }
                finally
                {
                    transaction.Commit();
                    conn.Close();
                }
            }

            return result > 0 ? true : false;
        }

        List<IDbDataParameter> GetParameters(T parameters)
        {
            List<IDbDataParameter> ls = new List<IDbDataParameter>();
            T t = new T();
            foreach (PropertyInfo pi in parameters.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic))
            {
                var valueObject = parameters.GetType().GetProperty(pi.Name).GetValue(parameters, null);
                if (valueObject != null)
                {
                    ls.Add(new NpgsqlParameter(pi.Name, valueObject));
                }
            }
            return ls;
        }

        void AssignParameters(List<IDbDataParameter> ps, NpgsqlCommand cmd)
        {
            foreach (IDbDataParameter parametro in ps)
            {
                cmd.Parameters.Add(parametro);
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

    }
}
