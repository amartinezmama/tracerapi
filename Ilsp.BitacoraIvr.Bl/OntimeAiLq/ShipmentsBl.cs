﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.OntimeAi;

namespace Ilsp.BitacoraIvr.Bl.OntimeAiLq
{
    public partial class ShipmentsBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly OnTimeDataContext _ctx;

        public ShipmentsBl(string connection, string userName, OnTimeDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new OnTimeDataContext(connection);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Clean all shipments that has a specific bitacora
        /// </summary>
        /// <param name="bitacoraid">Bitacora</param>
        /// <returns></returns>
        public bool ClearAllBitacoras(long bitacoraid)
        {
            var result = _ctx.sp_shipments_clear_bitacora(bitacoraid, _userName);
            return result > 0;
        }

        /// <summary>
        /// Seacrh Delivery Numbers
        /// </summary>
        /// <param name="customerId">Customer</param>
        /// <param name="search">Shipment</param>
        /// <returns></returns>
        public IEnumerable<Shipment> SearchDeliveryNumber(int customerId, string search)
        {
            return _ctx.sp_shipments_delivery_numbers_search(customerId, search, _userName).Select(x => new Shipment
            {
                ShipmentId = x.ShipmentNumber,
                AppointmentDate = x.AppintmentDate?.ToString("yyyy/MM/dd HH:mm:ss"),
                ShipmentNumber = x.ShipmentNumber
            }).ToArray();
        }

        /// <summary>
        /// Valid if shipment number is assigned
        /// </summary>
        /// <param name="shipmentnumber">Shipment Number</param>
        /// <param name="bitacoraid">Bitacora</param>
        /// <returns></returns>
        public bool ValidShipmentNumberAssigned(string shipmentnumber, long? bitacoraid)
        {
            var result = _ctx.sp_shipments_valid_assignation(shipmentnumber, bitacoraid).ToList();
            return result.Any();
        }


        /// <summary>
        /// Update Shipment Numbers with BitacoraId
        /// </summary>
        /// <param name="bitacoraId">Bitacora</param>
        /// <param name="shipmentNumber">Shipment Number</param>
        /// <param name="userName">User Name</param>
        /// <returns></returns>
        public bool UpdateShipmentsWithBitacoraNumber(long bitacoraId, string shipmentNumber)
        {
            return _ctx.sp_shipments_update_bitacora(shipmentNumber, bitacoraId, _userName) > 0;
        }

    }
}
