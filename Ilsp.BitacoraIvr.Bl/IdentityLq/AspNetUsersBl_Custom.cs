﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.Identity;

namespace Ilsp.BitacoraIvr.Bl.IdentityLq
{
    public partial class AspNetUsersBl
    {
        private readonly IEnumerable<string> _rolsAssign = new List<string>
        {
            "Monitorista",
            "Supervisor CM",
            "Checklist",
            "Capturista"
        };

        public IEnumerable<UserEnt> GetAllCustom()
        {
            var items = _ctx.ivr_sp_search_users(string.Empty).Distinct().Select(x => new UserEnt
            {
                Id = x.UserName,
                UserName = x.UserName,
                Roles = x.RolName.Split(',')
            }).Where(u => u.Roles.Any(r => _rolsAssign.Any(x => x.ToLower().Equals(r.ToLower())))).ToList();
            var data = items.AsQueryable();
            return data.ToArray();
        }

        public IEnumerable<UserEnt> Search(string search)
        {
            var items = _ctx.ivr_sp_search_users(search).Distinct().Select(x => new UserEnt
            {
                Id = x.UserName,
                UserName = x.UserName,
                Roles = x.RolName.Split(',')
            }).Where(u => u.Roles.Any(r => _rolsAssign.Any(x => x.ToLower().Equals(r.ToLower())))).ToList();
            var data = items.AsQueryable();
            return data.Take(20).ToArray();
        }

        public IEnumerable<UserEnt> GetUsersForAssignBitacoras()
        {
            var rolsAssign = new List<string>
            {
                "Monitorista",
                "Supervisor CM",
                "Checklist"
            };
            var items = _ctx.ivr_sp_search_users(string.Empty).Distinct().Select(x => new UserEnt
            {
                Id = x.UserName,
                UserName = x.UserName,
                Roles = x.RolName.Split(',')
            }).Where(u => u.Roles.Any(r => rolsAssign.Any(x => x.ToLower().Equals(r.ToLower())))).ToList();
            var data = items.AsQueryable();
            return data.ToArray();
        }

        public IEnumerable<UserEnt> GetActiveUsers(string search)
        {
            var rolsAssign = new List<string>
            {
                "Monitorista",
                "Supervisor CM"
            };
            var items = _ctx.ivr_sp_search_users(search).Distinct().Select(x => new UserEnt
            {
                Id = x.UserName,
                UserName = x.UserName,
                Roles = x.RolName.Split(',')
            }).Where(u => u.Roles.Any(r => rolsAssign.Any(x => x.ToLower().Equals(r.ToLower())))).ToList();
            var data = items.AsQueryable();
            return data.ToArray();
        }

        public IEnumerable<UserEnt> GetAllActiveUsers(string search)
        {
            var items = _ctx.ivr_sp_search_users(search).Distinct().Select(x => new UserEnt
            {
                Id = x.UserName,
                UserName = x.UserName,
                Roles = x.RolName.Split(',')
            }).ToList();
            var data = items.AsQueryable();
            return data.ToArray();
        }

    }
}