﻿using System;
using Ilsp.BitacoraIvr.Dl;

namespace Ilsp.BitacoraIvr.Bl.IdentityLq
{
	public partial class AspNetUserRolesBl : IDisposable
	{
        private readonly string _connection;
        private readonly string _userName;
        private readonly IdentityDataContext _ctx;

        public AspNetUserRolesBl(string connection, string userName, IdentityDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new IdentityDataContext(connection);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
	}
}
