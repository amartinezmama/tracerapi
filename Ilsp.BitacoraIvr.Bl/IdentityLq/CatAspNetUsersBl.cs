﻿using System;
using System.Linq;
using System.Xml.Linq;
using Ilsp.BitacoraIvr.Dl;

namespace Ilsp.BitacoraIvr.Bl.IdentityLq
{
    public partial class CatAspNetUsersBl : IDisposable
    {
        private readonly string _connection;
        private readonly string _userName;
        private readonly IdentityDataContext _ctx;

        public CatAspNetUsersBl(string connection, string userName, IdentityDataContext ctx = null)
        {
            _connection = connection;
            _userName = userName;
            _ctx = ctx ?? new IdentityDataContext(connection);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public XElement GetUserByRoles(string rolesName)
        {
            var roles = _ctx.sp_get_users_by_role(rolesName).ToArray();
            return new XElement("users", roles.Select(u => new XElement("user", new XAttribute("name", u.user), new XAttribute("role", u.role))));
        }

    }
}
