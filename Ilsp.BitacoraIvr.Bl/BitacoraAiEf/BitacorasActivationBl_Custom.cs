﻿using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.Rp.BitacoraAi;
using Ilsp.BitacoraIvr.Tl.ExpressionBuilder.Builders;
using System;
using System.Collections.Generic;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public class BitacorasActivationBl_Custom : BaseBl<bitacoras_ax_activation, BitacorasAxActivationRepository>, IDisposable
    {

        private readonly string _connection;
        private readonly string _secondConnection;
        private readonly string _userName;
        private readonly ExpressionBuilder _builder;
        public readonly BitacoraAiDataContext _ctx;

        public BitacorasActivationBl_Custom(string connection, string userName, string secondConnection = null)
        {
            Repository.Init(new Dl.BitacoraAi.BitacoraAi(connection));
            _connection = connection;
            _secondConnection = secondConnection;
            _userName = userName;
            _ctx = new BitacoraAiDataContext(secondConnection);
            _builder = new ExpressionBuilder();
        }

        /// <summary>
        /// Save new register on BitacorasStays
        /// </summary>
        /// <param name="item">Row to create</param>
        /// <param name="propertiesCondition">Properties to make condition for search duplicate rows</param>
        /// <param name="errors"></param>
        /// <param name="condition">Condition</param>
        /// <returns></returns>
        public bool Save(bitacoras_ax_activation item, List<Filter> propertiesCondition, List<string> errors, string condition = "AND")
        {
            var predicate = _builder.GetFilterExpression<bitacoras_ax_activation>(propertiesCondition, condition);
            var itemonDb = Repository.GetFirstOrDefault(predicate);

            if (itemonDb == null)
            {
                return Repository.Save(item);
            }

            errors.Add(Resources.Resource.The_Name_Already_Exist);
            return false;
        }


        /// <summary>
        /// Save new register on BitacorasStays
        /// </summary>
        /// <param name="item">Row to create</param>
        /// <param name="propertiesCondition">Properties to make condition for search duplicate rows</param>
        /// <param name="errors"></param>
        /// <param name="condition">Condition</param>
        /// <returns></returns>
        public bool SaveWithOutTransaction(bitacoras_ax_activation item, List<Filter> propertiesCondition, List<string> errors, string condition = "AND")
        {
            var predicate = _builder.GetFilterExpression<bitacoras_ax_activation>(propertiesCondition, condition);
            var itemonDb = Repository.GetFirstOrDefault(predicate);

            if (itemonDb == null)
            {
                return Repository.SaveWithOutTransaction(item);
            }

            errors.Add(Resources.Resource.The_Name_Already_Exist);
            return false;
        }


        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
