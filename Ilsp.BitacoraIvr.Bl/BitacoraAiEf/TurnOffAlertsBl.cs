﻿using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public class TurnOffAlertsBl : IDisposable
    {
        private readonly BitacoraAiDataContext _db;

        public TurnOffAlertsBl(BitacoraAiDataContext db)
        {
            _db = db;
        }

        public IEnumerable<TurnOffAlertsModel> GetBitacorasAlerts()
        {
            var result = _db.sp_get_bitacoras_alerts_status()
                .Select(item => new TurnOffAlertsModel
                {
                    id = item.id,
                    id_bitacora = item.id_bitacora,
                    alias_bitacora = item.alias_bitacora,
                    id_gps = item.id_gps,
                    alias = item.alias,
                    id_alert = item.id_alert,
                    name_alert = item.name_alert,
                    minutes_disabled = item.minutes_disabled,
                    observations = item.observations,
                    user_create = item.user_create,
                    create_date = item.create_date,
                    user_cancel = item.user_cancel,
                    cancel_date = item.cancel_date,
                    status = item.status,
                    status_description = item.status_description
                }).ToArray().Reverse();

            return result;
        }

        public IEnumerable<AlertsBitacora> Bitacoras()
        {
            var result = _db.sp_get_bitacoras_for_bitacoras_alerts_status()
                .Select(x => new AlertsBitacora
                {
                    id_bitacora = x.id_bitacora,
                    alias = x.alias,
                    status = x.status
                }).ToArray();

            return result;
        }

        public IEnumerable<GetGpsBitacoraModel> GetGpsBitacoras(string idBitacora)
        {
            var resultSp = _db.sp_get_gps_for_bitacoras_alerts_status(idBitacora)
                .Select(x => new GetGpsBitacoraModel
                {
                    bitacora_id = x.bitacora_id,
                    gps_id = x.gps_id,
                    alias = x.alias,
                    type = x.type
                }).ToArray();

            return resultSp;
        }

        public IEnumerable<GetAlertsModel> GetAlertsBitacora()
        {
            var result = _db.sp_get_alerts_for_bitacoras_alerts_status()
                .Select(x => new GetAlertsModel
                {
                    id_alert = x.id_alert,
                    description = x.description
                }).ToArray();

            return result;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
