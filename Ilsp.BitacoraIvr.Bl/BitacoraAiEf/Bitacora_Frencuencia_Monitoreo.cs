﻿using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.Common;
using System;
using System.Linq;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{


    public class Bitacora_Frencuencia_Monitoreo : IDisposable
    {

        private readonly BitacoraAiDataContext _db;

        public Bitacora_Frencuencia_Monitoreo(BitacoraAiDataContext db)
        {
            _db = db;
        }

        public KendoResponse GetGridBitacoras(KendoRequest model, string username, bool all)
        {
            var filterSp = string.Empty;
            var orderSp = string.Empty;

            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filterSp = string.Join("|",
                    model.Filter.Filters.Select(f => $"{f.Field},{f.Value.ToLower()}")
                        .ToArray());
            }

            if (model.Sort != null && model.Sort.Count > 0)
            {
                orderSp = string.Join("|",
                    model.Sort.Select(x => $"{x.Field},{(x.Dir == "asc" ? "1" : "0")}").ToArray());
            }

            var datatemp =
               _db.sp_bitacora_get_grid_bitacoras(filterSp, orderSp, model.Page, model.Take, username, all).ToList()
                   .Select(x => new PreCaptureGrid
                   {
                       Id = x.Id.ToString(),
                       Alias = x.Alias,
                       Frecuency = x.Frecuency,
                       Status = x.Status,
                       CreateDate = x.CreateDate,
                       Customer = x.Customer,
                       TransportLine = x.TransportLine,
                       Origin = x.Origin,
                       Destiny = x.Destiny,
                       DeliveryNumber = x.DeliveryNumber,
                       AppointmentDate = x.AppointmentDate,
                       ServiceType = x.ServiceType,
                       MonitoringType = x.MonitoringType,
                       Plate = x.Plate,
                       Economic = x.Economic,
                       Brand = x.Brand,
                       Model = x.Model,
                       Color = x.Color,
                       VehicleType = x.VehicleType,
                       CustodyType = x.CustodyType,
                       Selected = false,
                       Total = x.Total ?? 0
                   }).ToList();

            var count = 0;
            if (datatemp.Count > 0)
            {
                count = datatemp.First().Total;
            }
            return new KendoResponse(datatemp.ToArray(), count);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
