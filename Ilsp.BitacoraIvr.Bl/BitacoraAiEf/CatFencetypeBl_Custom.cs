﻿using System.Collections.Generic;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class CatFencetypeBl
    {
        public FenceTypeEnt First(int id)
        {
            var filters = new List<Filter> { new Filter { Field = "id", Value = id.ToString(), Operator = "Equals" } };
            var predicate = _builder.GetFilterExpression<cat_fencetype>(filters);
            var response = Repository.GetFirst(predicate);
            if (response != null)
            {
                return new FenceTypeEnt
                {
                    Id = response.Id,
                    Name = response.Name,
                    Description = response.Description
                };
            }

            return null;
        }
    }
}
