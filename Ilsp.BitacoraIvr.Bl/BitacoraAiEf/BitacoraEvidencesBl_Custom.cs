﻿using System.Linq;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class BitacoraEvidencesBl
    {
        /// <summary>
        /// Method to get list of rows in pages
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <param name="includes">Another Entity</param>
        /// <returns></returns>
        public KendoResponse GetListPagerCustom(KendoRequest model, string[] includes = null)
        {
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(separator: ",",
                    value: model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => string.Format(format: "{0} {1}", arg0: x.Field, arg1: x.Dir))
                        .ToArray());
            }
            var predicate = _builder.GetFilterExpression<BitacoraEvidences>(model.Filter.Filters);
            var response = Repository.GetListPager(predicate, order, model.Skip, model.Take, includes);
            return new KendoResponse(response.Items.ToArray(), response.Total);
        }
    }
}
