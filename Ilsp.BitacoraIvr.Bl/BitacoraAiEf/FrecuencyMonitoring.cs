﻿using Ilsp.BitacoraIvr.Dl;
using System;
using System.Collections.Generic;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public class FrecuencyMonitoring : IDisposable
    {
        private readonly BitacoraAiDataContext _db;

        public FrecuencyMonitoring(BitacoraAiDataContext db)
        {
            _db = db;
        }

        public bool SetFrecuencyMonitoring(IEnumerable<string> bitacoras, int frecuency, string comment, string userName)
        {
            _db.sp_bitacora_change_frecuency_monitoring(string.Join(",", bitacoras), frecuency, comment, userName);
            return true;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
