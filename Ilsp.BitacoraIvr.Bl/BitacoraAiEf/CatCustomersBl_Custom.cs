﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class CatCustomersBl
    {

        /// <summary>
        /// Select one row
        /// </summary>
        /// <param name="propertiesCondition">Properties to make condition for search row</param>
        /// <param name="includes">Another Entity</param>
        /// <returns></returns>
        public CustomerEnt GetOneCustom(List<Filter> propertiesCondition, string[] includes = null)
        {
            var predicate = _builder.GetFilterExpression<cat_customers>(propertiesCondition);
            var results = Repository.GetList(predicate, string.Empty).ToList();
            if (results.Any())
            {
                var item = results.First();
                return new CustomerEnt
                {
                    Id = item.id_customer,
                    Name = item.account_name.Trim(),
                    SendGeneralReport = item.send_general_report ?? false,
                    MailsGeneralReport = string.IsNullOrEmpty(item.email_general_report) ? new EmailEnt[0] : item.email_general_report.Split(',').ToList().Select(x => new EmailEnt
                    {
                        Name = x.Trim()
                    }).ToArray(),
                    OnTime = item.ontime,
                    ValidPreBitacora = item.validbitacora,
                    Status = item.status
                };
            }

            return null;
        }

        public bool UpdateCustom(CustomerEnt entity, List<string> errors, CustomerEnt customerBefore)
        {
            bool result;
            var filters = new List<Filter>
            {
                new Filter {Field = "id_customer", Value = entity.Id.ToString(), Operator = "Equals"}
            };
            result = Update(filters, itemToSave =>
            {
                itemToSave.account_name = entity.Name;
                itemToSave.send_general_report = entity.SendGeneralReport;
                itemToSave.email_general_report =
                    string.Join(",", entity.MailsGeneralReport.Select(x => x.Name));
                itemToSave.user_mod = _userName;
                itemToSave.mod_date = DateTime.Now;
            });
            if (result)
            {
                using (var bl = new CatCustomersOperativeKeysTransportLinesBl(_connection, _userName))
                {
                    foreach (var opKeyTl in customerBefore.OpKeyTransportLines.Where(b => !entity.OpKeyTransportLines.Any(a => a.Id.Equals(b.Id))))
                    {
                        filters = new List<Filter>
                        {
                            new Filter {Field = "id_customer", Value = entity.Id.ToString(), Operator = "Equals"},
                            new Filter {Field = "id_operative_key", Value = opKeyTl.Id.ToString(), Operator = "Equals"}
                        };
                        if (!bl.DeleteWithOutTransaction(filters))
                        {
                            result = false;
                            break;
                        }
                    }
                    foreach (var opKeyTl in entity.OpKeyTransportLines.Where(a => !customerBefore.OpKeyTransportLines.Any(b => b.Id.Equals(a.Id))))
                    {
                        filters = new List<Filter>();
                        if (!bl.SaveWithOutTransaction(new cat_customers_operative_keys_transport_lines
                        {
                            id_customer = entity.Id,
                            id_operative_key = opKeyTl.Id,
                            create_date = DateTime.Now,
                            create_user = _userName
                        }, filters))
                        {
                            result = false;
                            break;
                        }
                    }
                }
                using (var bl = new CatCustomersAxCustomersBl(_connection, _userName))
                {
                    foreach (var fiscalName in customerBefore.FiscalNames.Where(b => !entity.FiscalNames.Any(a => a.Id.Equals(b.Id))))
                    {
                        filters = new List<Filter>
                        {
                            new Filter {Field = "customer_id", Value = entity.Id.ToString(), Operator = "Equals"},
                            new Filter {Field = "ax_customer_id", Value = fiscalName.Id, Operator = "Equals"}
                        };
                        if (!bl.DeleteWithOutTransaction(filters))
                        {
                            result = false;
                            break;
                        }
                    }
                    foreach (var fiscalName in entity.FiscalNames)
                    {
                        filters = new List<Filter>();
                        if (!bl.SaveWithOutTransaction(new cat_customers_ax_customers
                        {
                            customer_id = entity.Id,
                            ax_customer_id = fiscalName.Id,
                            user_mod = _userName,
                            create_date = DateTime.Now
                        }, filters))
                        {
                            result = false;
                            break;
                        }
                    }
                }
                using (var bl = new CatCustomersOperativeKeysBl(_connection, _userName))
                {
                    foreach (var opKey in customerBefore.OperativeKeys.Where(b => !entity.OperativeKeys.Any(a => a.OperativeKey.Id.Equals(b.OperativeKey.Id) && a.Mail.ToLower().Equals(b.Mail.ToLower()))))
                    {
                        filters = new List<Filter>
                        {
                            new Filter {Field = "id_customer", Value = entity.Id.ToString(), Operator = "Equals"},
                            new Filter {Field = "id_operative_key", Value = opKey.OperativeKey.Id.ToString(), Operator = "Equals"},
                            new Filter {Field = "email", Value = opKey.Mail, Operator = "Equals"}
                        };
                        if (!bl.DeleteWithOutTransaction(filters))
                        {
                            result = false;
                            break;
                        }
                    }
                    foreach (var opKey in entity.OperativeKeys)
                    {
                        filters = new List<Filter>();
                        if (!bl.SaveWithOutTransaction(new cat_customers_operative_keys
                        {
                            id_customer = entity.Id,
                            id_operative_key = opKey.OperativeKey.Id,
                            email = opKey.Mail,
                            create_date = DateTime.Now,
                            create_user = _userName
                        }, filters))
                        {

                            result = false;
                            break;
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Method to get list of rows in pages
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <param name="logic">Contains or Equals</param>
        /// <param name="includes">Another Entity</param>
        /// <returns></returns>
        public KendoResponse GetListPagerCustom(KendoRequest model, string logic, string[] includes = null)
        {
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(separator: ",",
                    value: model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => string.Format(format: "{0} {1}", arg0: x.Field, arg1: x.Dir))
                        .ToArray());
            }
            var predicate = _builder.GetFilterExpression<cat_customers>(model.Filter.Filters, logic);
            var response = Repository.GetListPager(predicate, order, model.Skip, model.Take, includes);
            var items = response.Items.Select(item => new CustomerEnt
            {
                Id = item.id_customer,
                Name = item.account_name,
                EvidenceFields = item.CatCustomerFields.Where(x => x.StatusId.Equals(1)).Select(f => new EvidenceFieldEnt
                {
                    FieldId = f.Id,
                    Field = f.Field,
                    IsRequired = f.IsRequired
                }).ToArray()
            }).ToArray();
            return new KendoResponse(items, response.Total);
        }

        /// <summary>
        /// Method to get one item
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="includes">Another entities</param>
        /// <returns></returns>
        public CustomerEnt FirstCustomFields(int id, string[] includes)
        {
            var filters = new List<Filter> { new Filter { Field = "id_customer", Value = id.ToString(), Operator = "Equals" } };
            var predicate = _builder.GetFilterExpression<cat_customers>(filters);
            var response = Repository.GetFirst(predicate, includes);

            if (response != null)
            {
                return new CustomerEnt
                {
                    Id = response.id_customer,
                    Name = response.account_name,
                    EvidenceFields = response.CatCustomerFields.Where(x => x.StatusId.Equals(1)).Select(f => new EvidenceFieldEnt
                    {
                        FieldId = f.Id,
                        Field = f.Field,
                        IsRequired = f.IsRequired
                    }).ToArray()
                };
            }

            return null;
        }

    }
}
