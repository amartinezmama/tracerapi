﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class CatMonitoringTypesBl
    {
        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <returns></returns>
        public KendoResponse GetGridCustom(KendoRequest model, string[] includes)
        {
            var order = string.Empty;
            var filterClientSide = new[] { "name" };
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(",", model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => $"{x.Field} {(x.Dir?.ToLower() == "asc" ? "asc" : "desc")}").ToArray());
            }

            // compose the filter 
            var filter = "1 == 1";
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("&&", model.Filter.Filters
                    .Where(f => filterClientSide.Any(ft => ft.ToLower().Equals(f.Field.ToLower()))).Select(f =>
                        $"{f.Field}.ToLower().Contains(\"{f.Value.ToLower()}\")").ToArray());
            }

            if (model.Filter == null || model.Filter.Filters.Count == 0)
            {
                model.Filter = new KendoFilter
                {
                    Filters = new List<Filter>()
                };
                model.Filter.Filters = new List<Filter>();
            }

            model.Filter.Filters.Add(new Filter{Field = "status_id", Operator = "Equals", Value = "1"});

            var predicate = _builder.GetFilterExpression<cat_monitoring_types>(
                model.Filter.Filters.Where(f => !filterClientSide.Any(fc => fc.ToLower().Equals(f.Field.ToLower()))).ToList()
            );
            var response = Repository.GetList(predicate, string.Empty, includes);

            var items = response.Select(item =>
                new MonitoringTypeEnt
                {
                    Id = item.id_monitoring_type,
                    Name = item.description,
                    IsMirrorAccount = item.is_mirror_account ?? false,
                    Gpsrequired = item.gpsrequired,
                    Gpsallow = item.gpsallow,
                    Custodyrequired = item.custodyrequired,
                    Custodiesallow = item.custodiesallow,
                    Securitykitrequired = item.securitykitrequired,
                    Securitykitallow = item.securitykitallow,
                    Lockrequired = item.lockrequired,
                    Trabapatinrequired = item.trabapatinrequired,
                    IsCustodyAbandon = item.is_custody_abandon,
                    IsLoadingWitness = item.is_loading_witness ?? false
                }).ToList();
            if (string.IsNullOrEmpty(filter))
            {
                filter = "1 == 1";
            }
            var data = items.AsQueryable().Where(filter);
            return new KendoResponse(data.OrderBy(string.IsNullOrEmpty(order) ? "true": order)
                .Skip(model.Skip)
                .Take(model.Take == 0 ? data.Count() : model.Take)
                .ToArray(), data.Count());
        }
    }
}