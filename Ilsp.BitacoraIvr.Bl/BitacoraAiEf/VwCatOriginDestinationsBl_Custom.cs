﻿using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Collections.Generic;
using System.Linq;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class VwCatOriginDestinationsBl
    {

        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <returns></returns>
        public KendoResponse GetGridCustom(KendoRequest model, string[] includes = null)
        {
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(separator: ",",
                    value: model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => string.Format(format: "{0} {1}", arg0: x.Field, arg1: x.Dir))
                        .ToArray());
            }

            var filters = new List<Filter> { new Filter { Field = "id_status", Value = "1", Operator = "Equals" } };
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filters.AddRange(model.Filter.Filters
                    .Select(
                        f => new Filter
                        {
                            Field = f.Field,
                            Value = f.Value,
                            Operator = f.Operator
                        }));
            }

            var predicate = _builder.GetFilterExpression<vw_cat_origin_destinations>(filters);
            var response = Repository.GetListPager(predicate, order, model.Skip, model.Take, includes);

            var items = response.Items.Select(item =>
                new OriginAndDestinyGrid
                {
                    Id = item.id_origin_destination,
                    Name = item.name,
                    Address = item.address,
                    Location = item.location
                }).ToList();

            return new KendoResponse(items.ToArray(), response.Total);
        }

    }
}
