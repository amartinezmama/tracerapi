﻿using System.Linq;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class VwCatCustomersBl
    {
        /// <summary>
        /// Get Items
        /// </summary>
        /// <param name="model">Filters, Orders and Pages</param>
        /// <param name="includes">Includes</param>
        /// <returns></returns>
        public KendoResponse GetGridCustom(KendoRequest model, string[] includes = null)
        {
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(separator: ",",
                    value: model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => string.Format(format: "{0} {1}", arg0: x.Field, arg1: x.Dir))
                        .ToArray());
            }
            var predicate = _builder.GetFilterExpression<vw_cat_customers>(model.Filter.Filters);
            var response = Repository.GetListPager(predicate, order, model.Skip, model.Take, includes);
            var data = response.Items.Select(x => new CustomerGrid
            {
                Id = x.id,
                Name = x.name,
                FiscalName = x.fiscalname
            }).ToList();
            return new KendoResponse(data.ToArray(), response.Total);
        }

    }
}