﻿using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class BitacorasBl
    {

        public BitacoraEnt GetToEdit(long bitacoraId)
        {
            var bitacora = new BitacoraEnt();
            var filters = new List<Filter>
            {
                new Filter
                {
                    Field = "id_bitacora",
                    Value = bitacoraId.ToString(),
                    Operator = "Equals"
                }
            };
            var predicate = _builder.GetFilterExpression<bitacoras>(filters);
            var includes = new[]
            {
                "cat_customers",
                "cat_service_types",
                "cat_catalogs_common",
                "cat_monitoring_types",
                "cat_security_kits",
                "bitacoras_custodies"
            };
            var result = Repository.GetFirst(predicate, includes);
            bitacora.Id = result.id_bitacora.ToString(CultureInfo.CurrentCulture);
            bitacora.Alias = result.alias;
            bitacora.Status = result.status;
            bitacora.Customer = new CustomerEnt
            {
                Id = result.cat_customers.id_customer,
                Name = result.cat_customers.account_name.Trim(),
                FiscalName = result.cat_customers.fiscal_name?.Trim(),
                FiscalAddress = result.cat_customers.fiscal_address?.Trim(),
                OnTime = result.cat_customers.ontime,
                ValidPreBitacora = result.cat_customers.validbitacora
            };
            if (result.ax_customer_id != null)
            {
                using (var axCustomerBl = new CatAxCustomerBl(_secondConnection, _userName))
                {
                    bitacora.BusinessCustomer = axCustomerBl.First(bitacora.Customer.Id, result.ax_customer_id);
                }
            }

            using (var transportLineBl = new CatTransportLinesBl(_secondConnection, _userName))
            {
                bitacora.TransportLine = transportLineBl.First(bitacora.Customer.Id, result.ax_transport_line_id);
            }

            bitacora.ServiceType = new ServiceTypeEnt
            {
                Id = result.cat_service_types.id_service_type,
                Name = result.cat_service_types.description.Trim()
            };
            if (bitacora.BusinessCustomer != null && result.ax_proyect_id != null)
            {
                using (var projectBl = new CatProjectsBl(_secondConnection, _userName))
                {
                    bitacora.Project = projectBl.First(bitacora.BusinessCustomer.Id, result.ax_proyect_id);
                }
            }

            if (bitacora.Project != null && result.ax_agreement_id != null)
            {
                using (var agreementBl = new CatAgreementsBl(_secondConnection, _userName))
                {
                    bitacora.Agreement = agreementBl.First(bitacora.Customer.Id, result.ax_agreement_id ?? 0);
                }
            }

            bitacora.CustodyFolio = result.custody_folio?.Trim();
            if (result.origin_id != null)
            {
                using (var originBl = new BitacoraAiLq.CatOriginsDestinationsBl(_secondConnection, _userName))
                {
                    bitacora.Origin = originBl.FirstOrigin(bitacora.Customer.Id, result.origin_id ?? 0);
                }
            }

            bitacora.Advance = result.advance?.Trim();
            bitacora.ProductType = result.product_type_id == null
                ? null
                : new ProductTypeEnt
                {
                    Id = result.cat_catalogs_common.cat_type_id?.Trim(),
                    Name = result.cat_catalogs_common.cat_type_name?.Trim(),
                    Description = result.cat_catalogs_common.cat_type_description?.Trim(),
                    Type = result.cat_catalogs_common.cat_type?.Trim(),
                    Color = result.cat_catalogs_common.cat_color?.Trim()
                };
            bitacora.MonitoringType = result.monitoring_type_id == null
                ? null
                : new MonitoringTypeEnt
                {
                    Id = result.cat_monitoring_types.id_monitoring_type,
                    Name = result.cat_monitoring_types.description?.Trim(),
                    IsMirrorAccount = result.cat_monitoring_types.is_mirror_account,
                    User = result.cat_monitoring_types.user_mod?.Trim(),
                    Gpsrequired = result.cat_monitoring_types.gpsrequired,
                    Gpsallow = result.cat_monitoring_types.gpsallow,
                    Custodyrequired = result.cat_monitoring_types.custodyrequired,
                    Custodiesallow = result.cat_monitoring_types.custodiesallow,
                    Securitykitrequired = result.cat_monitoring_types.securitykitrequired,
                    Securitykitallow = result.cat_monitoring_types.securitykitallow,
                    Lockrequired = result.cat_monitoring_types.lockrequired,
                    Trabapatinrequired = result.cat_monitoring_types.trabapatinrequired
                };
            using (var destiniesBl = new BitacorasDestinationsBl(_connection, _userName, _secondConnection))
            {
                filters = new List<Filter>
                {
                    new Filter
                    {
                        Field = "bitacora_id",
                        Value = bitacora.Id,
                        Operator = "Equals"
                    }
                };
                includes = new[] { "bitacoras_destinations_delivery_numbers" };
                var destiniesResult = destiniesBl.GetList(filters, new List<KendoSort>(), "AND", includes);
                bitacora.Destinies = destiniesResult.Select(d => new DestinyEnt
                {
                    Id = d.destination_id,
                    BitacoraDestinyId = d.id_bitacora_destination,
                    DeliveryNumber = d.bitacoras_destinations_delivery_numbers.First().delivery_number,
                    AppointmentDate = d.meeting_date.ToString("yyyy/MM/dd HH:mm:ss")
                }).ToArray();
            }

            using (var transportBl = new BitacorasTransportationsBl(_connection, _userName))
            {
                filters = new List<Filter>
                {
                    new Filter
                    {
                        Field = "id_bitacora",
                        Value = bitacora.Id,
                        Operator = "Equals"
                    }
                };
                includes = new[]
                {
                    "bitacoras_transportations_operators",
                    "bitacoras_transportations_stamps",
                    "cat_transport_lines_vehicles",
                    "bitacoras_transportations_colors",
                    "cat_unit_types",
                    "cat_brands"
                };
                var transportResult = transportBl.GetOne(filters, includes);
                if (transportResult != null)
                {
                    bitacora.Transport = new TransportEnt
                    {
                        Id = transportResult.id_bitacora_transportation,
                        Operators = transportResult.bitacoras_transportations_operators?.Select(o => new OperatorEnt
                        {
                            Id = o.operator_id,
                            Name = o.name?.Trim(),
                            CellPhoeOne = o.cellphone?.Trim(),
                            CellPhoeTwo = o.second_cellphone?.Trim()
                        }).ToArray(),
                        Plate = transportResult.id_vehicle == null
                            ? null
                            : new PlateEnt
                            {
                                Id = transportResult.id_vehicle,
                                NumPlate = transportResult.cat_transport_lines_vehicles?.num_plate,
                                UnitType = new UnitTypeEnt
                                {
                                    Id = transportResult.cat_unit_types.id_unit_type,
                                    Name = transportResult.cat_unit_types.description?.Trim(),
                                    UnitsAllow = transportResult.cat_unit_types.unitsallow,
                                    MinUnitsAllow = transportResult.cat_unit_types.minunits,
                                    IsValid = transportResult.cat_unit_types.isvalid
                                },
                                Brand = transportResult.cat_brands == null
                                    ? null
                                    : new BrandEnt
                                    {
                                        Id = transportResult.cat_brands.id_brand,
                                        Name = transportResult.cat_brands.name?.Trim(),
                                        IsValid = transportResult.cat_brands.isvalid
                                    },
                                Economic = transportResult.economic?.Trim(),
                                Model = transportResult.model == null
                                    ? null
                                    : new UnitModelEnt
                                    {
                                        Id = transportResult.model ?? 0,
                                        Name = transportResult?.ToString()
                                    },
                                Comments = transportResult.comments?.Trim(),
                                Colors = transportResult.bitacoras_transportations_colors?.Select(c => new ColorEnt
                                {
                                    Id = c.id_color?.Trim()
                                }).ToArray(),
                                TransportLine = bitacora.TransportLine
                            },
                        UnitStamps = transportResult.bitacoras_transportations_stamps?.Select(u => new UnitStampEnt
                        {
                            //Id = u.stamp_id,
                            Name = u.stamp?.Trim()
                        }).ToArray(),
                        Observations = transportResult.comments?.Trim()
                    };
                }
            }

            if (bitacora.Transport != null)
            {
                if (bitacora.Transport.Plate != null)
                {
                    using (var colorsBl = new CatColorsBl(_connection, _userName))
                    {
                        filters = bitacora.Transport.Plate.Colors.Select(c => new Filter
                        {
                            Field = "id_color",
                            Value = c.Id,
                            Operator = "Equals"
                        }).ToList();
                        bitacora.Transport.Plate.Colors = colorsBl.GetList(filters, new List<KendoSort>(), "OR").Select(
                            c =>
                                new ColorEnt
                                {
                                    Id = c.id_color?.Trim(),
                                    Name = c.description?.Trim(),
                                    IsValid = c.isvalid
                                }).ToArray();
                    }
                }

                using (var transportGpssBl = new BitacorasTransportationsGpsBl(_connection, _userName))
                {
                    filters = new List<Filter>
                {
                    new Filter
                    {
                        Field = "id_bitacora_transportation",
                        Value = bitacora.Transport.Id.ToString(),
                        Operator = "Equals"
                    }
                };
                    includes = new[] { "cat_gps" };
                    var transportGpssResult = transportGpssBl.GetList(filters, new List<KendoSort>(), "AND", includes);
                    bitacora.Transport.Gpss = transportGpssResult.Select(g => new GpsEnt
                    {
                        Id = g.cat_gps.id_gps?.Trim(),
                        Name = g.cat_gps.alias?.Trim(),
                        Type = g.cat_gps.gps_type_id ?? 0
                    }).ToArray();
                }

                using (var transportContainersBl = new BitacorasContainersBl(_connection, _userName))
                {
                    filters = new List<Filter>
                {
                    new Filter
                    {
                        Field = "bitacora_id",
                        Value = bitacora.Id,
                        Operator = "Equals"
                    }
                };
                    includes = new[] { "cat_container_types", "cat_colors" };
                    var transportContainersResult = transportContainersBl
                        .GetList(filters, new List<KendoSort>(), "AND", includes).ToList();
                    bitacora.Transport.Containers = new List<ContainerEnt>();
                    transportContainersResult.ForEach(c =>
                    {
                        var container = new ContainerEnt
                        {
                            Id = c.id_container,
                            ContainerType = new ContainerTypeEnt
                            {
                                Id = c.cat_container_types.id_container_type,
                                Name = c.cat_container_types.description?.Trim(),
                                IsValid = c.cat_container_types.isvalid
                            },
                            Economic = c.economic?.Trim(),
                            Color = new ColorEnt
                            {
                                Id = c.cat_colors.id_color?.Trim(),
                                Name = c.cat_colors.description?.Trim(),
                                IsValid = c.cat_colors.isvalid
                            },
                            Plate = c.plates?.Trim(),
                            Capacity = c.capacity?.Trim(),
                            UnitStamp = c.stamp?.Trim()
                        };
                        if (c.ax_lock_id != null)
                        {
                            using (var lockBl = new CatLocksBl(_secondConnection, _userName))
                            {
                                container.Lock = lockBl.First(c.ax_lock_id.Trim());
                            }
                        }

                        if (c.ax_padlock_id != null)
                        {
                            using (var padLockBl = new CatPadLocksBl(_secondConnection, _userName))
                            {
                                container.PadLock = padLockBl.First(c.ax_padlock_id.Trim());
                            }
                        }

                        if (c.ax_key_padlock_id != null)
                        {
                            using (var keyBl = new CatKeysBl(_secondConnection, _userName))
                            {
                                container.Key = keyBl.First(c.ax_key_padlock_id.Trim());
                            }
                        }

                        bitacora.Transport.Containers.Add(container);
                    });
                }
            }

            if (result.security_kit_id != null)
            {
                bitacora.SecurityKit = new SecurityKitEnt
                {
                    Id = result.cat_security_kits.id_security_kit,
                    Name = result.cat_security_kits.name.Trim()
                };
            }

            if (result.bitacoras_custodies.Any())
            {
                var custody = result.bitacoras_custodies.First();
                bitacora.Custody = new CustodyEnt
                {
                    Id = custody.custody_id
                };
                using (var catCostodyTypeBl = new CatCustodianTypesBl(_connection, _userName))
                {
                    filters = new List<Filter>
                    {
                        new Filter
                        {
                            Field = "id_custodian_type",
                            Value = custody.custodian_type_id.ToString(),
                            Operator = "Equals"
                        }
                    };
                    var custodyTypeResult = catCostodyTypeBl.GetOne(filters);
                    if (custodyTypeResult != null)
                    {
                        bitacora.Custody.CustodyType = new CustodyTypeEnt
                        {
                            Id = custodyTypeResult.id_custodian_type,
                            Name = custodyTypeResult.description?.Trim(),
                            ShowCabin = custodyTypeResult.showcabin,
                            ShowVehicle = custodyTypeResult.showvehicle
                        };
                    }
                }

                using (var custodyCabinsBl = new BitacorasCustodiesCabinBl(_connection, _userName))
                {
                    filters = new List<Filter>
                    {
                        new Filter
                        {
                            Field = "custody_id",
                            Value = bitacora.Custody.Id.ToString(),
                            Operator = "Equals"
                        }
                    };
                    bitacora.Custody.Cabins = custodyCabinsBl.GetList(filters, new List<KendoSort>(), "AND").Select(c =>
                        new CustodyCabinEnt
                        {
                            Id = c.cabin_id,
                            Custodian = new CustodianEnt
                            {
                                Id = c.ax_custodian_id?.Trim()
                            },
                            ShortNumber = c.short_number?.Trim(),
                            LargeNumber = c.large_number?.Trim()
                        }).ToList();
                }

                using (var custodyVehiclesBl = new BitacorasCustodiesVehicleBl(_connection, _userName))
                {
                    filters = new List<Filter>
                    {
                        new Filter
                        {
                            Field = "custody_id",
                            Value = bitacora.Custody.Id.ToString(),
                            Operator = "Equals"
                        }
                    };
                    bitacora.Custody.Vehicles = custodyVehiclesBl.GetList(filters, new List<KendoSort>(), "AND").Select(
                        v => new CustodyVehicleEnt
                        {
                            Id = v.vehicle_id,
                            Custodian = new CustodianEnt
                            {
                                Id = v.ax_custodian_id?.Trim()
                            },
                            ShortNumber = v.short_number?.Trim(),
                            LargeNumber = v.large_number?.Trim(),
                            Vehicle = new CustodyUnitEnt
                            {
                                AxId = v.gps_ax_id?.Trim(),
                                Id = v.gps_id?.Trim()
                            }
                        }).ToList();
                }

                using (var custodiansBl = new CatCustodiansBl(_secondConnection, _userName))
                {
                    foreach (var cabin in bitacora.Custody.Cabins)
                    {
                        cabin.Custodian = custodiansBl.First(cabin.Custodian.Id);
                    }

                    foreach (var vehicle in bitacora.Custody.Vehicles)
                    {
                        vehicle.Custodian = custodiansBl.First(vehicle.Custodian.Id);
                    }
                }

                if (bitacora.Custody.Vehicles != null && bitacora.Custody.Vehicles.Count > 0)
                {
                    using (var gpsBl = new CatGpsBl(_connection, _userName))
                    {
                        filters = bitacora.Custody.Vehicles.Select(g => new Filter
                        {
                            Field = "id_gps",
                            Value = g.Vehicle.Id,
                            Operator = "Equals"
                        }).ToList();
                        var gpssResult = gpsBl.GetList(filters, new List<KendoSort>(), "OR").ToList();
                        foreach (var vehicle in bitacora.Custody.Vehicles)
                        {
                            var gpss = gpssResult.Where(g => g.id_gps.Equals(vehicle.Vehicle.Id)).ToList();
                            if (gpss.Any())
                            {
                                vehicle.Vehicle.Name = gpss.First().alias?.Trim();
                            }
                        }
                    }
                }
            }

            return bitacora;
        }

    }
}