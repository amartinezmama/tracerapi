﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class CatCustomersOperativeKeysBl
    {

        /// <summary>
        /// Method to get list by filters
        /// </summary>
        /// <param name="filters">Filters</param>
        /// <param name="includes">Another Entities</param>
        /// <returns></returns>
        public IEnumerable<CustomerOpKeyEnt> GetListCustom(List<Filter> filters, string[] includes = null)
        {
            var predicate = _builder.GetFilterExpression<cat_customers_operative_keys>(filters);
            return Repository.GetList(predicate, string.Empty, includes).Select(x => new CustomerOpKeyEnt
            {
                Id = x.id_customer_operative_key,
                OperativeKey = new OperativeKeyEnt
                {
                    Id = x.id_operative_key,
                    Description = x.cat_operative_keys.description
                },
                Mail = x.email
            }).ToArray();
        }

    }
}
