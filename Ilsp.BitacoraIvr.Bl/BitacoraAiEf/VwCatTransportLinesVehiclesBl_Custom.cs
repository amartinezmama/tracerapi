﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class VwCatTransportLinesVehiclesBl
    {
        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <param name="includes">Other entities</param>
        /// <returns></returns>
        public KendoResponse GetGridCustom(KendoRequest model, string[] includes)
        {
            // order the results
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(separator: ",",
                    value: model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => string.Format(format: "{0} {1}", arg0: x.Field, arg1: x.Dir))
                        .ToArray());
            }

            if (model.Filter == null || model.Filter.Filters.Count == 0)
            {
                model.Filter = new KendoFilter
                {
                    Filters = new List<Filter>()
                };
            }

            var predicate = _builder.GetFilterExpression<vw_cat_transport_lines_vehicles>(model.Filter.Filters);
            var response = Repository.GetListPager(predicate, order, model.Skip, model.Take, includes);
            var items = response.Items.Select(item => new PlateGrid
            {
                Id = item.Id,
                NumPlate = item.NumPlate.ToUpper().Trim(),
                UnitType = item.UnitType?.ToUpper().Trim(),
                Economic = item.Economic?.ToUpper().Trim(),
                Brand = item.Brand?.ToUpper().Trim(),
                Model = item.Model?.ToUpper().Trim()
            }).ToList();
            return new KendoResponse(items.ToArray(), response.Total);
        }

    }
}
