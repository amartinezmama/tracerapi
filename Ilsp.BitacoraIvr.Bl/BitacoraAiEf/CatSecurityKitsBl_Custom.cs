﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class CatSecurityKitsBl
    {
        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <param name="includes">Other entities</param>
        /// <returns></returns>
        public KendoResponse GetGridCustom(KendoRequest model, string[] includes)
        {
            var order = string.Empty;
            var filterClientSide = new[] { "geofences", "routes" };
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(",", model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => $"{x.Field} {(x.Dir?.ToLower() == "asc" ? "asc" : "desc")}").ToArray());
            }

            // compose the filter 
            var filter = "1 == 1";
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("&&", model.Filter.Filters
                    .Where(f => filterClientSide.Any(ft => ft.ToLower().Equals(f.Field.ToLower()))).Select(f =>
                        $"{f.Field}.ToLower().Contains(\"{f.Value.ToLower()}\")").ToArray());
            }

            var filters = new List<Filter> { new Filter { Field = "id_status", Value = "1", Operator = "Equals" } };
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filters.AddRange(model.Filter.Filters.Where(f => !filterClientSide.Any(fc => fc.Equals(f.Field)))
                    .Select(
                        f => new Filter
                        {
                            Field = f.Field,
                            Value = f.Value,
                            Operator = f.Operator
                        }));
            }

            var predicate = _builder.GetFilterExpression<cat_security_kits>(filters);
            var response = Repository.GetList(predicate, string.Empty, includes);

            var items = response.Select(item =>
                new SecurityKitGridEnt
                {
                    Id = item.id_security_kit,
                    Name = item.name,
                    Description = item.description,
                    Geofences =
                        (item.cat_security_kits_georeferences.Count + item.cat_security_kits_control_points.Count).ToString(),
                    Routes = item.cat_security_kits_routes.Count.ToString()
                }).ToList();
            if (string.IsNullOrEmpty(filter))
            {
                filter = "1 == 1";
            }
            var data = items.AsQueryable().Where(filter);
            return new KendoResponse(data.OrderBy(string.IsNullOrEmpty(order) ? "true": order)
                .Skip(model.Skip)
                .Take(model.Take == 0 ? data.Count() : model.Take)
                .ToArray(), data.Count());
        }

        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <param name="customers">Customers</param>
        /// <param name="includes">Other entities</param>
        /// <returns></returns>
        public KendoResponse GetGridCustomByCustomers(KendoRequest model, IEnumerable<int> customers, string[] includes)
        {
            var order = string.Empty;
            var filterClientSide = new[] { "geofences", "routes" };
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(",", model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => $"{x.Field} {(x.Dir?.ToLower() == "asc" ? "asc" : "desc")}").ToArray());
            }

            // compose the filter 
            var filter = "1 == 1";
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("&&", model.Filter.Filters
                    .Where(f => filterClientSide.Any(ft => ft.ToLower().Equals(f.Field.ToLower()))).Select(f =>
                        $"{f.Field}.ToLower().Contains(\"{f.Value.ToLower()}\")").ToArray());
            }

            var filters = new List<Filter> { new Filter { Field = "id_status", Value = "1", Operator = "Equals" } };
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filters.AddRange(model.Filter.Filters.Where(f => !filterClientSide.Any(fc => fc.Equals(f.Field)))
                    .Select(
                        f => new Filter
                        {
                            Field = f.Field,
                            Value = f.Value,
                            Operator = f.Operator
                        }));
            }

            var predicate = _builder.GetFilterExpression<cat_security_kits>(filters);
            var response = Repository.GetList(predicate, string.Empty, includes);

            var items = response.Where(item => item.cat_customers_security_kits.Any(cs => customers.Any(c => c.Equals(cs.customer_id)))).Select(item =>
               new SecurityKitGridEnt
               {
                   Id = item.id_security_kit,
                   Name = item.name,
                   Description = item.description,
                   Geofences =
                       (item.cat_security_kits_georeferences.Count + item.cat_security_kits_control_points.Count).ToString(),
                   Routes = item.cat_security_kits_routes.Count.ToString()
               }).ToList();
            if (string.IsNullOrEmpty(filter))
            {
                filter = "1 == 1";
            }
            var data = items.AsQueryable().Where(filter);
            return new KendoResponse(data.OrderBy(string.IsNullOrEmpty(order) ? "true": order)
                .Skip(model.Skip)
                .Take(model.Take == 0 ? data.Count() : model.Take)
                .ToArray(), data.Count());
        }

        /// <summary>
        /// Method to get one item
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="includes">Other entities</param>
        /// <returns></returns>
        public SecurityKitEnt FirstCustom(int id, string[] includes)
        {
            var filters = new List<Filter>
                {new Filter {Field = "id_security_kit", Value = id.ToString(), Operator = "Equals"}};
            var predicate = _builder.GetFilterExpression<cat_security_kits>(filters);
            var response = Repository.GetList(predicate, string.Empty, includes);
            using (var blCustomers = new BitacoraAiLq.CatCustomersBl(_secondConnection, _userName))
            {
                using (var blGeofences = new CatGeofencesBl(_connection, _userName))
                {
                    using (var blFenceType = new CatFencetypeBl(_connection, _userName))
                    {
                        using (var blRoutes = new CatRoutesBl(_connection, _userName))
                        {
                            var items = new List<SecurityKitEnt>();
                            response.ToList().ForEach(item =>
                            {
                                var geofences = new List<FenceTechnologicalKitEnt>();
                                item.cat_security_kits_georeferences.ToList().ForEach(geo =>
                                {
                                    geofences.Add(new FenceTechnologicalKitEnt
                                    {
                                        Id = geo.id,
                                        FenceType = blFenceType.First(geo.fence_type_id),
                                        Geofence = blGeofences.FirstCustom(geo.geofence_id, new[] { "cat_geofences_zone_type", "cat_geofences_type" }),
                                        NotificationInGeofence = geo.noti_in_geofence,
                                        NotificationOutGeofence = geo.noti_out_geofence,
                                        NotificationTimeWait = geo.noti_time
                                    });
                                });

                                var controlPoints = new List<ControlPointEnt>();
                                item.cat_security_kits_control_points.ToList().ForEach(cp =>
                                {
                                    controlPoints.Add(new ControlPointEnt
                                    {
                                        Id = cp.id_security_kit_control_point,
                                        GeofenceInit = blGeofences.FirstCustom(cp.start_point_id, new[] { "cat_geofences_zone_type", "cat_geofences_type" }),
                                        GeofenceEnd = blGeofences.FirstCustom(cp.start_point_id, new[] { "cat_geofences_zone_type", "cat_geofences_type" }),
                                        TravelTime = cp.time ?? 0
                                    });
                                });

                                var routes = item.cat_security_kits_routes.Select(route =>
                                    blRoutes.FirstCustom(route.id_route, new[] { "cat_catalogs_common" })).ToArray();

                                items.Add(new SecurityKitEnt
                                {
                                    Id = item.id_security_kit,
                                    Name = item.description,
                                    Description = item.description,
                                    Customers = item.cat_customers_security_kits
                                        .Select(x => blCustomers.First(x.customer_id)).ToArray(),
                                    Geofences = geofences.ToArray(),
                                    ControlPoints = controlPoints.ToArray(),
                                    Routes = routes
                                });
                            });


                            if (items.Any())
                            {
                                return items.First();
                            }

                            return null;
                        }
                    }
                }
            }
        }
    }
}