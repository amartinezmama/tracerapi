﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class CatUnitTypesBl
    {
        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <returns></returns>
        public KendoResponse GetGridCustom(KendoRequest model)
        {
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(",", model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => $"{x.Field} {(x.Dir?.ToLower() == "asc" ? "asc" : "desc")}").ToArray());
            }

            // compose the filter 
            var filter = string.Empty;
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("|", model.Filter.Filters.Select(f => $"{f.Field},{f.Value.ToLower()}").ToArray());
            }

            var filters = new List<Filter> { new Filter { Field = "active", Value = "1", Operator = "Equals" } };
            var predicate = _builder.GetFilterExpression<cat_unit_types>(filters);
            var response = Repository.GetList(predicate, string.Empty);

            var items = response.Select(item =>
                new UnitTypeEnt
                {
                    Id = item.id_unit_type,
                    Name = item.description,
                    UnitsAllow = item.unitsallow,
                    MinUnitsAllow = item.minunits,
                    IsValid = item.isvalid
                }).ToList();
            var data = items.AsQueryable().Where(filter);
            return new KendoResponse(data.OrderBy(string.IsNullOrEmpty(order) ? "true": order)
                .Skip(model.Skip)
                .Take(model.Take == 0 ? data.Count() : model.Take)
                .ToArray(), data.Count());
        }
    }
}
