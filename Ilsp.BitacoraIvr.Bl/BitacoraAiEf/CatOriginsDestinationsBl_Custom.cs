﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class CatOriginsDestinationsBl
    {
        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <returns></returns>
        public KendoResponse GetGridCustom(KendoRequest model)
        {
            var order = string.Empty;
            var filterClientSide = new[] { "address", "location" };
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(",", model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => $"{x.Field} {(x.Dir?.ToLower() == "asc" ? "asc" : "desc")}").ToArray());
            }

            // compose the filter 
            var filter = "1 == 1";
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("&&", model.Filter.Filters
                    .Where(f => filterClientSide.Any(ft => ft.ToLower().Equals(f.Field.ToLower()))).Select(f =>
                        $"{f.Field}.ToLower().Contains(\"{f.Value.ToLower()}\")").ToArray());
            }

            var filters = new List<Filter> { new Filter { Field = "id_status", Value = "1", Operator = "Equals" } };
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filters.AddRange(model.Filter.Filters.Where(f => !filterClientSide.Any(fc => fc.Equals(f.Field)))
                    .Select(
                        f => new Filter
                        {
                            Field = f.Field,
                            Value = f.Value,
                            Operator = f.Operator
                        }));
            }

            var predicate = _builder.GetFilterExpression<cat_origins_destinations>(filters);
            var response = Repository.GetList(predicate, string.Empty);

            var items = response.Select(item =>
                new OriginAndDestinyGrid
                {
                    Id = item.id_origin_destination,
                    Name = item.name,
                    Address = $"{item.street} {item.number}, {item.postcode} {item.town} {item.municipality}, {item.state}",
                    Location = $"{item.latitude.ToString()}, {item.longitude.ToString()}"
                }).ToList();

            if (string.IsNullOrEmpty(filter))
            {
                filter = "1 == 1";
            }
            var data = items.AsQueryable().Where(filter);
            return new KendoResponse(data.OrderBy(string.IsNullOrEmpty(order) ? "true": order)
                .Skip(model.Skip)
                .Take(model.Take == 0 ? data.Count() : model.Take)
                .ToArray(), data.Count());
        }

        /// <summary>
        /// Method to get one item
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="includes">Other entities</param>
        /// <returns></returns>
        public OriginAndDestinyEnt FirstCustom(int id, string[] includes)
        {
            var filters = new List<Filter> { new Filter { Field = "id_origin_destination", Value = id.ToString(), Operator = "Equals" } };
            var predicate = _builder.GetFilterExpression<cat_origins_destinations>(filters);
            var response = Repository.GetList(predicate, string.Empty, includes);

            var items = response.Select(item =>
                new OriginAndDestinyEnt
                {
                    Id = item.id_origin_destination,
                    Name = item.name,
                    Alias = item.alias,
                    Address = item.address,
                    Street = item.street,
                    Town = item.town,
                    Municipality = item.municipality,
                    Number = item.number,
                    State = item.state,
                    Postcode = item.postcode,
                    Latitude = item.latitude.ToString(),
                    Longitude = item.longitude.ToString()
                }).ToList();
            if (items.Any())
            {
                return items.First();
            }

            return null;
        }
    }
}
