﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.Tl;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class CatGeofencesBl
    {
        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <param name="includes">Other entities</param>
        /// <returns></returns>
        public KendoResponse GetGridCustom(KendoRequest model, string[] includes)
        {
            var order = string.Empty;
            var filterClientSide = new[] { "zoneType", "geofenceType", "startPoint" };
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(",", model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => $"{x.Field} {(x.Dir?.ToLower() == "asc" ? "asc" : "desc")}").ToArray());
            }

            // compose the filter 
            var filter = "1 == 1";
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("&&", model.Filter.Filters
                    .Where(f => filterClientSide.Any(ft => ft.ToLower().Equals(f.Field.ToLower()))).Select(f =>
                        $"{f.Field}.ToLower().Contains(\"{f.Value.ToLower()}\")").ToArray());
            }

            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                model.Filter.Filters.Add(new Filter { Field = "id_status", Value = "1", Operator = "Equals" });
            }
            else
            {
                model.Filter = new KendoFilter
                {
                    Filters = new List<Filter> ()
                };
                model.Filter.Filters = new List<Filter> { new Filter { Field = "id_status", Value = "1", Operator = "Equals" } };
            }

            var predicate = _builder.GetFilterExpression<cat_geofences>(
                model.Filter.Filters.Where(f => !filterClientSide.Any(fc => fc.ToLower().Equals(f.Field.ToLower()))).ToList()
            );
            var response = Repository.GetList(predicate, string.Empty, includes);

            var items = response.Select(item =>
                new GeofenceGridEnt
                {
                    Id = item.id_geofence,
                    Name = item.name,
                    Description = item.description,
                    ZoneType = item.cat_geofences_zone_type.name,
                    GeofenceType = item.cat_geofences_type.name,
                    StartPoint = GooglePoints.GetFirstPoint(item.geographypoints.AsText())
                }).ToList();
            if (string.IsNullOrEmpty(filter))
            {
                filter = "1 == 1";
            }
            var data = items.AsQueryable().Where(filter);
            return new KendoResponse(data.OrderBy(string.IsNullOrEmpty(order) ? "true": order)
                .Skip(model.Skip)
                .Take(model.Take == 0 ? data.Count() : model.Take)
                .ToArray(), data.Count());
        }

        /// <summary>
        /// Method to get one item
        /// </summary>
        /// <param name="geofence">Id</param>
        /// <param name="includes">Another entities</param>
        /// <returns></returns>
        public GeofenceEnt FirstCustom(int geofence, string[] includes)
        {
            var filters = new List<Filter> { new Filter { Field = "id_geofence", Value = geofence.ToString(), Operator = "Equals" } };
            var predicate = _builder.GetFilterExpression<cat_geofences>(filters);
            var response = Repository.GetList(predicate, string.Empty, includes);

            var items = response.Select(item =>
                new GeofenceEnt
                {
                    Id = item.id_geofence,
                    Name = item.name,
                    Description = item.description,
                    ZoneType = new ZoneTypeEnt
                    {
                        Id = item.id_zone_type,
                        Name = item.cat_geofences_zone_type.name,
                        Color = item.cat_geofences_zone_type.color
                    },
                    GeofenceType = new GeofenceTypeEnt
                    {
                        Id = item.id_geofences_type,
                        Name = item.cat_geofences_type.name
                    },
                    Radio = item.radio ?? 0,
                    Color = item.color,
                    Time = item.time,
                    Points = GooglePoints.GetPoints(item.geographypoints.AsText())
                }).ToList();
            if (items.Any())
            {
                return items.First();
            }

            return null;
        }

    }
}
