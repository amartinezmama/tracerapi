﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class BitacorasStaysBl
    {
        /// <summary>
        /// Method to get list of rows in pages
        /// </summary>
        /// <param name="filters">Filters</param>
        /// <returns></returns>
        public IEnumerable<StayEnt> GetStaysByBitacora(List<Filter> filters)
        {
            var predicate = _builder.GetFilterExpression<BitacorasStays>(filters);
            var response = Repository.GetList(predicate, string.Empty).ToList();
            return response.Select(item => new StayEnt
            {
                Id = item.Id,
                BitacoraId = item.BitacoraId,                
                TimeStay = item.TimeStay,
                PlaceStay = item.PlaceStay
            }).ToArray();
        }
    }
}
