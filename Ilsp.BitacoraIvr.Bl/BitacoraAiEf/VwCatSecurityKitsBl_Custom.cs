﻿using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class VwCatSecurityKitsBl
    {

        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <param name="includes">Other entities</param>
        /// <returns></returns>
        public KendoResponse GetGridCustom(KendoRequest model, string[] includes = null)
        {
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(separator: ",",
                    value: model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => string.Format(format: "{0} {1}", arg0: x.Field, arg1: x.Dir))
                        .ToArray());
            }

            var predicate = _builder.GetFilterExpression<vw_cat_security_kits>(model.Filter.Filters);
            var response = Repository.GetListPager(predicate, order, model.Skip, model.Take, includes);

            var items = response.Items.Select(item =>
                new SecurityKitGridEnt
                {
                    Id = item.Id,
                    Name = item.Name,
                    Description = item.Description,
                    Geofences = item.Geofences,
                    Routes = item.Routes
                }).ToList();
            return new KendoResponse(items.ToArray(), response.Total);
        }

    }
}
