﻿using Ilsp.BitacoraIvr.Dl;
using Ilsp.BitacoraIvr.Ent.Common;
using System;
using System.Linq;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public class OriginDestinyBl : IDisposable
    {
        private readonly BitacoraAiDataContext _db;

        public OriginDestinyBl(BitacoraAiDataContext db)
        {
            _db = db;
        }

        public KendoResponse GetGrid(KendoRequest model)
        {
            var filterSp = string.Empty;
            var orderSP = string.Empty;

            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filterSp = string.Join("|",
                    model.Filter.Filters.Select(f => $"{f.Field},{f.Value.ToLower()}")
                        .ToArray());
            }

            if (model.Sort != null && model.Sort.Count > 0)
            {
                orderSP = string.Join("|",
                    model.Sort.Select(x => $"{x.Field},{(x.Dir == "asc" ? "1" : "0")}").ToArray());
            }

            var datatemp =
                    _db.sp_origins_destinations_get_grid(filterSp, orderSP, model.Page, model.Take)
                    .Select(item => new OriginDestinyGrid
                    {
                        Id = item.Id ?? 0,
                        Name = item.Name,
                        Alias = item.Alias,
                        Address = item.Address,
                        Street = item.Street,
                        Town = item.Town,
                        Municipality = item.Municipality,
                        Number = item.Number,
                        State = item.State,
                        Postcode = item.Postcode,
                        Latitude = item.Latitude ?? 0,
                        Longitude = item.Longitude ?? 0,
                        Total = item.Total ?? 0
                    }).ToList();

            var count = 0;
            if (datatemp.Count > 0)
            {
                count = datatemp.First().Total;
            }
            return new KendoResponse(datatemp.ToArray(), count);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

    }
}
