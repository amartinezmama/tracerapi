﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.Rp.BitacoraAi;
using Ilsp.BitacoraIvr.Tl.ExpressionBuilder.Builders;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
	public partial class BitacorasCustodiesCabinBl : BaseBl<bitacoras_custodies_cabin, BitacorasCustodiesCabinRepository>, IDisposable
	{
        private readonly string _connection;
        private readonly string _secondConnection;
        private readonly string _userName;
        private readonly ExpressionBuilder _builder;

        public BitacorasCustodiesCabinBl(string connection, string userName, string secondConnection = null)
        {
            Repository.Init(new Dl.BitacoraAi.BitacoraAi(connection));
            _connection = connection;
            _secondConnection = secondConnection;
            _userName = userName;
            _builder = new ExpressionBuilder();
        }

        /// <summary>
        /// Get al rows of bitacoras_custodies_cabin
        /// </summary>
        /// <returns></returns>
        public IEnumerable<bitacoras_custodies_cabin> GetAll()
        {
            return Repository.GetAll();
        }

        /// <summary>
        /// Method to get list by filters
        /// </summary>
        /// <param name="filters">Filters</param>
        /// <param name="sorts">Orders</param>
        /// <param name="logic">Contains or Equals</param>
        /// <param name="includes">Another Entities</param>
        /// <returns></returns>
        public IEnumerable<bitacoras_custodies_cabin> GetList(List<Filter> filters, List<KendoSort> sorts, string logic, string[] includes = null)
        {
            var order = string.Join(separator: ",",
				value: sorts.Select(x => string.Format(format: "{0} {1}", arg0: x.Field, arg1: x.Dir)).ToArray());
            var predicate = _builder.GetFilterExpression<bitacoras_custodies_cabin>(filters, logic);
            return Repository.GetList(predicate, order, includes);
        }

        /// <summary>
        /// Method to get list of rows in pages
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <param name="logic">Contains or Equals</param>
        /// <param name="includes">Another Entity</param>
        /// <returns></returns>
        public KendoResponse GetListPager(KendoRequest model, string logic, string[] includes = null)
        {
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(separator: ",",
                    value: model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => string.Format(format: "{0} {1}", arg0: x.Field, arg1: x.Dir))
                        .ToArray());
            }
            var predicate = _builder.GetFilterExpression<bitacoras_custodies_cabin>(model.Filter.Filters, logic);
            var response = Repository.GetListPager(predicate, order, model.Skip, model.Take, includes);
            return new KendoResponse(response.Items.ToArray(), response.Total);
        }

        /// <summary>
        /// Select one row
        /// </summary>
        /// <param name="propertiesCondition">Properties to make condition for search row</param>
        /// <param name="includes">Another Entity</param>
        /// <returns></returns>
        public bitacoras_custodies_cabin GetOne(List<Filter> propertiesCondition, string[] includes = null)
        {
            var predicate = _builder.GetFilterExpression<bitacoras_custodies_cabin>(propertiesCondition);
            return Repository.GetFirst(predicate, includes);
        }

        /// <summary>
        /// Save new register on bitacoras_custodies_cabin
        /// </summary>
        /// <param name="item">Row to create</param>
        /// <param name="propertiesCondition">Properties to make condition for search duplicate rows</param>
        /// <param name="errors"></param>
        /// <param name="condition">Condition</param>
        /// <returns></returns>
        public bool Save(bitacoras_custodies_cabin item, List<Filter> propertiesCondition, List<string> errors, string condition = "AND")
        {
            var predicate = _builder.GetFilterExpression<bitacoras_custodies_cabin>(propertiesCondition, condition);
            var itemonDb = Repository.GetFirstOrDefault(predicate);
            if (itemonDb == null)
            {
                return Repository.Save(item);
            }
            errors.Add(Resources.Resource.The_Name_Already_Exist);
            return false;
        }

        /// <summary>
        /// Save new register on bitacoras_custodies_cabin
        /// </summary>
        /// <param name="item">Row to create</param>
        /// <param name="propertiesCondition">Properties to make condition for search duplicate rows</param>
        /// <returns></returns>
        public bool SaveWithOutTransaction(bitacoras_custodies_cabin item, List<Filter> propertiesCondition)
        {
            var predicate = _builder.GetFilterExpression<bitacoras_custodies_cabin>(propertiesCondition);
            var itemonDb = Repository.GetFirstOrDefault(predicate);
            if (itemonDb == null)
            {
                return Repository.SaveWithOutTransaction(item);
            }
            //Mensaje el registro ya existe
            return false;
        }

        /// <summary>
        /// Update from bitacoras_custodies_cabin
        /// </summary>
        /// <param name="propertiesCondition">Properties to make condition for get row to update</param>
        /// <param name="action">Specific roperties to update</param>
        /// <returns></returns>
        public bool Update(List<Filter> propertiesCondition, Action<bitacoras_custodies_cabin> action)
        {
            var predicate = _builder.GetFilterExpression<bitacoras_custodies_cabin>(propertiesCondition);
            var itemonDb = Repository.GetFirst(predicate);
            if (itemonDb == null)
            {
                //Mensaje el registro no existe
                return false;
            }
            return Repository.Update(itemonDb, action);
        }

        /// <summary>
        /// Update from bitacoras_custodies_cabin
        /// </summary>
        /// <param name="propertiesCondition">Properties to make condition for get row to update</param>
        /// <param name="action">Specific roperties to update</param>
        /// <returns></returns>
        public bool UpdateWithOutTransaction(List<Filter> propertiesCondition, Action<bitacoras_custodies_cabin> action)
        {
            var predicate = _builder.GetFilterExpression<bitacoras_custodies_cabin>(propertiesCondition);
            var itemonDb = Repository.GetFirst(predicate);
            if (itemonDb == null)
            {
                //Mensaje el registro no existe
                return false;
            }
            return Repository.UpdateWithOutTransaction(itemonDb, action);
        }

        /// <summary>
        /// Physical delete from bitacoras_custodies_cabin
        /// </summary>
        /// <param name="propertiesCondition">Properties to make condition for get row to delete</param>
        /// <returns></returns>
        public bool Delete(List<Filter> propertiesCondition)
        {
            var predicate = _builder.GetFilterExpression<bitacoras_custodies_cabin>(propertiesCondition);
            var itemonDb = Repository.GetFirst(predicate);
            if (itemonDb == null)
            {
                //Mensaje el registro no existe
                return false;
            }
            return Repository.Delete(itemonDb);
        }

        /// <summary>
        /// Physical delete from files_gps_historic
        /// </summary>
        /// <param name="propertiesCondition">Properties to make condition for get row to delete</param>
        /// <param name="condition">AND or OR</param>
        /// <returns></returns>
        public bool DeleteMultiple(List<Filter> propertiesCondition, string condition)
        {
            var predicate = _builder.GetFilterExpression<bitacoras_custodies_cabin>(propertiesCondition, condition);
            var itemsonDb = Repository.GetList(predicate, string.Empty).ToList();
            if (!itemsonDb.Any())
            {
                //Mensaje el registro no existe
                return false;
            }
            return Repository.DeleteMassive(itemsonDb);
        }

        /// <summary>
        /// Physical delete from files_gps_historic
        /// </summary>
        /// <param name="propertiesCondition">Properties to make condition for get row to delete</param>
        /// <param name="condition">AND or OR</param>
        /// <returns></returns>
        public bool DeleteMultipleWithOutTransaction(List<Filter> propertiesCondition, string condition)
        {
            var predicate = _builder.GetFilterExpression<bitacoras_custodies_cabin>(propertiesCondition, condition);
            var itemsonDb = Repository.GetList(predicate, string.Empty).ToList();
            if (!itemsonDb.Any())
            {
                //Mensaje el registro no existe
                return false;
            }
            return Repository.DeleteMassiveWithOutTransaction(itemsonDb);
        }

        /// <summary>
        /// Physical delete from bitacoras_custodies_cabin
        /// </summary>
        /// <param name="propertiesCondition">Properties to make condition for get row to delete</param>
        /// <returns></returns>
        public bool DeleteWithOutTransaction(List<Filter> propertiesCondition)
        {
            var predicate = _builder.GetFilterExpression<bitacoras_custodies_cabin>(propertiesCondition);
            var itemonDb = Repository.GetFirst(predicate);
            if (itemonDb == null)
            {
                //Mensaje el registro no existe
                return false;
            }
            return Repository.DeleteWithOutTransaction(itemonDb);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
	}
}
