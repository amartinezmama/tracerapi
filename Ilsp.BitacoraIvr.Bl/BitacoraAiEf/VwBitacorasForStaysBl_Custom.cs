﻿namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    //public partial class VwBitacorasForStaysBl
    //{
    //    /// <summary>
    //    /// Method to get list of rows in pages
    //    /// </summary>
    //    /// <param name="model">Object that contains data to create pages</param>
    //    /// <param name="includes">Another Entity</param>
    //    /// <returns></returns>
    //    public KendoResponse GetGridBitacorasForStays(KendoRequest model, string[] includes = null)
    //    {
    //        var order = string.Empty;
    //        // order the results
    //        if (model.Sort != null && model.Sort.Count > 0)
    //        {
    //            order = string.Join(separator: ",",
    //                value: model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => string.Format(format: "{0} {1}", arg0: x.Field, arg1: x.Dir))
    //                    .ToArray());
    //        }

    //        var predicate = _builder.GetFilterExpression<vw_bitacoras_for_stays>(model.Filter.Filters);
    //        var response = Repository.GetListPager(predicate, order, model.Skip, model.Take, includes);
    //        var items = response.Items.Select(item => new BitacoraStayEnt
    //        {
    //            BitacoraId = item.BitacoraId,
    //            Alias = item.Alias,
    //            CustomerId = item.CustomerId,
    //            CustomerName = item.CustomerName,
    //            AxCustomerId = item.AxCustomerId,
    //            AxCustomerName = item.AxCustomerName,
    //            ProjectId = item.ProjectId,
    //            ProjectName = item.ProjectName,
    //            CustodyFolio = item.CustodyFolio,
    //            OriginId = item.OriginId,
    //            OriginName = item.OriginName,
    //            Destiny = item.Destiny,
    //        }).ToArray();
    //        return new KendoResponse(items, response.Total);
    //    }

    //    /// <summary>
    //    /// Method to get list of rows in pages
    //    /// </summary>
    //    /// <param name="filters">Filters</param>
    //    /// <returns></returns>
    //    public BitacoraStayEnt GetOneBitacorasForStays(List<Filter> filters)
    //    {
    //        var predicate = _builder.GetFilterExpression<vw_bitacoras_for_stays>(filters);
    //        var response = Repository.GetList(predicate, string.Empty).ToList();
    //        if (response.Any())
    //        {
    //            var item = response.First();
    //            return new BitacoraStayEnt
    //            {
    //                BitacoraId = item.BitacoraId,
    //                Alias = item.Alias,
    //                CustomerId = item.CustomerId,
    //                CustomerName = item.CustomerName,
    //                AxCustomerId = item.AxCustomerId,
    //                AxCustomerName = item.AxCustomerName,
    //                ProjectId = item.ProjectId,
    //                ProjectName = item.ProjectName,
    //                CustodyFolio = item.CustodyFolio,
    //                OriginId = item.OriginId,
    //                OriginName = item.OriginName,
    //                Destiny = item.Destiny,
    //            };
    //        }

    //        return null;
    //    }
    //}
}
