﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class CatCustomersUsersBl
    {
        /// <summary>
        /// Method to get list by filters
        /// </summary>
        /// <param name="kendoFilters"></param>
        /// <param name="sorts">Orders</param>
        /// <param name="includes">Another Entities</param>
        /// <returns></returns>
        public IEnumerable<cat_customers_users> GetListCustom(List<KendoFilter> kendoFilters, IEnumerable<KendoSort> sorts, string[] includes = null)
        {
            var order = string.Join(separator: ",",
                value: sorts.Select(x => string.Format(format: "{0} {1}", arg0: x.Field, arg1: x.Dir)).ToArray());
            var predicate = _builder.GetFilterExpressionMany<cat_customers_users>(kendoFilters);
            return Repository.GetList(predicate, order, includes);
        }
    }
}
