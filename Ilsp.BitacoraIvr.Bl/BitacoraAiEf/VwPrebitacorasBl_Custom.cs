﻿using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using System.Linq;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class VwPrebitacorasBl
    {

        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <param name="canSeeBitacoras">Allow all bitacoras</param>
        /// <returns></returns>
        public KendoResponse GetGridPreBitacoras(KendoRequest model, bool canSeeBitacoras, string[] includes = null)
        {
            var order = string.Empty;
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(",", model.Sort.Select(x => string.Format(format: "{0} {1}", arg0: x.Field, arg1: x.Dir)).ToList());
            }

            if (!canSeeBitacoras)
            {
                model.Filter.Filters.Add(new Filter { Field = "UsersAssigned", Value = _userName, Operator = "Contains" });
            }

            var predicate = _builder.GetFilterExpression<vw_prebitacoras>(model.Filter.Filters.ToList());
            var response = Repository.GetListPager(predicate, order, model.Skip, model.Take, includes);
            var items = response.Items.Select(item => new BitacoraGrid
            {
                Id = item.Id.ToString(),
                Alias = item.Alias,
                Observations = item.Observations,
                CreateDate = item.CreateDate,
                TimeInactive = item.TimeInactive,
                StatusId = item.StatusId,
                Status = item.Status,
                Customer = item.Customer,
                AxCustomer = item.AxCustomer,
                ServiceTypeName = item.ServiceTypeName,
                ProjectName = item.ProjectName,
                AgreementName = item.AgreementName,
                Origin = item.Origin,
                CustodyFolio = item.CustodyFolio,
                ProductName = item.ProductName,
                MonitoringTypeName = item.MonitoringTypeName,
                TransportLineName = item.TransportLineName,
                SecurityKitName = item.SecurityKitName,
                ServiceOrder = item.ServiceOrder
            }).ToList();
            return new KendoResponse(items.ToArray(), response.Total);
        }

    }
}
