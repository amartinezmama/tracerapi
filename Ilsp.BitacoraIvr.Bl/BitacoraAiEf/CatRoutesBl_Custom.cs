﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;
using Ilsp.BitacoraIvr.Tl;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class CatRoutesBl
    {
        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <param name="includes">Other entities</param>
        /// <returns></returns>
        public KendoResponse GetGridCustom(KendoRequest model, string[] includes)
        {
            var order = string.Empty;
            var filterClientSide = new[] { "relevance", "originPointStr", "destinyPointStr" };
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(",", model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => $"{x.Field} {(x.Dir?.ToLower() == "asc" ? "asc" : "desc")}").ToArray());
            }

            // compose the filter 
            var filter = "1 == 1";
            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                filter = string.Join("&&", model.Filter.Filters
                    .Where(f => filterClientSide.Any(ft => ft.ToLower().Equals(f.Field.ToLower()))).Select(f =>
                        $"{f.Field}.ToLower().Contains(\"{f.Value.ToLower()}\")").ToArray());
            }

            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                model.Filter.Filters.Add(new Filter { Field = "id_status", Value = "1", Operator = "Equals" });
            }
            else
            {
                model.Filter = new KendoFilter
                {
                    Filters = new List<Filter>()
                };
                model.Filter.Filters = new List<Filter> { new Filter { Field = "id_status", Value = "1", Operator = "Equals" } };
            }

            var predicate = _builder.GetFilterExpression<cat_routes>(
                model.Filter.Filters.Where(f => !filterClientSide.Any(fc => fc.ToLower().Equals(f.Field.ToLower()))).ToList()
            );
            var response = Repository.GetList(predicate, string.Empty, includes);

            var items = response.Select(item =>
                new RouteGrid
                {
                    Id = item.id_route,
                    Name = item.name,
                    Description = item.description,
                    Relevance = item.cat_catalogs_common.cat_type_name,
                    Distance = item.distance.ToString(),
                    Time = item.time,
                    OriginAddress = item.originaddress,
                    OriginPointStr = GooglePoints.GetFirstPoint(item.waypoints.AsText()),
                    DestinyAddress = item.destinyaddress,
                    DestinyPointStr = GooglePoints.GetLastPoint(item.waypoints.AsText())
                }).ToList();
            if (string.IsNullOrEmpty(filter))
            {
                filter = "1 == 1";
            }
            var data = items.AsQueryable().Where(filter);
            return new KendoResponse(data.OrderBy(string.IsNullOrEmpty(order) ? "true": order)
                .Skip(model.Skip)
                .Take(model.Take == 0 ? data.Count() : model.Take)
                .ToArray(), data.Count());
        }

        /// <summary>
        /// Method to get one item
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="includes">Another entities</param>
        /// <returns></returns>
        public RouteEnt FirstCustom(int id, string[] includes)
        {
            var filters = new List<Filter> { new Filter { Field = "id_route", Value = id.ToString(), Operator = "Equals" } };
            var predicate = _builder.GetFilterExpression<cat_routes>(filters);
            var response = Repository.GetList(predicate, string.Empty, includes);

            var items = response.Select(item =>
                new RouteEnt
                {
                    Id = item.id_route,
                    Name = item.name,
                    Description = item.description,
                    Radio = item.radio,
                    Relevance = new RelevanceEnt
                    {
                        Id = item.cat_catalogs_common.cat_type_id,
                        Name = item.cat_catalogs_common.cat_type_name,
                        Description = item.cat_catalogs_common.cat_type_description,
                        Color = item.cat_catalogs_common.cat_color,
                        Type = item.cat_catalogs_common.cat_type
                    },
                    Distance = item.distance,
                    Time = item.time,
                    //PointsEncoded
                    OriginAddress = item.originaddress,
                    OriginPoint = GooglePoints.GetPoints(item.waypoints.AsText()).FirstOrDefault(),
                    DestinyAddress = item.destinyaddress,
                    DestinyPoint = GooglePoints.GetPoints(item.waypoints.AsText()).LastOrDefault(),
                    Waypoints = GooglePoints.GetPoints(item.waypoints.AsText()),
                    Points = GooglePoints.GetPoints(item.geom.AsText())
                }).ToList();
            if (items.Any())
            {
                return items.First();
            }

            return null;
        }

    }
}
