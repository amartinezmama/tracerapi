﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class VwDynamicRoutesBl
    {
        /// <summary>
        /// Method to get list by filters
        /// </summary>
        /// <param name="bitacoraId">BitacoraId</param>
        /// <returns></returns>
        public BitacoraDynamicRoutesEnt GetByBitacora(string bitacoraId)
        {
            var filters = new List<Filter> { new Filter { Field = "BitacoraId", Operator = "Equals", Value = bitacoraId } };
            var predicate = _builder.GetFilterExpression<vw_DynamicRoutes>(filters);
            var response = Repository.GetList(predicate, string.Empty).ToList();
            if (response.Any())
            {
                var item = response.First();
                var gpsList = new List<GpsEnt>();
                var routeList = new List<RouteEnt>();
                var alertList = new List<AlertEnt>();
                if (!string.IsNullOrEmpty(item.Gpss))
                {
                    var gpss = item.Gpss.Split(',');
                    foreach (var gpsItem in gpss)
                    {
                        var gpsSplit = gpsItem.Split('|');
                        gpsList.Add(new GpsEnt
                        {
                            Id = gpsSplit[0],
                            Name = gpsSplit[1]
                        });
                    }
                }

                if (!string.IsNullOrEmpty(item.Routes))
                {
                    var routes = item.Routes.Split(',');
                    using (var bl = new CatRoutesBl(_connection, _userName))
                    {
                        routeList.AddRange(routes.Select(routeItem =>
                            bl.FirstCustom(int.Parse(routeItem), new[] { "cat_catalogs_common" })));
                    }
                }

                if (!string.IsNullOrEmpty(item.Alerts))
                {
                    var alerts = item.Alerts.Split(',');
                    using (var bl = new BitacorasAlertsBl(_connection, _userName))
                    {
                        foreach (var alertItem in alerts)
                        {
                            filters = new List<Filter>
                            {
                                new Filter {Field = "id_bitacora_alert", Operator = "Equals", Value = alertItem}
                            };
                            var alertItemDb = bl.GetOne(filters, new[] { "cat_alerts" });
                            alertList.Add(new AlertEnt
                            {
                                Id = alertItemDb.alert_id,
                                Name = alertItemDb.cat_alerts.description,
                                Latitude = (alertItemDb.latitude ?? 0).ToString(CultureInfo.InvariantCulture),
                                Longitude = (alertItemDb.longitude ?? 0).ToString(CultureInfo.InvariantCulture),
                                Image = alertItemDb.cat_alerts.image
                            });
                        }
                    }
                }

                return new BitacoraDynamicRoutesEnt
                {
                    BitacoraId = item.BitacoraId.ToString(CultureInfo.InvariantCulture),
                    Alias = item.Alias,
                    Gpss = gpsList,
                    Routes = routeList,
                    Alerts = alertList
                };
            }

            return null;
        }

    }
}
