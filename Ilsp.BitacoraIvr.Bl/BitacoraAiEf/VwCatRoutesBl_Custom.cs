﻿using System.Collections.Generic;
using System.Linq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.Common;
using Ilsp.BitacoraIvr.EntBitacoraAi;

namespace Ilsp.BitacoraIvr.Bl.BitacoraAiEf
{
    public partial class VwCatRoutesBl
    {

        /// <summary>
        /// Method to get all of rows by page
        /// </summary>
        /// <param name="model">Object that contains data to create pages</param>
        /// <param name="includes">Other entities</param>
        /// <returns></returns>
        public KendoResponse GetGridCustom(KendoRequest model, string[] includes)
        {
            var order = string.Empty;
            var filterClientSide = new[] { "originPointStr", "destinyPointStr" };
            // order the results
            if (model.Sort != null && model.Sort.Count > 0)
            {
                order = string.Join(separator: ",",
                    value: model.Sort.Where(x => !string.IsNullOrEmpty(x.Dir)).Select(x => string.Format(format: "{0} {1}", arg0: x.Field, arg1: x.Dir))
                        .ToArray());
            }

            if (model.Filter != null && model.Filter.Filters.Count > 0)
            {
                model.Filter.Filters.Add(new Filter { Field = "id_status", Value = "1", Operator = "Equals" });
            }
            else
            {
                model.Filter = new KendoFilter
                {
                    Filters = new List<Filter>()
                };
                model.Filter.Filters = new List<Filter> { new Filter { Field = "id_status", Value = "1", Operator = "Equals" } };
            }

            var predicate = _builder.GetFilterExpression<vw_cat_routes>(
                model.Filter.Filters.Where(f => !filterClientSide.Any(fc => fc.ToLower().Equals(f.Field.ToLower()))).ToList()
            );
            var response = Repository.GetListPager(predicate, order, model.Skip, model.Take, includes);

            var items = response.Items.Select(item =>
                new RouteGrid
                {
                    Id = item.id,
                    Name = item.name,
                    Description = item.description,
                    Relevance = item.relevance,
                    Distance = item.distance.ToString(),
                    Time = item.time,
                    OriginAddress = item.originaddress,
                    //OriginPointStr = GooglePoints.GetFirstPoint(item.waypoints.AsText()),
                    DestinyAddress = item.destinyaddress,
                    //DestinyPointStr = GooglePoints.GetLastPoint(item.waypoints.AsText())
                }).ToList();
            return new KendoResponse(items.ToArray(), response.Total);
        }

    }
}
