﻿using System;
using System.Threading.Tasks;
using Ilsp.Common.Geocoding;
using Ilsp.Common.Geocoding.BusinessEntities;
using Ilsp.Common.Geocoding.BusinessEntities.Enums;

namespace Ilsp.BitacoraIvr.Bl.Geocoder
{
    public class ReverseGeocodingBI
    {
        public bool IsInitialized;
        private ReverseGeocoding _geocoder;

        #region Singleton

        private static ReverseGeocodingBI _instance;

        public static ReverseGeocodingBI Instance => _instance ?? (_instance = new ReverseGeocodingBI());

        public ReverseGeocodingBI()
        {
            IsInitialized = false;
        }

        public void Initialize(string geocodingConnectionString)
        {
            _geocoder = new ReverseGeocoding();
            IsInitialized = true;
        }

        #endregion

        public async Task<Address> GetAddress(double? latitude, double? longitude, MapEngine engine = MapEngine.LocalPg, int tries = 3)
        {
            try
            {
                return await _geocoder.GetAddressByPointAsync(latitude, longitude, engine);
            }
            catch (Exception)
            {
                if (tries > 0)
                {
                    return await GetAddress(latitude, longitude, MapEngine.HereMaps, --tries);
                }
                return new Address();
            }
        }
    }
}
