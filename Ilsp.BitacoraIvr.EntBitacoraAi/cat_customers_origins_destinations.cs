//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ilsp.BitacoraIvr.EntBitacoraAi
{
    
    using System;
    using System.Collections.Generic;
    
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Ilsp.BitacoraIvr.Res;
    using Ilsp.BitacoraIvr.Dl.BitacoraAi;
    using System.Runtime.Serialization;
    
    [DataContract(Namespace = "Ilsp Bitacora Ai")]
    [Table("cat_customers_origins_destinations")]
    public partial class cat_customers_origins_destinations
    {
        public cat_customers_origins_destinations()
        {
    		Initialize();
        }
    
    	[DataMember]
    	[Key]
    	[Column(Order = 0)]
    	[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int customer_id { get; set; }
    
    	[DataMember]
    	[Key]
    	[Column(Order = 1)]
    	[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int origin_destination_id { get; set; }
    
    	[DataMember]
    	[Key]
    	[Column(Order = 2)]
    	[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public byte type { get; set; }
    
    	[DataMember]
        [Required(ErrorMessageResourceName = ResourcesStrings.Entity_Generic_RequiredField, ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [StringLength(15, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string user_mod { get; set; }
    
    	[DataMember]
        public System.DateTime create_date { get; set; }
    
    	[DataMember]
        public Nullable<System.DateTime> mod_date { get; set; }
    
    	[DataMember]
    	
        public virtual cat_customers cat_customers { get; set; }
    
    	[DataMember]
    	
        public virtual cat_origins_destinations cat_origins_destinations { get; set; }
    
    	partial void Initialize();
    	
    }
}
