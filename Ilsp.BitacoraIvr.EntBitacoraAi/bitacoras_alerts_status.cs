//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ilsp.BitacoraIvr.EntBitacoraAi
{
    
    using System;
    using System.Collections.Generic;
    
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Ilsp.BitacoraIvr.Res;
    using Ilsp.BitacoraIvr.Dl.BitacoraAi;
    using System.Runtime.Serialization;
    
    [DataContract(Namespace = "Ilsp Bitacora Ai")]
    [Table("bitacoras_alerts_status")]
    public partial class bitacoras_alerts_status
    {
        public bitacoras_alerts_status()
        {
    		Initialize();
        }
    
    	[DataMember]
    	[Key]
        public int id_bitacoras_alerts_status { get; set; }
    
    	[DataMember]
        public decimal id_bitacora { get; set; }
    
    	[DataMember]
        [StringLength(50, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string id_gps { get; set; }
    
    	[DataMember]
        public int id_alert { get; set; }
    
    	[DataMember]
        public int minutes_disabled { get; set; }
    
    	[DataMember]
        [Required(ErrorMessageResourceName = ResourcesStrings.Entity_Generic_RequiredField, ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [StringLength(150, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string observations { get; set; }
    
    	[DataMember]
        [Required(ErrorMessageResourceName = ResourcesStrings.Entity_Generic_RequiredField, ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [StringLength(15, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string user_create { get; set; }
    
    	[DataMember]
        public System.DateTime create_date { get; set; }
    
    	[DataMember]
        [StringLength(15, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string user_cancel { get; set; }
    
    	[DataMember]
        public Nullable<System.DateTime> cancel_date { get; set; }
    
    	[DataMember]
        public short status { get; set; }
    
    	[DataMember]
    	
        public virtual bitacoras bitacoras { get; set; }
    
    	[DataMember]
    	
        public virtual cat_alerts cat_alerts { get; set; }
    
    	[DataMember]
    	
        public virtual cat_gps cat_gps { get; set; }
    
    	partial void Initialize();
    	
    }
}
