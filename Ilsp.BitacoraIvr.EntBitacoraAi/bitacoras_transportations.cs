//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ilsp.BitacoraIvr.EntBitacoraAi
{
    
    using System;
    using System.Collections.Generic;
    
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Ilsp.BitacoraIvr.Res;
    using Ilsp.BitacoraIvr.Dl.BitacoraAi;
    using System.Runtime.Serialization;
    
    [DataContract(Namespace = "Ilsp Bitacora Ai")]
    [Table("bitacoras_transportations")]
    public partial class bitacoras_transportations
    {
        public bitacoras_transportations()
        {
    		Initialize();
            this.bitacoras_transportations_gps = new HashSet<bitacoras_transportations_gps>();
            this.bitacoras_transportations_operators = new HashSet<bitacoras_transportations_operators>();
            this.bitacoras_transportations_colors = new HashSet<bitacoras_transportations_colors>();
            this.bitacoras_transportations_stamps = new HashSet<bitacoras_transportations_stamps>();
        }
    
    	[DataMember]
    	[Key]
        public int id_bitacora_transportation { get; set; }
    
    	[DataMember]
        public decimal id_bitacora { get; set; }
    
    	[DataMember]
        public Nullable<int> id_vehicle { get; set; }
    
    	[DataMember]
        [StringLength(500, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string comments { get; set; }
    
    	[DataMember]
        public Nullable<int> id_convoy { get; set; }
    
    	[DataMember]
        [StringLength(25, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string stamp { get; set; }
    
    	[DataMember]
        public Nullable<int> id_unit_type { get; set; }
    
    	[DataMember]
        [StringLength(20, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string economic { get; set; }
    
    	[DataMember]
        public Nullable<int> id_brand { get; set; }
    
    	[DataMember]
        public Nullable<int> model { get; set; }
    
    	[DataMember]
    	
        public virtual ICollection<bitacoras_transportations_gps> bitacoras_transportations_gps { get; set; }
    
    	[DataMember]
    	
        public virtual ICollection<bitacoras_transportations_operators> bitacoras_transportations_operators { get; set; }
    
    	[DataMember]
    	
        public virtual cat_brands cat_brands { get; set; }
    
    	[DataMember]
    	
        public virtual cat_transport_lines_vehicles cat_transport_lines_vehicles { get; set; }
    
    	[DataMember]
    	
        public virtual cat_unit_types cat_unit_types { get; set; }
    
    	[DataMember]
    	
        public virtual ICollection<bitacoras_transportations_colors> bitacoras_transportations_colors { get; set; }
    
    	[DataMember]
    	
        public virtual convoys convoys { get; set; }
    
    	[DataMember]
    	
        public virtual ICollection<bitacoras_transportations_stamps> bitacoras_transportations_stamps { get; set; }
    
    	[DataMember]
    	
        public virtual bitacoras bitacoras { get; set; }
    
    	partial void Initialize();
    	
    }
}
