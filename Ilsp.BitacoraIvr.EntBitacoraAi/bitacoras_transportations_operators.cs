//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ilsp.BitacoraIvr.EntBitacoraAi
{
    
    using System;
    using System.Collections.Generic;
    
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Ilsp.BitacoraIvr.Res;
    using Ilsp.BitacoraIvr.Dl.BitacoraAi;
    using System.Runtime.Serialization;
    
    [DataContract(Namespace = "Ilsp Bitacora Ai")]
    [Table("bitacoras_transportations_operators")]
    public partial class bitacoras_transportations_operators
    {
        public bitacoras_transportations_operators()
        {
    		Initialize();
        }
    
    	[DataMember]
    	[Key]
        public int operator_id { get; set; }
    
    	[DataMember]
        public int id_bitacora_transportation { get; set; }
    
    	[DataMember]
        [Required(ErrorMessageResourceName = ResourcesStrings.Entity_Generic_RequiredField, ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [StringLength(100, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string name { get; set; }
    
    	[DataMember]
        [Required(ErrorMessageResourceName = ResourcesStrings.Entity_Generic_RequiredField, ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [StringLength(20, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string cellphone { get; set; }
    
    	[DataMember]
        [StringLength(20, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string second_cellphone { get; set; }
    
    	[DataMember]
    	
        public virtual bitacoras_transportations bitacoras_transportations { get; set; }
    
    	partial void Initialize();
    	
    }
}
