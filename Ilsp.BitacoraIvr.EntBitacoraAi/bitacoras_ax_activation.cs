//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ilsp.BitacoraIvr.EntBitacoraAi
{
    
    using System;
    using System.Collections.Generic;
    
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Ilsp.BitacoraIvr.Res;
    using Ilsp.BitacoraIvr.Dl.BitacoraAi;
    using System.Runtime.Serialization;
    
    [DataContract(Namespace = "Ilsp Bitacora Ai")]
    [Table("bitacoras_ax_activation")]
    public partial class bitacoras_ax_activation
    {
        public bitacoras_ax_activation()
        {
    		Initialize();
        }
    
    	[DataMember]
    	[Key]
        public int id { get; set; }
    
    	[DataMember]
        public decimal id_bitacora { get; set; }
    
    	[DataMember]
        [StringLength(50, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string ax_customer_id { get; set; }
    
    	[DataMember]
        [StringLength(50, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string ax_proyect_id { get; set; }
    
    	[DataMember]
        public Nullable<long> ax_agreement_id { get; set; }
    
    	[DataMember]
        [StringLength(20, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string custody_folio { get; set; }
    
    	[DataMember]
        [StringLength(50, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string ax_service_order { get; set; }
    
    	[DataMember]
        [StringLength(1, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string status { get; set; }
    
    	[DataMember]
        public Nullable<System.DateTime> create_date { get; set; }
    
    	[DataMember]
        [StringLength(15, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string update_user { get; set; }
    
    	[DataMember]
        public Nullable<System.DateTime> update_date { get; set; }
    
    	[DataMember]
        [StringLength(300, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string message { get; set; }
    
    	[DataMember]
        [StringLength(500, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string ReasonCancellationComment { get; set; }
    
    	[DataMember]
        public Nullable<int> ReasonCancellationId { get; set; }
    
    	[DataMember]
        public Nullable<System.DateTime> ReasonCancellationDate { get; set; }
    
    	[DataMember]
        public Nullable<bool> SentToBilling { get; set; }
    
    	[DataMember]
        public Nullable<System.DateTime> SentToBillingDate { get; set; }
    
    	[DataMember]
        public Nullable<int> CancellationFolioStatusId { get; set; }
    
    	[DataMember]
        public Nullable<int> ServiceTypeId { get; set; }
    
    	[DataMember]
        [StringLength(500, ErrorMessageResourceName = ResourcesStrings.Entity_Generic_MaxLengthField, ErrorMessageResourceType = typeof(Resource))]
        public string CancellationRejectedComment { get; set; }
    
    	[DataMember]
        public Nullable<int> EmailIdUserRequestCancellation { get; set; }
    
    	[DataMember]
        public Nullable<int> BitacorasStaysId { get; set; }
    
    	[DataMember]
        public Nullable<bool> IsFalseMovement { get; set; }
    
    	[DataMember]
        public Nullable<System.DateTime> EndServiceDate { get; set; }
    
    	[DataMember]
    	
        public virtual bitacoras bitacoras { get; set; }
    
    	[DataMember]
    	
        public virtual BitacorasStays BitacorasStays { get; set; }
    
    	[DataMember]
    	
        public virtual CatReasonCancellation CatReasonCancellation { get; set; }
    
    	partial void Initialize();
    	
    }
}
