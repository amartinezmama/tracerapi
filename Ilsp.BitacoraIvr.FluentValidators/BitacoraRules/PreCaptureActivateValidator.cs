﻿using System;
using System.Linq;
using FluentValidation;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.FluentValidators.BitacoraRules
{
    public class PreCaptureActivateValidator : AbstractValidator<BitacoraEnt>, IDisposable
    {

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// Validates the parameters when the Pre-Capture is Activated
        /// </summary>
        public PreCaptureActivateValidator()
        {
            RuleFor(bitacora => bitacora.Customer)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Customer));
            RuleFor(bitacora => bitacora.BusinessCustomer)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.BusinessName));
            RuleFor(bitacora => bitacora.ServiceType)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.ServiceType));
            RuleFor(bitacora => bitacora.Project)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Project));
            RuleFor(bitacora => bitacora.Agreement)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Agreement));
            RuleFor(bitacora => bitacora.Origin)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Origin));
            RuleFor(bitacora => bitacora.Destinies)
                .NotNull().Must(bitacora => bitacora.Any())
                .WithMessage(string.Format(Resources.Resource.At_Least_Item_Is_Required, Resources.Resource.Destiny));
            When(bitacora => bitacora.ServiceType != null && bitacora.Destinies.Any(), () =>
            {
                RuleFor(bitacora => bitacora)
                    .Must(x => x.Destinies.Count <= x.ServiceType.Destinations)
                    .WithMessage(x => string.Format(Resources.Resource.Number_Of_Items_Exceeded_For_Field, Resources.Resource.Destinations, Resources.Resource.ServiceType));
            });
            RuleFor(bitacora => bitacora.MonitoringType)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.MonitoringType));
            RuleFor(bitacora => bitacora.TransportLine)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.TransportLine));
            RuleFor(bitacora => bitacora.Transport.Operators)
                .Must(bitacora => bitacora.Count <= 2)
                .WithMessage(string.Format(Resources.Resource.Max_Items_Is_Required, "2", Resources.Resource.Operators));
            RuleFor(bitacora => bitacora.Transport.Plate)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Plates));
            RuleFor(bitacora => bitacora.Transport.Gpss.Where(g => g.Type != null))
                .Must(bitacora => bitacora.Count() <= 2)
                .WithMessage(string.Format(Resources.Resource.Max_Items_Is_Required, "2", Resources.Resource.Gps));
            When(bitacora => bitacora.Transport.Plate != null, () =>
            {
                When(bitacora => bitacora.Transport.Plate.UnitType.MinUnitsAllow > 0, () =>
                {
                    RuleFor(bitacora => bitacora)
                        .Must(x => x.Transport.Containers.Count >= x.Transport.Plate.UnitType.MinUnitsAllow)
                        .WithMessage(x => string.Format(Resources.Resource.Min_Items_Is_Required, x.Transport.Plate.UnitType.MinUnitsAllow, Resources.Resource.Container));
                    RuleFor(bitacora => bitacora)
                        .Must(x => x.Transport.Containers.Count <= x.Transport.Plate.UnitType.UnitsAllow)
                        .WithMessage(x => string.Format(Resources.Resource.Max_Items_Is_Required, x.Transport.Plate.UnitType.UnitsAllow, Resources.Resource.Container));
                });
                When(bitacora => bitacora.Transport.Plate.UnitType.MinUnitsAllow.Equals(0), () =>
                {
                    RuleFor(bitacora => bitacora)
                        .Must(x => !x.Transport.Containers.Any())
                        .WithMessage(string.Format(Resources.Resource.Any_Item_Is_Not_Required, Resources.Resource.Container));
                });
            });
            When(bitacora => bitacora.MonitoringType != null, () =>
            {
                RuleFor(bitacora => bitacora)
                    .Must(x =>
                    (x.MonitoringType.Custodyrequired == null && x.Custody == null) ||
                    (x.MonitoringType.Custodyrequired == false) ||
                    (x.MonitoringType.Custodyrequired == true && x.Custody != null))
                    .WithMessage(x =>
                        (x.MonitoringType.Custodyrequired == null && x.Custody != null) ? string.Format(Resources.Resource.Any_Item_Is_Not_Required, Resources.Resource.Custody) : (
                        (x.MonitoringType.Custodyrequired == true && x.Custody == null) ? string.Format(Resources.Resource.At_Least_Item_Is_Required, Resources.Resource.Custody) : "")
                    );

                RuleFor(bitacora => bitacora).Must(x =>
                    (x.MonitoringType.Securitykitrequired == null && x.SecurityKit == null) ||
                    (x.MonitoringType.Securitykitrequired == true && x.SecurityKit != null) ||
                    (x.MonitoringType.Securitykitrequired == false)
                ).WithMessage(x =>
                    (x.MonitoringType.Securitykitrequired == null && x.SecurityKit != null) ? string.Format(Resources.Resource.Any_Item_Is_Not_Required, Resources.Resource.SecurityKit) :
                    (x.MonitoringType.Securitykitrequired == true && x.SecurityKit == null) ? string.Format(Resources.Resource.Model_Required, Resources.Resource.SecurityKit) : ""
                );
            });
        }
    }
}
