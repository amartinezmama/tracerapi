﻿using System;
using System.Linq;
using FluentValidation;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.FluentValidators.BitacoraRules
{
    public class BitacoraCustodyAbandonValidator : AbstractValidator<BitacoraEnt>, IDisposable
    {

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// Validates the parameters when the PreBitacora is created or updated
        /// </summary>
        public BitacoraCustodyAbandonValidator()
        {
            RuleFor(bitacora => bitacora.Customer)
                .NotEmpty()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Customer));
            When(bitacora => bitacora.MonitoringType != null, () =>
            {
                RuleFor(bitacora => bitacora)
                    .Must(x =>
                        (x.MonitoringType.Custodyrequired == null && x.Custody == null) ||
                        (x.MonitoringType.Custodyrequired == false) ||
                        (x.MonitoringType.Custodyrequired == true && x.Custody != null)
                    ).WithMessage(x =>
                        x.MonitoringType.Custodyrequired == null && x.Custody != null
                            ?
                            string.Format(Resources.Resource.Any_Item_Is_Not_Required, Resources.Resource.Custody)
                            :
                            x.MonitoringType.Custodyrequired == true && x.Custody == null
                                ? string.Format(Resources.Resource.At_Least_Item_Is_Required,
                                    Resources.Resource.Custody)
                                : ""
                    );
            });
        }
    }
}
