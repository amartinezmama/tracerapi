﻿using System;
using System.Linq;
using FluentValidation;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.FluentValidators.BitacoraRules
{
    public class TransportPreFoliosValidator : AbstractValidator<BitacoraEnt>, IDisposable
    {

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// Validates the parameters when the PreFolios is created or updated
        /// </summary>
        public TransportPreFoliosValidator()
        {
            RuleFor(bitacora => bitacora.Transport.Plate)
                .NotEmpty()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Plates))
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Plates));
            RuleFor(bitacora => bitacora.Transport.Gpss.Where(g => g.Type != null))
                .Must(bitacora => bitacora.Count() <= 2)
                .WithMessage(string.Format(Resources.Resource.Max_Items_Is_Required, "2", Resources.Resource.Gps));
            When(bitacora => bitacora.Transport.Plate != null, () =>
            {
                When(bitacora => bitacora.Transport.Plate.UnitType.UnitsAllow.Equals(1), () =>
                {
                    When(bitacora => bitacora.Transport.Plate.UnitType.MinUnitsAllow > 0, () =>
                    {
                        RuleFor(bitacora => bitacora)
                            .Must(x => x.Transport.Containers.Count >= x.Transport.Plate.UnitType.MinUnitsAllow)
                            .WithMessage(x => string.Format(Resources.Resource.Min_Items_Is_Required, x.Transport.Plate.UnitType.MinUnitsAllow, Resources.Resource.Container));
                    });
                });
                When(bitacora => bitacora.Transport.Plate.UnitType.UnitsAllow.Equals(0), () =>
                {
                    RuleFor(bitacora => bitacora)
                        .Must(x => !x.Transport.Containers.Any())
                        .WithMessage(string.Format(Resources.Resource.Any_Item_Is_Not_Required, Resources.Resource.Container));
                });
            });
        }
    }
}
