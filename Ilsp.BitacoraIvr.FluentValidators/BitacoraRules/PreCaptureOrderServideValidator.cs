﻿using System;
using FluentValidation;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.FluentValidators.BitacoraRules
{
    public class PreCaptureOrderServideValidator : AbstractValidator<BitacoraEnt>, IDisposable
    {
        /// <summary>
        /// Validates the parameters when in the Pre-Capture the service order is generated
        /// </summary>
        public PreCaptureOrderServideValidator()
        {
            RuleFor(bitacora => bitacora.Customer)
                .NotNull().NotEmpty()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Customer));
            RuleFor(bitacora => bitacora.BusinessCustomer)
                .NotNull().NotEmpty()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.BusinessName));
            RuleFor(bitacora => bitacora.Project)
                .NotNull().NotEmpty()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Project));
            RuleFor(bitacora => bitacora.Agreement)
                .NotNull().NotEmpty()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Agreement));
            RuleFor(bitacora => bitacora.CustodyFolio)
                .NotNull().NotEmpty()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Agreement));
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
