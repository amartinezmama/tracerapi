﻿using FluentValidation;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using System;
using System.Linq;


namespace Ilsp.BitacoraIvr.FluentValidators.BitacoraRules
{

    public class PreFoliosCreateValidator : AbstractValidator<BitacoraEnt>, IDisposable
    {

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// Validates the parameters when the PreFolios is created or updated
        /// </summary>
        public PreFoliosCreateValidator()
        {

            RuleFor(bitacora => bitacora.Customer)
               .NotNull()
               .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Customer));

            RuleFor(bitacora => bitacora.BusinessCustomer)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.BusinessName));

            RuleFor(bitacora => bitacora.ServiceType)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.ServiceType));

            RuleFor(bitacora => bitacora.Project)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Project));

            RuleFor(bitacora => bitacora.Agreement)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Agreement));

            RuleFor(bitacora => bitacora.Origin)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Origin));

            RuleFor(bitacora => bitacora.Destinies)
                .NotNull()
                .Must(bitacora => bitacora.Any())
                .WithMessage(string.Format(Resources.Resource.At_Least_Item_Is_Required, Resources.Resource.Destiny));

            RuleFor(bitacora => bitacora.ServiceType)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.ServiceType));

            RuleFor(bitacora => bitacora.Custody)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Custody));

            //RuleFor(bitacora => bitacora.SecurityKit)
            //    .NotNull()
            //    .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.SecurityKit));

            RuleFor(bitacora => bitacora.ProductType)
                .NotNull()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.ProductType));





            //RuleFor(bitacora => bitacora.Customer)
            //    .NotEmpty()
            //    .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Customer));
            //RuleFor(bitacora => bitacora.BusinessCustomer)
            //    .NotNull().NotEmpty()
            //    .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.BusinessName));
            //RuleFor(bitacora => bitacora.Project)
            //    .NotNull().NotEmpty()
            //    .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Project));
            //RuleFor(bitacora => bitacora.Agreement)
            //    .NotNull().NotEmpty()
            //    .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Agreement));
            //RuleFor(bitacora => bitacora.ServiceType)
            //    .NotEmpty()
            //    .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.ServiceType));
            //RuleFor(bitacora => bitacora.Origin)
            //    .NotEmpty()
            //    .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Origin));
            //RuleFor(bitacora => bitacora.Destinies)
            //    .NotNull()
            //    .WithMessage(string.Format(Resources.Resource.At_Least_Item_Is_Required, Resources.Resource.Destiny))
            //    .Must(bitacora => bitacora.Any())
            //    .WithMessage(string.Format(Resources.Resource.At_Least_Item_Is_Required, Resources.Resource.Destiny));
            //RuleFor(bitacora => bitacora.MonitoringType)
            //    .NotEmpty()
            //    .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.MonitoringType));
            //RuleFor(bitacora => bitacora.TransportLine)
            //    .NotEmpty()
            //    .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.TransportLine));
            //RuleFor(bitacora => bitacora.Transport.Operators)
            //    .Must(bitacora => bitacora.Any())
            //    .WithMessage(string.Format(Resources.Resource.Min_Items_Is_Required, "1", Resources.Resource.Operators))
            //    .Must(bitacora => bitacora.Count <= 2)
            //    .WithMessage(string.Format(Resources.Resource.Max_Items_Is_Required, "2", Resources.Resource.Operators));
            //RuleFor(bitacora => bitacora.Transport.Plate)
            //    .NotEmpty()
            //    .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Plates))
            //    .NotNull()
            //    .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Plates));
            //RuleFor(bitacora => bitacora.Transport.Gpss.Where(g => g.Type != null))
            //    .Must(bitacora => bitacora.Count() <= 2)
            //    .WithMessage(string.Format(Resources.Resource.Max_Items_Is_Required, "2", Resources.Resource.Gps));
            //When(bitacora => bitacora.Transport.Plate != null, () =>
            //{
            //    When(bitacora => bitacora.Transport.Plate.UnitType.UnitsAllow.Equals(1), () =>
            //    {
            //        When(bitacora => bitacora.Transport.Plate.UnitType.MinUnitsAllow > 0, () =>
            //        {
            //            RuleFor(bitacora => bitacora)
            //                .Must(x => x.Transport.Containers.Count >= x.Transport.Plate.UnitType.MinUnitsAllow)
            //                .WithMessage(x => string.Format(Resources.Resource.Min_Items_Is_Required, x.Transport.Plate.UnitType.MinUnitsAllow, Resources.Resource.Container));
            //        });
            //    });
            //    When(bitacora => bitacora.Transport.Plate.UnitType.UnitsAllow.Equals(0), () =>
            //    {
            //        RuleFor(bitacora => bitacora)
            //            .Must(x => !x.Transport.Containers.Any())
            //            .WithMessage(string.Format(Resources.Resource.Any_Item_Is_Not_Required, Resources.Resource.Container));
            //    });
            //});
            When(bitacora => bitacora.MonitoringType != null, () =>
            {
                RuleFor(bitacora => bitacora)
                    .Must(x =>
                    (x.MonitoringType.Custodyrequired == null && x.Custody == null) ||
                    (x.MonitoringType.Custodyrequired == false) ||
                    (x.MonitoringType.Custodyrequired == true && x.Custody != null)
                ).WithMessage(x =>
                    x.MonitoringType.Custodyrequired == null && x.Custody != null ? string.Format(Resources.Resource.Any_Item_Is_Not_Required, Resources.Resource.Custody) :
                    x.MonitoringType.Custodyrequired == true && x.Custody == null ? string.Format(Resources.Resource.At_Least_Item_Is_Required, Resources.Resource.Custody) : ""
                );

                When(bitacora => (bitacora.MonitoringType.Custodyrequired != null),
                    () =>
                    {
                        When(cusodia => cusodia.Custody != null, () =>
                        {
                            When(showCabin => showCabin.Custody.CustodyType.ShowCabin, () =>
                            {
                                RuleFor(bita => bita.Custody).
                                    Must(bita => bita.Cabins != null && bita.Cabins.Any())
                                    .WithMessage(
                                        "Se requiere custodia de cabina "
                                    );

                            });


                            When(showCabin => showCabin.Custody.CustodyType.ShowVehicle, () =>
                            {
                                RuleFor(bita => bita.Custody).
                                    Must(bita => bita.Vehicles != null && bita.Vehicles.Any())
                                    .WithMessage(
                                        "Se requiere custodia de vehículo"
                                    );

                            });
                            

                        });

                    });


                //RuleFor(bitacora => bitacora).Must(x =>
                //    (x.MonitoringType.Securitykitrequired == null && x.SecurityKit == null) ||
                //    (x.MonitoringType.Securitykitrequired == true && x.SecurityKit != null) ||
                //    (x.MonitoringType.Securitykitrequired == false)
                //).WithMessage(x =>
                //    x.MonitoringType.Securitykitrequired == null && x.SecurityKit != null ? string.Format(Resources.Resource.Any_Item_Is_Not_Required, Resources.Resource.SecurityKit) :
                //    x.MonitoringType.Securitykitrequired == true && x.SecurityKit == null ? string.Format(Resources.Resource.Model_Required, Resources.Resource.SecurityKit) : ""
                //);

                RuleFor(bitacora => bitacora)
                    .Must(x =>
                        (x.MonitoringType.Gpsrequired == null && !x.Transport.Gpss.Any() && !x.Transport.Containers.Any(tra => tra.Gpss.Any())) ||
                        (x.MonitoringType.Gpsrequired == false) ||
                        (x.MonitoringType.Gpsrequired == true && (x.Transport.Gpss.Any() || x.Transport.Containers.Any(tra => tra.Gpss.Any())))
                    ).WithMessage(x =>
                        x.MonitoringType.Gpsrequired == null && (x.Transport.Gpss.Any() || x.Transport.Containers.Any(tra => tra.Gpss.Any())) ? string.Format(Resources.Resource.Any_Item_Is_Not_Required, Resources.Resource.Gps) :
                            x.MonitoringType.Gpsrequired == true && !x.Transport.Gpss.Any() && !x.Transport.Containers.Any(tra => tra.Gpss.Any()) ? string.Format(Resources.Resource.At_Least_Item_Is_Required, Resources.Resource.Gps) : ""
                    );
            });
        }
    }

}
