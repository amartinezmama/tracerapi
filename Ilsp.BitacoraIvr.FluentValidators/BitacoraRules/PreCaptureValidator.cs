﻿using System;
using FluentValidation;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;

namespace Ilsp.BitacoraIvr.FluentValidators.BitacoraRules
{
    public class PreCaptureValidator : AbstractValidator<BitacoraEnt>, IDisposable
    {

        /// <summary>
        /// Validates the parameters when the Pre-Capture is created
        /// </summary>
        public PreCaptureValidator()
        {
            RuleFor(bitacora => bitacora.Customer)
                .NotNull().NotEmpty()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Customer));
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

    }
}
