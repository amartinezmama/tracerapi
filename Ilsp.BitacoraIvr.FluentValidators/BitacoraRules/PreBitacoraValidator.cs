﻿using FluentValidation;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Resources;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Ilsp.BitacoraIvr.FluentValidators.BitacoraRules
{
    public class PreBitacoraValidator : AbstractValidator<BitacoraEnt>, IDisposable
    {
        private readonly BitacorasBl _bl;

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// Validates the parameters when the PreBitacora is created or updated
        /// </summary>
        public PreBitacoraValidator(BitacorasBl bl)
        {
            _bl = bl;
            RuleFor(bitacora => bitacora.Customer)
                .NotEmpty()
                .WithMessage(string.Format(Resource.Model_Required, Resource.Customer));
            RuleFor(bitacora => bitacora.ServiceType)
                .NotEmpty()
                .WithMessage(string.Format(Resource.Model_Required, Resource.ServiceType));
            RuleFor(bitacora => bitacora.Origin)
                .NotEmpty()
                .WithMessage(string.Format(Resource.Model_Required, Resource.Origin));
            RuleFor(bitacora => bitacora.Destinies)
                .NotNull()
                .WithMessage(string.Format(Resource.At_Least_Item_Is_Required, Resource.Destiny))
                .Must(bitacora => bitacora.Any())
                .WithMessage(string.Format(Resources.Resource.At_Least_Item_Is_Required, Resources.Resource.Destiny));
            When(bitacora => bitacora.ServiceType != null && bitacora.Destinies.Any(), () =>
            {
                RuleFor(bitacora => bitacora)
                    .Must(x => x.Destinies.Count <= x.ServiceType.Destinations)
                    .WithMessage(x => string.Format(Resources.Resource.Number_Of_Items_Exceeded_For_Field, Resources.Resource.Destinations, Resources.Resource.ServiceType));
            });
            RuleFor(bitacora => bitacora.MonitoringType)
                .NotEmpty()
                .WithMessage(string.Format(Resource.Model_Required, Resource.MonitoringType));
            RuleFor(bitacora => bitacora.TransportLine)
                .NotEmpty()
                .WithMessage(string.Format(Resource.Model_Required, Resource.TransportLine));
            RuleFor(bitacora => bitacora.Transport.Operators)
                .Must(bitacora => bitacora.Any())
                .WithMessage(string.Format(Resource.Min_Items_Is_Required, "1", Resource.Operators))
                .Must(bitacora => bitacora.Count <= 2)
                .WithMessage(string.Format(Resource.Max_Items_Is_Required, "2", Resource.Operators));
            RuleFor(bitacora => bitacora.Transport.Plate)
                .NotEmpty()
                .WithMessage(string.Format(Resource.Model_Required, Resource.Plates))
                .NotNull()
                .WithMessage(string.Format(Resource.Model_Required, Resource.Plates));
            RuleFor(bitacora => bitacora.Transport.Gpss.Where(g => g.Type != null))
                .Must(bitacora => bitacora.Count() <= 2)
                .WithMessage(string.Format(Resource.Max_Items_Is_Required, "2", Resource.Gps));
            RuleFor(bitacora => bitacora.Transport.Gpss)
                .Must((bitacora, gpsList) => ValidGpssOnDb(bitacora.Id, gpsList))
                .WithMessage(Resource.One_or_more_devices_are_already_being_used_in_another_bitacora);
            When(bitacora => bitacora.Transport.Containers.Any(), () =>
            {
                RuleForEach(bitacora => bitacora.Transport.Containers)
                    .Must((bitacora, trailer) => ValidGpssOnDb(bitacora.Id, trailer.Gpss))
                    .WithMessage(Resource.One_or_more_devices_are_already_being_used_in_another_bitacora);
            });

            When(bitacora => bitacora.Transport.Plate != null, () =>
            {
                When(bitacora => bitacora.Transport.Plate.UnitType.MinUnitsAllow > 0, () =>
                {
                    RuleFor(bitacora => bitacora)
                        .Must(x => x.Transport.Containers.Count >= x.Transport.Plate.UnitType.MinUnitsAllow)
                        .WithMessage(x => string.Format(Resources.Resource.Min_Items_Is_Required, x.Transport.Plate.UnitType.MinUnitsAllow, Resources.Resource.Container));
                    RuleFor(bitacora => bitacora)
                        .Must(x => x.Transport.Containers.Count <= x.Transport.Plate.UnitType.UnitsAllow)
                        .WithMessage(x => string.Format(Resources.Resource.Max_Items_Is_Required, x.Transport.Plate.UnitType.UnitsAllow, Resources.Resource.Container));
                });
                When(bitacora => bitacora.Transport.Plate.UnitType.MinUnitsAllow.Equals(0), () =>
                {
                    RuleFor(bitacora => bitacora)
                        .Must(x => !x.Transport.Containers.Any())
                        .WithMessage(string.Format(Resource.Any_Item_Is_Not_Required, Resource.Container));
                });
            });
            When(bitacora => bitacora.MonitoringType != null, () =>
            {
                RuleFor(bitacora => bitacora)
                    .Must(x =>
                    (x.MonitoringType.Custodyrequired == null && x.Custody == null) ||
                    (x.MonitoringType.Custodyrequired == false) ||
                    (x.MonitoringType.Custodyrequired == true && x.Custody != null)
                ).WithMessage(x =>
                    x.MonitoringType.Custodyrequired == null && x.Custody != null ? string.Format(Resource.Any_Item_Is_Not_Required, Resource.Custody) :
                    x.MonitoringType.Custodyrequired == true && x.Custody == null ? string.Format(Resource.At_Least_Item_Is_Required, Resource.Custody) : ""
                );

                RuleFor(bitacora => bitacora).Must(x =>
                    (x.MonitoringType.Securitykitrequired == null && x.SecurityKit == null) ||
                    (x.MonitoringType.Securitykitrequired == true && x.SecurityKit != null) ||
                    (x.MonitoringType.Securitykitrequired == false)
                ).WithMessage(x =>
                    x.MonitoringType.Securitykitrequired == null && x.SecurityKit != null ? string.Format(Resource.Any_Item_Is_Not_Required, Resource.SecurityKit) :
                    x.MonitoringType.Securitykitrequired == true && x.SecurityKit == null ? string.Format(Resource.Model_Required, Resource.SecurityKit) : ""
                );

                RuleFor(bitacora => bitacora)
                    .Must(x =>
                        (x.MonitoringType.Gpsrequired == null && !x.Transport.Gpss.Any() && !x.Transport.Containers.Any(tra => tra.Gpss.Any())) ||
                        (x.MonitoringType.Gpsrequired == false) ||
                        (x.MonitoringType.Gpsrequired == true && (x.Transport.Gpss.Any() || x.Transport.Containers.Any(tra => tra.Gpss.Any())))
                    ).WithMessage(x =>
                        x.MonitoringType.Gpsrequired == null && (x.Transport.Gpss.Any() || x.Transport.Containers.Any(tra => tra.Gpss.Any())) ? string.Format(Resource.Any_Item_Is_Not_Required, Resource.Gps) :
                            x.MonitoringType.Gpsrequired == true && !x.Transport.Gpss.Any() && !x.Transport.Containers.Any(tra => tra.Gpss.Any()) ? string.Format(Resource.At_Least_Item_Is_Required, Resource.Gps) : ""
                    );
            });
        }

        private bool ValidGpssOnDb(string bitacoraId, IEnumerable<GpsEnt> gpsList)
        {
            var id = string.IsNullOrEmpty(bitacoraId) ? (decimal?)null : decimal.Parse(bitacoraId);
            return gpsList.All(device => _bl.ValidGpsOnPreBitacora(id, device.Id));
        }

    }
}
