﻿using FluentValidation;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using System;
using System.Linq;

namespace Ilsp.BitacoraIvr.FluentValidators.Convoys
{
    public class ConvoyValidator : AbstractValidator<ConvoyEnt>, IDisposable
    {
        /// <summary>
        /// Validates the parameters when the Pre-Capture is created
        /// </summary>
        public ConvoyValidator()
        {
            //RuleFor(c => c.Id)
            //    .NotNull()
            //    .NotEmpty()
            //    .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Customer));

            RuleFor(c => c.Name)
                .NotNull()
                .NotEmpty()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Customer));

            RuleFor(c => c.Color)
                .NotNull()
                .NotEmpty()
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Customer));

            RuleFor(c => c.BitacoraList)
                .NotNull()
                .NotEmpty()
                .Must(bl => bl == null || bl.Any())
                .WithMessage(string.Format(Resources.Resource.Model_Required, Resources.Resource.Customer));
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
