﻿namespace Ilsp.BitacoraIvr.Res
{
    public static class ResourcesStrings
    {
        #region Values
        public const string Entity_Generic_NoSpecialCharactersPattern = "^[a-zA-ZñÑ0-9_ ?áéíóúÁÉÍÓÚ',.-]*$";
        public const string Entity_Generic_TimeHourPattern = "^[0-9]?[0-9]?[0-9]:[0-5][0-9]";
        #endregion Values

        #region Keys
        public const string Entity_Generic_RequiredField = "Entity_Generic_RequiredField";
        public const string Entity_Generic_NoSpecialCharacters = "Entity_Generic_NoSpecialCharacters";
        public const string Entity_Generic_MaxLengthField = "Entity_Generic_MaxLengthField";
        public const string Entity_Generic_RangeMinValue = "Entity_Generic_RangeMinValue";
        public const string Entity_Generic_RangeMaxValue = "Entity_Generic_RangeMaxValue";
        public const string Entity_Generic_Range = "Entity_Generic_Range";
        public const string Entity_Generic_TimeHourError = "Entity_Generic_TimeHourError";
        #endregion Keys
    }
}
