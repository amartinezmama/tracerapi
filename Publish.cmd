echo on

%windir%\system32\inetsrv\appcmd stop apppool "AspNetCorePool"
dotnet restore C:/Users/Ingab/Documents/tracerapi/Ilsp.BitacoraIvr.WebApi/Ilsp.BitacoraIvr.WebApi.csproj
dotnet publish C:/Users/Ingab/Documents/tracerapi/Ilsp.BitacoraIvr.WebApi/Ilsp.BitacoraIvr.WebApi.csproj -c release -r win7-x86 /p:EnvironmentName=Development -o C:\inetpub\wwwroot\BitacoraIvrApi
%windir%\system32\inetsrv\appcmd start apppool "AspNetCorePool"