using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Ilsp.BitacoraIvr.LspApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .ConfigureServices((hostContext, services) =>
                {
                    var configuration = hostContext.Configuration;

                    var appSettingsConfigurationOptions = configuration.GetSection("GlobalConfiguration").Get<AppSettingsConfigurationOptions>();

                    services.AddSingleton(appSettingsConfigurationOptions);
                    services.AddHostedService<Worker>();
                });
    }
}
