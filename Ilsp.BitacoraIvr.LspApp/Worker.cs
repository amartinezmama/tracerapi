using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Transactions;
using FluentValidation.Results;
using Ilsp.BitacoraIvr.Bl.BitacoraAiLq;
using Ilsp.BitacoraIvr.Ent.BitacoraAi;
using Ilsp.BitacoraIvr.Ent.LoadShipmentPlan;
using Ilsp.BitacoraIvr.Ent.LspApp;
using Ilsp.BitacoraIvr.FluentValidators.BitacoraRules;
using Ilsp.BitacoraIvr.Tl;
using Ilsp.BitacoraIvr.Tl.ExcelData;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Timer = System.Timers.Timer;

namespace Ilsp.BitacoraIvr.LspApp
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private Timer _timer;
        private readonly string _ftpServer;
        private readonly string _ftpUser;
        private readonly string _ftpPassword;
        private readonly string _pendingShipments;
        private readonly string _processShipments;
        private readonly string _errorShipments;
        private readonly string _tempPath;
        private readonly string _resultsShipments;
        private readonly char _delimiterSplit;
        private readonly char _separatorDate;
        private readonly int _seconds;
        private readonly ConcurrentDictionary<string, FilePath> _currentFiles = new ConcurrentDictionary<string, FilePath>();
        private List<ColumnMap> _columns;
        private readonly string _lettersAccentsSpacePattern;
        private readonly string _platesPattern;
        private readonly string _connectionStringBitacora;

        private readonly AppSettingsConfigurationOptions _options;

        public Worker(ILogger<Worker> logger, AppSettingsConfigurationOptions options)
        {
            _options = options;
            _ftpServer = _options.Server;
            _ftpUser = _options.User;
            _ftpPassword = _options.Password;
            _pendingShipments = _options.PendingShipments;
            _processShipments = _options.ProcessShipments;
            _errorShipments = _options.ErrorShipments;
            _tempPath = _options.TempPath;
            _resultsShipments = _options.ResultsShipments;
            _delimiterSplit = char.Parse(_options.DelimiterSplit);
            _separatorDate = char.Parse(_options.SeparatorDate);
            _seconds = _options.TimerSeconds;

            _columns = _options.ColumnsConfiguration.ToList();

            _lettersAccentsSpacePattern = _options.LettersAccentsSpacePattern;
            _platesPattern = _options.PlatesPattern;
            _connectionStringBitacora = _options.ConnectionStringBitacora;

            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            //while (!stoppingToken.IsCancellationRequested)
            //{
            //    _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
            //    await Task.Delay(_seconds * 1000, stoppingToken);
            //}
            _logger.LogInformation("Service was started");
            _timer = new Timer { Interval = _seconds * 1000 };
            _timer.Elapsed += timer_Tick;
            _timer.Enabled = true;
        }

        private void timer_Tick(object sender, ElapsedEventArgs e)
        {
            try
            {
                ReadAndMoveFiles();
            }
            catch (Exception aE)
            {
                _logger.LogError(aE.ToString());
            }
        }

        /// <summary>
        /// FTP Connecction
        /// Read files and move to process folder
        /// Call initial validation
        /// </summary>
        private void ReadAndMoveFiles()
        {
            _timer.Stop();

            if (!Directory.Exists(_tempPath))
            {
                Directory.CreateDirectory(_tempPath);
            }

            var ftpRequest = (FtpWebRequest)WebRequest.Create(new Uri($"ftp://{_ftpServer}/{_pendingShipments}/", UriKind.Absolute));
            ftpRequest.UsePassive = true;
            ftpRequest.KeepAlive = false;
            ftpRequest.Credentials = new NetworkCredential(_ftpUser, _ftpPassword);
            ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
            try
            {
                #region obtiene los archivos del origen

                using (var response = (FtpWebResponse)ftpRequest.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream ?? throw new InvalidOperationException(), true))
                        {
                            while (!reader.EndOfStream)
                            {
                                var line = reader.ReadLine();
                                if (!string.IsNullOrEmpty(line))
                                {
                                    var nowStr = DateTime.Now.ToString("LSP_ddMMyyyy_HHmmss_");
                                    var originalFileNme = line;
                                    var fileName = $"{nowStr}{(line.Contains(".xlsx") ? line : line.Replace(".xls", ".xlsx"))}";
                                    var filePath = new FilePath
                                    {
                                        OriginalFileName = originalFileNme,
                                        ImportedFileName = fileName,
                                        ProcessFileName = fileName,
                                        ErrorsFileName = $"Errores_{fileName}",
                                        ResultlFileName = $"Resultados_{fileName}"
                                    };
                                    _logger.LogInformation($"Archivo agregado al Diccionario: {line}");
                                    _currentFiles.TryAdd(fileName, filePath);

                                }
                            }
                        }
                    }
                }
                try
                {

                    ParalellValidate();

                }
                catch (Exception aE)
                {
                    _logger.LogError(aE.StackTrace);
                }

                #endregion
            }
            catch (WebException aE)
            {
                var status = ((FtpWebResponse)aE.Response).StatusDescription;
                _logger.LogError(status);
            }
            _timer.Start();
        }

        /// <summary>
        /// Get and process in parallel all files 
        /// </summary>
        private void ParalellValidate()
        {
            Parallel.ForEach(_currentFiles, currentElement =>
            {
                var file = currentElement.Value;
                _logger.LogInformation($"Archivo en parallel: {file.OriginalFileName}");
                try
                {
                    try
                    {
                        using (var bl = new BitacorasBl(_connectionStringBitacora, "system"))
                        {
                            bl.CreateUpdateBitacoraMassiveLoad(0, file.OriginalFileName, string.Empty, DateTime.Now, string.Empty, DateTime.Now, 'C');
                        }

                        _logger.LogInformation("Insert state to bitacoras:mmasive_load");

                        ProcessFile(file);

                        #region recoge y mueve los archivos de error

                        MoveErrorFiles(_tempPath, file);

                        //if (Directory.Exists(_tempPath))
                        //{
                        //    DeleteDirectory(_tempPath);
                        //}

                        #endregion recoge y mueve los archivos de error

                        _currentFiles.TryRemove(currentElement.Key, out _);
                        _logger.LogInformation($"Elimina archivo en parallel: {file.OriginalFileName}");
                    }
                    catch (Exception aE)
                    {
                        _logger.LogError(aE.StackTrace);
                    }
                }
                catch (WebException aE)
                {
                    _logger.LogError($"El archivo {file.OriginalFileName} no se movio a la carpeta PendingShipments. Error: \n{aE}");
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="originalFileName">File ready to be relocated</param>
        /// /// <param name="lastFilName"></param>
        private void MoveFilesToProcessOrError(string originalFileName, string lastFilName)
        {
            try
            {
                //move file to be process to be processed
                var request = (FtpWebRequest)WebRequest.Create(new Uri($"ftp://{_ftpServer}/{_pendingShipments}/{originalFileName}", UriKind.Absolute));
                request.Method = WebRequestMethods.Ftp.Rename;
                request.Credentials = new NetworkCredential(_ftpUser, _ftpPassword);
                request.RenameTo = $"../{_processShipments}/{originalFileName}";
                var response = (FtpWebResponse)request.GetResponse();
                response.Close();
                _logger.LogInformation($"The file {originalFileName} was read");

                var newRequest = (FtpWebRequest)WebRequest.Create(new Uri($"ftp://{_ftpServer}/{_processShipments}/{originalFileName}", UriKind.Absolute));
                newRequest.Method = WebRequestMethods.Ftp.Rename;
                newRequest.Credentials = new NetworkCredential(_ftpUser, _ftpPassword);
                newRequest.RenameTo = lastFilName;
                var response2 = (FtpWebResponse)newRequest.GetResponse();

                System.Threading.Thread.Sleep(250);
                response2.Close();
            }
            catch (Exception aE)
            {
                _logger.LogError($"File: {originalFileName} was not moved from the PendingShipments folder. Error: \n{aE}");
            }
        }

        /// <summary>
        /// Method that deletes files and temporary file processing directory
        /// </summary>
        /// <param name="path">Temporary path where the files are processed</param>
        private void DeleteDirectory(string path)
        {
            foreach (var filename in Directory.GetFiles(path))
            {
                File.Delete(filename);
            }

            foreach (var subfolder in Directory.GetDirectories(path))
            {
                DeleteDirectory(subfolder);
            }

            Directory.Delete(path);
        }

        /// <summary>
        /// Method that validates the generalities of the excel file
        /// </summary>
        /// <param name="file">Name of file</param>
        /// <returns>Returns a flag with the result of success or failure of the validation</returns> 
        private void ProcessFile(FilePath file)
        {
            try
            {
                if (Path.GetExtension(file.OriginalFileName)?.ToLower() == ".xls" || Path.GetExtension(file.OriginalFileName)?.ToLower() == ".xlsx")
                {
                    MoveFilesToProcessOrError(file.OriginalFileName, file.ProcessFileName);
                    _logger.LogInformation($"El archivo {file.OriginalFileName} esta en proceso");

                    string localErrorPath = $"{_tempPath}{file.ErrorsFileName}";
                    var ftpProccesedPath = $"ftp://{_ftpServer}/{_processShipments}/";

                    var nameTemporalFile = file.ResultlFileName;
                    var temporalPath = _tempPath + nameTemporalFile;
                    var ftpfullpath = ftpProccesedPath + file.ProcessFileName;

                    CreateProcessFile(ftpfullpath, temporalPath);

                    var importFileName = temporalPath;

                    try
                    {
                        using (var bl = new BitacorasBl(_connectionStringBitacora, "system"))
                        {
                            bl.CreateUpdateBitacoraMassiveLoad(0, file.OriginalFileName,
                                importFileName.Replace(_tempPath, ""), DateTime.Now, localErrorPath,
                                DateTime.Now, 'P');
                        }

                        _logger.LogInformation("Proceso actualizado para en analisis de la carga masiva.");
                    }
                    catch (Exception aE)
                    {
                        _logger.LogError(aE.StackTrace);
                    }

                    try
                    {
                        var fileExtension = Path.GetExtension(importFileName).ToLower();
                        if (fileExtension.Equals(".xls") || fileExtension.Equals(".xlsx"))
                        {
                            DataTable dt;
                            try
                            {
                                var fileBytes = File.ReadAllBytes(importFileName);
                                dt = ExcelTo.ExcelToDataTable(fileBytes, true);
                                if (!_columns.Where(c => c.ColumnRequired).All(c => dt.Columns.Contains(c.ColumnName)))
                                {
                                    var messageException = "El archivo no contiene el numero de columnas requeridas necesarias.";
                                    _logger.LogError(messageException);
                                    CreateErrorExcelFile(file, new List<FinalValidation>
                                    {
                                        new FinalValidation
                                        {
                                            NumberRow = 1,
                                            SourceFile = importFileName,
                                            Messages = new List<FluentMessage>
                                            {
                                                new FluentMessage
                                                {
                                                    Comments = messageException,
                                                    Success = false
                                                }
                                            }
                                        }
                                    });
                                    return;
                                }
                            }
                            catch (Exception aE)
                            {
                                _logger.LogError($"In file: {Path.GetFileName(file.OriginalFileName)}, the information is not charged to the datatable. Error: \n{aE}");
                                var messageException = $"No se consiguio leer el archivo Excel por error: {aE.Message}";
                                CreateErrorExcelFile(file, new List<FinalValidation>
                                {
                                    new FinalValidation
                                    {
                                        NumberRow = 1,
                                        SourceFile = importFileName,
                                        Messages = new List<FluentMessage>
                                        {
                                            new FluentMessage
                                            {
                                                Comments = messageException,
                                                Success = false
                                            }
                                        }
                                    }
                                });
                                return;
                            }
                            var dataColumns = dt.Columns;
                            var dataRows = dt.Rows;
                            UpdateProcessFileAddHeaderResult(temporalPath, dt.Columns.Count);
                            for (var index = 0; index < dataRows.Count; index++)
                            {
                                var rowNumber = index + 1;
                                var validationsItem = new FinalValidation
                                {
                                    NumberRow = rowNumber,
                                    SourceFile = importFileName,
                                    Messages = new List<FluentMessage>()
                                };
                                var listMessages = new List<FluentMessage>();
                                var dataRow = dataRows[index];
                                //var isDuplicateOnFile = false;

                                //#region Valida duplicados en archivo

                                ////verifica que el registro a insertar no sea identico a alguno previo
                                //// ReSharper disable once LoopVariableIsNeverChangedInsideLoop
                                //for (var j = 0; j < index; j++)
                                //{
                                //    isDuplicateOnFile = _columns.All(c => dataRows[j][c.ColumnName].Equals(dataRow[c.ColumnName]));
                                //    if (isDuplicateOnFile)
                                //    {
                                //        var messageException = "El registro No. " + rowNumber + " esta duplicado";
                                //        _logger.LogError("En el archivo: " + Path.GetFileName(file.OriginalFileName) + ", el numero file " + rowNumber + " esta duplicado.");
                                //        listMessages.Add(new FluentMessage { Comments = messageException, Success = false });
                                //        break;
                                //    }
                                //}

                                //#endregion

                                //if (!isDuplicateOnFile)
                                //{
                                var messageRequired = "No se encontro el registro de la columna: {0} -> dato requerido";
                                #region Mapeo de datos

                                var bitacoraItem = new BitacoraEnt
                                {
                                    Destinies = new List<DestinyEnt>(),
                                    Custody = null,
                                    Status = "P"
                                };
                                var isValidBitacora = true;

                                if (IsValidColumn(dataColumns, "CLIENTE ILSP", dataRow, listMessages, ref isValidBitacora))
                                {
                                    using (var bl = new CatCustomersBl(_connectionStringBitacora, "system"))
                                    {
                                        var columnName = "CLIENTE ILSP";
                                        var list = bl.Search(dataRow[columnName].ToString()).ToList();
                                        if (list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                        {
                                            bitacoraItem.Customer = list.First(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower()));
                                        }
                                        else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                        {
                                            isValidBitacora = false;
                                            listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                        }
                                    }
                                }

                                if (IsValidColumn(dataColumns, "AVANCE", dataRow, listMessages, ref isValidBitacora))
                                {
                                    var columnName = "AVANCE";
                                    if (!string.IsNullOrEmpty(dataRow[columnName].ToString()))
                                    {
                                        if (dataRow[columnName].ToString().ToLower().Trim().Equals("Destino".ToLower()) || dataRow[columnName].ToString().ToLower().Trim().Equals("Avance".ToLower()))
                                        {
                                            bitacoraItem.Advance = dataRow[columnName].ToString();
                                        }
                                        else
                                        {
                                            var message = $"El valor de la columna '{columnName}' es invalido -> dato requerido";
                                            listMessages.Add(new FluentMessage { Comments = message, ColumnName = columnName, Success = false });
                                            isValidBitacora = false;
                                        }
                                    }
                                    else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                    {
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                    }
                                }

                                if (IsValidColumn(dataColumns, "ORIGEN", dataRow, listMessages, ref isValidBitacora))
                                {
                                    if (bitacoraItem.Customer != null)
                                    {
                                        using (var bl = new CatOriginsDestinationsBl(_connectionStringBitacora, "system"))
                                        {
                                            var columnName = "ORIGEN";
                                            var list = bl.SearchOrigins(bitacoraItem.Customer.Id, dataRow[columnName].ToString()).ToList();
                                            if (list.Any(item => item.Alias.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                            {
                                                bitacoraItem.Origin = list.First(item => item.Alias.ToLower().Equals(dataRow[columnName].ToString().ToLower()));
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Alias.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                            {
                                                SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }

                                if (IsValidColumn(dataColumns, "L�NEA", dataRow, listMessages, ref isValidBitacora))
                                {
                                    if (bitacoraItem.Customer != null)
                                    {
                                        using (var bl = new CatTransportLinesBl(_connectionStringBitacora, "system"))
                                        {
                                            var columnName = "L�NEA";
                                            var list = bl.Search(bitacoraItem.Customer.Id, dataRow[columnName].ToString()).ToList();
                                            if (list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                            {
                                                bitacoraItem.TransportLine = list.First(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower()));
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                            {
                                                SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }

                                if (IsValidColumn(dataColumns, "TIPO DE MONITOREO", dataRow, listMessages, ref isValidBitacora))
                                {
                                    using (var bl = new CatMonitoringTypesBl(_connectionStringBitacora, "system"))
                                    {
                                        var columnName = "TIPO DE MONITOREO";
                                        var list = bl.Search(dataRow[columnName].ToString()).ToList();
                                        if (list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                        {
                                            bitacoraItem.MonitoringType = list.First(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower()));
                                            bitacoraItem.IsMirrorAccount = bitacoraItem.MonitoringType.IsMirrorAccount;
                                        }
                                        else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                        {
                                            isValidBitacora = false;
                                            listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                        }
                                    }
                                }

                                if (IsValidColumn(dataColumns, "TIPO SERVICIO", dataRow, listMessages, ref isValidBitacora))
                                {
                                    using (var bl = new CatServiceTypesBl(_connectionStringBitacora, "system"))
                                    {
                                        var columnName = "TIPO SERVICIO";
                                        var list = bl.Search(dataRow[columnName].ToString()).ToList();
                                        if (list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                        {
                                            bitacoraItem.ServiceType = list.First(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower()));
                                        }
                                        else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                        {
                                            isValidBitacora = false;
                                            listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                        }
                                    }
                                }

                                #region Destinos

                                var isValidDestinations = true;
                                if (!IsValidColumn(dataColumns, "DESTINO", dataRow, listMessages, ref isValidBitacora))
                                {
                                    isValidDestinations = false;
                                    isValidBitacora = false;
                                }
                                if (!IsValidColumn(dataColumns, "FECHA DE CITA", dataRow, listMessages, ref isValidBitacora))
                                {
                                    isValidDestinations = false;
                                    isValidBitacora = false;
                                }
                                if (!IsValidColumn(dataColumns, "HORA DE CITA", dataRow, listMessages, ref isValidBitacora))
                                {
                                    isValidDestinations = false;
                                    isValidBitacora = false;
                                }

                                if (isValidDestinations)
                                {
                                    var destiniesSplit = dataRow["DESTINO"].ToString().Split(_delimiterSplit);
                                    var deliveryNumberSplit = dataRow["ENV�O"].ToString().Split(_delimiterSplit);
                                    var dateSplit = dataRow["FECHA DE CITA"].ToString().Split(_delimiterSplit);
                                    var timeSplit = dataRow["HORA DE CITA"].ToString().Split(_delimiterSplit);
                                    var numberItems = destiniesSplit.Length;
                                    if (dateSplit.Length == numberItems && timeSplit.Length == numberItems && (string.IsNullOrEmpty(dataRow["ENV�O"].ToString()) || deliveryNumberSplit.Length == numberItems))
                                    {
                                        for (int i = 0; i < destiniesSplit.Length; i++)
                                        {
                                            var destinyItem = destiniesSplit[i];
                                            var deliveryNumberItem = !string.IsNullOrEmpty(dataRow["ENV�O"].ToString()) ? deliveryNumberSplit[i] : "SE";
                                            var dateItem = dateSplit[i];
                                            var timeItem = timeSplit[i];

                                            var destiny = new DestinyEnt();
                                            var date = "";
                                            var destinyValid = true;
                                            string columnName;

                                            if (bitacoraItem.Customer != null)
                                            {
                                                using (var bl = new CatOriginsDestinationsBl(_connectionStringBitacora, "system"))
                                                {
                                                    columnName = "DESTINO";
                                                    var list = bl.SearchDestinies(bitacoraItem.Customer.Id, destinyItem).ToList();
                                                    if (list.Any(item => item.Alias.ToLower().Equals(destinyItem.ToLower())))
                                                    {
                                                        destiny.Destiny = list.First(item => item.Alias.ToLower().Equals(destinyItem.ToLower()));
                                                    }
                                                    else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                                    {
                                                        destinyValid = false;
                                                        isValidBitacora = false;
                                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                                    }
                                                    if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Alias.ToLower().Equals(destinyItem.ToLower())))
                                                    {
                                                        SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString());
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                isValidBitacora = false;
                                            }

                                            columnName = "ENV�O";
                                            if (!string.IsNullOrEmpty(deliveryNumberItem))
                                            {
                                                destiny.DeliveryNumber = deliveryNumberItem;
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                destinyValid = false;
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            else if (string.IsNullOrEmpty(deliveryNumberItem))
                                            {
                                                destiny.DeliveryNumber = string.Empty;
                                            }

                                            columnName = "FECHA DE CITA";
                                            if (!string.IsNullOrEmpty(dateItem))
                                            {
                                                var rgx = new Regex("^([0]{1}[1-9]{1}|[1]{1}[0-9]{1}|[2]{1}[0-9]{1}|[3]{1}[01]{1})[." + _separatorDate + "]([0]?[1-9]{1}|[1][0-2]{1})[." + _separatorDate + "]([0-9]{4})$");
                                                if (rgx.IsMatch(dateItem))
                                                {
                                                    date = dateItem;
                                                }
                                                else
                                                {
                                                    destinyValid = false;
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = $"El formato de la columna {columnName} es invalido", ColumnName = columnName, Success = false });
                                                }
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                destinyValid = false;
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }

                                            columnName = "HORA DE CITA";
                                            if (!string.IsNullOrEmpty(timeItem))
                                            {
                                                var rgx = new Regex("^(?:0?[0-9]|1[0-9]|2[0-3]):(00|30)$");
                                                if (rgx.IsMatch(timeItem))
                                                {
                                                    date += ' ' + timeItem;
                                                }
                                                else
                                                {
                                                    destinyValid = false;
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = $"El formato de la columna {columnName} es invalido", ColumnName = columnName, Success = false });
                                                }
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                destinyValid = false;
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }

                                            if (destinyValid)
                                            {
                                                try
                                                {
                                                    destiny.AppointmentDate = OptionsDateTime.ToDateTimeMx(date, _separatorDate).ToString("yyyy/MM/dd HH:mm:ss");
                                                    bitacoraItem.Destinies.Add(destiny);
                                                }
                                                catch (Exception)
                                                {
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = "Formato de fecha invalido", ColumnName = string.Empty, Success = false });
                                                }

                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = "Columnas de DESTINO, FECHA DE CITA, HORA DE CITA son invalidas.", ColumnName = string.Empty, Success = false });
                                    }
                                }
                                else
                                {
                                    isValidBitacora = false;
                                    listMessages.Add(new FluentMessage { Comments = "Columnas de DESTINO, FECHA DE CITA, HORA DE CITA son invalidas.", ColumnName = string.Empty, Success = false });
                                }



                                #endregion

                                #region Transporte

                                var transport = new TransportEnt
                                {
                                    Operators = new List<OperatorEnt>(),
                                    Gpss = new List<GpsEnt>(),
                                    Containers = new List<ContainerEnt>(),
                                    UnitStamps = new List<UnitStampEnt>()
                                };

                                #region Operadores
                                var isValidOperators = true;
                                if (!IsValidColumn(dataColumns, "NOMBRE OPERADOR", dataRow, listMessages, ref isValidBitacora, new Regex(_lettersAccentsSpacePattern)))
                                {
                                    isValidBitacora = false;
                                    isValidOperators = false;
                                }

                                if (!IsValidColumn(dataColumns, "MEDIO DEL OPERADOR", dataRow, listMessages, ref isValidBitacora))
                                {
                                    isValidBitacora = false;
                                    isValidOperators = false;
                                }

                                if (isValidOperators)
                                {
                                    var operatorsSplit = dataRow["NOMBRE OPERADOR"].ToString().Split(_delimiterSplit);
                                    var cellPhonesSplit = dataRow["MEDIO DEL OPERADOR"].ToString().Split(_delimiterSplit);
                                    var numberItems = operatorsSplit.Length;
                                    if (operatorsSplit.Length > 0 && (string.IsNullOrEmpty(dataRow["MEDIO DEL OPERADOR"].ToString()) || cellPhonesSplit.Length == numberItems))
                                    {
                                        for (int i = 0; i < operatorsSplit.Length; i++)
                                        {
                                            var operatorItemSplit = operatorsSplit[i];
                                            var deliveryNumberItem = !string.IsNullOrEmpty(dataRow["MEDIO DEL OPERADOR"].ToString()) ? cellPhonesSplit[i] : "0";

                                            var operatorItem = new OperatorEnt();
                                            var operatorValid = true;
                                            string columnName;

                                            columnName = "NOMBRE OPERADOR";
                                            if (!string.IsNullOrEmpty(operatorItemSplit))
                                            {
                                                operatorItem.Name = operatorItemSplit;
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                operatorValid = false;
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }

                                            columnName = "MEDIO DEL OPERADOR";
                                            if (!string.IsNullOrEmpty(deliveryNumberItem))
                                            {
                                                var rgx = new Regex("^[0-9]*$");
                                                if (rgx.IsMatch(deliveryNumberItem))
                                                {
                                                    operatorItem.CellPhoeOne = deliveryNumberItem;
                                                }
                                                else
                                                {
                                                    operatorValid = false;
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = $"El formato de la columna {columnName} es invalido", ColumnName = columnName, Success = false });
                                                }
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                operatorValid = false;
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            else if (string.IsNullOrEmpty(deliveryNumberItem))
                                            {
                                                operatorItem.CellPhoeOne = "0";
                                            }

                                            if (operatorValid)
                                            {
                                                transport.Operators = new List<OperatorEnt> { operatorItem };
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = "Columnas de DESTINO, FECHA DE CITA, HORA DE CITA son invalidas.", ColumnName = string.Empty, Success = false });
                                    }
                                }
                                else
                                {
                                    isValidBitacora = false;
                                    listMessages.Add(new FluentMessage { Comments = "Columnas de NOMBRE OPERADOR y/o MEDIO DEL OPERADOR son invalidas.", ColumnName = string.Empty, Success = false });
                                }

                                #endregion

                                #region Vehiculo

                                var vehicleItem = new PlateEnt
                                {
                                    Colors = new List<ColorEnt>()
                                };
                                var vehicleValid = true;
                                if (IsValidColumn(dataColumns, "PLACAS", dataRow, listMessages, ref isValidBitacora, new Regex(_platesPattern)))
                                {
                                    if (bitacoraItem.Customer != null && bitacoraItem.TransportLine != null)
                                    {
                                        var columnName = "PLACAS";
                                        using (var bl = new CatTransportLinesVehiclesBl(_connectionStringBitacora, "system"))
                                        {
                                            var list = bl.Search(bitacoraItem.Customer.Id, bitacoraItem.TransportLine.Id, dataRow[columnName].ToString()).ToList();
                                            if (list.Any(item => item.NumPlate.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                            {
                                                vehicleItem = list.First(item => item.NumPlate.ToLower().Equals(dataRow[columnName].ToString().ToLower()));
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                vehicleValid = false;
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.NumPlate.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                            {
                                                SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        vehicleValid = false;
                                        isValidBitacora = false;
                                    }
                                }
                                else
                                {
                                    vehicleValid = false;
                                }

                                if (IsValidColumn(dataColumns, "TIPO DE UNIDAD", dataRow, listMessages, ref isValidBitacora))
                                {
                                    if (bitacoraItem.Customer != null)
                                    {
                                        using (var bl = new CatUnitTypesBl(_connectionStringBitacora, "system"))
                                        {
                                            var columnName = "TIPO DE UNIDAD";
                                            var list = bl.Search(bitacoraItem.Customer.Id, dataRow[columnName].ToString()).ToList();
                                            if (list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                            {
                                                vehicleItem.UnitType = list.First(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower()));
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                            {
                                                SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }
                                else
                                {
                                    vehicleValid = false;
                                }

                                if (IsValidColumn(dataColumns, "MARCA", dataRow, listMessages, ref isValidBitacora))
                                {
                                    if (bitacoraItem.Customer != null)
                                    {
                                        using (var bl = new CatBrandsBl(_connectionStringBitacora, "system"))
                                        {
                                            var columnName = "MARCA";
                                            var list = bl.Search(bitacoraItem.Customer.Id, dataRow[columnName].ToString())
                                                .ToList();
                                            if (list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                            {
                                                vehicleItem.Brand = list.First(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower()));
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                            {
                                                SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }
                                else
                                {
                                    vehicleValid = false;
                                }

                                if (IsValidColumn(dataColumns, "COLOR", dataRow, listMessages, ref isValidBitacora))
                                {
                                    vehicleItem.Colors = new List<ColorEnt>();
                                    if (bitacoraItem.Customer != null)
                                    {
                                        var columnName = "COLOR";
                                        var collectionSplit = dataRow[columnName].ToString().Split(_delimiterSplit);
                                        using (var bl = new CatColorsBl(_connectionStringBitacora, "system"))
                                        {
                                            foreach (var itemSplit in collectionSplit)
                                            {
                                                var list = bl.Search(bitacoraItem.Customer.Id, itemSplit).ToList();
                                                if (list.Any(item => item.Name.ToLower().Equals(itemSplit.ToLower())))
                                                {
                                                    vehicleItem.Colors.Add(list.First(item => item.Name.ToLower().Equals(itemSplit.ToLower())));
                                                }
                                                else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                                {
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                                }
                                                if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Name.ToLower().Equals(itemSplit.ToLower())))
                                                {
                                                    SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), itemSplit);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }
                                else
                                {
                                    vehicleValid = false;
                                }

                                if (IsValidColumn(dataColumns, "NO. ECON�MICO", dataRow, listMessages, ref isValidBitacora, new Regex(_platesPattern)))
                                {
                                    var columnName = "NO. ECON�MICO";
                                    if (!string.IsNullOrEmpty(dataRow[columnName].ToString()))
                                    {
                                        vehicleItem.Economic = dataRow[columnName].ToString();
                                    }
                                    else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                    {
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                    }
                                    else if (string.IsNullOrEmpty(dataRow[columnName].ToString()))
                                    {
                                        vehicleItem.Economic = "SE";
                                    }
                                }
                                else
                                {
                                    vehicleValid = false;
                                }

                                if (IsValidColumn(dataColumns, "MODELO", dataRow, listMessages, ref isValidBitacora))
                                {
                                    if (bitacoraItem.Customer != null)
                                    {
                                        using (var bl = new CatModelsBl(_connectionStringBitacora, "system"))
                                        {
                                            var columnName = "MODELO";
                                            var list = bl.Search(bitacoraItem.Customer.Id, dataRow[columnName].ToString()).ToList();
                                            if (list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                            {
                                                var rgx = new Regex("^[0-9]*$");
                                                if (rgx.IsMatch(dataRow[columnName].ToString()))
                                                {
                                                    vehicleItem.Model = list.First(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower()));
                                                }
                                                else
                                                {
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = $"El formato de la columna {columnName} es invalido", ColumnName = columnName, Success = false });
                                                }
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }
                                else
                                {
                                    vehicleValid = false;
                                }

                                if (vehicleValid)
                                {
                                    transport.Plate = vehicleItem;
                                }

                                #endregion

                                if (IsValidColumn(dataColumns, "GPS", dataRow, listMessages, ref isValidBitacora))
                                {
                                    if (bitacoraItem.Customer != null)
                                    {
                                        var columnName = "GPS";
                                        if (!string.IsNullOrEmpty(dataRow[columnName].ToString().Trim()))
                                        {
                                            var collectionSplit = dataRow[columnName].ToString().Split(_delimiterSplit);
                                            if (collectionSplit.Any())
                                            {
                                                using (var bl = new CatGpsBl(_connectionStringBitacora, "system"))
                                                {
                                                    foreach (var itemSplit in collectionSplit)
                                                    {
                                                        var list = bl.Search(string.Empty, bitacoraItem.Customer.Id, itemSplit).ToList();
                                                        if (list.Any(item => item.Name.ToLower().Equals(itemSplit.ToLower())))
                                                        {
                                                            transport.Gpss.Add(list.First(item => item.Name.ToLower().Equals(itemSplit.ToLower())));
                                                        }
                                                        else
                                                        {
                                                            isValidBitacora = false;
                                                            listMessages.Add(new FluentMessage { Comments = $"No se encuentra el Gps {itemSplit} o no posiciona.", ColumnName = string.Empty, Success = false });
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }

                                #region Contenedor 1

                                if (!string.IsNullOrEmpty(dataRow["TIPO DE REMOLQUE_1"].ToString()) ||
                                    !string.IsNullOrEmpty(dataRow["N�MERO ECON�MICO_1"].ToString()) ||
                                    !string.IsNullOrEmpty(dataRow["COLOR_1"].ToString()) ||
                                    !string.IsNullOrEmpty(dataRow["PLACAS_1"].ToString()))
                                {
                                    var containerValid1 = true;
                                    if (string.IsNullOrEmpty(dataRow["TIPO DE REMOLQUE_1"].ToString()))
                                    {
                                        containerValid1 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "TIPO DE REMOLQUE_1"), ColumnName = "TIPO DE REMOLQUE_1", Success = false });
                                    }
                                    if (string.IsNullOrEmpty(dataRow["N�MERO ECON�MICO_1"].ToString()))
                                    {
                                        containerValid1 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "N�MERO ECON�MICO_1"), ColumnName = "N�MERO ECON�MICO_1", Success = false });
                                    }
                                    if (string.IsNullOrEmpty(dataRow["COLOR_1"].ToString()))
                                    {
                                        containerValid1 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "COLOR_1"), ColumnName = "COLOR_1", Success = false });
                                    }
                                    if (string.IsNullOrEmpty(dataRow["PLACAS_1"].ToString()))
                                    {
                                        containerValid1 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "PLACAS_1"), ColumnName = "PLACAS_1", Success = false });
                                    }

                                    if (containerValid1)
                                    {
                                        var containerItem1 = new ContainerEnt
                                        {
                                            Gpss = new List<GpsEnt>()
                                        };

                                        if (IsValidColumn(dataColumns, "TIPO DE REMOLQUE_1", dataRow, listMessages, ref isValidBitacora))
                                        {
                                            using (var bl = new CatContainerTypesBl(_connectionStringBitacora, "system"))
                                            {
                                                var columnName = "TIPO DE REMOLQUE_1";
                                                var list = bl.Search(dataRow[columnName].ToString()).ToList();
                                                if (list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                                {
                                                    containerItem1.ContainerType = list.First(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower()));
                                                }
                                                else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                                {
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                                }
                                            }
                                        }
                                        else
                                        {
                                            containerValid1 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "N�MERO ECON�MICO_1", dataRow, listMessages, ref isValidBitacora, new Regex(_platesPattern)))
                                        {
                                            var columnName = "N�MERO ECON�MICO_1";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString()))
                                            {
                                                containerItem1.Economic = dataRow[columnName].ToString();
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            else if (string.IsNullOrEmpty(dataRow[columnName].ToString()))
                                            {
                                                containerItem1.Economic = "SE";
                                            }
                                        }
                                        else
                                        {
                                            containerValid1 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "COLOR_1", dataRow, listMessages, ref isValidBitacora))
                                        {
                                            if (bitacoraItem.Customer != null)
                                            {
                                                using (var bl = new CatColorsBl(_connectionStringBitacora, "system"))
                                                {
                                                    var columnName = "COLOR_1";
                                                    var list = bl.Search(bitacoraItem.Customer.Id, dataRow[columnName].ToString()).ToList();
                                                    if (list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                                    {
                                                        containerItem1.Color = list.First(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower()));
                                                    }
                                                    else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                                    {
                                                        isValidBitacora = false;
                                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                                    }
                                                    if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                                    {
                                                        SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString());
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                isValidBitacora = false;
                                            }
                                        }
                                        else
                                        {
                                            containerValid1 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "PLACAS_1", dataRow, listMessages, ref isValidBitacora, new Regex(_platesPattern)))
                                        {
                                            var columnName = "PLACAS_1";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString()))
                                            {
                                                var rgx = new Regex("^[a-zA-Z0-9]*$");
                                                if (rgx.IsMatch(dataRow[columnName].ToString()))
                                                {
                                                    containerItem1.Plate = dataRow[columnName].ToString();
                                                }
                                                else
                                                {
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = $"El formato de la columna {columnName} es invalido", ColumnName = columnName, Success = false });
                                                }
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                        }
                                        else
                                        {
                                            containerValid1 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "SELLOS_1", dataRow, listMessages, ref isValidBitacora))
                                        {
                                            var columnName = "SELLOS_1";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString()))
                                            {
                                                containerItem1.UnitStamp = dataRow[columnName].ToString();
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                        }
                                        else
                                        {
                                            containerValid1 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "CAPACIDAD_1", dataRow, listMessages, ref isValidBitacora))
                                        {
                                            var columnName = "CAPACIDAD_1";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString()))
                                            {
                                                containerItem1.Capacity = dataRow[columnName].ToString();
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                        }
                                        else
                                        {
                                            containerValid1 = false;
                                        }

                                        if (containerValid1)
                                        {
                                            transport.Containers.Add(containerItem1);
                                        }
                                    }
                                }



                                #endregion

                                #region Contenedor 2

                                if (!string.IsNullOrEmpty(dataRow["TIPO DE REMOLQUE_2"].ToString()) ||
                                    !string.IsNullOrEmpty(dataRow["N�MERO ECON�MICO_2"].ToString()) ||
                                    !string.IsNullOrEmpty(dataRow["COLOR_2"].ToString()) ||
                                    !string.IsNullOrEmpty(dataRow["PLACAS_2"].ToString()))
                                {
                                    var containerValid2 = true;
                                    if (string.IsNullOrEmpty(dataRow["TIPO DE REMOLQUE_2"].ToString()))
                                    {
                                        containerValid2 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "TIPO DE REMOLQUE_2"), ColumnName = "TIPO DE REMOLQUE_2", Success = false });
                                    }
                                    if (string.IsNullOrEmpty(dataRow["N�MERO ECON�MICO_2"].ToString()))
                                    {
                                        containerValid2 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "N�MERO ECON�MICO_2"), ColumnName = "N�MERO ECON�MICO_2", Success = false });
                                    }
                                    if (string.IsNullOrEmpty(dataRow["COLOR_2"].ToString()))
                                    {
                                        containerValid2 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "COLOR_2"), ColumnName = "COLOR_2", Success = false });
                                    }
                                    if (string.IsNullOrEmpty(dataRow["PLACAS_2"].ToString()))
                                    {
                                        containerValid2 = false;
                                        isValidBitacora = false;
                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, "PLACAS_2"), ColumnName = "PLACAS_2", Success = false });
                                    }

                                    if (containerValid2)
                                    {
                                        var containerItem2 = new ContainerEnt
                                        {
                                            Gpss = new List<GpsEnt>()
                                        };
                                        if (IsValidColumn(dataColumns, "TIPO DE REMOLQUE_2", dataRow, listMessages, ref isValidBitacora))
                                        {
                                            using (var bl = new CatContainerTypesBl(_connectionStringBitacora, "system"))
                                            {
                                                var columnName = "TIPO DE REMOLQUE_2";
                                                var list = bl.Search(dataRow[columnName].ToString()).ToList();
                                                if (list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                                {
                                                    containerItem2.ContainerType = list.First(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower()));
                                                }
                                                else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                                {
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                                }
                                            }
                                        }
                                        else
                                        {
                                            containerValid2 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "N�MERO ECON�MICO_2", dataRow, listMessages, ref isValidBitacora, new Regex(_platesPattern)))
                                        {
                                            var columnName = "N�MERO ECON�MICO_2";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString()))
                                            {
                                                containerItem2.Economic = dataRow[columnName].ToString();
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            else if (string.IsNullOrEmpty(dataRow[columnName].ToString()))
                                            {
                                                containerItem2.Economic = "SE";
                                            }
                                        }
                                        else
                                        {
                                            containerValid2 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "COLOR_2", dataRow, listMessages, ref isValidBitacora))
                                        {
                                            if (bitacoraItem.Customer != null)
                                            {
                                                using (var bl = new CatColorsBl(_connectionStringBitacora, "system"))
                                                {
                                                    var columnName = "COLOR_2";
                                                    var list = bl.Search(bitacoraItem.Customer.Id, dataRow[columnName].ToString()).ToList();
                                                    if (list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                                    {
                                                        containerItem2.Color = list.First(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower()));
                                                    }
                                                    else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                                    {
                                                        isValidBitacora = false;
                                                        listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                                    }
                                                    if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                                    {
                                                        SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString());
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                isValidBitacora = false;
                                            }
                                        }
                                        else
                                        {
                                            containerValid2 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "PLACAS_2", dataRow, listMessages, ref isValidBitacora, new Regex("^[a-zA-Z0-9]*$")))
                                        {
                                            var columnName = "PLACAS_2";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString()))
                                            {
                                                var rgx = new Regex("^[a-zA-Z0-9]*$");
                                                if (rgx.IsMatch(dataRow[columnName].ToString()))
                                                {
                                                    containerItem2.Plate = dataRow[columnName].ToString();
                                                }
                                                else
                                                {
                                                    isValidBitacora = false;
                                                    listMessages.Add(new FluentMessage { Comments = $"El formato de la columna {columnName} es invalido", ColumnName = columnName, Success = false });
                                                }
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                        }
                                        else
                                        {
                                            containerValid2 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "SELLOS_2", dataRow, listMessages, ref isValidBitacora))
                                        {
                                            var columnName = "SELLOS_2";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString()))
                                            {
                                                containerItem2.UnitStamp = dataRow[columnName].ToString();
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                        }
                                        else
                                        {
                                            containerValid2 = false;
                                        }

                                        if (IsValidColumn(dataColumns, "CAPACIDAD_2", dataRow, listMessages, ref isValidBitacora))
                                        {
                                            var columnName = "CAPACIDAD_2";
                                            if (!string.IsNullOrEmpty(dataRow[columnName].ToString()))
                                            {
                                                containerItem2.Capacity = dataRow[columnName].ToString();
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                        }
                                        else
                                        {
                                            containerValid2 = false;
                                        }

                                        if (containerValid2)
                                        {
                                            transport.Containers.Add(containerItem2);
                                        }
                                    }
                                }

                                #endregion

                                bitacoraItem.Transport = transport;

                                #endregion

                                if (IsValidColumn(dataColumns, "KIT TECNOLOGICO", dataRow, listMessages, ref isValidBitacora))
                                {
                                    if (bitacoraItem.Customer != null)
                                    {
                                        using (var bl = new CatSecurityKitsBl(_connectionStringBitacora, "system"))
                                        {
                                            var columnName = "KIT TECNOLOGICO";
                                            var list = bl.Search(bitacoraItem.Customer.Id, dataRow[columnName].ToString()).ToList();
                                            if (list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                            {
                                                bitacoraItem.SecurityKit = list.First(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower()));
                                            }
                                            else if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
                                            {
                                                isValidBitacora = false;
                                                listMessages.Add(new FluentMessage { Comments = string.Format(messageRequired, columnName), ColumnName = columnName, Success = false });
                                            }
                                            if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.Notification) && !list.Any(item => item.Name.ToLower().Equals(dataRow[columnName].ToString().ToLower())))
                                            {
                                                SendNotificationEmail(_columns.First(c => c.ColumnName.Equals(columnName)), dataRow[columnName].ToString());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isValidBitacora = false;
                                    }
                                }

                                #endregion

                                if (isValidBitacora)
                                {
                                    #region Validacion Fluent

                                    ValidationResult fluentResult;
                                    using (var bl = new BitacorasBl(_connectionStringBitacora, "system"))
                                    {
                                        using (var val = new PreBitacoraValidator(bl))
                                        {
                                            ;
                                            fluentResult = val.Validate(bitacoraItem);
                                        }
                                    }
                                    if (!fluentResult.IsValid)
                                    {
                                        _logger.LogError($"En el archivo: {Path.GetFileName(file.OriginalFileName)}, la fila numero {rowNumber} contiene errores.");
                                        foreach (var validationFailure in fluentResult.Errors)
                                        {
                                            listMessages.Add(new FluentMessage
                                            {
                                                Comments = validationFailure.ErrorMessage,
                                                Success = false
                                            });
                                        }
                                    }

                                    #endregion

                                    #region Guardar Bitacora, valida que no exista

                                    if (fluentResult.IsValid)
                                    {
                                        var result = SaveBitacora(bitacoraItem, localErrorPath, rowNumber);
                                        if (result.Item1)
                                        {
                                            validationsItem.Bitacora = result.Item2;
                                            listMessages.Add(new FluentMessage
                                            {
                                                Comments = result.Item2,
                                                Success = result.Item1
                                            });
                                        }
                                        else
                                        {
                                            listMessages.Add(new FluentMessage
                                            {
                                                Comments = result.Item2,
                                                Success = result.Item1
                                            });
                                        }
                                    }

                                    #endregion
                                }
                                //}

                                validationsItem.Messages = listMessages;
                                if (listMessages.Any(item => !item.Success))
                                {
                                    CreateErrorExcelFile(file, new List<FinalValidation> { validationsItem });
                                }

                                UpdateProcessFile(temporalPath, new List<FinalValidation> { validationsItem }, dt.Columns.Count);
                            }
                        }
                        else
                        {
                            _logger.LogError($"El archivo: {Path.GetFileName(file.OriginalFileName)} no es un archivo Excel.");
                            CreateErrorExcelFile(file, new List<FinalValidation>
                            {
                                new FinalValidation
                                {
                                    NumberRow = 1,
                                    SourceFile = importFileName,
                                    Messages = new List<FluentMessage>
                                    {
                                        new FluentMessage
                                        {
                                            Comments = "El archivo no tiene extensi�n xls o xlsx.",
                                            Success = false
                                        }
                                    }
                                }
                            });
                            try
                            {
                                using (var bl = new BitacorasBl(_connectionStringBitacora, "system"))
                                {
                                    bl.CreateUpdateBitacoraMassiveLoad(0, file.OriginalFileName, importFileName.Replace(_tempPath, ""), DateTime.Now, localErrorPath, DateTime.Now, 'F');
                                }
                            }
                            catch (Exception aE)
                            {
                                _logger.LogError(aE.StackTrace);
                            }

                            return;
                        }
                    }
                    catch (Exception aE)
                    {
                        _logger.LogError($"El archivo: {aE}\n{Path.GetFileName(file.OriginalFileName)} no se puede leer.");
                        CreateErrorExcelFile(file, new List<FinalValidation>
                        {
                            new FinalValidation
                            {
                                NumberRow = 1,
                                SourceFile = importFileName,
                                Messages = new List<FluentMessage>
                                {
                                    new FluentMessage
                                    {
                                        Comments = "No se consiguio leer la informaci�n del archivo Excel." + aE.Message + ", favor de subir nuevamente la plantilla",
                                        Success = false
                                    }
                                }
                            }
                        });
                    }

                    try
                    {
                        using (var bl = new BitacorasBl(_connectionStringBitacora, "system"))
                        {
                            bl.CreateUpdateBitacoraMassiveLoad(0, file.OriginalFileName,
                                importFileName.Replace(_tempPath, ""), DateTime.Now, localErrorPath,
                                DateTime.Now, 'F');
                        }

                        _logger.LogInformation($"Proceso de carga masiva finalizado para el archivo {file.OriginalFileName}");

                    }
                    catch (Exception aE)
                    {
                        _logger.LogError(aE.StackTrace);
                    }

                }
                else
                {
                    MoveFilesToProcessOrError(file.OriginalFileName, file.ProcessFileName);
                    _logger.LogError($"Archivo: {file.OriginalFileName} no es un archivo Excel.");
                    CreateErrorExcelFile(file, new List<FinalValidation>
                    {
                        new FinalValidation
                        {
                            NumberRow = 1,
                            Messages = new List<FluentMessage>
                            {
                                new FluentMessage
                                {
                                    Comments = "No es un archivo de Excel",
                                    Success = false
                                }
                            }
                        }
                    });
                }
                MoveResultsProcessFile(file);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Archivo: {file.OriginalFileName} no fue movido a la carpeta PendingShipments. \nError: {ex}");
                CreateErrorExcelFile(file, new List<FinalValidation>
                {
                    new FinalValidation
                    {
                        NumberRow = 1,
                        Messages = new List<FluentMessage>
                        {
                            new FluentMessage
                            {
                                Comments = $"No se consiguio mover el archivo de la carpeta FTP  PendingShipments a FTP ProcessShipments: {ex.Message} Favor de volverlo a cargar.",
                                Success = false
                            }
                        }
                    }
                });
            }
        }

        private bool IsValidColumn(DataColumnCollection dataColumns, string columnName, DataRow dt, List<FluentMessage> lstMessages, ref bool isValidBitacora, Regex rgx = null)
        {
            if (dataColumns.Contains(columnName))
            {
                if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired) && string.IsNullOrEmpty(dt[columnName].ToString()))
                {
                    var message = $"El valor de la columna '{columnName}' es invalido -> dato requerido";
                    lstMessages.Add(new FluentMessage { Comments = message, ColumnName = columnName, Success = false });
                    isValidBitacora = false;
                    return false;
                }

                if (rgx != null && !string.IsNullOrEmpty(dt[columnName].ToString()))
                {
                    if (!rgx.IsMatch(dt[columnName].ToString()))
                    {
                        var message = $"El formato de la columna {columnName} es invalido";
                        lstMessages.Add(new FluentMessage { Comments = message, ColumnName = columnName, Success = false });
                        isValidBitacora = false;
                        return false;
                    }
                }

                if (_columns.Any(c => c.ColumnName.Equals(columnName) && !c.ColumnRequired) && string.IsNullOrEmpty(dt[columnName].ToString()))
                {
                    return true;
                }

                return true;
            }

            if (_columns.Any(c => c.ColumnName.Equals(columnName) && c.ColumnRequired))
            {
                var message = $"No se encontro la columna '{columnName}' en el archivo -> Columna requerida";
                lstMessages.Add(new FluentMessage { Comments = message, ColumnName = columnName, Success = false });
                isValidBitacora = false;
                return false;
            }

            if (!dataColumns.Contains(columnName))
            {
                return false;
            }

            return true;

        }

        private void SendNotificationEmail(ColumnMap dataColumn, string value)
        {
            //Proceso de envio de notificaciones
        }

        /// <summary>
        /// Method that constructs an item of bitacora, validates duplicity and inserts the record
        /// </summary>
        /// <param name="bitacoraItem">Bitacora</param>
        /// <param name="sourceFile">File that contains the record to be inserted</param>
        /// <param name="rowNumber">Row number of the File that contains the record to be inserted</param>
        /// <returns>Returns the flag indicating the success or failure of the insertion of the record</returns>
        private Tuple<bool, string> SaveBitacora(BitacoraEnt bitacoraItem, string sourceFile, int rowNumber)
        {
            string bitacoraId = null;
            _logger.LogInformation($"New register: \n{JsonConvert.SerializeObject(bitacoraItem)}");
            try
            {
                #region revision para no duplicacion de bitacoras

                try
                {
                    using (var bl = new BitacorasBl(_connectionStringBitacora, "system"))
                    {
                        if (bl.ValidDuplicateBitacora(bitacoraItem.Customer.Id,
                            bitacoraItem.Destinies.First().Destiny.Id, bitacoraItem.Destinies.First().DeliveryNumber,
                            bitacoraItem.Transport.Plate.NumPlate))
                        {
                            var errorDescription = "BIT�CORA DUPLICADA: ya se encuentra registrado en la base de datos.";
                            _logger.LogError($"In file: {sourceFile} the register #{rowNumber} is duplicated.");
                            return new Tuple<bool, string>(false, errorDescription);
                        }
                    }
                }
                catch (Exception aE)
                {
                    var errorDescription = "Error en la b�squeda de bitacora duplicada";
                    _logger.LogError($"In file: {sourceFile} and in the search for duplicity, an error occurred \n{aE}");
                    return new Tuple<bool, string>(false, errorDescription);

                }

                #endregion revision para no duplicacion de bitacoras

                #region insertar

                using (var scope = new TransactionScope())
                {
                    using (var bl = new BitacorasBl(_connectionStringBitacora, "system"))
                    {
                        if (bl.Create(bitacoraItem, ref bitacoraId))
                        {
                            scope.Complete();
                            _logger.LogInformation($"Bitacora was created: \n{JsonConvert.SerializeObject(bitacoraItem)}");
                            return new Tuple<bool, string>(true, bitacoraItem.Alias);

                        }

                        return new Tuple<bool, string>(false, "Intento fallido de inserci�n del registro.");
                    }
                }

                #endregion insertar
            }
            catch (Exception aE)
            {
                var errorDescription = "Intento fallido de inserci�n del registro.";
                _logger.LogError($"Bitacora create: \n{aE}\nIn file: {sourceFile}");
                return new Tuple<bool, string>(false, errorDescription);

            }

        }

        /// <summary>
        /// Method that generates the excel file for the error log
        /// </summary>
        /// <param name="fileItem">Error file path</param>
        /// <param name="lstErrors">Error list</param>
        private void CreateErrorExcelFile(FilePath fileItem, List<FinalValidation> lstErrors)
        {
            try
            {
                var localErrorPath = $"{_tempPath}{fileItem.ErrorsFileName}";
                if (!Directory.Exists(_tempPath))
                {
                    Directory.CreateDirectory(_tempPath);
                }
                var file = new FileInfo(localErrorPath);
                if (!File.Exists(localErrorPath))
                {
                    using (var excelPackage = new ExcelPackage(file))
                    {
                        var worksheet = excelPackage.Workbook.Worksheets.Add("Errores");
                        worksheet.Cells[1, 1].Value = "No.";
                        worksheet.Cells[1, 2].Value = "Fecha, Hora y Archivo procesado";
                        worksheet.Cells[1, 3].Value = "Descripci�n del error";
                        worksheet.Cells[1, 4].Value = "L�nea err�nea";
                        worksheet.Cells[1, 5].Value = "Columna err�nea";
                        worksheet.Cells[1, 6].Value = "Informaci�n err�nea ";
                        var row = 2;
                        foreach (var errorItem in lstErrors)
                        {
                            foreach (var itemMessage in errorItem.Messages)
                            {
                                worksheet.Cells[row, 1].Value = errorItem.NumberRow;
                                worksheet.Cells[row, 2].Value = Path.GetFileName(errorItem.SourceFile);
                                worksheet.Cells[row, 3].Value = itemMessage.Comments;
                                worksheet.Cells[row, 4].Value = errorItem.NumberRow.ToString();
                                worksheet.Cells[row, 5].Value = itemMessage.ColumnName;
                                worksheet.Cells[row, 6].Value = itemMessage.ColumnInformation;
                                row++;
                            }
                        }
                        worksheet.Cells.AutoFitColumns();
                        excelPackage.SaveAs(new FileInfo(localErrorPath));
                    }
                }
                else
                {
                    using (var excelPackage = new ExcelPackage(file))
                    {
                        var worksheet = excelPackage.Workbook.Worksheets[0];
                        var row = worksheet.Dimension.Rows + 1;
                        foreach (var errorItem in lstErrors)
                        {
                            foreach (var itemMessage in errorItem.Messages)
                            {
                                worksheet.Cells[row, 1].Value = errorItem.NumberRow;
                                worksheet.Cells[row, 2].Value = Path.GetFileName(errorItem.SourceFile);
                                worksheet.Cells[row, 3].Value = itemMessage.Comments;
                                worksheet.Cells[row, 4].Value = errorItem.NumberRow.ToString();
                                worksheet.Cells[row, 5].Value = itemMessage.ColumnName;
                                worksheet.Cells[row, 6].Value = itemMessage.ColumnInformation;
                                row++;
                            }
                        }
                        worksheet.Cells.AutoFitColumns();
                        excelPackage.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.ToString());
                throw;
            }
        }

        private void CreateProcessFile(string ftpfullpath, string inputfilepath)
        {
            try
            {
                if (!Directory.Exists(_tempPath))
                {
                    Directory.CreateDirectory(_tempPath);
                }
                using (var request = new WebClient())
                {
                    request.Credentials = new NetworkCredential(_ftpUser, _ftpPassword);
                    var fileData = request.DownloadData(ftpfullpath);
                    using (var fileNew = File.Create(inputfilepath))
                    {
                        fileNew.Write(fileData, 0, fileData.Length);
                        fileNew.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.ToString());
                throw;
            }
        }

        private void UpdateProcessFileAddHeaderResult(string pathFile, int columns)
        {
            try
            {
                if (!Directory.Exists(_tempPath))
                {
                    Directory.CreateDirectory(_tempPath);
                }
                var file = new FileInfo(pathFile);
                if (File.Exists(pathFile))
                {
                    using (var excelPackage = new ExcelPackage(file))
                    {
                        var worksheet = excelPackage.Workbook.Worksheets[0];
                        var numberColumns = columns; //worksheet.Dimension.Columns;
                        worksheet.Cells[1, numberColumns + 1].Value = "REGISTRO PROCESADO CON �XITO";
                        worksheet.Cells[1, numberColumns + 2].Value = "COMENTARIOS";
                        worksheet.Cells[1, numberColumns + 3].Value = "BIT�CORA";
                        //Se rellena los encabezados
                        using (var range = worksheet.Cells[1, numberColumns + 1, 1, numberColumns + 3])
                        {
                            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            range.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Crimson);
                            range.Style.Font.Color.SetColor(System.Drawing.Color.White);
                        }

                        worksheet.Column(numberColumns + 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Column(numberColumns + 1).Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        worksheet.Column(numberColumns + 2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Column(numberColumns + 2).Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        worksheet.Column(numberColumns + 3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Column(numberColumns + 3).Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        worksheet.Column(numberColumns + 1).Width = 20;
                        worksheet.Column(numberColumns + 2).Width = 40;
                        worksheet.Column(numberColumns + 3).Width = 10;
                        excelPackage.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.ToString());
                throw;
            }
        }

        private void UpdateProcessFile(string pathFile, List<FinalValidation> list, int columns)
        {
            try
            {
                if (!Directory.Exists(_tempPath))
                {
                    Directory.CreateDirectory(_tempPath);
                }
                var file = new FileInfo(pathFile);
                if (File.Exists(pathFile))
                {
                    using (var excelPackage = new ExcelPackage(file))
                    {
                        var worksheet = excelPackage.Workbook.Worksheets[0];
                        var numberColumns = columns;//worksheet.Dimension.Columns;
                        //Se rellenan los resultados
                        foreach (var rowItem in list)
                        {
                            var comment = string.Empty;
                            foreach (var message in rowItem.Messages)
                            {
                                comment += $"{message.Comments}\n";
                            }
                            worksheet.Cells[rowItem.NumberRow + 1, numberColumns + 1].Value = string.IsNullOrEmpty(rowItem.Bitacora) ? "NO" : "SI";
                            worksheet.Cells[rowItem.NumberRow + 1, numberColumns + 2].Value = string.IsNullOrEmpty(comment) ? "NA" : comment;
                            worksheet.Cells[rowItem.NumberRow + 1, numberColumns + 3].Value = string.IsNullOrEmpty(rowItem.Bitacora) ? "NA" : rowItem.Bitacora;
                        }
                        var totalRows = worksheet.Dimension.Rows;
                        using (var range = worksheet.Cells[1, numberColumns + 1, totalRows, numberColumns + 3])
                        {
                            range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            range.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        }
                        excelPackage.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.ToString());
                throw;
            }
        }

        ///  <summary>
        /// Method that moves via FTP the error files from temporal path to the folder ErrorShipments 
        ///  </summary>
        ///  <param name="localErrorDirectory">Temporary path where the files are processed</param>
        ///  <param name="file"></param>
        private void MoveErrorFiles(string localErrorDirectory, FilePath file)
        {
            const int bufferSize = 2048;
            var errorFiles = Directory.GetFiles(localErrorDirectory, "*.*", SearchOption.AllDirectories).Where(s => s.Contains(file.ErrorsFileName)).ToArray();
            if (errorFiles.Length > 0)
            {
                foreach (var fileItem in errorFiles)
                {
                    var request = (FtpWebRequest)WebRequest.Create(new Uri($"ftp://{_ftpServer}//{_errorShipments}//{Path.GetFileName(fileItem)}", UriKind.Absolute));
                    request.Credentials = new NetworkCredential(_ftpUser, _ftpPassword);
                    request.UseBinary = true;
                    request.UsePassive = true;
                    request.KeepAlive = true;
                    request.Method = WebRequestMethods.Ftp.UploadFile;
                    var ftpStream = request.GetRequestStream();
                    var localFileStream = new FileStream(fileItem ?? throw new InvalidOperationException(), FileMode.OpenOrCreate);
                    var byteBuffer = new byte[bufferSize];
                    var bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                    try
                    {
                        while (bytesSent != 0)
                        {
                            ftpStream.Write(byteBuffer, 0, bytesSent);
                            bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogInformation(ex.ToString());
                    }

                    localFileStream.Close();
                    ftpStream.Close();
                    File.Delete(fileItem);
                }
            }
        }

        private void MoveResultsProcessFile(FilePath file)
        {
            try
            {
                const int bufferSize = 2048;
                var files = Directory.GetFiles(_tempPath, "*.*", SearchOption.AllDirectories).Where(s => s.Contains(file.ResultlFileName)).ToArray();
                foreach (var path in files)
                {
                    var request = (FtpWebRequest)WebRequest.Create(new Uri($"ftp://{_ftpServer}//{_resultsShipments}//{Path.GetFileName(path)}", UriKind.Absolute));
                    request.Credentials = new NetworkCredential(_ftpUser, _ftpPassword);
                    request.UseBinary = true;
                    request.UsePassive = true;
                    request.KeepAlive = true;
                    request.Method = WebRequestMethods.Ftp.UploadFile;
                    var ftpStream = request.GetRequestStream();
                    var localFileStream = new FileStream(path ?? throw new InvalidOperationException(), FileMode.OpenOrCreate);
                    var byteBuffer = new byte[bufferSize];
                    var bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                    try
                    {
                        while (bytesSent != 0)
                        {
                            ftpStream.Write(byteBuffer, 0, bytesSent);
                            bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogInformation(ex.ToString());
                    }

                    localFileStream.Close();
                    ftpStream.Close();
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.ToString());
                throw;
            }
        }
    }
}
