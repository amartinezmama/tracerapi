﻿using System.Collections.Generic;
using Ilsp.BitacoraIvr.Ent.LoadShipmentPlan;

namespace Ilsp.BitacoraIvr.LspApp
{
    public class AppSettingsConfigurationOptions
    {
        public string Server { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string TempPath { get; set; }
        public string PendingShipments { get; set; }
        public string ProcessShipments { get; set; }
        public string ResultsShipments { get; set; }
        public string ErrorShipments { get; set; }
        public string DelimiterSplit { get; set; }
        public string SeparatorDate { get; set; }
        public int TimerSeconds { get; set; }
        public string LettersAccentsSpacePattern { get; set; }
        public string PlatesPattern { get; set; }
        public string ConnectionStringBitacora { get; set; }
        public IEnumerable<ColumnMap> ColumnsConfiguration { get; set; }
    }
}
